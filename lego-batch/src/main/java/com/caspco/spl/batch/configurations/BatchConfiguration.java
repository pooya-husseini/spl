package com.caspco.spl.batch.configurations;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.context.annotation.Configuration;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 3/14/17
 *         Time: 2:26 PM
 */

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

}
