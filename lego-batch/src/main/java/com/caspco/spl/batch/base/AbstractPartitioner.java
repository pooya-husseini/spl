package com.caspco.spl.batch.base;

import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;

import java.util.Map;

/**
 * Created by saeedko on 3/13/17.
 */
public abstract class AbstractPartitioner implements Partitioner {

    @Override
    public final Map<String, ExecutionContext> partition(int i) {
        return doPartition(i);
    }

    public abstract Map<String, ExecutionContext> doPartition(int i);
}