package com.caspco.spl.batch.base;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * Created by saeedko on 3/13/17.
 */
public abstract class AbstractTasklet implements Tasklet {
    @Override
    public final RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        return doExecute(stepContribution, chunkContext);
    }

    public abstract RepeatStatus doExecute(StepContribution stepContribution, ChunkContext chunkContext);

}
