package com.caspco.spl.batch.base;


import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemReader;

/**
 * Created by saeedko on 3/13/17.
 */
public abstract class AbstractReader<T> implements ItemReader<T> {

    private StepExecution stepExecution;

    @BeforeStep
    public void beforeStep(StepExecution stepExecution) throws Exception {
        this.stepExecution = stepExecution;
    }

    @Override
    public final T read() throws Exception {
        return doRead();
    }

    public abstract T doRead() throws Exception;

    /**
     * Getter for property 'stepExecution'.
     *
     * @return Value for property 'stepExecution'.
     */
    public StepExecution getStepExecution() {
        return stepExecution;
    }
}
