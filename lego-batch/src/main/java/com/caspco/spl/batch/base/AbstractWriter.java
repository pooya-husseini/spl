package com.caspco.spl.batch.base;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 3/14/17
 *         Time: 2:17 PM
 */

import org.springframework.batch.item.ItemWriter;

import java.util.List;

/**
 * Created by saeedko on 3/13/17.
 */
public abstract class AbstractWriter<T> implements ItemWriter<T>{
    @Override
    public final void  write(List<? extends T> list) throws Exception {
        doWrite(list);
    }

    public abstract void doWrite(List<? extends T> list);
}