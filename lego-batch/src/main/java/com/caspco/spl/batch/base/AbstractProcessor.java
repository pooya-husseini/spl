package com.caspco.spl.batch.base;

import org.springframework.batch.item.ItemProcessor;

/**
 * Created by saeedko on 3/13/17.
 */
public abstract class AbstractProcessor<T, D> implements ItemProcessor<T, D> {
    @Override
    public final D process(T t) {
        return doProcess(t);
    }

    public abstract D doProcess(T t);

}
