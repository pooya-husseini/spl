package ir.phsys.sitereader;

import com.caspco.xview.wrench.annotations.cg.EnumeratedWith;
import com.caspco.xview.wrench.annotations.cg.Translation;
import com.caspco.xview.wrench.annotations.cg.Translations;
import com.caspco.xview.wrench.locale.Locale;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 8/12/15
 *         Time: 1:45 PM
 */

@Entity
@Table
@Translations({@Translation(label = "Manual run", locale = Locale.English_United_States), @Translation(label = "راه اندازی دستی", locale = Locale.Persian_Iran)})
public class ManualRunEntity {
    private String tag;
    private Long id;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


//    @GeneralParameter(type = "newsTag", translations = {
//            @Translation(locale = Locale.English_United_States, label = "Agency tag"),
//            @Translation(locale = Locale.Persian_Iran, label = "نام خبرگزاری")
//    })
    @EnumeratedWith(UserManagementRole.class)
    @Translations({@Translation(locale = Locale.English_United_States, label = "Agency tag"),
            @Translation(locale = Locale.Persian_Iran, label = "نام خبرگزاری")})
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }


}
