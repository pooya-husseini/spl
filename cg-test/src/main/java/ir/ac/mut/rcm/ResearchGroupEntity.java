package ir.ac.mut.rcm;

import com.caspco.xview.wrench.annotations.cg.AutoInject;
import com.caspco.xview.wrench.annotations.cg.Searchable;
import com.caspco.xview.wrench.annotations.cg.Translation;
import com.caspco.xview.wrench.annotations.cg.Translations;
import com.caspco.xview.wrench.locale.Locale;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 22/07/14
 *         Time: 13:19
 */
@Entity
@Table(name = "ResearchGroup")
@Audited
@Translations({
        @Translation(label = "Research group", locale = Locale.English_United_States),
        @Translation(label = "گروه های پژوهشی", locale = Locale.Persian_Iran)
})
public class ResearchGroupEntity {

    private Long id;
    private String researchGroupTitle;
    private ResearchCenterEntity researchCenter;


    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(cascade = CascadeType.MERGE)
    @NotNull
    @AutoInject
    public ResearchCenterEntity getResearchCenter() {
        return researchCenter;
    }

    public void setResearchCenter(ResearchCenterEntity researchCenter) {
        this.researchCenter = researchCenter;
    }


    @Translations({
            @Translation(label = "عنوان", locale = Locale.Persian_Iran),
            @Translation(label = "Title", locale = Locale.English_United_States)
    })
    @Searchable
    @Column(name = "title")
    public String getResearchGroupTitle() {
        return researchGroupTitle;
    }

    public void setResearchGroupTitle(String title) {
        this.researchGroupTitle = title;
    }


    //    @Version
//    public Integer getVersion() {
//        return version;
//    }
//
//    public void setVersion(Integer version) {
//        this.version = version;
//    }
}