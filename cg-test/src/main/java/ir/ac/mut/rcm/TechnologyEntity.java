package ir.ac.mut.rcm;


import com.caspco.xview.wrench.annotations.cg.AutoInject;
import com.caspco.xview.wrench.annotations.cg.Searchable;
import com.caspco.xview.wrench.annotations.cg.Translation;
import com.caspco.xview.wrench.annotations.cg.Translations;
import com.caspco.xview.wrench.locale.Locale;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 22/07/14
 *         Time: 13:16
 */
@Entity
@Table(name="Technology")
@Translations({
        @Translation(label = "Technology", locale = Locale.English_United_States),
        @Translation(label = "فناوری های گلوگاهی", locale = Locale.Persian_Iran)})
@Audited
public class TechnologyEntity {

    private Long id;
    private String technologyTitle;
    private ResearchCenterEntity researchCenter;
//    private Integer version;

    @NotNull
    @Translations({
            @Translation(label = "Title", locale = Locale.English_United_States),
            @Translation(label = "عنوان", locale = Locale.Persian_Iran)})
    @Searchable
    @Column(name = "title")
    public String getTechnologyTitle() {
        return technologyTitle;
    }

    public void setTechnologyTitle(String title) {
        this.technologyTitle = title;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @NotNull
    @Translations({
            @Translation(label = "Research center", locale = Locale.English_United_States),
            @Translation(label = "مرکز پژوهشی", locale = Locale.Persian_Iran)})
    @AutoInject
    public ResearchCenterEntity getResearchCenter() {
        return researchCenter;
    }

    public void setResearchCenter(ResearchCenterEntity researchCenter) {
        this.researchCenter = researchCenter;
    }

//    @Version
//    public Integer getVersion() {
//        return version;
//    }
//
//    public void setVersion(Integer version) {
//        this.version = version;
//    }
}