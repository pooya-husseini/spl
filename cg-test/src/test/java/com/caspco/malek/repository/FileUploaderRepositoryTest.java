//package com.caspco.malek.repository;
//
//import com.caspco.malek.FileUploaderEntity;
//import com.caspco.utils.io.FileUtils;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.transaction.TransactionConfiguration;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.io.IOException;
//import java.util.List;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = "classpath*:test-context.xml")
//@Transactional(noRollbackFor = Exception.class)
//@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
//public class FileUploaderRepositoryTest {
//
//    @Autowired
//    private FileUploaderRepository repository;
//
//    @Test
//    public void testInsertFile() throws IOException {
//        FileUploaderEntity entity = new FileUploaderEntity();
//        byte[] bytes = FileUtils.toByteArray("/home/pooya/Desktop/1.mp3");
//        entity.setContent(bytes);
//        entity.setName("a.mp3");
//
//        repository.save(entity);
//    }
//
//    @Test
//    public void testSelectFile() throws IOException {
//        List<FileUploaderEntity> all = repository.findAll();
//
//        for (FileUploaderEntity entity : all) {
//            FileUtils.toFile("/home/pooya/Desktop/"+entity.getName(),entity.getContent());
//        }
//    }
//}
