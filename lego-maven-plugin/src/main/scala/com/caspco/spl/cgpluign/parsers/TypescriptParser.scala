package com.caspco.spl.cgpluign.parsers

import java.io.Serializable

import scala.util.parsing.combinator.JavaTokenParsers
import scala.collection.JavaConverters._
import scala.util.matching.Regex

/**
  * Created by pooya on 11/5/16.
  */

class TypescriptParser extends JavaTokenParsers {
  def root: Parser[Product with Serializable] = "class" ~> ident ~ (opt(impls ~> repsep(allType, ",")) ~> "{" ~> rep(decField) <~ "}") ^^ {
    case name ~ (fs: List[FieldExpression]) =>
      ClassExpression(name, fs)
    case _ => Nil
  }

  def impls: Parser[String] = "implements" | "extends"

  def method: Parser[~[~[~[~[~[Option[String], String], String], String], String], String]] = opt(modifier) ~ ident ~ "(" ~ ")" ~ "{" ~ "}"

  def decField: Parser[FieldExpression] = decorator ~> opt(modifier) ~> ident ~ opt(":" ~ fullIdent) ~ opt(";") ^^ {
    case name ~ Some(":" ~ t) ~ Some(";") => FieldExpression(name, t)
    case name ~ Some(":" ~ t) ~ None => throw new Exception(s"Injectable field $name should ends with semicolon")
    case name => throw new Exception(s"Injectable field $name should have types")
  }

  def simpleField: Parser[~[String, Option[~[String, List[String]]]]] = opt(modifier) ~> ident ~ opt(":" ~ repsep(fullIdent, "|")) <~ ";"

  def decorator = "@lego.Inject()"

  def modifier: Parser[String] = "private" | "public" | "protected"

  def fullIdent: Regex = "([a-zA-Z_$][a-zA-Z\\d_$]*\\.)*[a-zA-Z_$][a-zA-Z\\d_$]*".r

  def genericType: Parser[~[~[~[String, String], List[String]], String]] = fullIdent ~ "<" ~ repsep(fullIdent, ",") ~ ">"

  def allType: Parser[Serializable] = genericType | fullIdent
}

object TypeScriptExpr extends TypescriptParser {
  def parseInputAsJava(a: String): java.util.List[ClassExpressionAsJava] = {
    parseInput(a).map(i => ClassExpressionAsJava(i.className, i.field.asJava)).asJava
  }

  def parseInput(a: String): List[ClassExpression] = {
    (0.until(a.length)
      .filter(a.startsWith("class ", _)) ++ List(a.length))
      .sliding(2)
      .toList
      .map(i => a.substring(i.head, i.last))
      .filterNot(i => i.matches("class(\\s+)="))
      .map(s => {
        s.substring(0, s.indexOf("{")) + "{" + 0.until(s.length)
          .filter(s.startsWith("@lego.Inject", _))
          .map(i => (i, s.indexOf(";", i) + 1))
          .map(tuple => s.substring(tuple._1, tuple._2)).mkString + "}"
      })
      .map(i => parseAll(root, i))
      .collect {
        case x: Success[ClassExpression] => x.get
      }.filter(_.field.nonEmpty)
  }
}

case class ClassExpression(className: String, field: List[FieldExpression])

case class ClassExpressionAsJava(className: String, field: java.util.List[FieldExpression])

case class FieldExpression(name: String, fieldType: String)