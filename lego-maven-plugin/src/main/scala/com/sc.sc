import org.json4s._
import org.json4s.jackson.JsonMethods._


val json=
  """
    |{
    |	"server": {
    |		"port": 9898,
    |		"reports": {
    |			"basepath": "reports/"
    |		},
    |		"compression": {
    |			"enabled": true,
    |			"mime-types": "application/json,application/xml,text/html,text/xml,text/plain,application/javascript"
    |		}
    |	},
    |	"sso": {
    |		"url": "http://192.168.246.133:9763/store/site/blocks/user/login/ajax/login.jag"
    |	},
    |	"stub": {
    |		"url": "http://tejaratasanservices.niopdc.ir/NIOPDC_BANKCOMM.svc?wsdl"
    |	},
    |	"spring": {
    |		"datasource": {
    |			"url": "jdbc:mysql://192.168.247.132:3306/niopdc",
    |			"username": "root",
    |			"password": "6=Q25u/b/})8zXeR",
    |			"driver-class-name": "com.mysql.jdbc.Driver",
    |			"testWhileIdle": true,
    |			"validationQuery": "SELECT 1",
    |			"sqlScriptEncoding": "UTF-8"
    |		},
    |		"jpa": {
    |			"database": "mysql",
    |			"database-platform": "MYSQL",
    |			"show-sql": false,
    |			"properties": {
    |				"hibernate": {
    |					"dialect": "org.hibernate.dialect.MySQL5Dialect",
    |					"connection": {
    |						"characterEncoding": "utf-8",
    |						"CharSet": "utf-8",
    |						"useUnicode": true
    |					}
    |				}
    |			},
    |			"hibernate": {
    |				"naming": {
    |					"implicit-strategy": "com.caspco.spl.data.naming.SplRelationalImplicitNamingStrategy",
    |					"physical-strategy": "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy"
    |				},
    |				"ddl-auto": "update"
    |			}
    |		},
    |		"http": {
    |			"encoding": {
    |				"charset": "UTF-8",
    |				"enabled": true,
    |				"force": true
    |			}
    |		}
    |	},
    |	"logging": {
    |		"level": {
    |			"root": "info"
    |		}
    |	},
    |	"spl": {
    |		"web": {
    |			"jackson-fail-unknown-props": false
    |		},
    |		"common": {
    |			"scan-package": "com.caspco.infra.stubs.channel"
    |		}
    |	},
    |	"seller": {
    |		"resolver": {
    |			"mode": "SELLER"
    |		}
    |	}
    |}
  """.stripMargin

implicit val formats: DefaultFormats.type = org.json4s.DefaultFormats

val j=parse(json).extract[Map[String, Any]]

def makeMap(x: Map[String, Any]): List[(String, String)] = {
  def innerMakeMap(prefix: String, x: Map[String, Any]): List[(String, String)] = {
    x.toList.flatMap {
      case (head, (tail: Map[String, Any])) =>
        innerMakeMap(prefix + "." + head, tail)
      case (head, tail) =>
        List((prefix + "." + head, tail.toString))
    }
  }
  innerMakeMap("",x)

}


makeMap(j).map (i=>s"${i._1}=${i._2}").mkString("\n")
