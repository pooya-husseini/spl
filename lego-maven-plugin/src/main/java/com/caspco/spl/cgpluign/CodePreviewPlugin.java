package com.caspco.spl.cgpluign; /**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 21/06/14
 *         Time: 14:49
 */

import com.caspco.spl.cg.bootstrap.LegoPluginJobType;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name = "preview", threadSafe = true)
public class CodePreviewPlugin extends AbstractLegoMavenPlugin {
    @Override
    protected LegoPluginJobType task() {
        return LegoPluginJobType.PREVIEW;
    }
}