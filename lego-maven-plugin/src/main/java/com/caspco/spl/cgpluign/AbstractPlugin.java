package com.caspco.spl.cgpluign;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.springframework.util.ClassUtils;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 21/06/14
 *         Time: 14:49
 */

public abstract class AbstractPlugin extends AbstractMojo {

    public abstract MavenProject getMavenProject();

    public abstract List<Runnable> getTasks();

    public void execute() throws MojoExecutionException {
        final ClassLoader loader = getClassLoader();
        ClassUtils.overrideThreadContextClassLoader(loader);

        for (Runnable runnable : getTasks()) {
            final Thread thread = new Thread(runnable);
            thread.run();
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public ClassLoader getClassLoader() {
        List<String> classPaths = new ArrayList<>();

        final Set artifacts = getMavenProject().getDependencyArtifacts();

        for (Object obj : artifacts) {
            if (obj instanceof Artifact) {
                Artifact artifact = (Artifact) obj;
                String path = artifact.getFile().getAbsolutePath();

                if (artifact.getScope().contains("compile")) {
                    classPaths.add(path);
                }
            }
        }
        classPaths.add(getMavenProject().getBuild().getOutputDirectory());

        URL[] classpathUrls = new URL[classPaths.size()];
        try {
            for (int i = 0; i < classPaths.size(); i++) {
                classpathUrls[i] = new File(classPaths.get(i)).toURI().toURL();
            }

        } catch (MalformedURLException e) {
            return this.getClass().getClassLoader();
        }

        return new URLClassLoader(classpathUrls, this.getClass().getClassLoader());
    }
}