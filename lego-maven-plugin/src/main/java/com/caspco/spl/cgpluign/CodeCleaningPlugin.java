package com.caspco.spl.cgpluign;
/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 21/06/14
 *         Time: 14:49
 */

import com.caspco.spl.cg.bootstrap.LegoPluginJobType;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name = "clean", threadSafe = true)
public class CodeCleaningPlugin extends AbstractLegoMavenPlugin {

    @Override
    protected LegoPluginJobType task() {
        return LegoPluginJobType.CLEAN;
    }
}