package com.caspco.spl.cgpluign;
//import com.caspco.infrastructure.xview.generator.CodeGenerator;

import com.caspco.spl.cg.bootstrap.CGConfig;
import com.caspco.spl.cg.bootstrap.CodeGenerationConfiguration;
import com.caspco.spl.cg.bootstrap.CodeGenerator;
import com.caspco.spl.cgpluign.parsers.TypeScriptExpr;
import com.caspco.spl.model.annotations.cg.EnumEntity;
import com.caspco.spl.wrench.beans.scanner.TypeScanner;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.FilterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 21/06/14
 *         Time: 17:57
 */
public class PluginTask implements Runnable {
    private final CodeGenerationConfiguration config;
    private final String packagePrefix;
    private final ProjectBuilder projectBuilder;
    private String baseDir;
    //    private Reflections reflections;
    private TypeScanner typeScanner;
    private static final Logger LOGGER = LoggerFactory.getLogger(PluginTask.class);


    public PluginTask(String baseDir, String packagePrefix, CodeGenerationConfiguration config, ProjectBuilder projectBuilder) {
        this.baseDir = baseDir;
        this.config = config;
        this.packagePrefix = packagePrefix;
        this.projectBuilder = projectBuilder;
        Reflections.log = null;
    }

    @Override
    public void run() {

        config.getCgConfig().setPrefix(baseDir);

        if (packagePrefix != null && !packagePrefix.trim().isEmpty()) {

            List<Class<?>> entities = new ArrayList<>();
            entities.addAll(extractEntityClasses());

            List<Class<?>> enums = new ArrayList<>();
            enums.addAll(extractEnumEntityClasses());

            config.setClasses(entities);
            config.setEnums(enums);
            switch (config.getTask()) {
                case CLEAN:
                    CodeGenerator.entityCleaner(config);
                    break;
                case GENERATE:
                    CodeGenerator.entityCodeGenerator(config);
                    break;
                case PREVIEW:
                    CodeGenerator.entityCodeGenerator(config);
                    projectBuilder.build();
                    CodeGenerator.entityCodePreviewer(config);
                    break;
                case COLLECT_RESOURCES:
                    doResourceJobs();
                    break;
            }
        } else if (config.getCgConfig().getSimpleConfiguration(CGConfig.BootstrapAttribute.INPUT_PATH) != null) {
//            CodeGenerator.xmlCodeGenerator(config);
        } else {
            throw new IllegalArgumentException("Package prefix or Input path should not be empty!");
        }
    }

    private void doResourceJobs() {
        createReflections();
        String resourcePath = this.config.getCgConfig()
                .getConfiguration(CGConfig.BootstrapAttribute.SERVER_SIDE_RESOURCE_PATH, String.class);

//        Reflections reflections = new Reflections(new ConfigurationBuilder()
//                .addClassLoader(ClasspathHelper.contextClassLoader())
//                .setUrls(ClasspathHelper.forClassLoader())
//                .setScanners(new ResourcesScanner()));

        File f = new File(resourcePath + "/static/app/gen-src/");

        if (!f.exists()) {
            f.mkdirs();
        }
        if (resourcePath != null) {

            collectMetadata(resourcePath + "/static/app/gen-src/metadata.json", typeScanner
                    .getResources(Pattern.compile("^.*\\.ts$"))
                    .stream()
                    .filter(i -> !i.endsWith(".m.ts"))
                    .filter(i -> i.contains("/app/"))
                    .collect(Collectors.toList()));

            collectFileNames(resourcePath + "/static/app/gen-src/partial-menus", typeScanner.getResources(Pattern.compile(".*\\.menu\\.json")));
            collectFileContents(resourcePath + "/static/app/gen-src/dependencies", typeScanner.getResources(Pattern.compile(".*\\.dependencies")));
        } else {
            throw new IllegalStateException("Resource path does not set");
        }
    }

    private void collectMetadata(String filePath, Collection<String> resources) {
        File file = new File(filePath);
        file.delete();
        try {
            file.createNewFile();
            final FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(
                    "{" + resources.stream()
                            .map(i -> {
                                LOGGER.debug("Going to parse content of " + i);
                                return Pair.of(i, this.getFileContent(i));
                            })
                            .flatMap(i -> {
                                try {
                                    return TypeScriptExpr.parseInputAsJava(i.getRight()).stream();
                                } catch (Exception x) {
                                    throw new IllegalArgumentException("An error occurred in file " + i.getLeft(), x);
                                }
                            })
                            .map(i -> "\"" + i.className() + "\":{" +
                                    "\"fields\":{" + i.field().stream()
                                    .map(f -> "\"" + f.name() + "\":\"" + f.fieldType() + "\"")
                                    .collect(Collectors.joining(",")) + "}}")
                            .collect(Collectors.joining(",")) + "}");
            fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private void collectFileNames(String filePath, Set<String> locations) {
        File file = new File(filePath);
        file.delete();
        try {
            file.createNewFile();

            final FileWriter fileWriter = new FileWriter(file);
            for (String resource : locations) {
                String configuration = config.getCgConfig().getConfiguration(CGConfig.BootstrapAttribute.REMOVE_MENU_PATH_PREFIX, false, String.class);
                String toSave = resource + "\n";
                if (configuration != null && resource.startsWith(configuration)) {
                    toSave = resource.substring(configuration.length()) + "\n";
                }
                fileWriter.write(toSave);
            }
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void collectFileContents(String filePath, Set<String> locations) {
        File file = new File(filePath);
        file.delete();
        try {
            file.createNewFile();
            final FileWriter fileWriter = new FileWriter(file);
            for (String resource : locations) {
                fileWriter.write(getFileContent(resource) + "\n");
            }
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String getFileContent(String filePath) {
        try {
            final StringWriter stringWriter = new StringWriter();

            InputStream is = ClasspathHelper.contextClassLoader().getResourceAsStream(filePath);
            stringWriter.write(IOUtils.toString(is));
            is.close();

            stringWriter.flush();
            stringWriter.close();
            return stringWriter.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Set<Class<?>> extractEntityClasses() {
        createReflections();
        return typeScanner.getTypesAnnotatedWith(Entity.class);
    }

    private Set<Class<?>> extractEnumEntityClasses() {
        createReflections();
        return typeScanner.getTypesAnnotatedWith(EnumEntity.class);
    }

    private void createReflections() {
        if (typeScanner == null) {
            if (packagePrefix.contains(";")) {
                String[] split = packagePrefix.split(";");
                FilterBuilder filterBuilder = null;
                Set<URL> urls = new HashSet<>();
                for (String s : split) {
                    if (filterBuilder == null) {
                        filterBuilder = new FilterBuilder().include(FilterBuilder.prefix(s));
                    } else {
                        filterBuilder = filterBuilder.include(FilterBuilder.prefix(s));
                    }
                    urls.addAll(ClasspathHelper.forPackage(s));
                }

                this.typeScanner = new TypeScanner(urls, filterBuilder);
                this.typeScanner.init();

            } else {
                this.typeScanner = new TypeScanner(packagePrefix);
                this.typeScanner.init();
            }
        }
//        if (reflections == null) {
//            if (packagePrefix.contains(";")) {
//                String[] split = packagePrefix.split(";");
//                FilterBuilder filterBuilder = null;
//                Set<URL> urls = new HashSet<>();
//                for (String s : split) {
//                    if (filterBuilder == null) {
//                        filterBuilder = new FilterBuilder().include(FilterBuilder.prefix(s));
//                    } else {
//                        filterBuilder = filterBuilder.include(FilterBuilder.prefix(s));
//                    }
//                    urls.addAll(ClasspathHelper.forPackage(s));
//                }
//
//                reflections = new Reflections(
//                        new ConfigurationBuilder()
//                                .filterInputsBy(filterBuilder)
//                                .setUrls(urls).setScanners(new SubTypesScanner(), new TypeAnnotationsScanner())
//                );
//            } else {
//                reflections = new Reflections(new ConfigurationBuilder()
//                        .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(packagePrefix)))
//                        .setUrls(ClasspathHelper.forPackage(packagePrefix))
//                        .setScanners(new SubTypesScanner(), new TypeAnnotationsScanner())
//                );
//            }
    }
}
//}