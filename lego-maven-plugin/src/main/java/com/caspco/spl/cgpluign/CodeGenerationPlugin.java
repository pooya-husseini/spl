package com.caspco.spl.cgpluign; /**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 21/06/14
 *         Time: 14:49
 */

import com.caspco.spl.cg.bootstrap.LegoPluginJobType;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name = "generate",threadSafe = true)
public class CodeGenerationPlugin extends AbstractLegoMavenPlugin {
    @Override
    protected LegoPluginJobType task() {
        return LegoPluginJobType.GENERATE;
    }
}