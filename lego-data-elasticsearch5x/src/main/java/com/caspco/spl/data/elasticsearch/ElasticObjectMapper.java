package com.caspco.spl.data.elasticsearch;

import com.caspco.spl.data.elasticsearch.model.Field;
import com.caspco.spl.wrench.utils.reflection.ClassUtils;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 8/14/17
 * Time: 2:05 PM
 */
public class ElasticObjectMapper extends ObjectMapper{
    private static ElasticObjectMapper ourInstance = new ElasticObjectMapper();

    public static ElasticObjectMapper getInstance() {
        return ourInstance;
    }

    private ElasticObjectMapper() {
        SimpleModule module = new SimpleModule();
        module.addSerializer(Date.class, new SplElasticDateTypeSerializer());
        module.addDeserializer(Date.class, new SplElasticDateTypeDeserializer());
        this.registerModule(module);
    }


    class SplElasticDateTypeSerializer extends JsonSerializer<Date> implements ContextualSerializer {

        private final Field annotation;

        public SplElasticDateTypeSerializer() {
            annotation = null;

        }

        public SplElasticDateTypeSerializer(Field annotation) {
            this.annotation = annotation;
        }

        @Override
        public void serialize(Date value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            if (annotation != null) {
                gen.writeString(new SimpleDateFormat(annotation.pattern()).format(value));
            } else {
                gen.writeString(new SimpleDateFormat().format(value));
            }
        }


        @Override
        public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) throws JsonMappingException {
            Field annotation = property.getAnnotation(Field.class);

            return new SplElasticDateTypeSerializer(annotation);
        }
    }

    class SplElasticDateTypeDeserializer extends JsonDeserializer<Date> implements ContextualSerializer {

        private final Field annotation;

        public SplElasticDateTypeDeserializer() {
            annotation = null;
        }

        public SplElasticDateTypeDeserializer(Field annotation) {
            this.annotation = annotation;
        }


        @Override
        public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) throws JsonMappingException {
            Field annotation = property.getAnnotation(Field.class);

            return new SplElasticDateTypeSerializer(annotation);
        }

        @Override
        public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
            try {
                if (annotation != null) {

                    return new SimpleDateFormat(this.annotation.pattern()).parse(p.getText());
                } else {
                    Optional<Field> a = ClassUtils.create(p.getCurrentValue().getClass())
                            .findPropertyMetadata(p.getCurrentName())
                            .getAnnotation(Field.class);
                    if(a.isPresent()){
                        return new SimpleDateFormat(a.get().pattern()).parse(p.getText());
                    }
                    return new SimpleDateFormat().parse(p.getText());
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        }
    }
}
