package com.caspco.spl.data.elasticsearch.repository;

import com.caspco.spl.wrench.repository.SplRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.io.Serializable;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 6/28/17
 * Time: 3:35 PM
 */
@NoRepositoryBean
public interface SplElasticRepository<T, ID extends Serializable> extends Repository<T, ID>, SplRepository<T, ID> {

}
