package com.caspco.spl.data.elasticsearch.factory;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/28/17
 *         Time: 5:06 PM
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface SplElasticDocument {
    String index();
    String type();

}
