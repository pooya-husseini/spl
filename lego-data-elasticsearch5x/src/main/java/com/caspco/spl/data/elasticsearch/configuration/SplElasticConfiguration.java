package com.caspco.spl.data.elasticsearch.configuration;

import com.caspco.spl.data.elasticsearch.factory.EnableSplElasticRepository;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 7/1/17
 *         Time: 9:01 AM
 */

@Configuration
@EnableSplElasticRepository(basePackages = "${spl.common.scan-package}")
public class SplElasticConfiguration {

    @Bean(destroyMethod = "close")
    public TransportClient elasticClient(@Value("${spl.elastic.host}") String host, @Value("${spl.elastic.cluster-name}") String clusterName, @Value("${spl.elastic.port}") String port) throws UnknownHostException {
        Settings settings = Settings.builder()
                .put("client.transport.nodes_sampler_interval", "5s")
                .put("client.transport.sniff", false)
                .put("transport.tcp.compress", true)
                .put("cluster.name", clusterName)
                .build();

        TransportClient client = new PreBuiltTransportClient(settings);

        client.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(host), Integer.parseInt(port)));
        return client;
    }

}