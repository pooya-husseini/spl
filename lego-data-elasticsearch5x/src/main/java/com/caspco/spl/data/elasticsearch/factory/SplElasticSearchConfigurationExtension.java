package com.caspco.spl.data.elasticsearch.factory;

import com.caspco.spl.data.elasticsearch.repository.SplElasticRepository;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.data.repository.config.AnnotationRepositoryConfigurationSource;
import org.springframework.data.repository.config.RepositoryConfigurationExtensionSupport;
import org.springframework.data.repository.config.XmlRepositoryConfigurationSource;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Collections;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/28/17
 *         Time: 4:55 PM
 */
public class SplElasticSearchConfigurationExtension extends RepositoryConfigurationExtensionSupport {

    @Override
    protected String getModulePrefix() {
        return "elasticsearch";
    }

    @Override
    public String getRepositoryFactoryClassName() {
        return SplElasticFactoryBean.class.getName();
    }

    @Override
    public void postProcess(BeanDefinitionBuilder builder, AnnotationRepositoryConfigurationSource config) {
        AnnotationAttributes attributes = config.getAttributes();
        builder.addPropertyReference("elasticClient", attributes.getString("clientBeanName"));
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.data.repository.config.RepositoryConfigurationExtensionSupport#postProcess(org.springframework.beans.factory.support.BeanDefinitionBuilder, org.springframework.data.repository.config.XmlRepositoryConfigurationSource)
     */
    @Override
    public void postProcess(BeanDefinitionBuilder builder, XmlRepositoryConfigurationSource config) {
//        Element element = config.getElement();
//        builder.addPropertyReference("elasticsearchOperations", element.getAttribute("elasticsearch-template-ref"));
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.data.repository.config.RepositoryConfigurationExtensionSupport#getIdentifyingAnnotations()
     */
    @Override
    protected Collection<Class<? extends Annotation>> getIdentifyingAnnotations() {
        return Collections.singleton(SplElasticDocument.class);
    }

    /*
    * (non-Javadoc)
    * @see org.springframework.data.repository.config.RepositoryConfigurationExtensionSupport#getIdentifyingTypes()
    */
    @Override
    protected Collection<Class<?>> getIdentifyingTypes() {
        return Collections.singletonList(SplElasticRepository.class);
    }
}
