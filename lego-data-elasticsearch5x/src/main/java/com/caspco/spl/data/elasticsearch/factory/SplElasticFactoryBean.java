package com.caspco.spl.data.elasticsearch.factory;


import com.caspco.spl.data.elasticsearch.repository.SplElasticRepository;
import com.caspco.spl.data.elasticsearch.repository.SplElasticRepositoryImpl;
import org.elasticsearch.client.transport.TransportClient;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.core.GenericTypeResolver;
import org.springframework.data.repository.core.NamedQueries;
import org.springframework.data.repository.query.DefaultEvaluationContextProvider;
import org.springframework.data.repository.query.EvaluationContextProvider;
import org.springframework.data.repository.query.QueryLookupStrategy;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 6/28/17
 * Time: 3:38 PM
 */
public class SplElasticFactoryBean implements FactoryBean<SplElasticRepository> {

    private final Class<?> repositoryInterface;
    private QueryLookupStrategy.Key queryLookupStrategyKey;
    private boolean lazyInit = false;
    private Class<?> repositoryBaseClass;
    private NamedQueries namedQueries;
    private TransportClient elasticClient;
    private EvaluationContextProvider evaluationContextProvider = DefaultEvaluationContextProvider.INSTANCE;

    public SplElasticFactoryBean(String interfaceName) throws ClassNotFoundException {
        this.repositoryInterface = Class.forName(interfaceName);
    }

    public QueryLookupStrategy.Key getQueryLookupStrategyKey() {
        return queryLookupStrategyKey;
    }

    public void setQueryLookupStrategyKey(QueryLookupStrategy.Key queryLookupStrategyKey) {
        this.queryLookupStrategyKey = queryLookupStrategyKey;
    }

    @Override
    public SplElasticRepository getObject() throws Exception {

        Class<?> typeArgument = GenericTypeResolver.resolveTypeArguments(this.repositoryInterface, SplElasticRepository.class)[0];
        SplElasticRepositoryImpl repository = new SplElasticRepositoryImpl(this.elasticClient,typeArgument);
        return (SplElasticRepository) Enhancer.create(this.repositoryInterface,
                (MethodInterceptor) (o, method, objects, methodProxy) -> method.invoke(repository, objects)
        );
    }

    @Override
    public Class<?> getObjectType() {
        return SplElasticRepository.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    public boolean isLazyInit() {
        return lazyInit;
    }

    public void setLazyInit(boolean lazyInit) {
        this.lazyInit = lazyInit;
    }

    public Class<?> getRepositoryBaseClass() {
        return repositoryBaseClass;
    }

    public void setRepositoryBaseClass(Class<?> repositoryBaseClass) {
        this.repositoryBaseClass = repositoryBaseClass;
    }

    public NamedQueries getNamedQueries() {
        return namedQueries;
    }

    public void setNamedQueries(NamedQueries namedQueries) {
        this.namedQueries = namedQueries;
    }

    public EvaluationContextProvider getEvaluationContextProvider() {
        return evaluationContextProvider;
    }

    public void setEvaluationContextProvider(EvaluationContextProvider evaluationContextProvider) {
        this.evaluationContextProvider = evaluationContextProvider;
    }

    public TransportClient getElasticClient() {
        return elasticClient;
    }

    public void setElasticClient(TransportClient elasticClient) {
        this.elasticClient = elasticClient;
    }

    //    private ElasticsearchOperations operations;
//
//    protected MyElasticFactoryBean(Class<? extends T> repositoryInterface) {
//        super(repositoryInterface);
//    }
//
//    public void setElasticsearchOperations(ElasticsearchOperations operations) {
//        Assert.notNull(operations, "ElasticsearchOperations must not be null!");
//        setMappingContext(operations.getElasticsearchConverter().getMappingContext());
//        this.operations = operations;
//    }
//
//
//
//    @Override
//    protected RepositoryFactorySupport createRepositoryFactory() {
//        return new SplElasticFactory(operations);
//    }
//
//
//    public static class SplElasticFactory extends ElasticsearchRepositoryFactory {
//
//        private final ElasticsearchOperations operations;
//
//        public SplElasticFactory(ElasticsearchOperations elasticsearchOperations) {
//            super(elasticsearchOperations);
//            this.operations = elasticsearchOperations;
//        }
//
//        @Override
//        protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
//            return super.getRepositoryBaseClass(metadata);
//        }
//
//        @Override
//        protected Object getTargetRepository(RepositoryInformation metadata) {
//            return new SplElasticRepositoryImpl(this.getEntityInformation(metadata.getDomainType()),this.operations);
//        }
//    }

}
