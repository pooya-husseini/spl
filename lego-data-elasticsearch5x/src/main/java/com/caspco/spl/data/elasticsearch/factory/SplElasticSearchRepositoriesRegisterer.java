package com.caspco.spl.data.elasticsearch.factory;

import org.springframework.data.repository.config.RepositoryBeanDefinitionRegistrarSupport;
import org.springframework.data.repository.config.RepositoryConfigurationExtension;

import java.lang.annotation.Annotation;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/28/17
 *         Time: 4:54 PM
 */
public class SplElasticSearchRepositoriesRegisterer extends RepositoryBeanDefinitionRegistrarSupport {
    @Override
    protected Class<? extends Annotation> getAnnotation() {
        return EnableSplElasticRepository.class;
    }

    @Override
    protected RepositoryConfigurationExtension getExtension() {
        return new SplElasticSearchConfigurationExtension();
    }
}
