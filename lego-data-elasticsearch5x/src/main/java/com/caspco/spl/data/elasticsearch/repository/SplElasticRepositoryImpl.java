package com.caspco.spl.data.elasticsearch.repository;

import com.caspco.spl.data.elasticsearch.ElasticObjectMapper;
import com.caspco.spl.data.elasticsearch.factory.SplElasticDocument;
import com.caspco.spl.data.elasticsearch.model.IndexQuery;
import com.caspco.spl.model.wrench.exception.NotImplementedException;
import com.caspco.spl.model.wrench.exception.SplBaseException;
import com.caspco.spl.wrench.utils.reflection.ClassUtils;
import com.caspco.spl.wrench.utils.reflection.PropertyMetadata;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.types.OrderSpecifier;
import org.apache.log4j.Logger;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.search.SearchHit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.Id;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 6/28/17
 * Time: 3:36 PM
 */

@NoRepositoryBean
public class SplElasticRepositoryImpl<T, ID extends Serializable> implements SplElasticRepository<T, ID> {
    private static final Logger LOGGER = Logger.getLogger(SplElasticRepositoryImpl.class);
    private final TransportClient elasticClient;
    private final Class<?> typeArgument;
    private final ObjectMapper objectMapper;
//    private Class<T> modelType = (Class<T>) GenericTypeResolver.resolveTypeArguments(this.getClass(), SplElasticRepositoryImpl.class)[0];

    public SplElasticRepositoryImpl(TransportClient elasticClient, Class<?> typeArgument) {
        this.typeArgument = typeArgument;
        this.elasticClient = elasticClient;
        this.objectMapper= ElasticObjectMapper.getInstance();
    }
//    private final ElasticsearchEntityInformation<?, Serializable> entityInformation;
//    private final ElasticsearchOperations operations;

//    public SplElasticRepositoryImpl(ElasticsearchEntityInformation<?, Serializable> entityInformation, ElasticsearchOperations operations) {
//        this.entityInformation = entityInformation;
//        this.operations = operations;
//    }

    @Override
    public <S extends T> List<S> save(Iterable<S> entities) {
        throw new NotImplementedException();
    }

    @Override
    public Iterable<T> findAll(Iterable<ID> ids) {
        throw new NotImplementedException();
    }

    @Override
    public void delete(Iterable<? extends T> entities) {
        throw new NotImplementedException();
    }

    @Override
    public void deleteAll() {
        throw new NotImplementedException();
    }

    @Override
    public Class getTypeArgument() {
        return typeArgument;
    }

    @Override
    public long count() {
        throw new NotImplementedException();
    }

    @Override
    public long count(T t) {
        throw new NotImplementedException();
    }

    @Override
    public <S extends T> S save(S obj) {
        try {
            prepareIndex(createIndexQuery(obj)).execute().actionGet().getId();
//            LOGGER.debug("Trying to save entity");
//            String index = obj.getClass().getSimpleName().toLowerCase();
//            LOGGER.debug("Trying to initiate json mapper");
//            StopWatch stopWatch = new StopWatch();
//            stopWatch.start();
//            ObjectMapper objectMapper = new ObjectMapper();
//            String s = objectMapper.writeValueAsString(obj);
//            stopWatch.stop();
//            LOGGER.debug(String.format("Json string already created in %d ms", stopWatch.getLastTaskTimeMillis()));
//            stopWatch.start();
//            IndexRequest indexRequest = new IndexRequest(index, index);
//            indexRequest.source(s, XContentType.JSON);
//            IndexResponse response = this.elasticClient.index(indexRequest).actionGet();
//            stopWatch.stop();
//            LOGGER.debug(String.format("Json saved in %d ms", stopWatch.getLastTaskTimeMillis()));
        } catch (JsonProcessingException e) {
            throw new SplBaseException(e);
        }
        return null;
    }

    private IndexRequestBuilder prepareIndex(IndexQuery indexQuery) throws JsonProcessingException {
        SplElasticDocument annotation = getMetaData();
        IndexRequestBuilder builder = elasticClient.prepareIndex(annotation.index(), annotation.type());

        String s = objectMapper.writeValueAsString(indexQuery.getObject());
        builder.setSource(s);
        return builder;
    }



    private SplElasticDocument getMetaData() {
        if (!typeArgument.isAnnotationPresent(SplElasticDocument.class)) {
            throw new IllegalStateException("Class " + typeArgument.getName() + " should annotated with SplElasticDocument");
        }
        return typeArgument.getAnnotation(SplElasticDocument.class);
    }


    @Override
    public T findOne(ID id) {
        throw new NotImplementedException();
    }

    @Override
    public List<T> find(T e) {
        throw new NotImplementedException();
    }

    @Override
    public List<T> findAll() {
        SplElasticDocument annotation = getMetaData();
        List<T> ts = new ArrayList<>();
//        QueryBuilder query = new SimpleQueryStringBuilder("*:*");
        Iterator<SearchHit> iterator = this.elasticClient
                .prepareSearch(annotation.index())
                .execute().actionGet().getHits().iterator();
        try {
            while (iterator.hasNext()) {
                SearchHit next = iterator.next();
                String json = objectMapper.writeValueAsString(next.getSource());
                ts.add((T) objectMapper.readValue(json, typeArgument));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return ts;
    }

    @Override
    public Iterable<T> findAll(Sort sort) {
        throw new NotImplementedException();
    }

    @Override
    public Page<T> findAll(Pageable sort) {
        throw new NotImplementedException();
    }

    @Override
    public boolean exists(ID id) {
        throw new NotImplementedException();
    }

    @Override
    public boolean exists(T e) {
        throw new NotImplementedException();
    }

    @Override
    public void delete(ID id) {
        throw new NotImplementedException();
    }

    @Override
    public void delete(T e) {
        throw new NotImplementedException();
    }

    @Override
    public void deleteAllInBatch() {
        throw new NotImplementedException();
    }

    @Override
    public List<String> searchColumn(String column, String value) {
        throw new NotImplementedException();
    }

    @Override
    public List<T> findActives(T e) {
        throw new NotImplementedException();
    }

    @Override
    public List<T> findActives(T e, OrderSpecifier<?>[] orders) {
        throw new NotImplementedException();
    }

    @Override
    public List<T> find(T e, OrderSpecifier<?>[] orders) {
        throw new NotImplementedException();
    }


    private IndexQuery createIndexQuery(T entity) {
        IndexQuery query = new IndexQuery();
        query.setObject(entity);
        query.setId(stringIdRepresentation(extractIdFromBean(entity)));
        return query;
    }

    public String stringIdRepresentation(ID id) {
        return String.valueOf(id);
    }

    protected ID extractIdFromBean(T entity) {
        List<PropertyMetadata> properties = ClassUtils.create(entity.getClass()).filterProperties(i -> i.isAnnotatedWith(Id.class));
        if (properties.size() <= 0) {
            return null;
        } else if (properties.size() > 1) {
            throw new IllegalArgumentException("Entity should have only one id!");
        } else {
            return (ID) properties.get(0).getValue(entity);
        }
    }
}


