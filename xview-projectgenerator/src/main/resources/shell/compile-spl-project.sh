export JDK_HOME=/home/pooya/Programs/Programs/jdk1.8.0_31
export JAVA_HOME=$JDK_HOME/jre
export PATH=$PATH:$JAVA_HOME/bin
mvn -o -f $1 clean install -PCodeGeneration && mvn -o -f $1 clean install
