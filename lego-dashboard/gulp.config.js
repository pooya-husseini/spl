var appPath = 'app/';
var client = './src/main/resources/static/';
var scriptPath = client + '/scripts';
var port = 8080;
var config = {
    buildPath: '',
    styles: [],
    indexPage: client + "index.html",
    browserSync: {
        proxy: 'localhost:' + port,
        port: 3000,
        files: ['**/*.*'],
        ghostMode: { // these are the defaults t,f,t,t
            clicks: true,
            location: false,
            forms: true,
            scroll: true
        },
        logLevel: 'debug',
        logPrefix: 'gulp-patterns',
        notify: true,
        reloadDelay: 1000
    },
    bower: {
        json: require('./bower.json'),
        directory: client + 'vendors',
        ignorePath: './../../'
    },
    cssSources: {
        sources: [
            client + 'app/css/**/*.css',
            "!" + client + 'app/dist/**/*.css'
        ]
    },
    jsSources: {
        client: client + "app/js/**/*.js",
        exclude: ["!/**/app.js", "!/**/all.js"],
        sundry: ['./gulpfile.js', './gulpfile.config.js'],
        finalFiles: [
            client + 'app/dist/lego.js',
            client + 'app/dist/project.js'
        ],
        sources: [
            client + 'app/gen-src/**/*.js',
            client + 'app/**/*.js',
            client + 'app/app.js',
            client + 'app/**/templates.js'
        ],
        injectable: [
            scriptPath + '/quick-sidebar.js',
            scriptPath + '/demo.js',
            scriptPath + '/layout.js',
            scriptPath + '/metronic.js',
            client + 'app/**/*.module.js',
            client + 'app/**/*.js',
            '!' + client + '/app/**/all.js',
            '!' + client + '/app/**/app.js',
            '!' + client + '/app/**/utils.js',
            '!' + client + '/app/**/route.js',
            '!' + client + '/app/**/config.js',
        ]
    },
};


module.exports.config = config;
module.exports.getWireDepOptions = function () {
    return {
        bowerJson: config.bower.json,
        directory: config.bower.directory,
        ignorePath: config.bower.ignorePath
    };
};