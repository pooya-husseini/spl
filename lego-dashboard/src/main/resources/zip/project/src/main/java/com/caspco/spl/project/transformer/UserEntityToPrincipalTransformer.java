package com.caspco.spl.project.transformer;

/**
 * THIS IS AN AUTO GENERATED CODE.
 *
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Mar 1, 2015 6:18:24 PM
 * @version 1.0.0
 */


import com.caspco.spl.project.model.UserEntity;
import com.caspco.xview.wrench.beans.transformer.SimpleBeanTransformer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserEntityToPrincipalTransformer extends SimpleBeanTransformer<UserEntity, User> {

    @Override
    public User convert(UserEntity source, Object parent) {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(source.getRole());
        return new User(source.getUsername(), source.getPassword(), authorities);
    }

}