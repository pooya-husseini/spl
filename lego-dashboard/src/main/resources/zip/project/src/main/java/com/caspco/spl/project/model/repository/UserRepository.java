package com.caspco.spl.project.model.repository;

/**
* THIS IS AN AUTO GENERATED CODE.
* @author : XVIEW CODE GENERATOR
* Email : husseini@caspco.ir
* Date: Jan 2, 2016 1:10:14 PM
* @version 1.0.0
*/

import com.caspco.xview.wrench.data.SwiftRepository;
import com.caspco.spl.project.model.*;
import com.caspco.xview.wrench.annotations.cg.Generated;

@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public interface UserRepository extends SwiftRepository<UserEntity,java.lang.Long> {

    UserEntity findByUsername(String username);
}