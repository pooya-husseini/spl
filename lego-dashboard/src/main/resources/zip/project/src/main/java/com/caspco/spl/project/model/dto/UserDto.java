package com.caspco.spl.project.model.dto;

/**
* THIS IS AN AUTO GENERATED CODE.
* @author : XVIEW CODE GENERATOR
* Email : husseini@caspco.ir
* Date: Jan 2, 2016 1:10:14 PM
* @version 1.0.0
*/


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.caspco.spl.project.model.*;
import com.caspco.xview.wrench.annotations.cg.Generated;



    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
    public class UserDto {


    
    private                             java.lang.Long              id;

    
    private                             java.lang.String              firstName;

    
    private                             java.lang.String              lastName;

    
    private                             java.lang.String              password;

    
    private                             java.lang.String              username;

    
    private                             java.lang.String              role;

    
    private                             java.util.Date              lastLoginDate;


    
    
        
    public                             java.lang.Long              getId(){
    return this.id;
    }

    public void setId(                            java.lang.Long             id){
    this.id=id;
    }

    
    
        
    public                             java.lang.String              getFirstName(){
    return this.firstName;
    }

    public void setFirstName(                            java.lang.String             firstName){
    this.firstName=firstName;
    }

    
    
        
    public                             java.lang.String              getLastName(){
    return this.lastName;
    }

    public void setLastName(                            java.lang.String             lastName){
    this.lastName=lastName;
    }

    
    
                @com.caspco.xview.wrench.annotations.cg.security.Password
    
    public                             java.lang.String              getPassword(){
    return this.password;
    }

    public void setPassword(                            java.lang.String             password){
    this.password=password;
    }

    
    
        
    public                             java.lang.String              getUsername(){
    return this.username;
    }

    public void setUsername(                            java.lang.String             username){
    this.username=username;
    }

    
    
        
    public                             java.lang.String              getRole(){
    return this.role;
    }

    public void setRole(                            java.lang.String             role){
    this.role=role;
    }

    
    
        
    public                             java.util.Date              getLastLoginDate(){
    return this.lastLoginDate;
    }

    public void setLastLoginDate(                            java.util.Date             lastLoginDate){
    this.lastLoginDate=lastLoginDate;
    }
    }