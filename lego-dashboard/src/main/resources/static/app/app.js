/**
 *         @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 1/6/16
 *         Time: 5:22 PM
 */

(function (angular) {
    angular.module('application', ['lego.ui.route']);
})(window.angular);