package com.caspco.spl.projectgenerator.exceptions;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 3/9/16
 *         Time: 5:18 PM
 */
public class IllegalPasswordException extends RuntimeException {

    public IllegalPasswordException() {
    }

    public IllegalPasswordException(String message) {
        super(message);
    }

    public IllegalPasswordException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalPasswordException(Throwable cause) {
        super(cause);
    }

    public IllegalPasswordException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
