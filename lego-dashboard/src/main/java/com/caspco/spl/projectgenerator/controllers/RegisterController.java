package com.caspco.spl.projectgenerator.controllers;

import com.caspco.spl.projectgenerator.dto.RegisterDto;
import com.caspco.spl.projectgenerator.exceptions.IllegalPasswordException;
import com.caspco.spl.projectgenerator.models.UserEntity;
import com.caspco.spl.projectgenerator.service.PrincipalService;
import com.caspco.spl.projectgenerator.transformer.RegisterDtoToUserEntityTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 1/5/16
 *         Time: 9:47 PM
 */

@RestController
@Transactional
@RequestMapping("/credentials")
public class RegisterController {

    @Autowired
    private PrincipalService userService;

    @Autowired
    private RegisterDtoToUserEntityTransformer transformer;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public void register(@RequestBody RegisterDto dto) throws IOException {
        if (!dto.getPassword().equals(dto.getRepass())) {
            throw new IllegalPasswordException("Password and repass aren't same");
        }
        UserEntity entity = transformer.convert(dto);
        userService.save(entity);
    }
}