package com.caspco.spl.projectgenerator.security;


import com.caspco.spl.projectgenerator.models.UserEntity;
import com.caspco.spl.projectgenerator.service.PrincipalService;
import com.caspco.spl.projectgenerator.transformer.UserEntityToSplUserDetailsTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 12/6/15
 *         Time: 2:18 PM
 */
@Service
public class UserSecurityProvider implements UserDetailsService {

//    private User defaultUser;

    @Autowired
    private PrincipalService userService;
    @Autowired
    private UserEntityToSplUserDetailsTransformer transformer;


    public UserSecurityProvider() {
//        this.defaultUser = new User("admin", "admin", AuthorityUtils.createAuthorityList("ROLE_ADMIN"));
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserEntity entity = userService.findByUsername(s);
        return transformer.convert(entity);
    }
}