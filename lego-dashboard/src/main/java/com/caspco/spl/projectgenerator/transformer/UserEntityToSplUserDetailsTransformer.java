package com.caspco.spl.projectgenerator.transformer;


/**
 * THIS IS AN AUTO GENERATED CODE.
 *
 * @author : Lego Code Generator
 * Email : husseini@caspco.ir
 * Date: Dec 9, 2015 3:26:16 PM
 * @version 1.0.0
 */


import com.caspco.spl.projectgenerator.models.SplUserDetails;
import com.caspco.spl.projectgenerator.models.UserEntity;
import com.caspco.spl.wrench.annotations.cg.Generated;
import com.caspco.spl.wrench.beans.transformer.SimpleBeanTransformer;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collections;


@Component
@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public class UserEntityToSplUserDetailsTransformer extends SimpleBeanTransformer<UserEntity, SplUserDetails> {


    @Override
    public SplUserDetails convert(UserEntity source, Object parent) {
        return new SplUserDetails(
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")),
                source.getPassword(),
                source.getUsername()
        );
    }
}