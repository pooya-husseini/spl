package com.caspco.spl.projectgenerator.transformer;


/**
 * THIS IS AN AUTO GENERATED CODE.
 *
 * @author : Lego Code Generator
 * Email : husseini@caspco.ir
 * Date: Dec 9, 2015 3:26:16 PM
 * @version 1.0.0
 */


import com.caspco.spl.projectgenerator.dto.UserDto;
import com.caspco.spl.projectgenerator.models.UserEntity;
import com.caspco.spl.wrench.annotations.cg.Generated;
import com.caspco.spl.wrench.beans.transformer.SimpleBeanTransformer;
import org.springframework.stereotype.Component;


@Component
@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public class UserDtoTransformer extends SimpleBeanTransformer<UserDto, UserEntity> {

    @Override
    public UserEntity convert(UserDto o, Object parent) {
        if (o == null) {
            return null;
        }
        UserEntity e = new UserEntity();
        e.setId(o.getId());
        e.setFirstName(o.getFirstName());
        e.setLastName(o.getLastName());
        e.setUsername(o.getUsername());
        e.setLastLoginDate(o.getLastLoginDate());

        return e;
    }
}