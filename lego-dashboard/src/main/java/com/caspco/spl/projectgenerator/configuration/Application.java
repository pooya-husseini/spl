package com.caspco.spl.projectgenerator.configuration;

import com.caspco.spl.data.swift.SwiftRepositoryFactoryBean;
import com.caspco.spl.wrench.spring.configuration.WrenchConfiguration;
import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"com.caspco.spl.projectgenerator"})
@PropertySource("classpath:version.properties")
@EntityScan(basePackages = {"com.caspco.spl.projectgenerator", "com.caspco.spl.data.revision"})
@EnableJpaRepositories(value = {"com.caspco.spl.projectgenerator", "com.caspco.spl.wrench"}, repositoryFactoryBeanClass = SwiftRepositoryFactoryBean.class)
@EnableEncryptableProperties
@Import(WrenchConfiguration.class)
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}