package com.caspco.spl.webactivator.json;


import com.caspco.spl.model.annotations.json.SecuredValue;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.ser.std.StringSerializer;

import java.io.IOException;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 16/09/14
 *         Time: 18:44
 */
public class SecuredValueSerializer extends JsonSerializer<String> implements ContextualSerializer {

    @Override
    public void serialize(String data, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeString("");
    }

    @Override
    public Class<String> handledType() {
        return String.class;
    }

    @Override
    public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) throws JsonMappingException {
        if (property == null) {
            return new StringSerializer();
        }
        SecuredValue ann = property.getAnnotation(SecuredValue.class);
        if (ann == null) {
            ann = property.getContextAnnotation(SecuredValue.class);
        }
        if (ann == null) {
            return new StringSerializer();
        }
        return new SecuredValueSerializer();
    }
}