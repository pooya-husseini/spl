package com.caspco.spl.webactivator.beans.rest;

/**
 * @author : Pooya Hosseini
 * Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 * Date: 12/8/15
 * Time: 11:02 AM
 */

import com.caspco.spl.model.annotations.cg.Translation;
import com.caspco.spl.model.wrench.Locale;
import com.caspco.spl.wrench.annotations.permission.Permission;
import com.caspco.spl.wrench.beans.transformer.SmartTransformer;
import com.caspco.spl.wrench.services.SplService;
import com.caspco.spl.wrench.utils.enums.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class SplReadOnlyRestService<D, R extends SplService> extends RestOptionsResolver {
    protected Class<D> dType = (Class<D>) GenericTypeResolver.resolveTypeArguments(this.getClass(), SplReadOnlyRestService.class)[0];

    @Autowired
    protected R service;
    @Autowired
    protected SmartTransformer transformer;
    private int pageSize = 10;

    @RequestMapping(method = RequestMethod.GET)
    @Permission(name= "Search" ,translations = {
            @Translation(label = "جستجو" , locale = Locale.Persian_Iran),
            @Translation(label = "Search" , locale = Locale.English_United_States)
    })
    public D getById(@RequestParam("id") Long id) {
        return transformer.convert(service.findById(id), dType);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @Permission(name= "Search" ,translations = {
            @Translation(label = "جستجو" , locale = Locale.Persian_Iran),
            @Translation(label = "Search" , locale = Locale.English_United_States)
    })
    public List<D> getAll() {
        return transformer.convert(service.findAll(), dType);
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @Permission(name= "Search" ,translations = {
            @Translation(label = "جستجو" , locale = Locale.Persian_Iran),
            @Translation(label = "Search" , locale = Locale.English_United_States)
    })
    public Long getCount(@Valid @RequestBody D dto) {
        return service.count(transformer.convert(dto, service.getModelType()));
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @Permission(name= "Search" ,translations = {
            @Translation(label = "جستجو" , locale = Locale.Persian_Iran),
            @Translation(label = "Search" , locale = Locale.English_United_States)
    })
    public List<D> search(@RequestBody D dto, @RequestParam(defaultValue = "0", value = "pageIndex") int pageIndex) {
        List<?> entities = service.findBySample(transformer.convert(dto, service.getModelType()));
        return transformer.convert(entities, dType);
    }

//    @RequestMapping(value = "/report", method = RequestMethod.POST)
//    public void report(@RequestParam("reportType") String reportType, @RequestParam("locale") String locale, @Valid @RequestBody D dto, HttpServletResponse response) {
//        service.report(reportType, transformer.convert(dto, service.getModelType()), response, locale);
//    }

    //    @RequestMapping(value = "/enumTypes", method = RequestMethod.GET)
    public List<EnumUtils.Value> getEnumTypes(Class<? extends Enum> e) {
        return EnumUtils.convertEnumToList(e.getEnumConstants());
    }

    @ExceptionHandler
    public abstract void handle(HttpMessageNotReadableException e);

    @RequestMapping(value = "/searchColumn", method = RequestMethod.GET)
    public List<String> searchColumns(@RequestParam("column") String column, @RequestParam("value") String value) {
        return service.searchColumn(column, value);
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    protected R getService() {
        return service;
    }

    /**
     * Setter for property 'service'.
     *
     * @param service Value to set for property 'service'.
     */
    @Autowired
    public void setService(R service) {
        this.service = service;
    }

    @Override
    protected Map<String, String> extraParameters() {
        HashMap<String, String> options = new HashMap<>();
        if (service.isActivable()) {
            options.put("activeable", "true");
        }
        return options;
    }

    @RequestMapping(value = "/activeCheck", method = RequestMethod.POST)
    @Permission(name= "Search" ,translations = {
            @Translation(label = "جستجو" , locale = Locale.Persian_Iran),
            @Translation(label = "Search" , locale = Locale.English_United_States)
    })
    public Map<String,Boolean> isActive(@RequestBody D dto) {
        return Collections.singletonMap("result", service.isActive(transformer.convert(dto, service.getModelType())));
    }

    @RequestMapping(value = "/searchActives", method = RequestMethod.POST)
    @Permission(name= "Search" ,translations = {
            @Translation(label = "جستجو" , locale = Locale.Persian_Iran),
            @Translation(label = "Search" , locale = Locale.English_United_States)
    })
    public List<D> searchActives(@RequestBody D dto, @RequestParam(defaultValue = "0", value = "pageIndex") int pageIndex) {
        return transformer.convert(service.findActivesBySample(transformer.convert(dto,service.getModelType())),dType);
    }
}