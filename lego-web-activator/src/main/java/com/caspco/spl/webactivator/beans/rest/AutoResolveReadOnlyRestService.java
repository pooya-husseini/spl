//package com.caspco.spl.wrench.beans.rest;
//
///**
// * @author : Pooya Hosseini
// * Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
// * Date: 12/8/15
// * Time: 11:02 AM
// */
//
//import com.caspco.spl.wrench.beans.service.SimpleService;
//import com.caspco.spl.wrench.beans.transformer.BasicTransformer;
//import com.caspco.spl.wrench.beans.transformer.SimpleBeanTransformer;
//import com.caspco.spl.wrench.utils.enums.EnumUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.converter.HttpMessageNotReadableException;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletResponse;
//import javax.validation.Valid;
//import java.util.List;
//
//@Transactional
//public abstract class AutoResolveReadOnlyRestService<D, R extends SimpleService> extends RestOptionsResolver {
//
//    private final Class<D> dType;
//
//    protected R service;
//    private int pageSize = 10;
//
//    @Autowired
//    private BasicTransformer basicTransformer;
//
//    protected AutoResolveReadOnlyRestService(Class<D> dType, Class<E> eType) {
//        this.dType = dType;
//        this.eType = eType;
//
//    }
//
//    protected AutoResolveReadOnlyRestService(Class<D> dType, Class<E> eType, R service) {
//        this.dType = dType;
//        this.eType = eType;
//        this.service = service;
//    }
//
//    @RequestMapping(method = RequestMethod.GET)
//    public D getById(@RequestParam("id") Long id) {
//        return getETransformer().convert((E) service.findById(id));
//    }
//
//    @RequestMapping(value = "/all", method = RequestMethod.GET)
//    public List<D> getAll() {
//        return getETransformer().convert(service.findAll());
//    }
//
//    @RequestMapping(value = "/count", method = RequestMethod.GET)
//    public Long getCount(@Valid @RequestBody D dto) {
//        return service.count(getDTransformer().convert(dto));
//    }
//
//    @RequestMapping(value = "/search", method = RequestMethod.POST)
//    public List<D> search(@RequestBody D dto, @RequestParam(defaultValue = "0", value = "pageIndex") int pageIndex) {
//        List<E> entities = service.findBySample(getDTransformer().convert(dto));
//        return getETransformer().convert(entities);
//    }
//
//    @RequestMapping(value = "/report", method = RequestMethod.POST)
//    public void report(@RequestParam("reportType") String reportType, @RequestParam("locale") String locale, @Valid @RequestBody D dto, HttpServletResponse response) {
//        service.report(reportType, getDTransformer().convert(dto), response, locale);
//    }
//
//    //    @RequestMapping(value = "/enumTypes", method = RequestMethod.GET)
//    public List<EnumUtils.Value> getEnumTypes(Class<? extends Enum> e) {
//        return EnumUtils.convertEnumToList(e.getEnumConstants());
//    }
//
//    @ExceptionHandler
//    public abstract void handle(HttpMessageNotReadableException e);
//
//    @RequestMapping(value = "/searchColumn", method = RequestMethod.GET)
//    public List<String> searchColumns(@RequestParam("column") String column, @RequestParam("value") String value) {
//        return service.searchColumn(column, value);
//    }
//
//    public int getPageSize() {
//        return pageSize;
//    }
//
//    public void setPageSize(int pageSize) {
//        this.pageSize = pageSize;
//    }
//
//
//    protected R getService() {
//        return service;
//    }
//
//    /**
//     * Setter for property 'service'.
//     *
//     * @param service Value to set for property 'service'.
//     */
//    public void setService(R service) {
//        this.service = service;
//    }
//
//    protected SimpleBeanTransformer<D, E> getDTransformer() {
//        return basicTransformer.getConverter(dType, eType);
//    }
//
//    protected SimpleBeanTransformer<E, D> getETransformer() {
//        return basicTransformer.getConverter(eType, dType);
//    }
//
//}