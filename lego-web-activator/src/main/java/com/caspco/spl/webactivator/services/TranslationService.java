package com.caspco.spl.webactivator.services;

import com.caspco.spl.model.annotations.cg.*;
import com.caspco.spl.model.annotations.rcg.SplException;
import com.caspco.spl.webactivator.beans.rest.RestOptionsResolver;
import com.caspco.spl.wrench.annotations.permission.Permission;
import com.caspco.spl.wrench.beans.scanner.TypeScanner;
import com.caspco.spl.wrench.utils.reflection.ClassUtils;
import com.caspco.spl.wrench.utils.string.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.reflections.util.ClasspathHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.Entity;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.management.ManagementFactory;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Member;
import java.lang.reflect.Modifier;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/28/17
 * Time: 11:22 AM
 */

@Service
public class TranslationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TranslationService.class);

    private final static Map<Class<? extends Annotation>, Set<Class<?>>> ANNOTATION_CACHE = new ConcurrentHashMap<>();

    @Autowired
    private TypeScanner typeScanner;
    private Map<String, String> translations = new ConcurrentHashMap<>();

    @PostConstruct
    public void init() {
        boolean debug = ManagementFactory.getRuntimeMXBean().
                getInputArguments().toString().contains("jdwp");
        extractOperationTranslations();
        extractGeneralParameters();
        extractMethodTranslations();
        loadAndScanTranslationFolder(debug);
        extractMenuTranslations();
        extractExceptionTranslations();
        extractPermissionTranslations();
    }

    private void extractPermissionTranslations() {
        typeScanner.getSubTypesOf(RestOptionsResolver.class)
                .stream()
                .filter(i -> !Modifier.isAbstract(i.getModifiers()))
                .map(ClassUtils::create)
                .forEach(i ->
                        i.filterMethodsStream(x -> x.isAnnotatedWith(Permission.class))
                                .forEach(x -> {
                                            x.getAnnotation(Permission.class).ifPresent(a -> {
                                                Arrays.stream(a.translations()).forEach(tr -> {
                                                    String key;
                                                    if (a.autoResolve()) {
                                                        key = tr.locale().getLocale() + "." + i.getClazz().getSimpleName().replace("RestService", "") + "." + a.name();
                                                    } else {
                                                        key = tr.locale().getLocale() + "." + a.name();
                                                    }
                                                    translations.put(key, tr.label());
                                                });
                                            });
                                        }
                                )
                );
    }

    private void extractGeneralParameters() {
        ANNOTATION_CACHE
                .computeIfAbsent(Entity.class, i -> typeScanner.getTypesAnnotatedWith(Entity.class))
                .forEach(entity -> Stream.of(entity.getMethods()).forEach(method -> {
                    if (method.isAnnotationPresent(GeneralParameter.class)) {
                        GeneralParameter gp = method.getAnnotation(GeneralParameter.class);
                        Stream.of(gp.translations()).forEach(tr -> {
                            translations.put(tr.locale().getLocale() + ".GeneralParameterType." + StringUtils.makeConstant(gp.type()), tr.label());
                        });
                    }
                }));
    }

    private static Map<String, List<String>> groupByWithPrefix(List<String> list) {
        return list.stream()
                .map(i -> Pair.of(i.substring(0, i.lastIndexOf(".")), i))
                .collect(Collectors.groupingBy(Pair::getLeft, Collectors.mapping(Pair::getRight, Collectors.toList())));
    }

    private static Stream<Pair<String, String>> getProp(String pathname, InputStream is) {
        try {
            Properties properties = new Properties();
            InputStreamReader isr = new InputStreamReader(is, "utf-8");
            properties.load(isr);
            isr.close();
            return properties
                    .entrySet()
                    .stream()
                    .map(item -> {
                        String key = pathname.substring(pathname.lastIndexOf("-") + 1, pathname.indexOf(".properties")) + "." + pathname.substring(pathname.contains("/") ? pathname.lastIndexOf("/") + 1 : 0, pathname.lastIndexOf("-"));
                        if (!item.getKey().equals("title")) {
                            key += "." + item.getKey();
                        }
                        String value = String.valueOf(item.getValue());

                        return Pair.of(key, value);
                    });
        } catch (Exception x) {
            throw new RuntimeException(x);
        }
    }

    private void extractExceptionTranslations() {
        typeScanner.getTypesAnnotatedWith(SplException.class)
                .forEach(clazz ->
                        Arrays.stream(clazz.getAnnotation(SplException.class).translations())
                                .forEach(translation ->
                                        translations.put(translation.locale().getLocale() + ".exception." + clazz.getName(), translation.label())
                                )
                );
    }

    private void extractOperationTranslations() {
        typeScanner.getTypesAnnotatedWith(Operations.class)
                .forEach(clazz -> {
                    if (clazz.isAnnotationPresent(Operations.class)) {
                        Arrays.stream(clazz.getAnnotation(Operations.class).value())
                                .forEach(operation -> {
                                            Arrays.stream(operation.translations())
                                                    .forEach(translation -> {
                                                        String key = translation.locale().getLocale() + "." + normalizeEntityName(clazz.getSimpleName()) + "." + operation.name();
                                                        translations.put(key, translation.label());
                                                    });
                                            if (operation.model() != Void.class) {
                                                ClassUtils utils = ClassUtils.create(operation.model());
                                                utils.filterProperties(i -> i.isAnnotatedWith(Translations.class))
                                                        .stream()
                                                        .map(i -> Pair.of(i.getName(), i.filterAnnotations(ann -> ann.annotationType() == Translations.class).stream().map(item -> (Translations) item).flatMap(item -> Arrays.stream(item.value())).collect(Collectors.toList())))
                                                        .forEach(pair -> {
                                                            String k = pair.getLeft();
                                                            pair.getRight().forEach(translation -> {
                                                                String key = translation.locale().getLocale() + "." + normalizeEntityName(operation.model().getSimpleName()) + "." + k;
                                                                translations.put(key, translation.label());
                                                            });
                                                        });
                                            }
                                        }
                                );
                    }
                });
    }

    private void extractMenuTranslations() {
        Map<String, Class<?>> typesMap = ANNOTATION_CACHE
                .computeIfAbsent(Entity.class, i -> typeScanner.getTypesAnnotatedWith(Entity.class))
                .stream()
                .filter(type -> type.isAnnotationPresent(Translations.class))
                .collect(Collectors.toMap(Class::getName, i -> i));

        List<String> names = new ArrayList<>(typesMap.keySet());

        groupByWithPrefix(names).forEach((key, value) -> {
            Package pkg = Package.getPackage(key);

            if (pkg.isAnnotationPresent(Translations.class)) {
                Translation[] translationsArray = pkg.getAnnotation(Translations.class).value();
                Arrays.stream(translationsArray).forEach(translation -> translations.put(translation.locale().getLocale() + "." + key, translation.label()));
            }

            value.forEach(item -> {

                Class<?> clazz = typesMap.get(item);
                Translations translationsAnnotation = clazz.getAnnotation(Translations.class);

                Translation[] translationsArray = translationsAnnotation.value();
                String className = item.substring(item.lastIndexOf(".") + 1);
                final String itemName = normalizeEntityName(className);

                Arrays.stream(translationsArray).forEach(translation -> translations.put(translation.locale().getLocale() + "." + itemName, translation.label()));

                if (clazz.isAnnotationPresent(MenuEntries.class)) {
                    MenuEntry[] annotation = clazz.getAnnotation(MenuEntries.class).value();
                    Arrays.stream(annotation).forEach(menuEntry -> Arrays.stream(menuEntry.translations()).forEach(translation -> translations.put(translation.locale().getLocale() + "." + itemName + "." + menuEntry.name(),
                            translation.label())));
                }
            });
        });
    }

    private String normalizeEntityName(String item) {
        final String itemName;
        if (item.endsWith("Entity")) {
            itemName = item.substring(0, item.length() - "Entity".length());
        } else {
            itemName = item;
        }
        return itemName;
    }

    private void extractMethodTranslations() {

        Set<Class<?>> typesAnnotatedWith = ANNOTATION_CACHE
                .computeIfAbsent(Entity.class, i -> typeScanner.getTypesAnnotatedWith(Entity.class));
        typesAnnotatedWith.addAll(typeScanner.getTypesAnnotatedWith(EnumEntity.class));
        for (Class<?> clazz : typesAnnotatedWith) {
            StringBuilder className = new StringBuilder(clazz.getSimpleName());

            if (className.toString().endsWith("Entity")) {
                className.delete(className.length() - "Entity".length(), className.length());
            }
            if (clazz.isEnum()) {
                Arrays.stream(clazz.getEnumConstants())
                        .map(i -> {
                            try {
                                return clazz.getField(((Enum) i).name());
                            } catch (NoSuchFieldException e) {
                                throw new RuntimeException(e);
                            }
                        })
                        .filter(x -> x.isAnnotationPresent(Translations.class))
                        .forEach(method -> extractTranslations(className, method));
            } else {
                Arrays.stream(clazz.getMethods())
                        .filter(x -> x.isAnnotationPresent(Translations.class))
                        .forEach(method -> extractTranslations(className, method));

                Arrays.stream(clazz.getMethods())
                        .filter(x -> x.isAnnotationPresent(OperationForAppend.class))
                        .map(i -> i.getAnnotation(OperationForAppend.class))
                        .forEach(ann -> Arrays.stream(ann.translations()).forEach(tr -> translations.put(tr.locale().getLocale() + "." + className + "." + ann.operationName(), tr.label())));
            }
        }
    }

    private void extractTranslations(StringBuilder className, AccessibleObject method) {
        Translations annotation = method.getAnnotation(Translations.class);
        String methodName = ((Member) method).getName();

        if (methodName.startsWith("get") || methodName.startsWith("set")) {
            methodName = methodName.substring(3);
            methodName = methodName.substring(0, 1).toLowerCase() + methodName.substring(1);
        } else if (methodName.startsWith("is")) {
            methodName = methodName.substring(2);
            methodName = methodName.substring(0, 1).toLowerCase() + methodName.substring(1);
        }

        String finalMethodName = methodName;
        Arrays.stream(annotation.value())
                .forEach(i -> translations.put(i.locale().getLocale() + "." + className + "." + finalMethodName, i.label()));
    }

    private void loadAndScanTranslationFolder(boolean debug) {
        try {
            URL url = this.getClass().getClassLoader().getResource("translations");

            if (url == null) {
                return;
            }

            Set<String> resources = typeScanner.getTranslationsResources(Pattern.compile(".*"));

            if (resources != null) {
                resources.forEach(i -> {
                    try {
                        InputStream is = ClasspathHelper.contextClassLoader().getResourceAsStream(i);
                        if (is != null) {
                            ConcurrentMap<String, String> prop = getProp(i, is)
                                    .collect(Collectors.toConcurrentMap(Pair::getLeft, Pair::getRight));
                            translations.putAll(prop);
                        } else {
                            throw new IllegalStateException("Resource not found! " + i);
                        }
                        is.close();

                    } catch (Exception x) {
                        throw new RuntimeException(x);
                    }
                });
            }
        } catch (Exception x) {
            throw new RuntimeException(x);
        }
    }

    public String translate(String locale,String key) {
        return translations.get(locale + "." + key);
    }

    private class TranslationQueueReader implements Runnable {
        private final Logger LOGGER = LoggerFactory.getLogger(TranslationQueueReader.class);

        private WatchService myWatcher;

        public TranslationQueueReader(WatchService myWatcher) {
            this.myWatcher = myWatcher;
        }

        @Override
        public void run() {

            try {
                WatchKey key = myWatcher.take();
                while (key != null) {
                    key.pollEvents().stream()
                            .filter(event -> event.context() instanceof Path)
                            .flatMap(item -> {
                                try {
                                    String path = ((Path) item.context()).toFile().getName();
                                    FileInputStream fis = new FileInputStream(getPathObject(path).toFile());
                                    Stream<Pair<String, String>> prop = getProp(path, fis);
                                    fis.close();
                                    return prop;
                                } catch (Exception e) {
                                    throw new RuntimeException(e);
                                }
                            })
                            .forEach(item -> translations.put(item.getLeft(), item.getRight()));
                    key.reset();
                    key = myWatcher.take();
                }
            } catch (InterruptedException e) {
                LOGGER.info("An error occurred on watching", e);
            }
            LOGGER.info("Stopping thread");
        }

        private Path getPathObject(String path) {
            ClassLoader classLoader = this.getClass().getClassLoader();
            try {
                return Paths.get(classLoader.getResource("translations/" + path).toURI());
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public Map<String, String> getTranslations() {
        return translations;
    }


}
