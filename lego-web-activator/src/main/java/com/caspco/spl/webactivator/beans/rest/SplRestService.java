package com.caspco.spl.webactivator.beans.rest;

/*
 * @author : Pooya Hosseini
 * Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 * Date: 12/8/15
 * Time: 11:02 AM
 */

import com.caspco.spl.model.annotations.cg.Translation;
import com.caspco.spl.model.wrench.Locale;
import com.caspco.spl.wrench.annotations.permission.Permission;
import com.caspco.spl.wrench.services.SplService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

public abstract class SplRestService<D, R extends SplService> extends SplReadOnlyRestService<D, R> {

    private int pageSize = 10;

    public SplRestService() {
        super();
    }

    //creation services
    @Permission(name = "Save" , translations = {
            @Translation(label = "ذخیره" , locale = Locale.Persian_Iran) ,
            @Translation(label = "Save" , locale = Locale.English_United_States)})
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public D add(@Valid @RequestBody D dto) {
        return (D) transformer.convert(service.save(transformer.convert(dto, service.getModelType())), dType);
    }



    // updating services
    @RequestMapping(method = RequestMethod.PUT)
    @Permission(name = "Update" , translations = {
            @Translation(label = "ویرایش" , locale = Locale.Persian_Iran),
            @Translation(label = "Update" , locale = Locale.English_United_States)
    })
    public D update(@RequestParam("id") Long id, @Valid @RequestBody D dto) {

        if (service.exists(id)) {
            Object entity = service.save(transformer.convert(dto, service.getModelType()));
            return transformer.convert(entity, dType);
        } else {
            throw new EntityNotFoundException();
        }
    }

    // deleting services
    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Permission(name = "Delete" , translations = {
            @Translation(label = "حذف" , locale = Locale.Persian_Iran),
            @Translation(label = "Delete" , locale = Locale.English_United_States)
    })
    public void deleteById(@RequestParam("id") Long id) {
        service.removeById(id);
    }

    @RequestMapping(value = "/changeActiveStatus", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @Permission(name = "Update" , translations = {
            @Translation(label = "Update" , locale = Locale.English_United_States),
            @Translation(label = "ویرایش" , locale = Locale.Persian_Iran),
    })
    public D changeActiveStatus(@RequestBody D dto) {
        return (D) transformer.convert(this.service.changeActiveStatus(transformer.convert(dto, service.getModelType())), dType);
    }
}