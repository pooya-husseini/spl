package com.caspco.spl.webactivator.report;

import com.caspco.spl.model.annotations.cg.Report;
import com.caspco.spl.model.wrench.Locale;
import com.caspco.spl.wrench.utils.reflection.TypeUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 20/09/14
 *         Time: 12:58
 */
public abstract class ReportEngine {


//    protected TypeUtils typeUtils;
    @Value("${server.reports.basepath:NONE}")
    private String reportUrl;

//    public ReportUtil(URL reportUrl) {
//        this.reportUrl = reportUrl;
//    }

    public ClassPathResource getReportClassPath() {
//        String path = reportUrl.getPath();
//        String path = reportUrl;
//        if (!path.endsWith(File.separator)) {
//            path += File.separator;
//        }
        return new ClassPathResource(reportUrl);
    }

    protected String getReportName(Class<?> clazz) {
        final Report annotation = clazz.getAnnotation(Report.class);

        if (annotation != null) {
            return (String) TypeUtils.INSTANCE.getAnnotationValue(annotation, "name");
        } else {
            return clazz.getSimpleName().replace("Entity", "");
        }
    }

    public abstract void sendReport(List<?> items, String reportId, Locale locale, String reportType,
                                    HttpServletResponse response);

}