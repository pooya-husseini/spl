package com.caspco.spl.webactivator.json;

import com.caspco.spl.webactivator.beans.conf.SplWebConfigs;
import com.caspco.spl.wrench.beans.transformer.CustomTypeResolverBuilder;
import com.caspco.spl.wrench.beans.transformer.SplTypeIdResolver;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 1/22/17
 * Time: 4:58 PM
 */

@Component
public class SplObjectMapper extends ObjectMapper {
    private final static String KEY= "spring.jackson.date-format";
    @Autowired
    private CustomTypeResolverBuilder customTypeResolverBuilder;

    @Autowired
    private SplWebConfigs splWebConfigs;

    @Autowired
    private SplTypeIdResolver resolver;

    @Autowired
    private Environment env;

    public SplObjectMapper() {
        SimpleModule module = new SimpleModule("SecuredValue", new Version(1, 0, 0, null));
        module.addSerializer(String.class, new SecuredValueSerializer());
        registerModule(module);
    }

    @PostConstruct
    public void init() {

        this.enableDefaultTyping();
        customTypeResolverBuilder.init(JsonTypeInfo.Id.NAME, resolver);
        customTypeResolverBuilder.inclusion(JsonTypeInfo.As.PROPERTY);
        customTypeResolverBuilder.typeProperty("@type");
        this.setDefaultTyping(customTypeResolverBuilder);
        if (splWebConfigs.getJacksonFailUnknownProps() != null) {
            configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, splWebConfigs.getJacksonFailUnknownProps());
        }
        if(env.containsProperty(KEY)) {
            setDateFormat(new SimpleDateFormat(env.getProperty(KEY)));
        }
    }
}