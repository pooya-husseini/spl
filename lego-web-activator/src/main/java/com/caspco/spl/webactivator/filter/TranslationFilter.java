package com.caspco.spl.webactivator.filter;

import com.caspco.spl.webactivator.services.TranslationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/*
 * Created by pooya on 9/21/16.
 */

@Component
public class TranslationFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(TranslationFilter.class);

    //    private static Reflections reflections;
    @Autowired
    private TranslationService translationService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (servletRequest instanceof HttpServletRequest) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
            String requestURI = httpServletRequest.getRequestURI().replace("/translations/", "");
            String key = requestURI.substring(0, requestURI.indexOf("/"));
            String locale = requestURI.substring(requestURI.indexOf("/") + 1);

            String s = translationService.translate(locale, key);
            if (s == null) {
                LOGGER.warn("Translation of " + key + " not found");
            } else {
                servletResponse.getWriter().write(s);
            }
        }
    }

    @Override
    public void destroy() {

    }
}