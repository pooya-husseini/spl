package com.caspco.spl.webactivator.spring.configuration;

import com.caspco.spl.webactivator.filter.TranslationFilter;
import com.caspco.spl.webactivator.report.jasper.JasperReportRenderer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 11/16/15
 *         Time: 6:46 PM
 */

@Configuration
@ComponentScan(basePackages = {"com.caspco.spl.webactivator"})
//@EntityScan(basePackages = {"com.caspco.spl.wrench"})
@PropertySource("classpath:version.properties")
public class WebActivatorConfiguration {

    @Bean
    public FilterRegistrationBean translationFilterRegistration(TranslationFilter translationFilter) throws IOException {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(translationFilter);
        registration.addUrlPatterns("/translations/*");
        registration.setName("translationFilter");
        registration.setOrder(1);
        return registration;
    }

    @Bean
    public JasperReportRenderer reportRenderer() {
        return new JasperReportRenderer();
    }
}