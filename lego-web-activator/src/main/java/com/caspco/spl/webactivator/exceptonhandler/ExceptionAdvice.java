package com.caspco.spl.webactivator.exceptonhandler;


import com.caspco.spl.model.wrench.exception.SplBaseException;
import com.caspco.spl.wrench.exceptionhandler.ExceptionPersister;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 12/16/15
 *         Time: 3:48 PM
 */

@ControllerAdvice
public class ExceptionAdvice {

    @Autowired
    private ExceptionScanner exceptionScanner;
    @Autowired(required = false)
    private ExceptionPersister exceptionPersister;

//    @Autowired(required = false)
//    private ExceptionTranslator exceptionTranslator;

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "An error occurred on server!")
    @ExceptionHandler(Exception.class)
    public void logError(Exception e, HttpServletResponse response) throws SplBaseException {
        try {
            SplBaseException translateException = exceptionScanner.resolveException(e);
//            response.addHeader("exception_type", translateException.);
            response.setHeader("exceptionType", translateException.getExceptionType());
            String s;
            if(translateException.getCause()!=null) {
                s = ExceptionUtils.getStackTrace(translateException.getCause());
            }else{
                s = ExceptionUtils.getStackTrace(translateException);
            }
            response.setHeader("exceptionStackTrace", s.length() > 1000 ? s.substring(0, 1000) : s);

            response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value());
            if (exceptionPersister != null) {
                exceptionPersister.persist(translateException);
            }
            throw translateException;
        } catch (IOException e1) {
            throw exceptionScanner.resolveException(e1);
        }
    }
//    private SplBaseException translateException(Exception x) {
//        if (exceptionTranslator != null) {
//            return exceptionTranslator.translateException(x);
//        }
//        return new SplBaseException(x);
//    }
}