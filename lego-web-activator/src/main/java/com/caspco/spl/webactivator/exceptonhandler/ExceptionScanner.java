package com.caspco.spl.webactivator.exceptonhandler;

import com.caspco.spl.model.annotations.rcg.SplException;
import com.caspco.spl.model.wrench.exception.SplBaseException;
import com.caspco.spl.wrench.beans.scanner.TypeScanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/27/17
 *         Time: 1:08 PM
 */

@Component
public class ExceptionScanner {

    @Autowired
    private TypeScanner typeScanner;

    private Map<Class<? extends Exception>, Class<? extends SplBaseException>> resolverMap = new ConcurrentHashMap<>();

    @PostConstruct
    public void init() {
        Set<Class<?>> classSet = typeScanner.getTypesAnnotatedWith(SplException.class);
        if (classSet != null) {
            classSet.forEach(i -> {
                if (SplBaseException.class.isAssignableFrom(i)) {
                    Class<? extends SplBaseException> item = (Class<? extends SplBaseException>) i;
                    SplException annotation = i.getAnnotation(SplException.class);
                    if (annotation.resolveFor() != Exception.class) {
                        resolverMap.put(annotation.resolveFor(), item);
                    } else {
                        resolverMap.put(item, DummyException.class);
                    }
                } else {
                    throw new IllegalStateException("Exception class " + i.getName() + " should be extend from SplBaseException");
                }
            });
        }
    }

    public SplBaseException resolveException(Exception e) throws SplBaseException{
        try {
            Class<? extends SplBaseException> clazz = this.resolverMap.get(e.getClass());
            if (clazz == null) {
                return new SplBaseException(e.getMessage(), e.getCause());
            } else {
                if (clazz == DummyException.class) {
                    return (SplBaseException) e;
                } else {
                    SplBaseException splBaseException = clazz.newInstance();
                    return splBaseException.resolve(e);
                }
            }
        } catch (Exception x){
            throw new SplBaseException(x);
        }
    }

    @SplException(translations = {

    })
    private static class DummyException extends SplBaseException {
    }
}