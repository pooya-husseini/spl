package com.caspco.spl.webactivator.beans.rest;

import com.caspco.spl.model.annotations.cg.DisabledRestMethod;
import com.caspco.spl.model.web.RestMethod;
import com.caspco.spl.model.web.RestOption;
import com.caspco.spl.wrench.annotations.permission.Permission;
import com.caspco.spl.wrench.beans.rest.PermissionMethodModel;
import com.caspco.spl.wrench.beans.rest.RestPermission;
import com.caspco.spl.wrench.interfaces.HasPermission;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
 * Created by mokaram on 10/02/2016.
 */
public abstract class RestOptionsResolver {
    @Autowired(required = false)
    private HasPermission hasPermission;

    protected Map<String, String> extraParameters() {
        return Collections.emptyMap();
    }

    public RestPermission getPermissions() {
        final RestPermission restPermission = new RestPermission();
        final String clazzSimpleName = this.getClass().getSimpleName().replace("RestService", "");
        restPermission.setClazzName(this.getClass().getSimpleName(),clazzSimpleName);
        restPermission.setPermissionMethods(new ArrayList<>());

        List<PermissionMethodModel> models = Arrays.stream(this.getClass().getMethods())
                .filter(i -> i.isAnnotationPresent(Permission.class))
                .map(i -> {
                    Permission permission = i.getAnnotation(Permission.class);
                    String permissionName;
                    String permissionLabel ;
                    if (permission.autoResolve()) {
                        permissionName = clazzSimpleName + "." + permission.name();
                        permissionLabel =  permission.name();
                    } else {
                        permissionName = permission.name();
                        permissionLabel = permissionName;
                    }

                    return new PermissionMethodModel(permissionName, i.getName(),permissionLabel);
                })
                .collect(Collectors.toList());

        restPermission.setPermissionMethods(models);
        return restPermission;
    }



    @RequestMapping(value = "/options", method = RequestMethod.POST)
    public RestOption getOptions() {

        Method[] methods = this.getClass().getMethods();

        List<RestMethod> restMethodList = Arrays.stream(methods)
                .filter(method -> method.isAnnotationPresent(RequestMapping.class) && !method.isAnnotationPresent(DisabledRestMethod.class))
                .filter(method -> {

                    if (method.isAnnotationPresent(Permission.class) && hasPermission != null) {
                        Permission permission = method.getAnnotation(Permission.class);
                        return hasPermission.hasAuthority(this.getClass(), permission);
                    } else {
                        return true;
                    }
                })
                .map(method -> {
                    RequestMapping annotation = method.getAnnotation(RequestMapping.class);
                    String[] urls = annotation.value();
                    String url = "";

                    List<String> restMethods = Arrays.stream(annotation.method()).map(Enum::name).collect(Collectors.toList());
                    if (restMethods == null || restMethods.isEmpty()) {
                        restMethods = new ArrayList<>();
                        restMethods.add(RequestMethod.GET.name());
                    }
                    if (urls.length == 0) {
                        urls = annotation.path();
                    }

                    if (urls.length > 0) {
                        url = urls[0];
                    }

                    Map<String, String> restParams = makingRestParams(url, method);
                    boolean array = method.getReturnType().isArray() || Collection.class.isAssignableFrom(method.getReturnType());
                    return new RestMethod(method.getName(), restMethods, restParams, url, array);
                }).collect(Collectors.toList());
        return new RestOption(restMethodList, extraParameters());
    }

    private Map<String, String> makingRestParams(String url, Method method) {
        Stream<Pair<String, String>> pairStream = Arrays.stream(method.getParameters())
                .filter(param -> param.isAnnotationPresent(RequestParam.class))
                .map(i -> i.getAnnotation(RequestParam.class))
                .map(annotation -> {
                    if (!annotation.name().isEmpty()) {
                        return Pair.of(annotation.name(), "@" + annotation.name());
                    } else {
                        return Pair.of(annotation.value(), "@" + annotation.value());
                    }
                });
        if (!url.isEmpty()) {
            pairStream = Stream.concat(pairStream, Stream.of(Pair.of("url", url.startsWith("/") ? url.substring(1) : url)));
        }
        return pairStream.collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
    }
}