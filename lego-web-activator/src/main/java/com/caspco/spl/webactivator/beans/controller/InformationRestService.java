package com.caspco.spl.webactivator.beans.controller;

import com.caspco.spl.model.web.dto.InformationDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mokaram on 11/22/2016.
 */

@RestController
@RequestMapping("/inform")
public class InformationRestService{

    @Value("${version:1.0}")
    public String version ;

    @RequestMapping()
    public InformationDto getInformation(){
        return new InformationDto(version);
    }
}
