package com.caspco.spl.webactivator.beans.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 10/14/17
 * Time: 3:01 PM
 */

@ConfigurationProperties(prefix = "spl.web")
@Component
public class SplWebConfigs {
    private Boolean jacksonFailUnknownProps;
    private Integer timeout;
    private String keystoreSecret;

    public Boolean getJacksonFailUnknownProps() {
        return jacksonFailUnknownProps;
    }

    public void setJacksonFailUnknownProps(Boolean jacksonFailUnknownProps) {
        this.jacksonFailUnknownProps = jacksonFailUnknownProps;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public String getKeystoreSecret() {
        return keystoreSecret;
    }

    public void setKeystoreSecret(String keystoreSecret) {
        this.keystoreSecret = keystoreSecret;
    }
}
