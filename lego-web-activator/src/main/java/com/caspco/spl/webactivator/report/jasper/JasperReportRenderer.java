package com.caspco.spl.webactivator.report.jasper;

import com.caspco.spl.model.wrench.Locale;
import com.caspco.spl.webactivator.report.ReportEngine;
import com.caspco.spl.wrench.beans.proxy.EntityProxy;
import com.caspco.spl.wrench.utils.report.ReportUtils;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXmlExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRProperties;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 03/12/14
 *         Time: 16:00
 */

@CacheConfig(cacheNames = "reportUtil")
public class JasperReportRenderer extends ReportEngine {

    private static final Logger LOGGER = Logger.getLogger(JasperReportRenderer.class);
    @Autowired(required = false)
    private EntityProxy translator;
    @Autowired
    private ReportUtils reportUtil;

//    @Autowired
//    private Cache cache;


//    private URL reportBasePath;
//    private Map<String, JasperReport> cache = new HashMap<>();

    //    @Autowired
//    public JasperReportUtil( URL reportBasePath) {
//        super(reportBasePath);
//    }
//    @Cacheable
//    public File[] iterateReportFiles() throws IOException {
//        File file = getReportClassPath().getFile();
//        if (file.isDirectory()) {
//            File[] innerFiles = file.listFiles(new FileFilter() {
//                @Override
//                public boolean accept(File pathname) {
//                    return pathname.getPath().endsWith(".jrxml");
//                }
//            });
//
//            Assert.notNull(innerFiles);
//
//            return innerFiles;
//        }
//        return new File[0];
//    }


    public void sendReport(List<?> items,
                           String reportId,
                           Locale locale,
                           String reportType,
                           HttpServletResponse response) {

        try {
            ;
            LOGGER.info("Going to enhance objects report");
            Stream<Object> objectStream = items.stream().map(elem -> {
                if (translator != null) {
                    return translator.translate(elem, locale);
                }
                return elem;
            });
            LOGGER.info("Object enhancement already succeed");
            String reportName;
            if (reportId.contains(".")) {
                reportName = getReportName(Class.forName(reportId));
            } else {
                reportName = reportId;
            }
            JRProperties.setProperty("net.sf.jasperreports.default.pdf.font.name", "fonts/xb-zar.ttf");
            JRProperties.setProperty("net.sf.jasperreports.default.pdf.encoding", "Identity-H");
//        JRProperties.setProperty("net.sf.jasperreports.export.character.encoding", "Identity-H");
            JRProperties.setProperty("net.sf.jasperreports.default.pdf.embedded", true);

            JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(objectStream.collect(Collectors.toList()));
            String fileName = String.format("%s%s.jrxml", getReportClassPath().getURL().getFile(), reportName);
            JasperReport jasperReport = reportUtil.compiledReport(fileName);
            HashMap<String, Object> parameters = new HashMap<>();

            parameters.put(JRParameter.REPORT_LOCALE, locale.getJavaLocale());
            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, collectionDataSource);
            response.reset();
            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "attachment; filename=\"" + reportName + "\"");
            JRExporter exporter;

            switch (reportType.toUpperCase()) {
                case "CSV":
                    exporter = new JRCsvExporter();
                    break;
                case "DOCX":
                    exporter = new JRDocxExporter();
                    break;
                case "XLSX":
                    exporter = new JRXlsxExporter();
                    break;
                case "HTML":
                    exporter = new JRHtmlExporter();
                    break;
                case "XML":
                    exporter = new JRXmlExporter();
                    break;
                case "PDF":
                    exporter = new JRPdfExporter();
                    break;
                default:
                    throw new IllegalArgumentException(String.format("Report extension %s is not supported!", reportType));
            }
//        exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,"/home/pooya/report2.pdf");
//        File f = File.createTempFile("HHHH", new Date().toString());

            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
//        exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "Identity-H");
//        exporter.setParameter(JRExporterParameter.OUTPUT_FILE,f);
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
//        InputStream is = new FileInputStream(f);
            LOGGER.info("Going to export the report");
            exporter.exportReport();
            LOGGER.info("Report already exported");
        } catch (JRException | IOException | ClassNotFoundException e) {
            LOGGER.error("Exception occurred on rendering report", e);
            throw new RuntimeException(e);
        }
    }
}