//package com.caspco.spl.wrench.beans.rest;
//
///**
// * @author : Pooya Hosseini
// * Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
// * Date: 12/8/15
// * Time: 11:02 AM
// */
//
//import com.caspco.spl.wrench.beans.service.SimpleService;
//import org.springframework.http.HttpStatus;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.*;
//
//import javax.persistence.EntityNotFoundException;
//import javax.validation.Valid;
//
//@Transactional
//public abstract class AutoResolveRestService<D, R extends SimpleService> extends AutoResolveReadOnlyRestService<D,  R> {
//
//    private int pageSize = 10;
//
//
//    public AutoResolveRestService(Class<D> dType, Class<E> eType, R service) {
//        super(dType, eType, service);
//    }
//
//    //creation services
//    @RequestMapping(value = "/create", method = RequestMethod.POST)
//    @ResponseStatus(HttpStatus.CREATED)
//    public D add(@Valid @RequestBody D dto) {
//        return getETransformer().convert((E) service.save(getDTransformer().convert(dto)));
//    }
//
//    // updating services
//    @RequestMapping(method = RequestMethod.PUT)
//    public D update(@RequestParam("id") Long id, @Valid @RequestBody D dto) {
//
//        if (service.exists(id)) {
//            E entity = (E) service.save(getDTransformer().convert(dto));
//            return getETransformer().convert(entity);
//        } else {
//            throw new EntityNotFoundException();
//        }
//    }
//
//    // deleting services
//    @RequestMapping(method = RequestMethod.DELETE)
//    @ResponseStatus(HttpStatus.OK)
//    public void deleteById(@RequestParam("id") Long id) {
//        service.removeById(id);
//    }
//
//
//}