package com.caspco.spl.webactivator.json;


import com.caspco.spl.model.annotations.data.GeneralParameterFormatter;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;

import java.io.IOException;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 16/09/14
 *         Time: 18:44
 */
public class GeneralParameterDeserializer extends JsonDeserializer<String> implements ContextualDeserializer {
    private final String mainType;


    public GeneralParameterDeserializer() {
        mainType = null;
    }

    public GeneralParameterDeserializer(String mainType) {
        this.mainType = mainType;
    }

    @Override
    public JsonDeserializer<?> createContextual(DeserializationContext ctxt, BeanProperty property) throws JsonMappingException {
                GeneralParameterFormatter ann = property.getAnnotation(GeneralParameterFormatter.class);
        if (ann == null) {
            ann = property.getContextAnnotation(GeneralParameterFormatter.class);
        }
        if(ann==null) {
            return new StringDeserializer();
        }
        return new GeneralParameterDeserializer(ann.type());
    }

    @Override
    public String deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        return jp.getValueAsString();

    }

//    private String mainType;
//
//    @Override
//    public void serialize(String data, JsonGenerator gen, SerializerProvider provider) throws IOException {
//        gen.writeString(mainType+data);
//    }
//
//    public GeneralParameterDeserializer(String mainType) {
//        this.mainType = mainType;
//    }
//
//    public GeneralParameterDeserializer() {
//        mainType = null;
//    }
//
//    @Override
//    public Class<String> handledType() {
//        return String.class;
//    }
//
//
//
//    @Override
//    public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) throws JsonMappingException {

//    }
}