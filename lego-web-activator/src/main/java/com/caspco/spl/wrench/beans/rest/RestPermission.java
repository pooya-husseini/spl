package com.caspco.spl.wrench.beans.rest;

import java.util.List;

/**
 * Created by saeedko on 5/31/17.
 */
public class RestPermission {
    private String clazzName;
    private String clazzSimpleName;
    private List<PermissionMethodModel> permissionMethods;

    public String getClazzName() {
        return clazzName;
    }

    public void setClazzName(String clazzName,String clazzSimpleName ) {
        this.clazzName = clazzName;
        this.clazzSimpleName = clazzSimpleName;
    }

    public List<PermissionMethodModel> getPermissionMethods() {
        return permissionMethods;
    }

    public void setPermissionMethods(List<PermissionMethodModel> permissionMethods) {
        this.permissionMethods = permissionMethods;
    }

    public String getClazzSimpleName() {
        return clazzSimpleName;
    }

    public void setClazzSimpleName(String clazzSimpleName) {
        this.clazzSimpleName = clazzSimpleName;
    }
}