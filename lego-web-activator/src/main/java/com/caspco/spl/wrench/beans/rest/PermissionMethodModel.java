package com.caspco.spl.wrench.beans.rest;

/**
 * Created by saeedko on 5/31/17.
 */
public class PermissionMethodModel {
    private String permissionName;
    private String methodName;
    private String permissionLabel;

    public PermissionMethodModel(String permissionName, String methodName, String permissionLabel) {
        this.permissionName = permissionName;
        this.methodName = methodName;
        this.permissionLabel = permissionLabel;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getPermissionLabel() {
        return permissionLabel;
    }

    public void setPermissionLabel(String permissionLabel) {
        this.permissionLabel = permissionLabel;
    }
}
