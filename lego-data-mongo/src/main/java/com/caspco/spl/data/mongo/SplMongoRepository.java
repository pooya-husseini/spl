package com.caspco.spl.data.mongo;

import com.caspco.spl.wrench.repository.SplRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/10/17
 *         Time: 5:44 PM
 */

@NoRepositoryBean
public interface SplMongoRepository<T, ID extends Serializable> extends QueryDslPredicateExecutor<T>, MongoRepository<T, ID>, SplRepository<T, ID> {

}
