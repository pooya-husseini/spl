package com.caspco.spl.data.mongo.repository;

import com.caspco.spl.data.BaseQueryDslDataAccess;
import com.caspco.spl.data.SplQueryDslConfig;
import com.caspco.spl.data.mongo.SplMongoRepository;
import com.caspco.spl.model.wrench.exception.NotImplementedException;
import com.caspco.spl.wrench.utils.reflection.ClassUtils;
import com.caspco.spl.wrench.utils.reflection.PropertyMetadata;
import com.google.common.collect.Lists;
import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.dsl.PathBuilder;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.repository.query.MongoEntityInformation;
import org.springframework.data.mongodb.repository.support.QueryDslMongoRepository;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.querydsl.SimpleEntityPathResolver;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/10/17
 *         Time: 5:51 PM
 */

@NoRepositoryBean
public class SplMongoRepositoryImpl<T, ID extends Serializable> extends QueryDslMongoRepository<T, ID> implements SplMongoRepository<T, ID>, BaseQueryDslDataAccess<T, ID> {

    private final Class<T> typeArgument;
    private final ClassUtils classUtils;
    private final EntityPath<T> path;
    private final PathBuilder<T> builder;
//    private final TypeUtils typeUtils;

    public SplMongoRepositoryImpl(MongoEntityInformation<T, ID> entityInformation, MongoOperations mongoOperations) {
        this(entityInformation, mongoOperations, SimpleEntityPathResolver.INSTANCE);
    }

    public SplMongoRepositoryImpl(MongoEntityInformation<T, ID> entityInformation, MongoOperations mongoOperations, EntityPathResolver resolver) {
        super(entityInformation, mongoOperations, resolver);
        this.typeArgument = entityInformation.getJavaType();
//        this.typeUtils = typeUtils;
        this.classUtils = ClassUtils.create(typeArgument);
//        activable=classUtils.existsProperties(i -> i.isAnnotatedWith(EnableFilter.class));
        List<PropertyMetadata> metadataList = classUtils.filterProperties(i -> i.isAnnotatedWith(Id.class));
        if (metadataList.isEmpty()) {
            throw new IllegalArgumentException("Entity " + this.typeArgument + " should contain an id field");
        }
//        this.idField = metadataList.get(0);
//        this.builder = new PathBuilder<>(path.getType(), path.getMetadata());
        this.path = resolver.createPath(typeArgument);
        this.builder = new PathBuilder<>(path.getType(), path.getMetadata());

    }

    @Override
    public <S extends T> List<S> save(Iterable<S> entities) {
        if (entities != null) {
            return Lists.newArrayList(entities).stream().map(i -> this.save(i)).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    @Override
    public Class<T> getTypeArgument() {
        return typeArgument;
    }

    @Override
    public PathBuilder<T> getBuilder() {
        return builder;
    }

    @Override
    public SplQueryDslConfig getConfig() {
        return new SplQueryDslConfig(false);
    }

    @Override
    public void deleteAllInBatch() {
        throw new NotImplementedException();
    }

}
