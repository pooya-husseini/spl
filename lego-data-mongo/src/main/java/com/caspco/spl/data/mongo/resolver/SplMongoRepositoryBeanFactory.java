package com.caspco.spl.data.mongo.resolver;

import com.caspco.spl.data.mongo.SplMongoRepository;
import com.caspco.spl.data.mongo.repository.SplMongoRepositoryImpl;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactory;
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactoryBean;
import org.springframework.data.mongodb.repository.support.QueryDslMongoRepository;
import org.springframework.data.repository.core.RepositoryInformation;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

import java.io.Serializable;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/10/17
 *         Time: 5:39 PM
 */
public class SplMongoRepositoryBeanFactory<T extends QueryDslMongoRepository<S, ID>, S, ID extends Serializable>  extends MongoRepositoryFactoryBean<T,S,ID>{
    /**
     * Creates a new {@link MongoRepositoryFactoryBean} for the given repository interface.
     *
     * @param repositoryInterface must not be {@literal null}.
     */
    public SplMongoRepositoryBeanFactory(Class<? extends T> repositoryInterface) {
        super(repositoryInterface);
    }

    protected RepositoryFactorySupport getFactoryInstance(MongoOperations operations){
        return new MyFactory(operations);
    }

    public static class MyFactory extends MongoRepositoryFactory{

        private final MongoOperations mongoOperations;

        /**
         * Creates a new {@link MongoRepositoryFactory} with the given {@link MongoOperations}.
         *
         * @param mongoOperations must not be {@literal null}.
         */
        public MyFactory(MongoOperations mongoOperations) {
            super(mongoOperations);
            this.mongoOperations = mongoOperations;
        }

        @Override
        protected Object getTargetRepository(RepositoryInformation information) {
            return new SplMongoRepositoryImpl(this.getEntityInformation(information.getDomainType()),mongoOperations);
        }

        @Override
        protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
            return SplMongoRepository.class;
        }
    }
}
