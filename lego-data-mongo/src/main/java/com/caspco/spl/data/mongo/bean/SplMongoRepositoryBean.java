//package com.caspco.spl.data.mongo.bean;
//
//import com.caspco.spl.wrench.repository.SplRepository;
//import org.springframework.data.mongodb.core.MongoOperations;
//import org.springframework.data.mongodb.repository.query.MongoEntityInformation;
//import org.springframework.data.mongodb.repository.support.QueryDslMongoRepository;
//import org.springframework.data.querydsl.EntityPathResolver;
//import org.springframework.data.repository.NoRepositoryBean;
//
//import java.io.Serializable;
//import java.util.List;
//
///**
// * @author : Pooya. h
// *         Email : husseini@caspco.ir
// *         Date: 6/10/17
// *         Time: 5:31 PM
// */
//
//@NoRepositoryBean
//public class SplMongoRepositoryBean<T, ID extends Serializable> extends QueryDslMongoRepository<T, ID> implements SplRepository<T,ID> {
//
//    public SplMongoRepositoryBean(MongoEntityInformation<T, ID> entityInformation, MongoOperations mongoOperations) {
//        super(entityInformation, mongoOperations);
//    }
//
//    public SplMongoRepositoryBean(MongoEntityInformation<T, ID> entityInformation, MongoOperations mongoOperations, EntityPathResolver resolver) {
//        super(entityInformation, mongoOperations, resolver);
//    }
//
//
//    @Override
//    public Class getTypeArgument() {
//        throw new IllegalStateException();
//    }
//
//    @Override
//    public long count(T t) {
//        throw new IllegalStateException();
//    }
//
//    @Override
//    public List<T> find(T e) {
//        throw new IllegalStateException();
//    }
//
//    @Override
//    public boolean exists(T e) {
//        throw new IllegalStateException();
//    }
//
//    @Override
//    public void deleteAllInBatch() {
//        throw new IllegalStateException();
//    }
//
//    @Override
//    public List<String> searchColumn(String column, String value) {
//        throw new IllegalStateException();
//    }
//
//    @Override
//    public <S extends T> S changeActiveStatus(S entity) {
//        throw new IllegalStateException();
//    }
//
//    @Override
//    public boolean isActivable() {
//        throw new IllegalStateException();
//    }
//
//    @Override
//    public <S extends T> Boolean getActiveStatus(S e) {
//        throw new IllegalStateException();
//    }
//
//    @Override
//    public List<T> findActives(T e) {
//        throw new IllegalStateException();
//    }
//}
