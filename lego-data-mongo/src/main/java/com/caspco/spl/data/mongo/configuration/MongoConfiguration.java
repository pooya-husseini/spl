package com.caspco.spl.data.mongo.configuration;

import com.caspco.spl.data.mongo.SplMongoRepository;
import com.caspco.spl.data.mongo.resolver.SplMongoRepositoryBeanFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/10/17
 *         Time: 5:24 PM
 */

@Configuration
@EnableMongoRepositories(
        basePackages = "${spl.common.scan-package}",
        repositoryBaseClass = SplMongoRepository.class,
        repositoryFactoryBeanClass = SplMongoRepositoryBeanFactory.class
)
public class MongoConfiguration {

}
