package com.caspco.spl.data.service;

/*
  @author : Пуя Гуссейни
 * Email : info@pooya-hfp.ir
 * Date: 23/06/14
 * Time: 17:37
 */

import com.caspco.spl.model.annotations.web.RequestMethod;
import com.caspco.spl.model.annotations.web.RestParameter;
import com.caspco.spl.model.annotations.web.RestResource;
import com.caspco.spl.wrench.beans.scanner.TypeScanner;
import com.caspco.spl.wrench.locale.LocaleService;
import com.caspco.spl.wrench.repository.SplRepository;
import com.caspco.spl.wrench.services.SplAsyncService;
import com.caspco.spl.wrench.services.SplService;
import com.caspco.spl.wrench.spring.configuration.SplCommonProperties;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.GenericTypeResolver;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.io.Serializable;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Transactional(propagation = Propagation.REQUIRES_NEW)
public abstract class AbstractSplDataService<E, K extends Serializable, R extends SplRepository<E, K>>
        implements SplService<E, K>, InitializingBean, SplAsyncService<E, K> {

    private Class<E> modelType = (Class<E>) GenericTypeResolver.resolveTypeArguments(this.getClass(), AbstractSplDataService.class)[0];

    @Autowired
    protected R repository;

    @Autowired
    private SplCommonProperties splProperties;

    private Map<Class<?>, SplRepository> repositoryMap;

    @Autowired
    private LocaleService localeService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private TypeScanner typeScanner;

    @Override
    public void afterPropertiesSet() throws Exception {
        this.repositoryMap = new HashMap<>();
        if (Modifier.isAbstract(modelType.getModifiers())) {


            Set<Class<? extends E>> subTypes = typeScanner.getSubTypesOf(modelType);

            Map<String, SplRepository> beansOfType = applicationContext.getBeansOfType(SplRepository.class);

            if (beansOfType != null) {
                for (Map.Entry<String, SplRepository> entry : beansOfType.entrySet()) {
                    Class typeArgument = entry.getValue().getTypeArgument();
                    if (subTypes.contains(typeArgument)) {
                        this.repositoryMap.put(typeArgument, entry.getValue());
                    }
                }
            }
        }
    }


    public Boolean isEmpty() {
        return repository.count() == 0;
    }

    @RestResource(name = "add")
    public E save(E obj) {
        return repository.save(obj);
    }

    @RestResource(name = "update", method = RequestMethod.PUT)
    public E update(@RestParameter(name = "id") K id, E obj) {
        if (exists(id)) {
            return save(obj);
        } else {
            throw new EntityNotFoundException();
        }
    }

    @RestResource(name = "getById", method = RequestMethod.POST)
    public E findById(@RestParameter(name = "id") K id) {
        return repository.findOne(id);
    }

    @RestResource(name = "search", method = RequestMethod.POST)
    public List<E> findBySample(E e) {
        if (!this.repositoryMap.isEmpty()) {
            return this.repositoryMap.get(e.getClass()).find(e);
        }
        return repository.find(e);
    }

    @RestResource(name = "getAll", method = RequestMethod.GET)
    public List<E> findAll() {
        return repository.findAll();
    }

    public Boolean exists(K id) {
        return repository.exists(id);
    }

    public Boolean exists(E e) {
        if (!repositoryMap.isEmpty()) {
            return repositoryMap.get(e.getClass()).exists(e);
        }
        return repository.exists(e);
    }

    @RestResource(name = "getCount", method = RequestMethod.POST)
    public Long count(E e) {
        if (!repositoryMap.isEmpty()) {
            return repositoryMap.get(e.getClass()).count(e);
        }
        return repository.count(e);
    }

    public Long countAll() {
        return repository.count();
    }

    // delete operations
    @RestResource(name = "deleteById", method = RequestMethod.DELETE)
    public void removeById(K id) {
        repository.delete(id);
    }

    public void remove(E e) {
        repository.delete(e);
    }

    public void removeAll() {
        repository.deleteAllInBatch();
    }


    @RestResource(name = "searchColumn", method = RequestMethod.POST)
    public List<String> searchColumn(@RestParameter(name = "column") String column, @RestParameter(name = "value") String value) {
        return repository.searchColumn(column, value);
    }

    @RestResource(method = RequestMethod.DELETE)
    public E changeActiveStatus(E entity) {
        return repository.changeActiveStatus(entity);
    }

    /**
     * Getter for property 'modelType'.
     *
     * @return Value for property 'modelType'.
     */
    public Class<E> getModelType() {
        return modelType;
    }

    public Boolean isActivable() {
        return repository.isActivable();
    }

    public Boolean isActive(E e) {
        return repository.getActiveStatus(e);
    }

    public List<E> findActivesBySample(E e) {
        return repository.findActives(e);
    }
}