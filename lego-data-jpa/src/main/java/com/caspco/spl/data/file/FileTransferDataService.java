package com.caspco.spl.data.file;

import com.caspco.spl.data.service.AbstractSplDataService;
import org.springframework.stereotype.Service;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 3/11/17
 *         Time: 9:01 AM
 */

@Service
public class FileTransferDataService extends AbstractSplDataService<FileTransferEntity,Long,FileTransferRepository> {

}
