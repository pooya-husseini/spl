package com.caspco.spl.data.file;


import com.caspco.spl.model.annotations.cg.Ignore;
import com.caspco.spl.model.annotations.cg.IgnoreType;
import org.hibernate.envers.Audited;

import javax.persistence.*;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 3/8/17
 *         Time: 11:35 AM
 */
@Entity
@Table(name = "FileTable")
@Audited
@Ignore(mods = {IgnoreType.ALL})
public class FileTransferEntity {

    private Long id;
    private byte[] content;
    private String name;
    private String contentType;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Lob
    @Column(nullable = false)
    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    @Column(nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(nullable = false)
    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}