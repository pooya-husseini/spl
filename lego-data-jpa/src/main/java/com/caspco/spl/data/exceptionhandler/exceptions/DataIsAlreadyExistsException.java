package com.caspco.spl.data.exceptionhandler.exceptions;

import com.caspco.spl.model.annotations.cg.Translation;
import com.caspco.spl.model.annotations.rcg.SplException;
import com.caspco.spl.model.wrench.Locale;
import com.caspco.spl.model.wrench.exception.SplBaseException;
import org.springframework.dao.DataIntegrityViolationException;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/27/17
 *         Time: 12:53 PM
 */

@SplException(
        translations = {
                @Translation(locale = Locale.Persian_Iran, label = "مقدار وارد شده تکراری است"),

        },
        resolveFor = DataIntegrityViolationException.class
)
public class DataIsAlreadyExistsException extends SplBaseException {
}