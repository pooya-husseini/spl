package com.caspco.spl.data.swift;

import com.caspco.spl.data.BaseQueryDslDataAccess;
import com.caspco.spl.data.SplQueryDslConfig;
import com.caspco.spl.data.beans.SplRelationalRepository;
import com.caspco.spl.model.data.HistoricalModel;
import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.AbstractJPAQuery;
import com.querydsl.jpa.impl.JPAQuery;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditEntity;
import org.springframework.data.jpa.repository.support.CrudMethodMetadata;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.QueryDslJpaRepository;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.querydsl.SimpleEntityPathResolver;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author : Pooya Hosseini
 * Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 * Date: 11/7/15
 * Time: 2:04 PM
 */
@NoRepositoryBean
public class SplRepositoryImpl<T, ID extends Serializable> extends QueryDslJpaRepository<T, ID> implements SplRelationalRepository<T, ID>, BaseQueryDslDataAccess<T, ID> {

    private final EntityPath<T> path;
    private final PathBuilder<T> builder;
    private final EntityManager em;
    private final Querydsl querydsl;
    private final Class<T> typeArgument;
    private final EntityPathResolver resolver;

    public SplRepositoryImpl(JpaEntityInformation<T, ID> entityInformation, EntityManager entityManager) {
        this(entityInformation, entityManager, SimpleEntityPathResolver.INSTANCE);
    }

    public SplRepositoryImpl(JpaEntityInformation<T, ID> entityInformation, EntityManager entityManager,
                             EntityPathResolver resolver) {
        super(entityInformation, entityManager, resolver);
        this.typeArgument = entityInformation.getJavaType();
        this.path = resolver.createPath(typeArgument);
        this.resolver = resolver;
        this.builder = new PathBuilder<>(path.getType(), path.getMetadata());

        this.querydsl = new Querydsl(entityManager, builder);
        this.em = entityManager;
    }

    public AbstractJPAQuery<T, JPAQuery<T>> select(Predicate... predicate) {
        return new Querydsl(em, builder).createQuery();
    }

    @Transactional()
    public int delete(Predicate predicate) throws InterruptedException {
        List<T> all = findAll(predicate);
        for (T t : all) {
            super.delete(t);
        }
        return all.size();
    }

    public T getEntityAtVersion(Number revision) {
        return (T) AuditReaderFactory.get(em).createQuery().forEntitiesAtRevision(typeArgument, revision).add(AuditEntity.revisionNumber().eq(revision)).getSingleResult();
    }

    public Number getLastRevisionNumber() {
        return (Number) AuditReaderFactory.get(em).createQuery().forRevisionsOfEntity(typeArgument, true, true).addProjection(AuditEntity.revisionNumber().max()).getSingleResult();
    }

    @Override
    public Number getLastDeletedRevisionNumber() {
        return (Number) AuditReaderFactory.get(em).createQuery().forRevisionsOfEntity(typeArgument, true, true).add(AuditEntity.revisionType().eq(RevisionType.DEL)).addProjection(AuditEntity.revisionNumber().max()).getSingleResult();
    }

    @Override
    public Number getLastModifiedRevisionNumber() {
        return (Number) AuditReaderFactory.get(em).createQuery().forRevisionsOfEntity(typeArgument, true, true).add(AuditEntity.revisionType().eq(RevisionType.MOD)).addProjection(AuditEntity.revisionNumber().max()).getSingleResult();
    }

    @Override
    public Number getLastInsertedRevisionNumber() {
        return (Number) AuditReaderFactory.get(em).createQuery().forRevisionsOfEntity(typeArgument, true, true).add(AuditEntity.revisionType().eq(RevisionType.ADD)).addProjection(AuditEntity.revisionNumber().max()).getSingleResult();
    }

    @Override
    public List<HistoricalModel> getRevisionNumberModificationType(Number revision) {
        List<Object> result = AuditReaderFactory.get(em).createQuery().forRevisionsOfEntity(typeArgument, true, true).add(AuditEntity.revisionNumber().eq(revision)).addProjection(AuditEntity.revisionType()).addProjection(AuditEntity.id()).getResultList();
        List<HistoricalModel> historicalEntities = new ArrayList<>();
        for (Object o : result) {
            if (o instanceof Object[]) {
                Object[] objects = (Object[]) o;
                if (objects[0] instanceof RevisionType) {
                    RevisionType revisionType = (RevisionType) objects[0];
                    HistoricalModel.ModificationType modificationType = null;
                    switch (revisionType) {
                        case ADD:
                            modificationType = HistoricalModel.ModificationType.INSERTED;
                            break;
                        case MOD:
                            modificationType = HistoricalModel.ModificationType.MODIFIED;
                            break;
                        case DEL:
                            modificationType = HistoricalModel.ModificationType.DELETED;
                            break;
                    }
                    historicalEntities.add(new HistoricalModel(objects[1], modificationType));
                }
            }
        }
        return historicalEntities;
    }

    @Override
    public void firePrePersist(Object entity) {

    }

    @Override
    public PathBuilder<T> getBuilder() {
        return this.builder;
    }

    @Override
    public SplQueryDslConfig getConfig() {
        return new SplQueryDslConfig(true);
    }

    public void setRepositoryMethodMetadata(CrudMethodMetadata crudMethodMetadata) {
        throw new IllegalStateException();
    }

//    private <S extends T> S save(S entity, boolean resetActivateField) {
//        setPrePersist(entity);
//        if (resetActivateField) {
//            ID id = (ID) idField.getValue(entity);
//            if (id != null && entity instanceof ActiveableEntity) {
//                T original = this.findOne(id);
//                ((ActiveableEntity) entity).setActive(((ActiveableEntity) original).getActive());
//            }
//            return super.save(entity);
//        } else {
//            setPrePersist(entity);
//            return super.save(entity);
//        }
//    }

    public <S extends T> S save(S entity) {
        return super.save(entity);
    }

    @Override
    public <S extends T> List<S> save(Iterable<S> entities) {
        return super.save(entities);
    }

    public <S extends T> S saveAndFlush(S entity) {
        throw new IllegalStateException();
    }

    public void flush() {
        throw new IllegalStateException();
    }

    @Override
    public Class<T> getTypeArgument() {
        return this.typeArgument;
    }
}