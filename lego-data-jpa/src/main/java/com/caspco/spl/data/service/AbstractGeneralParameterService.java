package com.caspco.spl.data.service;

/*
  @author : Пуя Гуссейни
 * Email : info@pooya-hfp.ir
 * Date: 23/06/14
 * Time: 17:37
 */


import com.caspco.spl.data.beans.SplRelationalRepository;

import java.io.Serializable;

public abstract class AbstractGeneralParameterService<E, K extends Serializable, R extends SplRelationalRepository<E, K>> extends AbstractSplDataService<E,K,R> {

}