package com.caspco.spl.data.beans;

import com.caspco.spl.model.data.HistoricalModel;
import com.caspco.spl.wrench.repository.SplRepository;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.AbstractJPAQuery;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 11/7/15
 *         Time: 2:03 PM
 */

@NoRepositoryBean
public interface SplRelationalRepository<T, ID extends Serializable> extends QueryDslPredicateExecutor<T>, JpaRepository<T, ID> ,SplRepository<T,ID>{
    AbstractJPAQuery<T, JPAQuery<T>> select(Predicate... predicate);

    int delete(Predicate predicate) throws InterruptedException;

    T getEntityAtVersion(Number revision);

    Number getLastRevisionNumber();

    Number getLastDeletedRevisionNumber();

    Number getLastModifiedRevisionNumber();

    Number getLastInsertedRevisionNumber();

    List<HistoricalModel> getRevisionNumberModificationType(Number revision);

//    List<T> find(T e);
//
//
//    boolean exists(T entity);
//
//    long count(T entity);
//
//    List<String> searchColumn(String column, String value);
//
//    boolean isActivable();
//
//    <S extends T> S changeActiveStatus(S entity);
//
//    <S extends T> Boolean getActiveStatus(S entity);
//
//    Class<T> getTypeArgument();
}