package com.caspco.spl.data.configuration;

import com.caspco.spl.data.beans.SplRelationalRepository;
import com.caspco.spl.data.swift.SplRepositoryFactoryBean;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 11/8/16
 *         Time: 5:56 PM
 */

@ComponentScan(basePackages = "com.caspco.spl.data")
@EnableJpaRepositories(basePackages = {"${spl.common.scan-package}", "com.caspco.spl.data"}, repositoryBaseClass = SplRelationalRepository.class, repositoryFactoryBeanClass = SplRepositoryFactoryBean.class)
@EntityScan(basePackages = {"com.caspco.spl.data", "${spl.common.scan-package}"})
@Configuration
@ImportResource("classpath:trx-aop.xml")
public class SplDataConfiguration {


}