package com.caspco.spl.data.file;

import com.caspco.spl.model.wrench.FileModel;
import com.caspco.spl.wrench.beans.transformer.SmartTransformer;
import com.caspco.spl.wrench.filetransfer.FilePersistorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/24/17
 *         Time: 12:26 PM
 */
@Component
@Transactional
public class FilePersisterServiceImpl implements FilePersistorService {

    @Autowired
    private FileTransferDataService dataService;
    @Autowired
    private SmartTransformer transformer;

    @Override
    public String persist(FileModel fileModel) {
        FileTransferEntity entity = transformer.convert(fileModel, FileTransferEntity.class);
        FileTransferEntity transferEntity = dataService.save(entity);
        return String.valueOf(transferEntity.getId());
    }

    @Override
    public FileModel load(String fileId) {
        FileTransferEntity transferEntity = dataService.findById(Long.valueOf(fileId));
        return transformer.convert(transferEntity, FileModel.class);
    }

}
