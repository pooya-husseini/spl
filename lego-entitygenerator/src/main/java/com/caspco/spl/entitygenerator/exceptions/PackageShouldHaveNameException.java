package com.caspco.spl.entitygenerator.exceptions;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 12/28/15
 *         Time: 7:09 PM
 */
public class PackageShouldHaveNameException extends RuntimeException {

    public PackageShouldHaveNameException() {
        super();
    }

    public PackageShouldHaveNameException(String message) {
        super(message);
    }

    public PackageShouldHaveNameException(String message, Throwable cause) {
        super(message, cause);
    }

    public PackageShouldHaveNameException(Throwable cause) {
        super(cause);
    }

    protected PackageShouldHaveNameException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
