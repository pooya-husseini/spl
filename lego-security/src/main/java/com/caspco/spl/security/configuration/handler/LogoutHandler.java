package com.caspco.spl.security.configuration.handler;

import com.caspco.spl.security.singlelogin.EnableSingleLoginBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by saeedko on 8/30/17.
 */
@Component
public class LogoutHandler implements LogoutSuccessHandler {
    @Autowired
    private UsernameContainer usernameContainer;
    @Autowired(required = false)
    private EnableSingleLoginBean enableSingleLoginBean;

    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        if(enableSingleLoginBean != null){
            User user = (User) authentication.getPrincipal();
            usernameContainer.remove(user.getUsername());
        }
        httpServletResponse.sendRedirect("/login.html");

    }
}
