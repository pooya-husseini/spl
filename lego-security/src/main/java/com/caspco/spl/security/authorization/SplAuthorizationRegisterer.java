package com.caspco.spl.security.authorization;

import com.caspco.spl.security.services.HasPermissionImpl;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 7/3/17
 *         Time: 9:58 AM
 */

public class SplAuthorizationRegisterer implements ImportBeanDefinitionRegistrar {

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        BeanDefinition enableBeanDefinition = new RootBeanDefinition(EnableAuthorizationBean.class);
        BeanDefinition hasPermissionBeanDefinition = new RootBeanDefinition(HasPermissionImpl.class);
        registry.registerBeanDefinition(EnableAuthorizationBean.class.getSimpleName(), enableBeanDefinition);
        registry.registerBeanDefinition(HasPermissionImpl.class.getSimpleName(), hasPermissionBeanDefinition);
    }
}