package com.caspco.spl.security.configuration.handler;

/**
 * Created by saeedko on 8/30/17.
 */
public class UserDetail {
    private String username;
    private String remoteAddress;

    public UserDetail(String username, String remoteAddress) {
        this.username = username;
        this.remoteAddress = remoteAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDetail that = (UserDetail) o;

        return !(remoteAddress != null ? !remoteAddress.equals(that.remoteAddress) : that.remoteAddress != null) && !(username != null ? !username.equals(that.username) : that.username != null);

    }

    @Override
    public int hashCode() {
        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + (remoteAddress != null ? remoteAddress.hashCode() : 0);
        return result;
    }
}

