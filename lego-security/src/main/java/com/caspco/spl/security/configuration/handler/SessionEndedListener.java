package com.caspco.spl.security.configuration.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.web.session.HttpSessionDestroyedEvent;
import org.springframework.stereotype.Component;

/**
 * Created by saeedko on 8/30/17.
 */
@Component
public class SessionEndedListener implements ApplicationListener<HttpSessionDestroyedEvent> {
    @Autowired
    private UsernameContainer usernameContainer;
//    @Override
//    public void onApplicationEvent(SessionDestroyedEvent sessionDestroyedEvent) {
//        SessionDestroyedEvent sdEvent = (SessionDestroyedEvent) sessionDestroyedEvent;
//        String username = "";
//        List<SecurityContext> lstSecurityContext = sdEvent
//                .getSecurityContexts();
//
//        for (SecurityContext securityContext : lstSecurityContext)
//        {
//            username=  securityContext.getAuthentication().getName();
//        }
//        usernameContainer.remove(username);
//    }

    @Override
    public void onApplicationEvent(HttpSessionDestroyedEvent event) {
        System.out.println(event);
    }
}
