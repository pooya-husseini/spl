package com.caspco.spl.security.services;

import com.caspco.spl.security.model.cache.UserPermissionService;
import com.caspco.spl.wrench.annotations.permission.Permission;
import com.caspco.spl.wrench.interfaces.HasPermission;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

/**
 * Created by saeedko on 5/17/17.
 */

public class HasPermissionImpl implements HasPermission{
    @Autowired
    private UserPermissionService permissionService;


    @Override
    public boolean hasAuthority(Class<?> baseClass, Permission permission) {
        String restServiceClazzName = "";
        if (baseClass != null) {
            restServiceClazzName = baseClass.getSimpleName().replace("RestService", "");
        }
        String s = String.format("%s.%s", restServiceClazzName, permission.name());
        try {
            return permissionService.currentUserPermissions().contains(s);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
//        return rolesResolver.getUserRoles().stream()
//                .map(i -> permissionService.getRolePermissions(i))
//                .flatMap(Collection::stream)
//                .map(PermissionEntity::getTitle)
//                .anyMatch(item -> item.equals(s));
    }
}
