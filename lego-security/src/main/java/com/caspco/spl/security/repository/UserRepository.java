package com.caspco.spl.security.repository;

/**
* THIS IS AN AUTO GENERATED CODE.
* @author : Lego Code Generator
* Email : husseini@caspco.ir
* Date: Oct 26, 2016 6:11:43 PM
* @version 2.7.1
*/

import com.caspco.spl.model.annotations.cg.Generated;
import com.caspco.spl.security.model.UserEntity;

@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public interface UserRepository extends AbstractUserRepository<UserEntity,Long> {

}