package com.caspco.spl.security.aspect;

import com.caspco.spl.security.authorization.EnableAuthorizationBean;
import com.caspco.spl.security.exceptions.AuthorizationException;
import com.caspco.spl.security.exceptions.NotUseBasicRestClass;
import com.caspco.spl.webactivator.beans.rest.RestOptionsResolver;
import com.caspco.spl.wrench.annotations.permission.Permission;
import com.caspco.spl.wrench.interfaces.HasPermission;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * Created by saeedko on 5/17/17.
 */
@Aspect
@Component
public class PermissionAspectService {

    @Autowired(required = false)
    private EnableAuthorizationBean splAuthorizationStatus;
    @Autowired(required = false)
    private HasPermission hasPermission;

    @Pointcut("execution(* com.caspco.spl.webactivator.beans.rest.RestOptionsResolver+.*(..)))")
    public void inheritedMethods() {

    }


    @Around("inheritedMethods() && @annotation(com.caspco.spl.wrench.annotations.permission.Permission)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        if (splAuthorizationStatus == null || hasPermission == null) {
            return joinPoint.proceed();
        } else {
            if (!(joinPoint.getTarget() instanceof RestOptionsResolver)) {
                throw new NotUseBasicRestClass();
            }
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            Method method = signature.getMethod();

            Permission annotation = method.getAnnotation(Permission.class);
            if (!hasPermission.hasAuthority(joinPoint.getTarget().getClass(), annotation)) {
                throw new AuthorizationException();
            }
            return joinPoint.proceed();
        }
    }

}