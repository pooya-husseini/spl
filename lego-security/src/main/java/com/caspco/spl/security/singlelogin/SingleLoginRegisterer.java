package com.caspco.spl.security.singlelogin;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

/**
 * Created by saeedko on 9/2/17.
 */
public class SingleLoginRegisterer implements ImportBeanDefinitionRegistrar {
    @Override
    public void registerBeanDefinitions(AnnotationMetadata annotationMetadata, BeanDefinitionRegistry beanDefinitionRegistry) {
        BeanDefinition singleBeanDefinition = new RootBeanDefinition(EnableSingleLoginBean.class);
        beanDefinitionRegistry.registerBeanDefinition(EnableSingleLoginBean.class.getSimpleName(), singleBeanDefinition);
    }
}
