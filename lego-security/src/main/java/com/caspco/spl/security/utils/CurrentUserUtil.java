

package com.caspco.spl.security.utils;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 12/16/15
 *         Time: 1:23 PM
 */

@Service
public class CurrentUserUtil {
//    @Autowired
//    private PermissionService permissionService;

    public boolean hasRole(String role) {
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        if (authorities != null) {
            return authorities.stream().anyMatch(i -> i.getAuthority().equals(role));
        }
//        List<PermissionEntity> permissions = permissionService.getRolePermissions(role);
//        for (PermissionEntity authority : permissions) {
//            if (role.equals(authority.getRoleTitle())) {
//                return true;
//            }
//        }
        return false;
    }
}
