package com.caspco.spl.security.rests;

import com.caspco.spl.security.model.dtos.UserDto;
import com.caspco.spl.security.services.UserService;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class UserLoginController extends AbstractLoginController<UserService, UserDto> {

}
