package com.caspco.spl.security.transformer;

import com.caspco.spl.security.model.dtos.PermissionDto;
import com.caspco.spl.security.model.PermissionEntity;
import com.caspco.spl.wrench.beans.transformer.SimpleBeanTransformer;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amini on 9/11/2016.
 */
@Component
public class PermissionDtoTransformer extends SimpleBeanTransformer<PermissionDto, PermissionEntity> {



    @Override
    public PermissionEntity convert(PermissionDto permissionDto, Object o) {
        PermissionEntity permission = new PermissionEntity();
        permission.setId(permissionDto.getId());
        permission.setTitle(permissionDto.getTitle());
        permission.setRoleTitle(permissionDto.getRoleTitle());
        return permission;
    }


    public List<PermissionEntity> convertList(List<PermissionDto> permissionDtoList) {
        List<PermissionEntity> dtoList = new ArrayList<>();
        for (PermissionDto permissionDto : permissionDtoList) {
            dtoList.add(convert(permissionDto));
        }
        return dtoList;
    }
}
