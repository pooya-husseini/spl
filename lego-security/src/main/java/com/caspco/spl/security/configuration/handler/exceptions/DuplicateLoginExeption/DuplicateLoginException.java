package com.caspco.spl.security.configuration.handler.exceptions.DuplicateLoginExeption;

import com.caspco.spl.model.annotations.cg.Translation;
import com.caspco.spl.model.annotations.rcg.SplException;
import com.caspco.spl.model.wrench.Locale;
import com.caspco.spl.model.wrench.exception.SplBaseException;

/**
 * Created by saeedko on 8/30/17.
 */
@SplException(translations = {
        @Translation(locale = Locale.Persian_Iran,label = "کاربر جاری آنلاین می باشد"),
        @Translation(locale = Locale.English_United_States,label = "User has already logged in!")
})
public class DuplicateLoginException extends SplBaseException {
}
