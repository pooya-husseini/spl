package com.caspco.spl.security.model.dtos;

/**
 * THIS IS AN AUTO GENERATED CODE.
 *
 * @author : Lego Code Generator
 * Email : husseini@caspco.ir
 * Date: Oct 26, 2016 6:11:43 PM
 * @version 2.7.1
 */


import com.caspco.spl.model.annotations.cg.Generated;
import com.caspco.spl.model.annotations.json.SecuredValue;
import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public class UserDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String password;
    private String username;
    private String role;


    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    @SecuredValue
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}