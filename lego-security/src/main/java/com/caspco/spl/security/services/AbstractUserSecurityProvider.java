package com.caspco.spl.security.services;

import com.caspco.spl.security.model.UserEntity;
import com.caspco.spl.wrench.beans.transformer.SmartTransformer;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;

/**
 * @author : Pooya Hosseini
 * Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 * Date: 12/6/15
 * Time: 2:18 PM
 */

public abstract class AbstractUserSecurityProvider<S extends AbstractUserService> implements UserDetailsService, InitializingBean {

    @Autowired
    private S service;

    //    private  UserLoginSuccessEvent<>

    protected User defaultUser;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private SmartTransformer basicTransformer;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

         if (defaultUser != null && !service.isEmpty()) {
            defaultUser = null;
        }

        if (defaultUser != null && defaultUser.getUsername().equalsIgnoreCase(s)) {
            return defaultUser;
        }

        User user = new User(s, "", Collections.EMPTY_LIST);
        user.eraseCredentials();

        UserEntity userEntity = service.findByUsername(user.getUsername());
        if (userEntity == null) {
            throw new UsernameNotFoundException("Invalid username/password.");
        }
        User result = basicTransformer.convert(userEntity, User.class);
        return new User(result.getUsername().trim(), result.getPassword().trim(), result.getAuthorities());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        String adminPassword = encoder.encode("admin");
        //        String adminPassword = "admin";
        this.defaultUser = new User("admin", adminPassword, AuthorityUtils.createAuthorityList("ROLE_ADMIN"));
    }


}