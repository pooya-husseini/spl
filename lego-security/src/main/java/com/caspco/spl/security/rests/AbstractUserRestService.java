package com.caspco.spl.security.rests;
/*
* THIS IS AN AUTO GENERATED CODE.
* @author : Lego Code Generator
* Email : husseini@caspco.ir
* Date: Oct 26, 2016 6:11:43 PM
* @version 2.7.1
*/


import com.caspco.spl.model.annotations.cg.Generated;
import com.caspco.spl.model.web.dto.PasswordDto;
import com.caspco.spl.model.web.exception.OldPasswordIsNotCorrect;
import com.caspco.spl.security.model.UserEntity;
import com.caspco.spl.security.model.dtos.UserDto;
import com.caspco.spl.security.services.AbstractUserService;
import com.caspco.spl.webactivator.beans.rest.SplRestService;
import com.caspco.spl.wrench.beans.transformer.SmartTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;

@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public abstract class AbstractUserRestService<D, R extends AbstractUserService> extends SplRestService<D, R> {

    @Autowired
    private PasswordEncoder passwordEncoder;

//public class UserRestService extends BasicRestService<UserDto, UserEntity, UserService> {
//    @Autowired
//    protected AbstractUserRestService(R service) {
//        super(service);
//    }

    @Override
    public void handle(HttpMessageNotReadableException e) {

    }

//    @RequestMapping(value = "/changePassword", method = RequestMethod.PUT)
//    public void changePassword(@Valid @RequestBody PasswordDto dto) {
//        UserDto userDto = new UserDto();
//        userDto.setUsername(dto.getUsername());
//
//        List<UserEntity> list = getService().findBySample(getDTransformer().convert(userDto));
//
//        if (list.size() != 1) {
//            throw new IllegalArgumentException("User is not valid!");
//        }
//
//        UserEntity domainObject = list.get(0);
//        if (!domainObject.getPassword().equals(dto.getOldPassword())) {
//            throw new com.caspco.spl.wrench.exception.OldPasswordIsNotCorrect();
//        }
//
//        domainObject.setPassword(dto.getPassword());
//        getService().save(domainObject);
//    }
    @RequestMapping(value = "/changePassword", method = RequestMethod.PUT)
    public UserDto changePassword(@Valid @RequestBody PasswordDto dto) {
        UserEntity result = service.findByUsername(dto.getUsername());

        if (result == null) {
            throw new IllegalArgumentException("User is not valid!");
        }

        if (!passwordEncoder.matches(dto.getOldPassword(), result.getPassword())) {
            throw new OldPasswordIsNotCorrect();
        }

//        if (!result.getPassword().equals(dto.getOldPassword())) {
//
//        }

        result.setPassword(passwordEncoder.encode(dto.getPassword()));
//        result.setPassword(dto.getPassword());
        return transformer.convert(getService().save(result), UserDto.class);
    }
}