package com.caspco.spl.security.services;

import com.caspco.spl.data.service.AbstractSplDataService;
import com.caspco.spl.security.model.UserEntity;
import com.caspco.spl.security.repository.AbstractUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.Serializable;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 1/19/17
 *         Time: 3:45 PM
 */
public class AbstractUserService<E extends UserEntity, K extends Serializable, R extends AbstractUserRepository<E, K>> extends AbstractSplDataService<E, K, R> {
    public E findByUsername(String username) {
        return repository.findByUsername(username);
    }

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public E save(E obj) {
        if (obj.getId() == null) {
//            E username = repository.findByUsername(obj.getUsername());
            obj.setPassword(passwordEncoder.encode("123"));
        }else {
            obj.setPassword(repository.findById(obj.getId()).getPassword());
        }
        return super.save(obj);
    }
}