package com.caspco.spl.security.rests;
/*
* THIS IS AN AUTO GENERATED CODE.
* @author : Lego Code Generator
* Email : husseini@caspco.ir
* Date: Oct 26, 2016 6:11:43 PM
* @version 2.7.1
*/


import com.caspco.spl.security.model.UserEntity;
import com.caspco.spl.security.model.dtos.UserDto;
import com.caspco.spl.security.services.UserService;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
@Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRES_NEW)
public class UserRestService extends AbstractUserRestService<UserDto, UserService> {

    @Override
    public void handle(HttpMessageNotReadableException e) {

    }

    @PostConstruct
    public void init() {
        System.out.println("Hello");
    }


    @RequestMapping(value = "/changeSetting", method = RequestMethod.PUT)
    public void changeSetting(@Valid @RequestBody UserDto dto) {
        UserDto userDto = new UserDto();
        userDto.setUsername(dto.getUsername());

        List<UserEntity> list = getService().findBySample(transformer.convert(userDto, UserEntity.class));

        if (list.size() != 1) {
            throw new IllegalArgumentException("User is not valid!");
        }

        UserEntity domainObject = list.get(0);
        domainObject.setFirstName(dto.getFirstName());
        domainObject.setLastName(dto.getLastName());

        getService().save(domainObject);

    }
}