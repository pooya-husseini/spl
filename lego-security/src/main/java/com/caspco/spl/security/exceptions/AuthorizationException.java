package com.caspco.spl.security.exceptions;


import com.caspco.spl.model.annotations.cg.Translation;
import com.caspco.spl.model.annotations.rcg.SplException;
import com.caspco.spl.model.wrench.Locale;
import com.caspco.spl.model.wrench.exception.SplBaseException;

/**
 * Created by saeedko on 5/17/17.
 */
@SplException(translations = {
        @Translation(label = "Not Authorization this Activity", locale = Locale.English_United_States),
        @Translation(label = "خطای سطح دسترسی", locale = Locale.Persian_Iran)

})
public class AuthorizationException extends SplBaseException {
}
