package com.caspco.spl.security.utils;

import java.io.UnsupportedEncodingException;
import java.util.ResourceBundle;

/**
 * Created by saeedko on 8/5/17.
 */
public class PropertiesFileReader {
    public static String getValue(String key , String bundleName){
        String value= "";
        if(bundleName == null){
            bundleName = "Permissions-fa_IR";
        }
        ResourceBundle resourceBundle =ResourceBundle.getBundle("translations/"+bundleName);
        try {
            value= new String(resourceBundle.getString(key).getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        catch(Exception ex){
            value = key;
        }
        return value;
    }
    public static String getValue(String key ){
        return getValue(key , null);
    }
}
