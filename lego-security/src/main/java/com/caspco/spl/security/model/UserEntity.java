package com.caspco.spl.security.model;

/*
  @author : Pooya Hosseini
 * Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 * Date: 10/12/2015
 * Time: 3:37 PM
 */

import com.caspco.spl.model.annotations.cg.*;
import com.caspco.spl.model.annotations.cg.security.Password;
import com.caspco.spl.model.annotations.cg.security.UserPrincipal;
import com.caspco.spl.model.wrench.Locale;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

//import org.jasypt.hibernate4.type.EncryptedStringType;

@Audited
@Translations(value = {
        @Translation(label = "User", locale = Locale.English_United_States),
        @Translation(label = "کاربران", locale = Locale.Persian_Iran)
})
@Operations({
        @Operation(name = "changePassword",translations = {
                @Translation(locale = Locale.Persian_Iran,label = "تغییر رمز عبور")
        })
})
@UserPrincipal
//@TypeDefs(
//        {
//                @TypeDef(
//                        name = "encryptedString",
//                        typeClass = EncryptedStringType.class,
//                        parameters = {
//                                @org.hibernate.annotations.Parameter(name = "encryptorRegisteredName",
//                                        value = "stringEncryptor")
//                        }
//                )
//        }
//)
@Table(name = "USER_MANAGEMENT")
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.INTEGER, length = 2)
@DiscriminatorValue("0")
@Ignore(mods = {
        IgnoreType.CLIENT_SIDE_CONTROLLERS,
        IgnoreType.CLIENT_SIDE_SERVICES,
        IgnoreType.DTO,
        IgnoreType.PRINCIPAL,
        IgnoreType.SERVER_SIDE_CONTROLLER,
        IgnoreType.SERVER_SIDE_SERVICES,
        IgnoreType.UI})
public class UserEntity {

    private Long id;
    private String firstName;
    private String lastName;
    private String password;
    private String username;
    private String role;
    private Date lastLoginDate;

    @Id
    @GeneratedValue
    @Column
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column
    @Searchable
    @NotNull
    @Translations({@Translation(label = "Name", locale = Locale.English_United_States), @Translation(label = "نام", locale = Locale.Persian_Iran)})
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column
    @Searchable
    @NotNull
    @Translations({@Translation(label = "Last Name", locale = Locale.English_United_States), @Translation(label = "نام خانوادگی", locale = Locale.Persian_Iran)})
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column
//    @Type(type = "encryptedString")
    @NotNull
    @Password
    @Translations({@Translation(label = "PassWord", locale = Locale.English_United_States), @Translation(label = "کلمه عبور", locale = Locale.Persian_Iran)})
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(unique = true)
    @Searchable
    @NotNull
    @Translations({@Translation(label = "User Name", locale = Locale.English_United_States), @Translation(label = "شناسه کاربری", locale = Locale.Persian_Iran)})
    @ExactOnSearch
    @Pattern(regexp = "[a-zA-Z]([0-9]|[a-zA-Z])+")
    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    @Column
    @Searchable
    @Translations({@Translation(label = "Role", locale = Locale.English_United_States), @Translation(label = "نقش", locale = Locale.Persian_Iran)})
    @NotNull
    @GeneralParameter(type = "USER_ROLE", translations = {@Translation(label = "Role", locale = Locale.English_United_States), @Translation(label = "نقش", locale = Locale.Persian_Iran)})
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @NotUi
    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

}