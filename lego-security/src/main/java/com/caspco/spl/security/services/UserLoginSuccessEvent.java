package com.caspco.spl.security.services;

import com.caspco.spl.security.model.UserEntity;

/**
 * Created by mokaram on 01/17/2017.
 */
public interface UserLoginSuccessEvent<E> {
    void success(E userEntity);
}
