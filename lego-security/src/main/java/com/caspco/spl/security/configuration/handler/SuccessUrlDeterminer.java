package com.caspco.spl.security.configuration.handler;

import javax.servlet.http.HttpServletRequest;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 11/12/17
 * Time: 6:00 PM
 */
public interface SuccessUrlDeterminer {
    String determine(HttpServletRequest request);

    Boolean canChangeUrl(HttpServletRequest request);
}
