package com.caspco.spl.security.transformer;


/**
* THIS IS AN AUTO GENERATED CODE.
* @author : Lego Code Generator
* Email : husseini@caspco.ir
* Date: Oct 26, 2016 6:11:43 PM
* @version 2.7.1
*/

import com.caspco.spl.model.annotations.cg.Generated;
import com.caspco.spl.security.model.UserEntity;
import com.caspco.spl.security.model.dtos.UserDto;
import com.caspco.spl.wrench.beans.transformer.SimpleBeanTransformer;
import org.springframework.stereotype.Component;


@Component
@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public class UserEntityTransformer extends SimpleBeanTransformer<UserEntity,UserDto> {
    
    @Override
    public UserDto convert(UserEntity o,Object parent) {
        if(o == null ){
            return null;
        }
        UserDto e=new UserDto();
        
            e.setId(o. getId());
            e.setFirstName(o. getFirstName());
            e.setLastName(o. getLastName());
            e.setPassword(o. getPassword());
            e.setUsername(o. getUsername());
            e.setRole(o. getRole());
                    return e;
    }
}