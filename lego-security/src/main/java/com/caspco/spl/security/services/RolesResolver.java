package com.caspco.spl.security.services;

import java.util.List;

/**
 * Created by amini on 9/19/2016.
 */

public interface RolesResolver {
    List<String> getUserRoles();

    List<String> getAllRoles();
}
