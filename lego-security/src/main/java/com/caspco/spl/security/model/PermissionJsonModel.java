package com.caspco.spl.security.model;

import java.util.List;

/**
 * Created by saeedko on 5/27/17.
 */
public class PermissionJsonModel {
    private String value;
    private List<PermissionJsonModel> children;

    public PermissionJsonModel() {

    }

    public PermissionJsonModel(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<PermissionJsonModel> getChildren() {
        return children;
    }

    public void setChildren(List<PermissionJsonModel> children) {
        this.children = children;
    }
}
