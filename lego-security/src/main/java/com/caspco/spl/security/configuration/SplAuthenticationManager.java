package com.caspco.spl.security.configuration;

import com.caspco.spl.security.services.AbstractUserSecurityProvider;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 7/16/17
 * Time: 9:30 AM
 */


public class SplAuthenticationManager implements AuthenticationManager {
    private AbstractUserSecurityProvider userSecurityProvider;
    private PasswordEncoder passwordEncoder;

    public SplAuthenticationManager(AbstractUserSecurityProvider userSecurityProvider, PasswordEncoder passwordEncoder) {
        this.userSecurityProvider = userSecurityProvider;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//        AbstractUserSecurityProvider userDetailService = getUserDetailService();
        UserDetails userDetails = userSecurityProvider.loadUserByUsername(String.valueOf(authentication.getPrincipal()));
        String credentials = String.valueOf(authentication.getCredentials());
        if (passwordEncoder.matches(credentials, userDetails.getPassword())) {
            return new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        } else {
            throw new BadCredentialsException("Credential is invalid");
        }
    }
//    private AbstractUserSecurityProvider getUserDetailService(){
////        if (providers.size() > 1) {
////            return providers.stream().filter(i -> i.getClass() != BasicUserSecurityProvider.class).findFirst().get();
////        } else {
////            return this.providers.get(0);
////        }
//        return null;
//    }
}
