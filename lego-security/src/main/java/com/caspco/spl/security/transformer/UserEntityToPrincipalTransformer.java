package com.caspco.spl.security.transformer;

/**
 * THIS IS AN AUTO GENERATED CODE.
 *
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Mar 1, 2015 6:18:24 PM
 * @version 1.0.0
 */


import com.caspco.spl.security.model.UserEntity;
import com.caspco.spl.wrench.beans.transformer.SimpleBeanTransformer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserEntityToPrincipalTransformer extends SimpleBeanTransformer<UserEntity, User> {

    @Override
    public User convert(UserEntity source, Object parent) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(source.getRole()));

        return new User(source.getUsername(), source.getPassword(), authorities);
    }

}