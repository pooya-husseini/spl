package com.caspco.spl.security.configuration.handler;

import org.springframework.security.authentication.AuthenticationProvider;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 12/10/17
 * Time: 1:55 PM
 */
public abstract class AbstractSplAuthProvider implements AuthenticationProvider {
}
