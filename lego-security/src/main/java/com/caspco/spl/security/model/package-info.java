/**
 * Created by pooya on 10/31/16.
 */

@Translations({@Translation(locale = Locale.Persian_Iran,label = "امنیت"),@Translation(locale = Locale.English_United_States,label = "Security"),@Translation(locale = Locale.Persian_Iran,label = "داشبورد"),@Translation(locale = Locale.English_United_States,label = "Dashboard")})
package com.caspco.spl.security.model;

import com.caspco.spl.model.annotations.cg.Translation;
import com.caspco.spl.model.annotations.cg.Translations;
import com.caspco.spl.model.wrench.Locale;