package com.caspco.spl.security.rests;

import com.caspco.spl.security.model.UserEntity;
import com.caspco.spl.security.services.AbstractUserService;
import com.caspco.spl.security.services.UserLoginSuccessEvent;
import com.caspco.spl.wrench.beans.transformer.SmartTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.userdetails.User;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@RequestMapping("/login")
@Transactional
public abstract class AbstractLoginController<S extends AbstractUserService, D > {

    @Autowired
    private S service;

//    @Autowired
//    private PermissionsCache permissionsCache;
    private Class<D> dClazz= (Class<D>) GenericTypeResolver.resolveTypeArguments(this.getClass(), AbstractLoginController.class)[1];

    @Autowired(required = false)
    private UserLoginSuccessEvent<D> userLoginSuccessEvent;

    @Autowired
    private SmartTransformer basicTransformer;

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public D getUser(HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession(false);
        final SecurityContext context = (SecurityContext) session.getAttribute("SPRING_SECURITY_CONTEXT");

        User auth = (User) context.getAuthentication().getPrincipal();
        UserEntity userEntity = service.findByUsername(auth.getUsername());
        if (userEntity == null) {
            return null;
        }
        D result = basicTransformer.convert(userEntity, dClazz);
//        permissionsCache.setPermissions(userEntity.getRole());
        if (result == null) {
            return basicTransformer.convert(auth, dClazz);
        }
        if (userLoginSuccessEvent != null) {
            userLoginSuccessEvent.success(result);
        }
        return result;
    }
}
