package com.caspco.spl.security.exceptions;

import com.caspco.spl.model.annotations.cg.Translation;
import com.caspco.spl.model.annotations.rcg.SplException;
import com.caspco.spl.model.wrench.Locale;
import com.caspco.spl.model.wrench.exception.SplBaseException;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 11/12/17
 * Time: 5:51 PM
 */

@SplException(translations = {
        @Translation(locale = Locale.Persian_Iran,label = "رمز عبور فایل مخزن کلید اشتباه است")
})
public class SplIllegalKeyStoreSecretException extends SplBaseException{

    public SplIllegalKeyStoreSecretException() {
    }

    public SplIllegalKeyStoreSecretException(String exceptionType) {
        super(exceptionType);
    }

    public SplIllegalKeyStoreSecretException(String message, String exceptionType) {
        super(message, exceptionType);
    }

    public SplIllegalKeyStoreSecretException(String message, Throwable cause, String exceptionType) {
        super(message, cause, exceptionType);
    }

    public SplIllegalKeyStoreSecretException(Throwable cause, String exceptionType) {
        super(cause, exceptionType);
    }

    public SplIllegalKeyStoreSecretException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String exceptionType) {
        super(message, cause, enableSuppression, writableStackTrace, exceptionType);
    }

    public SplIllegalKeyStoreSecretException(String message, Throwable cause) {
        super(message, cause);
    }

    public SplIllegalKeyStoreSecretException(Throwable cause) {
        super(cause);
    }

    public SplIllegalKeyStoreSecretException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
