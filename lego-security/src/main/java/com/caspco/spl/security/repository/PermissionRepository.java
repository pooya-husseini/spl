package com.caspco.spl.security.repository;

import com.caspco.spl.data.beans.SplRelationalRepository;
import com.caspco.spl.security.model.PermissionEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by amini on 9/11/2016.
 */
public interface PermissionRepository extends SplRelationalRepository<PermissionEntity, Long> {
    PermissionEntity findByTitle(String title);
    List<PermissionEntity> findByRoleTitle(String roleTitle);

    PermissionEntity findByRoleTitleAndTitle(String roleTitle, String title);

    @Transactional
    @Modifying
    @Query("delete from PermissionEntity p where p.roleTitle = ?1")
    void deleteByRoleTitle(String roleTitle);
}