package com.caspco.spl.security.services;

import com.caspco.spl.data.service.AbstractSplDataService;
import com.caspco.spl.security.model.PermissionEntity;
import com.caspco.spl.security.model.PermissionJsonModel;
import com.caspco.spl.security.model.QPermissionEntity;
import com.caspco.spl.security.repository.PermissionRepository;
import com.caspco.spl.security.utils.PropertiesFileReader;
import com.caspco.spl.webactivator.beans.rest.RestOptionsResolver;
import com.caspco.spl.wrench.beans.rest.PermissionMethodModel;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by amini on 9/11/2016.
 */
@Service
public class PermissionService extends AbstractSplDataService<PermissionEntity, Long, PermissionRepository> {

    public static final QPermissionEntity Q = QPermissionEntity.permissionEntity;

    private Map<String, RestOptionsResolver> restControllers;

    public Map<String, RestOptionsResolver> getRestControllers() {
        return restControllers;
    }

    @Autowired
    @Lazy
    public void setRestControllers(Map<String, RestOptionsResolver> restControllers) {
        this.restControllers = restControllers;
    }

    public List<PermissionJsonModel> retrievePermission() throws IOException {
        List<PermissionJsonModel> concatTree = new ArrayList<>();
        PermissionJsonModel model = new PermissionJsonModel();
        if (restControllers != null) {
            List<PermissionJsonModel> permissions = restControllers.values()
                    .stream()
                    .map(RestOptionsResolver::getPermissions)
                    .map(item -> {
                        PermissionJsonModel jsonModel = new PermissionJsonModel(item.getClazzSimpleName());
                        if (item.getPermissionMethods() != null) {
                            List<PermissionJsonModel> collect = item.getPermissionMethods()
                                    .stream()
                                    .map(PermissionMethodModel::getPermissionName)
                                    .distinct()
                                    .map(PermissionJsonModel::new)
                                    .collect(Collectors.toList());

                            jsonModel.setChildren(collect);
                        }
                        return jsonModel;
                    })
                    .collect(Collectors.toList());
            model.setValue(PropertiesFileReader.getValue("activity"));
            model.setChildren(permissions);
            concatTree.add(model);
            model = new PermissionJsonModel();
            URL keyFileURL = PermissionService.class.getClassLoader().getResource("permission.json");
            if(keyFileURL != null){
            ObjectMapper mapper = new ObjectMapper();
            model.setValue(PropertiesFileReader.getValue("additional"));
            model.setChildren(new ArrayList<>());
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            PermissionJsonModel finalMocel = model;

            Arrays.asList(mapper.readValue(keyFileURL, PermissionJsonModel[].class))
                    .forEach(json -> finalMocel.getChildren().add(json));
            concatTree.add(finalMocel);
            }
            return concatTree;
        } else {
            return Collections.emptyList();
        }

    }

    public List<PermissionEntity> getRolePermissions(String role) {
        return repository.findByRoleTitle(role);
    }

    public List<PermissionEntity> save(String role, List<PermissionEntity> permissions) {
        repository.deleteByRoleTitle(role);
        return repository.save(permissions);
    }

    public PermissionEntity findByTitle(String title) {
        return repository.findByTitle(title);
    }

}