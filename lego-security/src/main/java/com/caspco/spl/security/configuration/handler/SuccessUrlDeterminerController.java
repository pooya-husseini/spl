package com.caspco.spl.security.configuration.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Created by saeedko on 8/8/17.
 */
@Controller
public class SuccessUrlDeterminerController {

    @Autowired(required = false)
    private List<SuccessUrlDeterminer> determiners;

    @RequestMapping("/default")
    public String defaultAfterLogin(HttpServletRequest request , HttpServletResponse response) throws ServletException, IOException {
        if (determiners != null) {
            Optional<SuccessUrlDeterminer> first = determiners.stream().filter(a -> a.canChangeUrl(request)).findFirst();
            if(first.isPresent()){
//                return new View(first.get().determine(request));
                return first.get().determine(request);
            }
        }

//        if(session.getAttribute("OAUTH") !=null){
//            //request.getRequestDispatcher("/oauth/authorize").forward(request,response);
//            return new RedirectView("/oauth/confirm_access");
//
//            // request.getRequestDispatcher("/oauth/authorize").forward(request,response);
//            //return "redirect:/oauth/authorize";
//        }
        return "/#/";

    }
}