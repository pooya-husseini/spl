package com.caspco.spl.security.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by amini on 9/11/2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PermissionDto {
    private Long id;
    private String title;
    private String roleTitle;

    /**
     * Getter for property 'id'.
     *
     * @return Value for property 'id'.
     */
    public Long getId() {
        return id;
    }

    /**
     * Setter for property 'id'.
     *
     * @param id Value to set for property 'id'.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Getter for property 'title'.
     *
     * @return Value for property 'title'.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter for property 'title'.
     *
     * @param title Value to set for property 'title'.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter for property 'roleTitle'.
     *
     * @return Value for property 'roleTitle'.
     */
    public String getRoleTitle() {
        return roleTitle;
    }

    /**
     * Setter for property 'roleTitle'.
     *
     * @param roleTitle Value to set for property 'roleTitle'.
     */
    public void setRoleTitle(String roleTitle) {
        this.roleTitle = roleTitle;
    }
}
