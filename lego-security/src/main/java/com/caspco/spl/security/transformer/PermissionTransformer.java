package com.caspco.spl.security.transformer;

import com.caspco.spl.security.model.dtos.PermissionDto;
import com.caspco.spl.security.model.PermissionEntity;
import com.caspco.spl.wrench.beans.transformer.SimpleBeanTransformer;
import org.springframework.stereotype.Component;

/**
 * Created by amini on 9/11/2016.
 */
@Component
public class PermissionTransformer extends SimpleBeanTransformer<PermissionEntity, PermissionDto> {


    @Override
    public PermissionDto convert(PermissionEntity permission, Object o) {
        PermissionDto permissionDto = new PermissionDto();
        permissionDto.setId(permission.getId());
        permissionDto.setTitle(permission.getTitle());
        permission.setRoleTitle(permissionDto.getRoleTitle());
        return permissionDto;
    }
}
