package com.caspco.spl.security.configuration.handler;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by saeedko on 8/30/17.
 */
@Component
public class UsernameContainer {

    private static final Map<String, UserDetail> userNameSet = new ConcurrentHashMap<>();

    public boolean containsSessionId(String s) {
        return userNameSet.containsKey(s);
    }

    public UserDetail findByUserName(String username) {
        for (Map.Entry<String, UserDetail> entry : userNameSet.entrySet()) {
            if (entry.getValue().getUsername().equals(username)) {
                return entry.getValue();
            }
        }
        return null;
    }

    public boolean isAuthenticationLocal(UserDetail s) {
        UserDetail userName = findByUserName(s.getUsername());
        if (userName == null) {
            return false;
        }else if(userName.getRemoteAddress().equals(s.getRemoteAddress())){
            return false;
        }
        else {
            return true;
        }
    }

//    public boolean containsUserDetail(UserDetail s) {
//        for (Map.Entry<String, UserDetail> entry : userNameSet.entrySet()) {
//            if (entry.getValue().equals(s)) {
//                return true;
//            }
//        }
//
//        return false;
//    }

    public void add(String username, UserDetail detail) {
        userNameSet.put(username, detail);
    }

    public void remove(String username) {
        userNameSet.remove(username);
    }

    public void removeUserDetail(UserDetail detail) {
        for (Map.Entry<String, UserDetail> entry : userNameSet.entrySet()) {
            if (entry.getValue().equals(detail)) {
                userNameSet.remove(entry.getKey());
                break;
            }
        }
    }
}

