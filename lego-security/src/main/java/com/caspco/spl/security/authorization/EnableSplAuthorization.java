package com.caspco.spl.security.authorization;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 7/3/17
 *         Time: 9:56 AM
 */


@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Import(SplAuthorizationRegisterer.class)
public @interface EnableSplAuthorization {

}
