package com.caspco.spl.security.model.cache;

import com.caspco.spl.security.model.PermissionEntity;
import com.caspco.spl.security.model.PermissionJsonModel;
import com.caspco.spl.security.services.PermissionService;
import com.caspco.spl.security.services.RolesResolver;
import com.caspco.spl.security.utils.CurrentUserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by saeedko on 7/1/17.
 */
@Component
@Scope(value="session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserPermissionService {

    @Autowired
    private CurrentUserUtil currentUserUtil;

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private RolesResolver rolesResolver;

    private Set<String> getPermission(List<PermissionJsonModel> permissionJsonModels) {
        Set<String> result = new HashSet<>();
        permissionJsonModels.forEach(permissionJsonModel -> {
            permissionJsonModel.getChildren().forEach(child -> result.add(child.getValue()));
        });
        return result;
    }

    @Cacheable(value="UserPermissions",key = "#root.target")
    public Set<String> currentUserPermissions() throws IOException {
        if (currentUserUtil.hasRole("ROLE_ADMIN")) {
            return permissionService.retrievePermission().stream().flatMap(permissionJsonModel -> {
                if (permissionJsonModel.getChildren() != null) {
                    return getPermission(permissionJsonModel.getChildren()).stream();
                } else {
                    return Stream.empty();
                }
            }).collect(Collectors.toSet());
        } else {
            List<String> userRoles = rolesResolver.getUserRoles();
            if (userRoles != null) {
                return userRoles.stream()
                        .map(permissionService::getRolePermissions)
                        .filter(Objects::nonNull)
                        .filter(i -> !i.isEmpty()) // todo rewrite this line
                        .flatMap(Collection::stream)
                        .map(PermissionEntity::getTitle)
                        .collect(Collectors.toSet());
            }else{
                return Collections.emptySet();
            }

            // List<String> userRoles = rolesResolver.getUserRoles();
            // if (userRoles != null) {
//            return permissionService.getRolePermissions(role).stream().map(permissionEntity -> permissionEntity.getTitle()).collect(Collectors.toSet())
            //   }
        }
    }
}
