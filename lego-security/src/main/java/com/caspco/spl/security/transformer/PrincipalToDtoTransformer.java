package com.caspco.spl.security.transformer;

/*
* THIS IS AN AUTO GENERATED CODE.
* @author : Lego Code Generator
* Email : husseini@caspco.ir
* Date: Oct 26, 2016 6:11:43 PM
* @version 2.7.1
*/


import com.caspco.spl.security.model.dtos.UserDto;
import com.caspco.spl.wrench.beans.transformer.SimpleBeanTransformer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PrincipalToDtoTransformer extends SimpleBeanTransformer<User, UserDto> {

    @Override
    public UserDto convert(User source,Object parent) {

        UserDto user = new UserDto();
        user.setUsername(source.getUsername());
        user.setPassword(source.getPassword());
        if (!source.getAuthorities().isEmpty()) {
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.addAll(source.getAuthorities());

            user.setRole(authorities.get(0).getAuthority());
        }
        return user;
    }

}