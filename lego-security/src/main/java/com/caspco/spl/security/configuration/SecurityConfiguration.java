package com.caspco.spl.security.configuration;

import com.caspco.spl.data.swift.SplRepositoryFactoryBean;
import com.caspco.spl.security.configuration.handler.AbstractSplAuthProvider;
import com.caspco.spl.security.configuration.handler.LoginHandler;
import com.caspco.spl.security.configuration.handler.LogoutHandler;
import com.caspco.spl.security.exceptions.SplIllegalKeyStoreSecretException;
import com.caspco.spl.security.rests.AbstractLoginController;
import com.caspco.spl.security.rests.AbstractUserRestService;
import com.caspco.spl.security.rests.UserLoginController;
import com.caspco.spl.security.rests.UserRestService;
import com.caspco.spl.security.services.AbstractUserSecurityProvider;
import com.caspco.spl.security.services.UserService;
import com.caspco.spl.webactivator.beans.conf.SplWebConfigs;
import com.caspco.spl.wrench.beans.scanner.TypeScanner;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 9/11/2016
 * Time: 8:36 PM
 */

@Configuration
//@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan(
        basePackages = "com.caspco.spl.security",
        excludeFilters = {
                @ComponentScan.Filter(
                        type = FilterType.REGEX,
                        pattern = {"com\\.caspco\\.spl\\.security\\.rests\\.UserRestService",
                                "com\\.caspco\\.spl\\.security\\.rests\\.UserLoginController"}
                )
        }
)
@EntityScan(basePackages = "com.caspco.spl.security")
@EnableJpaRepositories(
        basePackages = {"com.caspco.spl.security"},
        repositoryFactoryBeanClass = SplRepositoryFactoryBean.class
)
@EnableCaching
@EnableConfigurationProperties
@EnableWebSecurity
//@EnableWebSecurity()
public class SecurityConfiguration implements EmbeddedServletContainerCustomizer {

    @Autowired
    private SplWebConfigs splWebConfigs;


    @Autowired
    private TypeScanner typeScanner;



    //    @Autowired
    //    private AbstractUserSecurityProvider provider;
    //    @Autowired
    //    private SplAuthenticationManager authenticationManager;
    //    @Autowired
    //    public PasswordEncoder encoder;


    public SecurityConfiguration() {
        super();
    }

    @Bean
    public Object userRestService() {
        Set<Class<? extends AbstractUserRestService>> subTypesOf = typeScanner.getSubTypesOf(AbstractUserRestService.class);
        if (subTypesOf.size() <= 1) {
            return new UserRestService();
        }
        return null;
    }

    @Bean
    public Object userLoginController() {
        Set<Class<? extends AbstractLoginController>> subTypesOf = typeScanner.getSubTypesOf(AbstractLoginController.class);
        if (subTypesOf.size() <= 1) {
            return new UserLoginController();
        }
        return null;
    }

    @Override
    public void customize(ConfigurableEmbeddedServletContainer container) {
//        if (this.splWebConfigs.getKeystoreSecret() == null) {
//            throw new SplIllegalKeyStoreSecretException("Ssl certificate secret exception");
//        }

        TomcatEmbeddedServletContainerFactory tomcat = (TomcatEmbeddedServletContainerFactory) container;
        tomcat.addConnectorCustomizers(connector -> {
            connector.setSecure(false);
//            connector.setScheme("https");
            connector.setScheme("http");
            Http11NioProtocol proto = (Http11NioProtocol) connector.getProtocolHandler();
            proto.setSSLEnabled(false);

            if (this.splWebConfigs.getTimeout() != null) {
                proto.setSessionTimeout(this.splWebConfigs.getTimeout());
                proto.setSelectorTimeout(this.splWebConfigs.getTimeout());
                proto.setConnectionTimeout(this.splWebConfigs.getTimeout());
            }

//            proto.setKeystoreFile(new DefaultResourceLoader().getResource("splKeystore.jks").getFilename());
//            try {
//                proto.setKeystoreFile("classpath:splKeystore.jks");
//            } catch (Exception x) {
//                throw new SplBaseException(x);
//            }
//            proto.setKeystorePass(this.splWebConfigs.getKeystoreSecret());
        });
        if (this.splWebConfigs.getTimeout() != null) {
            tomcat.setSessionTimeout(this.splWebConfigs.getTimeout(), TimeUnit.SECONDS);
        }
    }

    @Bean
    public HttpSessionListener httpSessionListener() {
        // MySessionListener should implement javax.servlet.http.HttpSessionListener
        return new HttpSessionListener() {
            @Override
            public void sessionCreated(HttpSessionEvent event) {
                event.getSession().setMaxInactiveInterval(30);
            }

            @Override
            public void sessionDestroyed(HttpSessionEvent se) {
                System.out.println("HEllo");
            }
        };
    }


    //    @Order(HIGHEST_PRECEDENCE)
    @Configuration
    static class PathConfiguration extends WebSecurityConfigurerAdapter {

        @Autowired(required = false)
        private AbstractSplAuthProvider provider;

        @Bean
        public AuthenticationSuccessHandler authenticationSuccessHandler() {
            return new LoginHandler();
        }

        @Autowired
        private AuthenticationManagerBuilder auth;

        @Autowired
        private LoginHandler loginHandler;

        @Autowired
        private LogoutHandler logoutHandler;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            //        http.userDetailsService(provider);
            if (provider == null) {
                auth.authenticationProvider(authProvider());
            }else{
                auth.authenticationProvider(provider);
            }

            http.authorizeRequests()
                    .antMatchers(
                            "/login*", "/logout*",
                            "/app/resources/**",
                            "/vendors/**"
                    )
                    .permitAll()
                    .anyRequest()
                    .authenticated()
                    .and()
                    .formLogin()
                    .successHandler(loginHandler)
                    .failureUrl("/login.html?error")
                    .loginProcessingUrl("/login")
                    .loginPage("/login.html")
                    .passwordParameter("password")
                    .usernameParameter("username")
                    //.defaultSuccessUrl("/default", true)
                    .permitAll()
                    .and()
                    .logout()
                    .logoutUrl("/logout")
                    .logoutSuccessHandler(logoutHandler)
                    .permitAll()
                    .and()
                    .csrf()
                    .disable()
                    .sessionManagement()
                    .maximumSessions(1000)

                    .expiredUrl("/logout?expired");
        }
        //    @Override
        //    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //        auth.inMemoryAuthentication().withUser("pooya").password("123").roles("ADMIN");
        ////        auth.userDetailsService(provider).passwordEncoder(encoder);
        ////        auth.userDetailsService(provider);
        ////       auth.authenticationProvider(authenticationManager);
        ////        auth.parentAuthenticationManager(authenticationManager);
        //    }

        @Bean
        public PasswordEncoder passwordEncoder() {
            return new BCryptPasswordEncoder(10);
        }

        @Bean
        public DaoAuthenticationProvider authProvider() {
            DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
            authProvider.setUserDetailsService(securityProvider());
            authProvider.setPasswordEncoder(passwordEncoder());
            return authProvider;
        }

        @Bean
        public AbstractUserSecurityProvider securityProvider() {
            return new AbstractUserSecurityProvider<UserService>() {
            };
        }

    }
}