package com.caspco.spl.security.rests;


import com.caspco.spl.model.wrench.exception.PermissionIsNullException;
import com.caspco.spl.security.authorization.EnableAuthorizationBean;
import com.caspco.spl.security.model.PermissionEntity;
import com.caspco.spl.security.model.PermissionJsonModel;
import com.caspco.spl.security.model.cache.UserPermissionService;
import com.caspco.spl.security.model.dtos.PermissionDto;
import com.caspco.spl.security.services.PermissionService;
import com.caspco.spl.security.services.RolesResolver;
import com.caspco.spl.webactivator.beans.rest.SplRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by amini on 8/30/2016.
 */

@Transactional
@RestController
@RequestMapping("/security/permission")
public class PermissionRestService extends SplRestService<PermissionDto, PermissionService> {

//    @Autowired
//    public PermissionRestService(PermissionService service) {
//        super(service);
//    }

    @Autowired
    private UserPermissionService currentUserPermissions;

    @Autowired(required = false)
    private EnableAuthorizationBean status;
    @Autowired
    private RolesResolver rolesResolver;

    @Override
    public void handle(HttpMessageNotReadableException e) {

    }

    @RequestMapping(value = "/fillTree", method = RequestMethod.GET)
    public List<String> fillTree(@RequestParam(value = "role") String role) {
        return getService().getRolePermissions(role).stream()
                .map(PermissionEntity::getTitle)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/userPermissions", method = RequestMethod.GET)
    public Set<String> getUserPermissions() throws IOException {
        return currentUserPermissions.currentUserPermissions();
    }

    @RequestMapping(value = "/permissionTree", method = RequestMethod.GET)
    public List<PermissionJsonModel> permissionTree() throws IOException {
        return service.retrievePermission();
    }

    @RequestMapping(value = "/allRoles", method = RequestMethod.GET)
    public List<String> getAllRoles() {
        return rolesResolver.getAllRoles();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/addNewPermissionsForRole", method = RequestMethod.POST)
    public List<PermissionEntity> addNewPermissionsForRole(@RequestParam(name = "role") String role, @RequestBody List<String> listOfSelected) throws PermissionIsNullException {
        if (listOfSelected != null) {
            List<PermissionEntity> permissions = listOfSelected.stream()
                    .map(i -> new PermissionEntity(i.replace("RestService", ""), role))
                    .collect(Collectors.toList());
            return getService().save(role, permissions);
        } else {
            throw new PermissionIsNullException();
        }
    }

    @RequestMapping(value = "/authorizationStatus", method = RequestMethod.GET)
    public Boolean authorizationStatus() {
        return status != null;
    }
}