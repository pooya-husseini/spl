package com.caspco.spl.security.model;

import com.caspco.spl.model.annotations.cg.Ignore;
import com.caspco.spl.model.annotations.cg.IgnoreType;
import com.caspco.spl.model.annotations.cg.Translation;
import com.caspco.spl.model.annotations.cg.Translations;
import com.caspco.spl.model.wrench.Locale;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by amini on 9/11/2016.
 */
@Entity
@Table(name = "Permission", uniqueConstraints = {
        @UniqueConstraint(name = "unq", columnNames = {"TITLE", "ROLE_TITLE"})
})
@Translations({@Translation(label = "Permission", locale = Locale.English_United_States), @Translation(label = "سطح دسترسی", locale = Locale.Persian_Iran)})
@Audited
@Ignore(mods = IgnoreType.ALL)
public class PermissionEntity {
    private Long id;
    private String title;
    private String roleTitle;

    public PermissionEntity() {

    }

    public PermissionEntity(String title, String roleTitle) {
        this.title = title;
        this.roleTitle = roleTitle;
    }

    public PermissionEntity(String title) {
        this.title = title;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Translations({@Translation(label = "Permission Title", locale = Locale.English_United_States), @Translation(label = "عنوان فعالیت", locale = Locale.Persian_Iran)})
    @NotNull
    @Size(min = 1)
    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    /**
     * Getter for property 'roleTitle'.
     *
     * @return Value for property 'roleTitle'.
     */
    @Column(name = "ROLE_TITLE")
    @NotNull
    @Size(min = 1)
    public String getRoleTitle() {
        return roleTitle;
    }

    /**
     * Setter for property 'roleTitle'.
     *
     * @param roleTitle Value to set for property 'roleTitle'.
     */

    public void setRoleTitle(String roleTitle) {
        this.roleTitle = roleTitle;
    }
}
