package com.caspco.spl.security.exceptions;


import com.caspco.spl.model.annotations.cg.Translation;
import com.caspco.spl.model.annotations.rcg.SplException;
import com.caspco.spl.model.wrench.Locale;
import com.caspco.spl.model.wrench.exception.SplBaseException;

/**
 * Created by saeedko on 6/10/17.
 */
@SplException(translations = {
        @Translation(label = "Not use basic rest class", locale = Locale.English_United_States),
        @Translation(label = "خطای عدم استفاده از کلاس های ماژول امنیت" , locale = Locale.Persian_Iran)

})
public class NotUseBasicRestClass extends SplBaseException {
}
