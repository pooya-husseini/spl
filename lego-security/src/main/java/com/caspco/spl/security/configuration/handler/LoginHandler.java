package com.caspco.spl.security.configuration.handler;

import com.caspco.spl.model.wrench.exception.SplBaseException;
import com.caspco.spl.security.singlelogin.EnableSingleLoginBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by saeedko on 8/30/17.
 */
@Component
public class LoginHandler implements AuthenticationSuccessHandler {
    @Autowired
    private UsernameContainer usernameContainer;

    @Autowired(required = false)
    private EnableSingleLoginBean enableSingleLoginBean;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {

        if (enableSingleLoginBean == null) {
//            redirect(httpServletResponse, "/default");
            httpServletRequest.getRequestDispatcher("/default").forward(httpServletRequest, httpServletResponse);
        } else {
            WebAuthenticationDetails details = (WebAuthenticationDetails) authentication.getDetails();
            User user = (User) authentication.getPrincipal();
            UserDetail userDetail = new UserDetail(user.getUsername(), details.getRemoteAddress());
//            Reflections reflections = new Reflections(
//                    new ConfigurationBuilder()
//                            .setUrls(ClasspathHelper.forPackage("com.caspco.spl"))
//            );
//            Set<Class<?>> types = reflections.getTypesAnnotatedWith(EnableSingleLogin.class);
//            types.forEach(i-> {
//                if (i.getAnnotation(EnableSingleLogin.class).mode().equals(LoginMode.IP)) {
            if (!usernameContainer.isAuthenticationLocal(userDetail)) {
                usernameContainer.add(user.getUsername(), userDetail);
                redirect(httpServletResponse, "/default");
            } else {
                SecurityContextHolder.clearContext();
                redirect(httpServletResponse, "/login.html");
            }
//                }else if(i.getAnnotation(EnableSingleLogin.class).mode().equals(LoginMode.USER_NAME)){
//                      if(usernameContainer.findByUserName(user.getUsername()) == null){
//                          usernameContainer.add(user.getUsername(),userDetail);
//                          redirect(httpServletResponse,"/default");
//                      } else {
//                          SecurityContextHolder.clearContext();
//                          redirect(httpServletResponse,"/login.html");
//
//                      }
//                }
//            });

//
//            if (usernameContainer.isAuthenticationLocal(userDetail)) {
//                SecurityContextHolder.clearContext();
//                httpServletResponse.sendRedirect("/login.html");
//                //throw new DuplicateLoginException();
//            } else {
//                usernameContainer.add(user.getUsername(), userDetail);
//                httpServletResponse.sendRedirect("/default");
//            }

        }

    }

    private void redirect(HttpServletResponse response, String url) {
        try {
            response.sendRedirect(url);
        } catch (IOException e) {
            throw new SplBaseException("Redirect exception", e);
        }
    }
}
