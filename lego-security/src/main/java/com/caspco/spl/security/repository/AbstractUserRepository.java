package com.caspco.spl.security.repository;

/**
* THIS IS AN AUTO GENERATED CODE.
* @author : Lego Code Generator
* Email : husseini@caspco.ir
* Date: Oct 26, 2016 6:11:43 PM
* @version 2.7.1
*/

import com.caspco.spl.data.beans.SplRelationalRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface AbstractUserRepository<T, ID extends Serializable> extends SplRelationalRepository<T,ID> {
    T findByUsername(String username);
    T findById(Long id);
}