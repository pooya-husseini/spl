package com.caspco.spl.security.services;

/**
* THIS IS AN AUTO GENERATED CODE.
* @author : Lego Code Generator
* Email : husseini@caspco.ir
* Date: Oct 26, 2016 6:11:43 PM
* @version 2.7.1
*/

import com.caspco.spl.model.annotations.cg.Generated;
import com.caspco.spl.security.model.UserEntity;
import com.caspco.spl.security.repository.UserRepository;
import org.springframework.stereotype.Service;


@Service
@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public class UserService extends AbstractUserService<UserEntity,Long,UserRepository> {
    public UserEntity findByUsername(String username) {
        return repository.findByUsername(username);
    }
}