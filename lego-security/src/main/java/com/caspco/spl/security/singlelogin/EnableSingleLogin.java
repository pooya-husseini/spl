package com.caspco.spl.security.singlelogin;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Created by saeedko on 9/2/17.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Import(SingleLoginRegisterer.class)
public @interface EnableSingleLogin {
    LoginMode mode();
}
