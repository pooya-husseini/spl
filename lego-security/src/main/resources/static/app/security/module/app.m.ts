@lego.Module(["lego.base","ivh.treeview", "permission", "permission.ng"],"lego.security")
export class security{

}

function TreeviewConfig(ivhTreeviewOptionsProvider) {
    ivhTreeviewOptionsProvider.set({
        //idAttribute: "id",
        //labelAttribute: "label",
        //childrenAttribute: "children",
        //selectedAttribute: "selected",
        //useCheckboxes: true,
        expandToDepth: 0,
        //indeterminateAttribute: "__ivhTreeviewIndeterminate",
        //expandedAttribute: "__ivhTreeviewExpanded",
        //defaultSelectedState: true,
        //validate: true,
        twistieCollapsedTpl: "<span class='glyphicon glyphicon-collapse-down'></span>",
        twistieExpandedTpl: "<span class='glyphicon glyphicon-collapse-up'></span>",
        twistieLeafTpl: ""
        //nodeTpl: "..."
    });
}

TreeviewConfig.$inject = ["ivhTreeviewOptionsProvider"];

function initializationFunction(PermPermissionStore, $http) {
    let permissions = [];
    $http.get("/security/permission/userPermissions").then(function (result) {
        permissions = result.data;
        PermPermissionStore.defineManyPermissions(permissions, function (permissionName) {
            return _.includes(permissions, permissionName);
        });
    });
}

initializationFunction.$inject = ["PermPermissionStore", "$http"];

angular.module("lego.security").config(TreeviewConfig).run(initializationFunction);