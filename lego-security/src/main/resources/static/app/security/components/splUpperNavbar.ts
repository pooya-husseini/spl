/**
 * Created by mokaram on 08/07/2016.
 */


export class SplSecurityNavbarComponentController implements ng.IComponentController {
    // static $inject = ["user"];
    static $inject = ["user", "UserOperationsService"];
    private currentUser: lego.IUserModel;

    public constructor(public userService: lego.IUserService, public userOperationsService: any) {
        // public constructor(public userService: lego.IUserServicey) {
    }

    $onInit() {
        this.userService.getCurrentUser().then((success: lego.IUserModel) => {
            this.currentUser = success;
        })
    }

    openChangePassword() {
        this.userOperationsService.changePassword();
    }

    openChangeSetting() {

    }
}

function Component(): ng.IComponentOptions {
    return {
        controller: SplSecurityNavbarComponentController,
        require: {parent: "^ngController"},
        templateUrl: "/app/templates/upperNavbar.html",
        transclude: true
    }
}

angular.module("lego.security").component("splSecurityNavbar", Component());
