@lego.OperationService("UserOperationsController", "app/view/UserOperations.html")
export class UserOperationsService implements lego.IBasicCrudService<lego.IBasicModel> {

    @lego.OperationMethod()
    edit(data): ng.IPromise<any> {
        return null;
    }

    @lego.OperationMethod()
    show(data): ng.IPromise<any> {
        return null;
    }

    @lego.OperationMethod()
    changePassword(data): ng.IPromise<any> {
        return null;
    }

}