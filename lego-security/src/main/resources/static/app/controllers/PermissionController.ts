/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : Lego Code Generator
 * Email : husseini@caspco.ir
 * Date: Jul 28, 2016 5:35:01 PM
 * @version 2.5-SNAPSHOT
 */


@lego.Controller({
    type: lego.ControllerType.CRUD_CONTROLLER,
    requestMapping: "/security/permission",
    module: "lego.security"
})
export class PermissionController implements lego.ILegoController<lego.IBasicModel,lego.IGeneralActions> {

    restService: lego.ILegoRestServiceOperations<lego.IBasicModel>;

    @lego.Resource('$http')
    private $http;
    @lego.Resource('$q')
    private $q;
    @lego.Resource('$timeout')
    private $timeout;
    @lego.Resource('Utils')
    private utils;
    @lego.Resource('ivhTreeviewMgr')
    private ivhTreeviewMgr;
    public model: {}|lego.IBasicModel;
    public tree;
    public actions: lego.ISimpleActions;
    public result;
    public root;
    public permission = [];
    public accessedPermissions: any = [];
    public securityTreeMenu = [];
    public legoMenu = [];
    public securityMenu = [];
    public concatMenu = [];
    public concatTempMenu = [];
    public treeTemp;
    @lego.Resource('NotificationUtils')
    private NotificationUtils;
    private fullTreeaccessedPermissions;

    public constructor() {
        this.actions = new ControllerActions(this);
    };

    getConfig(): lego.IAbstractControllerConfig<lego.IBasicModel, lego.IGeneralActions> {

        return {
        };
    }


    get selectedRole() {
        return this.result;
    }

    set selectedRole(param) {
        if (param != "") {
            this.restService.fillTree({role: param}).$promise.then((permissions) => {
                this.accessedPermissions = [];
                this.fullTreeaccessedPermissions = [];
                this.securityTreeMenu = [];
                let t = JSON.parse(JSON.stringify(this.treeTemp));


                this.accessedPermissions = [];
                _.forEach(t, (branch) => {
                    this.accessedPermissions.push(this.enablePermission(branch, permissions));

                });


                this.tree = this.accessedPermissions;
                this.ivhTreeviewMgr.validate(this.tree, false);
                this.ivhTreeviewMgr.validate(this.concatMenu, false);
            })
        }
        this.result = param;
    }

    public gatheringMenuData() {

        this.$http.get('/app/conf/menu.json').success((data) => {
            this.legoMenu = this.menuDataGathering(data);
        });
        this.$http.get('/app/conf/security.menu.json').success((data) => {
            this.securityMenu = this.menuDataGathering(data);
            this.concatMenu = this.legoMenu.concat(this.securityMenu);
            this.concatTempMenu = JSON.parse(JSON.stringify(this.concatMenu));

        });
        this.$http.get('/app/gen-src/partial-menus').success((data) => {

        })

    }


    public loadStates(): any {
        let resultDefered = this.$q.defer();
        this.$http.get("app/conf/menu.json").then((data) => {
            let menuItems: Array<any> = <Array<any>> data.data;
            this.$http.get("app/gen-src/partial-menus").then((partials) => {
                    let paths = (<string>partials.data).split("\n");
                    let collectedMenus = [];
                    let promises = [];

                    _.forEach(paths, (p) => {
                        let menuPath = p.trim();
                        if (menuPath) {
                            let defered = this.$q.defer();
                            this.$http.get(p).then((success) => {
                                collectedMenus = collectedMenus.concat(success.data);
                                defered.resolve(collectedMenus);
                            });
                            promises.push(defered.promise);
                        }
                    });
                    this.$q.all(promises).then((collectedMenus) => {
                        let menuResult = [];
                        _.forEach(collectedMenus, (value) => {
                            menuResult = menuItems.concat(value);
                        })
                        resultDefered.resolve(menuResult);
                    });
                }
            );
        });

        return resultDefered.promise;
    }

    $onInit() {
        var self = this;
        this.loadStates().then((data) => {
            _.forEach(data, (value) => {
                self.concatMenu = self.concatMenu.concat(self.menuDataGathering(value));
                this.concatTempMenu = JSON.parse(JSON.stringify(this.concatMenu));
            })
        });
        // this.gatheringMenuData();
        this.restService.permissionTree().$promise.then((result) => {
            this.tree = this.translateTree(result);
            this.treeTemp = JSON.parse(JSON.stringify(result));
        })


        this.restService.getAllRoles().$promise.then((result) => {
            this.model.role = result;
        })

    }

    menuDataGathering(data) {
        var mnuResult = [];
        _.forEach(data.menuItems, (allMenu) => {
            let mnu = {label: "", value: "", children: []};

            mnu.label = this.utils.translate(allMenu.labelKey);
            mnu.value = allMenu.labelKey;
            let i = 0;
            _.forEach(allMenu.menuActions, (child) => {
                // mnu.children.push(child.labelKey);
                mnu.children[i] = {};
                mnu.children[i].label = this.utils.translate(child.labelKey);
                mnu.children[i].value = mnu.value + "." + child.labelKey;
                i++;
            });
            mnuResult.push(mnu);
        });
        return mnuResult;
    }


    enablePermission(tree, permissionsForRole) {
        if (tree.children == undefined || tree.children.length == 0) {
            if (permissionsForRole.length != 0) {
                for (var item in permissionsForRole) {
                    if (tree.value == permissionsForRole[item]) {
                        tree.selected = true;
                    }
                }
            }
        }
        if (tree.children != undefined && tree.children.length != 0) {
            for (var child in tree.children) {
                this.enablePermission(tree.children[child], permissionsForRole);
            }
            return tree;
        }
    };

    selectedPermission(object, selectedPermissions: any[]) {
        if (object.selected != undefined && object.selected == true && (object.children == undefined || object.children.length == 0)) {
            selectedPermissions.push(object.value);
        }
        if (object.children != undefined && object.children.length != 0) {
            for (var child in object.children) {
                this.selectedPermission(object.children[child], selectedPermissions);
            }
            return selectedPermissions;
        }
    };

    addRole() {
        this.restService.add(this.model.newRole).$promise.then(() => {
            this.NotificationUtils.notifySuccess();
            this.model.newRole = '';
            this.loadRoles();
        });
    };

    deleteRole() {
        var selectedRow = this.selectedRole;
        this.restService.delete({id: selectedRow}).$promise.then((result) => {
            this.selectedRole = '';
            this.loadRoles();
        });
    };

    loadRoles() {
        this.restService.getAllRoles().$promise.then((result) => {
            if (!this.model) {
                this.model = {};
            }
            this.model.role = result;
        }, function (error) {
            console.log(error);
        });
    }

    create() {
        this.root = this.tree;
        var resultTree = [];
        var temp = [];
        _.forEach(this.root, ((value) => {
            temp = this.selectedPermission(value, []);
            resultTree = resultTree.concat(temp);
        }));
        if (this.result != undefined && this.result != '') {

            this.restService.addNewPermissionsForRole({role: this.result}, resultTree).$promise.then(() => {
                    this.NotificationUtils.notifySuccess();
                    this.selectedRole = '';
                }
            )
        }
    }

    translateTree(tree) {
        var translateResult = [];
        _.forEach(tree, (branch) => {
            branch.label = this.utils.translate(branch.value);
            translateResult.push(this.translateBranch(branch));
        });
        return translateResult;
    }

    translateBranch(object) {
        if (object.children != undefined) {
            for (var item in object.children) {
                object.children[item].label = this.utils.translate(object.children[item].value);
                this.translateBranch(object.children[item]);
            }
        }
        if (object.label != undefined) {
        }
        return object;
    }


}

class ControllerActions extends lego.LegoBasicActions <lego.IBasicModel> {
    constructor(public ctrl:PermissionController) {
        super()
    }


}