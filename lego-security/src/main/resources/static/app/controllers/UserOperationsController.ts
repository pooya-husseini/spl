import {UserOperationsService} from "../services/operations/UserOperationsService";
/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : Lego Code Generator
 * Email : husseini@caspco.ir
 * Date: Oct 26, 2016 6:11:43 PM
 * @version 2.7.1-SNAPSHOT
 */


@lego.Controller({
    type: lego.ControllerType.OPERATION_CONTROLLER,
    requestMapping:{ users:"/users"}
})
export class UserOperationsController implements lego.ILegoController<lego.IBasicModel,lego.ISimpleActions> {

    getConfig(): lego.IAbstractControllerConfig<lego.IBasicModel, lego.ISimpleActions> {
        let gridConfig = {
            data: this.model,
            columnDefs: [
                {
                    field: 'id',
                    displayName: 'User.id',
                    headerCellFilter: 'translate'

                    , visible: false
                },
                {
                    field: 'firstName',
                    displayName: 'User.firstName',
                    headerCellFilter: 'translate'

                },
                {
                    field: 'lastName',
                    displayName: 'User.lastName',
                    headerCellFilter: 'translate'

                },
                {
                    field: 'password',
                    displayName: 'User.password',
                    headerCellFilter: 'translate',
                    visible: false
                },
                {
                    field: 'username',
                    displayName: 'User.username',
                    headerCellFilter: 'translate'

                },
                {
                    field: 'role',
                    displayName: 'User.role',
                    headerCellFilter: 'translate'

                    , cellFilter: 'GeneralParameter:"USER_ROLE"'
                },
            ],
            onDoubleClick: (value) => {
            }
        };
        return {gridConf: gridConfig};
    }

    @lego.Resource()
    private $http;
    public passwordAgain: any;

    @lego.OperationValue()
    private data;

    @lego.Inject()
    private userService: UserOperationsService;

    public NotificationUtils: any;
    public Utils: any;
    model: {}|lego.IBasicModel;
    actions: lego.ISimpleActions;
    restService;
    @lego.Resource()
    public $state;

    public constructor() {
        this.actions = new ControllerActions(this);
    }

    beforeInit() {
        if (this.data) {
            this.model = this.data;
        }
    }

    $onInit() {

    }

    public getCurrentUser() {
        this.$http.get("/login/user").then((success: any) => {
            this.model.username = success.data.username;
        })
    }
}

class ControllerActions extends lego.LegoBasicModalActions<lego.IBasicModel> {

    constructor(public ctrl:UserOperationsController) {
        super()
    }

    changePassword = () => {
        this.ctrl.getCurrentUser();
        let dto = {
            oldPassword: this.ctrl.model.oldPassword,
            password: this.ctrl.model.password,
            username: this.ctrl.model.username
        };
        if(this.ctrl.model.oldPassword && this.ctrl.model.password && this.ctrl.passwordAgain){
            this.ctrl.restService.users.changePassword(dto).$promise.then(() => {
                this.ctrl.NotificationUtils.notifySuccess();
                this.reset();
                this.ctrl.$state.reload();
            }, () => {
                // this.NotificationUtils.notifyFailed();

            });
        }else{
            this.ctrl.NotificationUtils.notify('UserSetting.fillAll', lego.NotificationTypes.DANGER);
        }

    };

    reset(){
        super.reset();
        this.ctrl.passwordAgain='';
    }

}