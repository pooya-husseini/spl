/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : Lego Code Generator
 * Email : husseini@caspco.ir
 * Date: Oct 26, 2016 6:11:43 PM
 * @version 2.7.1-SNAPSHOT
 */


@lego.Controller({
    type: lego.ControllerType.CRUD_CONTROLLER,
    requestMapping: {User: "/users"}
})
export class UserSettingController implements lego.ILegoController<lego.IBasicModel,lego.IGeneralActions>,lego.IGeneralActions {

    model: {}|lego.IBasicModel;
    actions: lego.ISimpleActions;
    public Utils: lego.IUtils;
    public NotificationUtils: lego.INotificationUtils;
    public restService;
    @lego.Resource()
    private $http;
    public beforeChange: any;
    public passwordAgain: any;
    @lego.Resource()
    public $state;

    public constructor() {

        this.actions = new ControllerActions(this);
    }

    getConfig(): lego.IAbstractControllerConfig<lego.IBasicModel, lego.ISimpleActions> {
        let gridConf: lego.ILegoGridOptions = {}
        return {

            restService: this.restService.User,
            gridConf: gridConf
        };
    }

    $onInit() {
        this.getCurrentUser();

    }

    public getCurrentUser() {
        this.$http.get("/login/user").then((success: any) => {
            this.beforeChange = success.data;
            this.model.firstName = success.data.firstName;
            this.model.lastName = success.data.lastName;
            this.model.username = success.data.username;
        })
    }

}
class ControllerActions extends lego.LegoBasicActions<lego.IBasicModel> {

    constructor(public ctrl: UserSettingController) {
        super()
    }

    changePassword() {
        let dto = {
            oldPassword: this.ctrl.model.oldPassword,
            password: this.ctrl.model.password,
            username: this.ctrl.model.username
        };
        if (this.ctrl.model.oldPassword && this.ctrl.model.password && this.ctrl.passwordAgain) {
            this.ctrl.restService.User.changePassword(dto).$promise.then(
                //onSuccess
                () => {
                    this.ctrl.NotificationUtils.notifySuccess();
                    this.reset();
                    this.ctrl.$state.reload();
                },
                //onFailure
                () => {
                    // this.ctrl.NotificationUtils.notifyFailed();
                });
        }
        else {
            this.ctrl.NotificationUtils.notify('UserSetting.fillAll', lego.NotificationTypes.DANGER);
        }
    }

    changeSetting() {
        if (this.ctrl.model.firstName && this.ctrl.model.lastName) {
            let dto = {
                firstName: this.ctrl.model.firstName,
                lastName: this.ctrl.model.lastName,
                username: this.ctrl.model.username
            };

            if (this.ctrl.model.firstName != this.ctrl.beforeChange.firstName || this.ctrl.model.lastName != this.ctrl.beforeChange.lastName) {
                this.ctrl.restService.User.changeSetting(dto).$promise.then(
                    //onSuccess
                    () => {
                        this.ctrl.NotificationUtils.notifySuccess();
                        this.ctrl.NotificationUtils.notify('UserSetting.exit', lego.NotificationTypes.SUCCESS);
                        //onFailure
                    }, () => {
                        this.ctrl.NotificationUtils.notifyFailed();
                    });
            }
            else {
                this.ctrl.NotificationUtils.notify('UserSetting.noChange', lego.NotificationTypes.WARNING);
            }
        }
    }

    reset(){
        super.reset();
        this.ctrl.passwordAgain='';
    }

}