/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : Lego Code Generator
 * Email : husseini@caspco.ir
 * Date: Oct 26, 2016 6:11:43 PM
 * @version 2.7.1-SNAPSHOT
 */


import {UserOperationsService} from "../services/operations/UserOperationsService";

@lego.Controller({
    type: lego.ControllerType.CRUD_CONTROLLER,
    requestMapping: {
        User: "/users"
    }
})
export class UserController implements lego.ILegoController<lego.IBasicModel,lego.IGeneralActions>,lego.IGeneralActions {

    @lego.Inject()
    public userService: UserOperationsService;
    public model: {}|lego.IBasicModel;
    public actions: lego.IGeneralActions;
    public mainGrid: lego.ILegoGridOptions;
    public Utils: lego.IUtils;
    public restService;
    public NotificationUtils;
    public passwordAgain;

    public constructor() {
        this.actions = new ControllerActions(this);
    };

    getConfig(): lego.IAbstractControllerConfig<lego.IBasicModel, lego.IGeneralActions> {

        let gridConf: lego.ILegoGridOptions = {

            columnDefs: [
                {
                    field: 'id',
                    displayName: 'User.id',
                    headerCellFilter: 'translate'

                    , visible: false
                },
                {
                    field: 'firstName',
                    displayName: 'User.firstName',
                    headerCellFilter: 'translate'

                },
                {
                    field: 'lastName',
                    displayName: 'User.lastName',
                    headerCellFilter: 'translate'

                },
                {
                    field: 'password',
                    displayName: 'User.password',
                    headerCellFilter: 'translate',
                    visible: false
                },
                {
                    field: 'username',
                    displayName: 'User.username',
                    headerCellFilter: 'translate'

                },
                {
                    field: 'role',
                    displayName: 'User.role',
                    headerCellFilter: 'translate'

                    , cellFilter: 'GeneralParameter:"USER_ROLE"'
                },
            ]
        };
        return {
            crudService: this.userService,
            restService: this.restService.User,
            gridConf: gridConf
        };
    }

    $onInit() {

    }


}
class ControllerActions extends lego.LegoBasicActions <lego.IBasicModel> {
    constructor(public ctrl:UserController) {
        super()
    }

    create(){
        super.create();
        this.reset();
    }

    reset(){
        super.reset();
        this.ctrl.passwordAgain='';
    }
}
