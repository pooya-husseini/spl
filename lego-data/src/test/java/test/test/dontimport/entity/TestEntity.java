package test.test.dontimport.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 09/12/14
 *         Time: 10:02
 */

@Entity(name = "TestEntity")
@Table(name = "TestTable")
@NamedQueries({
        @NamedQuery(query = "update TestEntity a set a.mainType='Updated' where a.id= :id", name = "UpdateMainType"),
        @NamedQuery(query = "from TestEntity a where a.id > :id", name = "Select1"),
        @NamedQuery(query = "from TestEntity a where a.id > :id and a.code= :code", name = "Select2"),
        @NamedQuery(query = "select a from TestEntity a join a.children c where  c.code = :code", name = "Select3"),
})
@NamedNativeQueries({
        @NamedNativeQuery(query = "UPDATE DropIt a SET a.mainType='Updated' WHERE a.id= :id", name = "NativeUpdateMainType"),
        @NamedNativeQuery(query = "SELECT * FROM DropIt WHERE id > :id", name = "NativeSelect1"),
        @NamedNativeQuery(query = "SELECT * FROM DropIt a WHERE a.id > :id AND a.code= :code", name = "NativeSelect2"),
})
public class TestEntity {
    private Long id;
    @NotNull
    private String mainType;
    @NotNull
    private String code;
    @NotNull
    private String description;

    private List<TestEntity> children;
//    private List<TestEntity> parents;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Column
    public String getCode() {
        return this.code;
    }

    public void setCode(final String code) {
        this.code = code;
    }


    @Column
    public String getDescription() {

        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Column
    public String getMainType() {
        return this.mainType;
    }

    public void setMainType(final String mainType) {
        this.mainType = mainType;
    }

    @ManyToMany(cascade = CascadeType.ALL)
    @Column
    public List<TestEntity> getChildren() {
        return children;
    }

//    @ManyToMany(cascade = CascadeType.ALL)
//    @Column
//    public List<TestEntity> getParents() {
//        return parents;
//    }

    public void setChildren(List<TestEntity> children) {
        this.children = children;
    }
//    public void setParents(List<TestEntity> parents) {
//        this.parents = parents;
//    }

    @Override
    public String toString() {
        return "TestEntity{" +
                "id=" + id +
                ", mainType='" + mainType + '\'' +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", children=" + children +

                '}';
    }
}