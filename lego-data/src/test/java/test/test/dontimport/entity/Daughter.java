package test.test.dontimport.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 09/12/14
 *         Time: 10:02
 */

@Entity
@Table(name = "Daughter")
@DiscriminatorValue("2")
public class Daughter extends Parent{

//    private Long id;
    private String name;

//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column
//    public Long getId() {
//        return this.id;
//    }
//
//    public void setId(final Long id) {
//        this.id = id;
//    }

    @Column
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Daughter{" +
                "name='" + name + '\'' +
                '}';
    }
}