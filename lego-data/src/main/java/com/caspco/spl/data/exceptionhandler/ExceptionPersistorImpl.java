package com.caspco.spl.data.exceptionhandler;

import com.caspco.spl.data.exceptionhandler.repository.LogRepository;
import com.caspco.spl.wrench.exceptionhandler.ExceptionPersister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/24/17
 *         Time: 11:18 AM
 */

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ExceptionPersistorImpl implements ExceptionPersister {

    @Autowired
    private LogRepository logRepository;

    @Override
    public void persist(Exception e) {
        LogEntity logEntity = new LogEntity();
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        e.printStackTrace(pw);
        logEntity.setTime(new Date());
        logEntity.setExceptionMessage(sw.getBuffer().toString());
        logRepository.save(logEntity);
    }
}
