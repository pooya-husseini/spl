package com.caspco.spl.data.naming;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitJoinColumnNameSource;
import org.hibernate.internal.util.StringHelper;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 7/16/17
 *         Time: 5:27 PM
 */
public class SplRelationalImplicitNamingStrategy extends SpringImplicitNamingStrategy {
    @Override
    public Identifier determineJoinColumnName(ImplicitJoinColumnNameSource source) {
        final String name;

        if (source.getNature() == ImplicitJoinColumnNameSource.Nature.ELEMENT_COLLECTION
                || source.getAttributePath() == null) {
            String text = source.getReferencedTableName().getText();
            if (StringHelper.isNotEmpty(text) && !text.equalsIgnoreCase(source.getEntityNaming().getJpaEntityName())) {
                name = text
                        + '_'
                        + source.getReferencedColumnName().getText();
            } else {
                name = transformEntityName(source.getEntityNaming())
                        + '_'
                        + source.getReferencedColumnName().getText();
            }


        } else {
            name = transformAttributePath(source.getAttributePath())
                    + '_'
                    + source.getReferencedColumnName().getText();
        }
        return toIdentifier(name, source.getBuildingContext());
    }
}
