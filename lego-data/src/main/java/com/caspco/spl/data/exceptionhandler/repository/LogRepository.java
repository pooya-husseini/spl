package com.caspco.spl.data.exceptionhandler.repository;


import com.caspco.spl.data.beans.SplRelationalRepository;
import com.caspco.spl.data.exceptionhandler.LogEntity;

/**
 * THIS IS AN AUTO GENERATED CODE.
 *
 * @author : XVIEW CODE GENERATOR
 *         Email : husseini@caspian.com
 *         Date: Nov 24, 2014 2:13:05 PM
 * @version 1.0.0
 */


public interface LogRepository extends SplRelationalRepository<LogEntity, Long> {

}