//package com.caspco.spl.wrench.utils.reflection;
//
//import com.caspco.spl.wrench.annotations.cg.Searchable;
//import org.junit.Test;
//import org.springframework.util.Assert;
//
//import javax.persistence.GeneratedValue;
//import javax.persistence.Id;
//import java.lang.annotation.Annotation;
//import java.lang.reflect.Method;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.List;
//import java.util.stream.Collectors;
//
///**
// * The type Class utils test.
// *
// * @author : Pooya Hosseini         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com         Date: 3/6/16         Time: 3:54 PM
// */
//public class ClassUtilsTest {
//
//    private ClassUtils classUtils = new ClassUtils(TestClass.class);
//
//
//    /**
//     * Test find field.
//     *
//     * @throws Exception the exception
//     */
//    @Test
//    public void testFindField() throws Exception {
//        Assert.notNull(classUtils.findField("name"));
//    }
//
//    /**
//     * Test get property descriptors.
//     *
//     * @throws Exception the exception
//     */
//    @Test
//    public void testGetPropertyDescriptors() throws Exception {
//        Assert.isTrue(classUtils.getPropertyDescriptors().count() == 3);
//    }
//
//    /**
//     * Test get property descriptor.
//     *
//     * @throws Exception the exception
//     */
//    @Test
//    public void testGetPropertyDescriptor() throws Exception {
//        Assert.notNull(classUtils.getPropertyDescriptor("name"));
//    }
//
//    /**
//     * Test get property annotations.
//     *
//     * @throws Exception the exception
//     */
//    @Test
//    public void testGetPropertyAnnotations() throws Exception {
//        List<Class<? extends Annotation>> classes = classUtils.getPropertyAnnotations("id").map(Annotation::annotationType).collect(Collectors.toList());
//        Assert.isTrue(!classes.retainAll(Arrays.asList(Id.class, GeneratedValue.class)));
//    }
//
//    /**
//     * Test get property annotation.
//     *
//     * @throws Exception the exception
//     */
//    @Test
//    public void testGetPropertyAnnotation() throws Exception {
//        Annotation id = classUtils.getPropertyAnnotation("id", Id.class);
//        Assert.notNull(id);
//    }
//
//    /**
//     * Test get property bound methods.
//     *
//     * @throws Exception the exception
//     */
//    @Test
//    public void testGetPropertyBoundMethods() throws Exception {
//        List<String> strings = classUtils.getPropertyBoundMethods("id").map(Method::getName).collect(Collectors.toList());
//        Assert.isTrue(!strings.retainAll(Arrays.asList("getId", "setId")));
//    }
//
//    /**
//     * Test is property annotated with.
//     *
//     * @throws Exception the exception
//     */
//    @Test
//    public void testIsPropertyAnnotatedWith() throws Exception {
//        Assert.isTrue(classUtils.isPropertyAnnotatedWith("id", Id.class));
//        Assert.isTrue(!classUtils.isPropertyAnnotatedWith("id", Searchable.class));
//    }
//
//    /**
//     * Test is property annotated with any.
//     *
//     * @throws Exception the exception
//     */
//    @Test
//    public void testIsPropertyAnnotatedWithAny() throws Exception {
//        Assert.isTrue(classUtils.isPropertyAnnotatedWithAny("id", Arrays.asList(Id.class, Searchable.class)));
//        Assert.isTrue(!classUtils.isPropertyAnnotatedWithAny("id", Collections.singletonList(Searchable.class)));
//    }
//
//    /**
//     * Test is property annotated with all.
//     *
//     * @throws Exception the exception
//     */
//    @Test
//    public void testIsPropertyAnnotatedWithAll() throws Exception {
//        Assert.isTrue(!classUtils.isPropertyAnnotatedWithAll("id", Arrays.asList(Id.class, Searchable.class)));
//        Assert.isTrue(!classUtils.isPropertyAnnotatedWithAll("id", Collections.singletonList(Searchable.class)));
//        Assert.isTrue(classUtils.isPropertyAnnotatedWithAll("id", Arrays.asList(Id.class, GeneratedValue.class)));
//    }
//
//    /**
//     * Test find properties annotated with all.
//     *
//     * @throws Exception the exception
//     */
//    @Test
//    public void testFindPropertiesAnnotatedWithAll() throws Exception {
//        Assert.isTrue(classUtils.findPropertiesAnnotatedWithAll(Arrays.asList(Id.class, Searchable.class)).count() == 0);
//        Assert.isTrue(classUtils.findPropertiesAnnotatedWithAll(Arrays.asList(Id.class, GeneratedValue.class)).count() == 1);
//    }
//
//    /**
//     * Test find properties annotated with.
//     *
//     * @throws Exception the exception
//     */
//    @Test
//    public void testFindPropertiesAnnotatedWith() throws Exception {
//        Assert.isTrue(classUtils.findPropertiesAnnotatedWith(Id.class).count() == 1);
//        Assert.isTrue(classUtils.findPropertiesAnnotatedWith(GeneratedValue.class).count() == 1);
//        Assert.isTrue(classUtils.findPropertiesAnnotatedWith(Searchable.class).count() == 2);
//    }
//
//    /**
//     * Test find properties annotated with any.
//     *
//     * @throws Exception the exception
//     */
//    @Test
//    public void testFindPropertiesAnnotatedWithAny() throws Exception {
//        Assert.isTrue(classUtils.findPropertiesAnnotatedWithAny(Arrays.asList(Id.class, Searchable.class)).count() == 3);
//        Assert.isTrue(classUtils.findPropertiesAnnotatedWithAny(Arrays.asList(Id.class, GeneratedValue.class)).count() == 1);
//    }
//
//
//    /**
//     * The type Test class.
//     */
//    class TestClass {
//        private Long id;
//        private String name;
//        private String family;
//
//        /**
//         * Getter for property 'id'.
//         *
//         * @return Value for property 'id'.
//         */
//
//        @Id
//        @GeneratedValue
//        public Long getId() {
//            return id;
//        }
//
//        /**
//         * Setter for property 'id'.
//         *
//         * @param id Value to set for property 'id'.
//         */
//        public void setId(Long id) {
//            this.id = id;
//        }
//
//        /**
//         * Getter for property 'name'.
//         *
//         * @return Value for property 'name'.
//         */
//        @Searchable
//        public String getName() {
//            return name;
//        }
//
//        /**
//         * Setter for property 'name'.
//         *
//         * @param name Value to set for property 'name'.
//         */
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        /**
//         * Getter for property 'family'.
//         *
//         * @return Value for property 'family'.
//         */
//        @Searchable
//        public String getFamily() {
//            return family;
//        }
//
//        /**
//         * Setter for property 'family'.
//         *
//         * @param family Value to set for property 'family'.
//         */
//        public void setFamily(String family) {
//            this.family = family;
//        }
//    }
//}