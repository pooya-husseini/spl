package com.caspco.spl.async;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 7/25/17
 * Time: 4:30 PM
 */

@Import(AsyncConfiguration.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableSplAsync {
}
