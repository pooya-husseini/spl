package com.caspco.spl.async;

import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 7/25/17
 * Time: 4:21 PM
 */
@EnableAsync
//@Configuration
public class AsyncConfiguration extends AsyncConfigurerSupport {
    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(100);
        executor.setMaxPoolSize(1000);
        executor.setQueueCapacity(100000);
        executor.setThreadNamePrefix("Spl-");
        executor.initialize();
        return executor;
    }
}