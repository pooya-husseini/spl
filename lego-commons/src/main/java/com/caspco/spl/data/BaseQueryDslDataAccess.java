package com.caspco.spl.data;

import com.caspco.spl.model.annotations.cg.EnableFilter;
import com.caspco.spl.model.annotations.cg.ExactOnSearch;
import com.caspco.spl.model.data.ActiveableEntity;
import com.caspco.spl.wrench.repository.SplRepository;
import com.caspco.spl.wrench.utils.date.DateRange;
import com.caspco.spl.wrench.utils.reflection.ClassUtils;
import com.caspco.spl.wrench.utils.reflection.PropertyMetadata;
import com.caspco.spl.wrench.utils.reflection.TypeUtils;
import com.google.common.collect.Lists;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 7/19/17
 *         Time: 5:46 PM
 */
public interface BaseQueryDslDataAccess<T, ID extends Serializable> extends SplRepository<T,ID>  ,QueryDslPredicateExecutor<T> {
//    default AbstractJPAQuery<T, JPAQuery<T>> select() {
//        return new Querydsl(getEntityManager(), builder).createQuery();
//    }
//
//    public T getEntityAtVersion(Number revision) {
//        return (T) AuditReaderFactory.get(em).createQuery().forEntitiesAtRevision(typeArgument, revision).add(AuditEntity.revisionNumber().eq(revision)).getSingleResult();
//    }
//
//    public Number getLastRevisionNumber() {
//        return (Number) AuditReaderFactory.get(em).createQuery().forRevisionsOfEntity(typeArgument, true, true).addProjection(AuditEntity.revisionNumber().max()).getSingleResult();
//    }
//
//    @Override
//    public Number getLastDeletedRevisionNumber() {
//        return (Number) AuditReaderFactory.get(em).createQuery().forRevisionsOfEntity(typeArgument, true, true).add(AuditEntity.revisionType().eq(RevisionType.DEL)).addProjection(AuditEntity.revisionNumber().max()).getSingleResult();
//    }
//
//    @Override
//    public Number getLastModifiedRevisionNumber() {
//        return (Number) AuditReaderFactory.get(em).createQuery().forRevisionsOfEntity(typeArgument, true, true).add(AuditEntity.revisionType().eq(RevisionType.MOD)).addProjection(AuditEntity.revisionNumber().max()).getSingleResult();
//    }
//
//    @Override
//    public Number getLastInsertedRevisionNumber() {
//        return (Number) AuditReaderFactory.get(em).createQuery().forRevisionsOfEntity(typeArgument, true, true).add(AuditEntity.revisionType().eq(RevisionType.ADD)).addProjection(AuditEntity.revisionNumber().max()).getSingleResult();
//    }

//    @Override
//    public List<HistoricalModel> getRevisionNumberModificationType(Number revision) {
//        List<Object> result = AuditReaderFactory.get(em).createQuery().forRevisionsOfEntity(typeArgument, true, true).add(AuditEntity.revisionNumber().eq(revision)).addProjection(AuditEntity.revisionType()).addProjection(AuditEntity.id()).getResultList();
//        List<HistoricalModel> historicalEntities = new ArrayList<>();
//        for (Object o : result) {
//            if (o instanceof Object[]) {
//                Object[] objects = (Object[]) o;
//                if (objects[0] instanceof RevisionType) {
//                    RevisionType revisionType = (RevisionType) objects[0];
//                    HistoricalModel.ModificationType modificationType = null;
//                    switch (revisionType) {
//                        case ADD:
//                            modificationType = HistoricalModel.ModificationType.INSERTED;
//                            break;
//                        case MOD:
//                            modificationType = HistoricalModel.ModificationType.MODIFIED;
//                            break;
//                        case DEL:
//                            modificationType = HistoricalModel.ModificationType.DELETED;
//                            break;
//                    }
//                    historicalEntities.add(new HistoricalModel(objects[1], modificationType));
//                }
//            }
//        }
//        return historicalEntities;
//    }

    default List<T> findActives(T e, OrderSpecifier<?>... orders) {
        if (e instanceof ActiveableEntity) {
            ((ActiveableEntity) e).setActive(true);
        }
        return Lists.newArrayList(findAll(makeEqualExpression(e, true), orders));
    }

    default List<T> findActives(T e) {
        if (e instanceof ActiveableEntity) {
            ((ActiveableEntity) e).setActive(true);
        }
        return Lists.newArrayList(findAll(makeEqualExpression(e, true)));
    }

    default List<T> find(T e) {
        BooleanExpression predicate = makeEqualExpression(e, false);
//        if (predicate == null) {
//            if (e.getClass() != this.typeArgument) {
//                String name = e.getClass().getName();
//                int index = name.lastIndexOf(".");
//                String clazzName = name.substring(index + 1);
//                name = name.substring(0, index) + ".Q" + clazzName;
//                String varName = (clazzName.substring(0, 1).toLowerCase() + clazzName.substring(1));
//                try {
//                    Class<?> clazz = Class.forName(name);
//                    EntityPathBase o = (EntityPathBase) clazz.getField(varName).get(null);
//
//                    PathBuilder projection = new PathBuilder(e.getClass(), resolver.createPath(e.getClass()).getMetadata());
//                    return createQuery(predicate).join(projection).on().list(projection);
////                    return (List<T>) createQuery(predicate).list(projection);
//                } catch (Exception e1) {
//                    throw new RuntimeException(e1);
//                }
//            }
//        }

        return Lists.newArrayList(findAll(predicate));
    }

    default List<T> find(T e, OrderSpecifier<?>... orders) {
        BooleanExpression predicate = makeEqualExpression(e, false);
        return Lists.newArrayList(findAll(predicate, orders));
    }

    default BooleanExpression makeEqualExpression(Object e, Boolean filterOnlyActives) {
        return makeEqualExpression(null, e, filterOnlyActives);
    }

    default BooleanExpression makeEqualExpression(String prefix, Object e, Boolean filterOnlyActives) {
        Map<String, Object> tempMap = TypeUtils.INSTANCE.objectToMap(e);
        Map<String, Object> map = enhanceObjectToMap(e, tempMap);

        if (map.isEmpty()) {
            map.putAll(tempMap);
        }
        Deque<Map.Entry<String, Object>> entries = new ArrayDeque<>(map.entrySet());
        BooleanExpression expression = null;
        if (entries.size() > 0) {
            Map.Entry<String, Object> entry = entries.pop();
            expression = getBooleanExpression(e.getClass(), prefix, entry, filterOnlyActives);
            while ((entry = entries.poll()) != null) {
                if (expression != null) {
                    expression = expression.and(getBooleanExpression(e.getClass(), prefix, entry, filterOnlyActives));
                } else {
                    expression = getBooleanExpression(e.getClass(), prefix, entry, filterOnlyActives);
                }
            }
        }
        return expression;
    }


    default Map<String, Object> enhanceObjectToMap(Object e, Map<String, Object> objectMap) {
        Map<String, Object> map = new HashMap<>();
        ClassUtils resolveType = ClassUtils.create(e.getClass());
        resolveType
                .filterProperties(i ->
                        i.isAnnotatedWith(Id.class) && objectMap.containsKey(i.getName())
                ).stream().findFirst().ifPresent(i -> {
                    resolveType
                            .filterProperties(j ->
                                    j.isAnnotatedWithAny(ManyToMany.class, ManyToOne.class, OneToOne.class, OneToMany.class))
                            .forEach(j -> {
                                if (objectMap.containsKey(j.getName())) {
                                    map.put(j.getName(), objectMap.get(j.getName()));
                                }
                            });
                    map.put(i.getName(), objectMap.get(i.getName()));
                }
        );

        return map;
    }

    default BooleanExpression getBooleanExpression(Class<?> type, String prefix, Map.Entry<String, Object> entry, Boolean filterOnlyActives) {
        return getBooleanExpression(type, prefix, entry.getKey(), entry.getValue(), filterOnlyActives);
    }

    default BooleanExpression getBooleanExpression(Class<?> type, String prefix, String name, Object value, Boolean filterOnlyActives) {
        String propertyName = (prefix == null || prefix.isEmpty() ? "" : prefix + ".") + name;
        BooleanExpression expression = null;
        ClassUtils classUtils = ClassUtils.create(type);
        PropertyMetadata propertyMetadata = classUtils.findPropertyMetadata(name);
        if (propertyMetadata.isAnnotatedWithAny(ManyToMany.class, OneToMany.class)) {
            Iterable<?> iterable = (Iterable<?>) value;
            ArrayDeque<Object> entries = new ArrayDeque<>(Lists.newArrayList(iterable));
            if (entries.size() > 0) {
                Object entry = entries.pop();
                expression = makeEqualExpression(propertyName, entry, filterOnlyActives);
                while ((entry = entries.poll()) != null) {
                    expression = expression.or(makeEqualExpression(propertyName, entry, filterOnlyActives));
                }
            }
        } else if (propertyMetadata.isAnnotatedWithAny(ManyToOne.class, OneToOne.class)) {
            expression = getBooleanExpression(value, propertyName, value, filterOnlyActives);
        } else if (propertyMetadata.isAnnotatedWith(EnableFilter.class)) {
            if (filterOnlyActives) {
                expression = this.getBuilder().get(propertyName, Boolean.class).eq(true);
            }
        } else if (propertyMetadata.isAnnotatedWith(ExactOnSearch.class)) {

            expression = this.getBuilder().get(propertyName).eq(value);
        } else {
            if (value instanceof Date) {
                Date date = (Date) value;
                DateRange dateRange = new DateRange(date);
                expression = this.getBuilder().getDate(propertyName, Date.class).between(dateRange.getDayStart(), dateRange.getDayEnd());
            } else if (value instanceof String) {
                String s = (String) value;
                if(this.getConfig().isMakeStringQueryLower()) {
                    expression = this.getBuilder().getString(propertyName).lower().like("%" + s.toLowerCase() + "%");
                }else{
                    expression = this.getBuilder().getString(propertyName).like("%" + s + "%");
                }
            } else {
                expression = this.getBuilder().get(propertyName).eq(value);
            }
        }
        return expression;
    }

    PathBuilder<T> getBuilder();

    default BooleanExpression getBooleanExpression(Object value, String propertyName, Object item, Boolean filterOnlyActives) {
        BooleanExpression expression = null;
        Map<String, Object> tempMap = TypeUtils.INSTANCE.objectToMap(item);
        Map<String, Object> map = enhanceObjectToMap(item, tempMap);

        if (map.isEmpty()) {
            map.putAll(tempMap);
        }

        Deque<Map.Entry<String, Object>> entries = new ArrayDeque<>(map.entrySet());
        if (entries.size() > 0) {
            Map.Entry<String, Object> entry = entries.pop();
            expression = getBooleanExpression(item.getClass(), propertyName, entry.getKey(), entry.getValue(), filterOnlyActives);

            while ((entry = entries.poll()) != null) {
                expression = expression.and(getBooleanExpression(value.getClass(), propertyName, entry.getKey(), entry.getValue(), filterOnlyActives));
            }
        }
        return expression;
    }

    default boolean exists(T entity) {
        return this.exists(makeEqualExpression(entity, false));
//        return select().from(path).where(makeEqualExpression(entity, false)).exists();
    }

    default long count(T entity) {
        return this.count(makeEqualExpression(entity, false));
    }


//    private <S extends T> S save(S entity, boolean resetActivateField) {
//        setPrePersist(entity);
//        if (resetActivateField) {
//            ID id = (ID) idField.getValue(entity);
//            if (id != null && entity instanceof ActiveableEntity) {
//                T original = this.findOne(id);
//                ((ActiveableEntity) entity).setActive(((ActiveableEntity) original).getActive());
//            }
//            return super.save(entity);
//        } else {
//            setPrePersist(entity);
//            return super.save(entity);
//        }
//    }

    default List<String> searchColumn(String column, String value) {
//        StringPath string = builder.getString(column);
//        return this.select().distinct().where(string.like(value));
//        return select().where(string.like(value)).distinct().list(string);

        return null;
    }

    SplQueryDslConfig getConfig();

    Class<T> getTypeArgument();
}
