package com.caspco.spl.data;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 7/19/17
 *         Time: 7:26 PM
 */
public class SplQueryDslConfig {
    private final boolean makeStringQueryLower;

    public SplQueryDslConfig(boolean makeStringQueryLower) {
        this.makeStringQueryLower = makeStringQueryLower;
    }

    public boolean isMakeStringQueryLower() {
        return makeStringQueryLower;
    }
}
