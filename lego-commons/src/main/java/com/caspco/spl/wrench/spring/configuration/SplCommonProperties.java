package com.caspco.spl.wrench.spring.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 7/5/17
 *         Time: 8:21 PM
 */
@ConfigurationProperties(prefix = "spl.common")
@Component
public class SplCommonProperties {
    private String scanPackage;

    public String getScanPackage() {
        return scanPackage;
    }

    public void setScanPackage(String scanPackages) {
        this.scanPackage = scanPackages;
    }
}