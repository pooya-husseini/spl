package com.caspco.spl.wrench.services;

import java.io.Serializable;
import java.util.List;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/24/17
 *         Time: 11:27 AM
 */
public interface SplService<E, K extends Serializable> {

    Boolean isEmpty();

    E save(E obj);

    E findById(K id);

    List<E> findBySample(E e);

    List<E> findAll();

    Boolean exists(K id);

    Boolean exists(E e);

    Long count(E e);

    Long countAll();

    // delete operations
    void removeById(K id);

    void remove(E e);

    void removeAll();

    List<String> searchColumn(String column, String value);

    E changeActiveStatus(E entity);

    /**
     * Getter for property 'modelType'.
     *
     * @return Value for property 'modelType'.
     */
    Class<E> getModelType();

    Boolean isActivable();

    Boolean isActive(E e);

    List<E> findActivesBySample(E e);
}
