package com.caspco.spl.wrench.utils.reflection;

@FunctionalInterface
public interface PropertyDescriptorFilter {
    boolean doFilter(PropertyMetadata descriptor);
}
