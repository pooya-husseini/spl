package com.caspco.spl.wrench.filetransfer;

import com.caspco.spl.model.wrench.FileModel;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/24/17
 *         Time: 12:22 PM
 */
public interface FilePersistorService {
    String persist(FileModel fileModel);

    FileModel load(String fileId);
}
