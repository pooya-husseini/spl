package com.caspco.spl.wrench.repository;


import com.caspco.spl.model.annotations.cg.EnableFilter;
import com.caspco.spl.model.annotations.data.FillWithCurrentDate;
import com.caspco.spl.model.annotations.data.HasEvents;
import com.caspco.spl.model.data.ActiveableEntity;
import com.caspco.spl.wrench.utils.reflection.ClassUtils;
import com.caspco.spl.wrench.utils.reflection.PropertyMetadata;
import com.querydsl.core.types.OrderSpecifier;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 11/7/15
 *         Time: 2:03 PM
 */

public interface SplRepository<T, ID extends Serializable> {

    <S extends T> List<S> save(Iterable<S> entities);

    Iterable<T> findAll(Iterable<ID> ids);

    void delete(Iterable<? extends T> entities);

    void deleteAll();

    Class getTypeArgument();

    long count();

    long count(T t);

    <S extends T> S save(S obj);

    T findOne(ID id);

    List<T> find(T e);

    List<T> findAll();

    Iterable<T> findAll(Sort sort);
    Iterable<T> findAll(Pageable sort);
    boolean exists(ID id);

    boolean exists(T e);

    void delete(ID id);

    void delete(T e);

    void deleteAllInBatch();

    List<String> searchColumn(String column, String value);

//    <S extends T> S changeActiveStatus(S entity);

//    boolean isActivable();

//    <S extends T> Boolean getActiveStatus(S e);

    List<T> findActives(T e);


    List<T> findActives(T e, OrderSpecifier<?>... orders);
    //
//    List<T> findActives(T e);
//
    List<T> find(T e, OrderSpecifier<?>... orders);


    default PropertyMetadata getIdField() {
        List<PropertyMetadata> metadataList = getClassUtils().filterProperties(i -> i.isAnnotatedWith(Id.class));
        if (metadataList.isEmpty()) {
            throw new IllegalArgumentException("Entity " + this.getTypeArgument() + " should contain an id field");
        }
        return metadataList.get(0);
    }

    default <S extends T> S changeActiveStatus(S entity) {
        if (entity instanceof ActiveableEntity) {
            ID id = (ID) getIdField().getValue(entity);
            T original = this.findOne(id);
            ((ActiveableEntity) original).setActive(!((ActiveableEntity) original).getActive());
            return this.save((S) original);
        }
        return entity;
    }

    default <S extends T> Boolean getActiveStatus(S entity) {
        if (entity instanceof ActiveableEntity) {
            ID id = (ID) getIdField().getValue(entity);
            T original = this.findOne(id);
            if (original == null) {
                return null;
            }
            return ((ActiveableEntity) original).getActive();
        }
        return null;
    }


    default ClassUtils getClassUtils() {
        return ClassUtils.create(this.getTypeArgument());

    }

    default boolean isActivable() {
        return getClassUtils().existsProperties(i -> i.isAnnotatedWith(EnableFilter.class));
    }

    default void firePrePersist(Object entity) {
        if (getTypeArgument().isAnnotationPresent(HasEvents.class)) {
            ClassUtils classUtils = ClassUtils.create(getTypeArgument());
            classUtils.filterProperties(i -> i.isAnnotatedWith(FillWithCurrentDate.class)).forEach(s ->
                    s.setValue(entity, new Date())
            );
        }
    }
}