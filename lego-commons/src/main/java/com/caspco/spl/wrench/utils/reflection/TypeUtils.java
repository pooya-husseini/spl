package com.caspco.spl.wrench.utils.reflection;

import com.caspco.spl.model.wrench.exception.SplBaseException;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.MethodDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class TypeUtils {

    public final static TypeUtils INSTANCE=new TypeUtils();

    private TypeUtils() {

    }

    /**
     * Gets annotation value.
     *
     * @param a    the a
     * @param name the name
     * @return the annotation value
     */
    public Object getAnnotationValue(Annotation a, String name) {
        try {

            MethodDescriptor[] descriptors = Introspector
                    .getBeanInfo(a.getClass())
                    .getMethodDescriptors();

            return Arrays.stream(descriptors)
                    .filter(item -> item.getName().equals(name))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Argument not found exception"))
                    .getMethod()
                    .invoke(a);
        } catch (IllegalAccessException | IntrospectionException | InvocationTargetException e) {
            throw new SplBaseException(e);
        }
    }

    /**
     * Object to string string.
     *
     * @param o         the o
     * @param delimiter the delimiter
     * @return the string
     */
    public String objectToString(Object o, String delimiter) {
        List<PropertyMetadata> descriptors = ClassUtils.create(o.getClass()).getPropertiesMetadata();
        return descriptors.stream()
                .filter(item -> item.getValue(o) != null)
                .map(item -> item.getName() + "=" + item.getValue(o) + delimiter)
                .reduce((x, y) -> x + y).orElse(null);
    }


    /**
     * Object to map map.
     *
     * @param o the o
     * @return the map
     */
    public Map<String, Object> objectToMap(Object o) {
        List<PropertyMetadata> descriptors = ClassUtils.create(o.getClass()).getPropertiesMetadata();
        return descriptors
                .stream()
                .filter(item -> item.getValue(o) != null)
                .collect(Collectors.toMap(PropertyMetadata::getName, item -> item.getValue(o)));
    }

    /**
     * Gets generic object type.
     *
     * @param t the t
     * @return the generic object type
     */
    public Class<?> getGenericObjectType(Type t) {
        if (t instanceof ParameterizedType) {
            Type elementType = ((ParameterizedType) t).getActualTypeArguments()[0];
            return (Class<?>) elementType;
        } else {
            return null;
        }
    }

    /**
     * Is types equals boolean.
     *
     * @param type1 the type 1
     * @param type2 the type 2
     * @return the boolean
     */
    public Boolean isTypesEquals(Class<?> type1, Class<?> type2) {
        return type1.isAssignableFrom(type2) || type2.isAssignableFrom(type1);
    }
}