package com.caspco.spl.wrench.interfaces;

import com.caspco.spl.wrench.annotations.permission.Permission;

/**
 * Created by saeedko on 5/17/17.
 */
public interface HasPermission {
    boolean hasAuthority(Class<?> baseClass, Permission activity);
}
