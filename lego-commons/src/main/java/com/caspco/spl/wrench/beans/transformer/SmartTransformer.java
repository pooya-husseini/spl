package com.caspco.spl.wrench.beans.transformer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SmartTransformer {

    @Autowired(required = false)
    private ObjectMapper objectMapper;



    @Autowired(required = false)
    private List<SimpleBeanTransformer> transformers;

    private Map<String, SimpleBeanTransformer> simpleBeanTransformerMap = new ConcurrentHashMap<>();
    private Map<String, List<String>> beanTransformers = new ConcurrentHashMap<>();

    @PostConstruct
    public void init() {
        if (transformers != null) {


            for (SimpleBeanTransformer transformer : transformers) {
                String inboundName = transformer.getInboundTypeArgument().getName();
                String outboundName = transformer.getOutboundTypeArgument().getName();
                String s = outboundName + inboundName;
                if (!beanTransformers.containsKey(inboundName)) {
                    beanTransformers.put(inboundName, new ArrayList<>());
                }
                beanTransformers.get(inboundName).add(outboundName);
                simpleBeanTransformerMap.put(s, transformer);
            }
        }
    }

    public <E, T> List<T> convert(List<E> o, Class<T> resultClass) {
        List<T> ts = new ArrayList<>();
        for (E e : o) {
            T convert = convert(e, resultClass);
            ts.add(convert);
        }
        return ts;
    }

    public <E, T> SimpleBeanTransformer<E, T> getConverter(Class<E> inputClass, Class<T> resultClass) {
        String s = resultClass.getName() + inputClass.getName();
        return simpleBeanTransformerMap.get(s);
    }

    public <E, T> T convert(E o, Class<T> resultClass) {

        String oName = o.getClass().getName();
        String resultName = resultClass.getName();
        boolean isAbstract = Modifier.isAbstract(resultClass.getModifiers());
        if (isAbstract) {
            List<String> classes = beanTransformers.get(oName);
            if (classes != null && !classes.isEmpty()) {
                if (classes.size() > 1) {
                    throw new IllegalArgumentException("Abstract class " + o.getClass() + " has more than one candidate for transformation");
                }
                /*else if (classes.isEmpty()) {
                    throw new IllegalArgumentException("There is no transformation candidate for abstract class " + o.getClass());
                }*/
                resultName = classes.get(0);
            }
        }
        String s = resultName + oName;

        SimpleBeanTransformer transformer = simpleBeanTransformerMap.get(s);
        if (transformer == null) {
//            if (isAbstract) {
//                throw new CanNotTransformException("Can not transform type " + oName + " to " + resultName);
//            } else {
            try {
                String json = objectMapper.writeValueAsString(o);
                return objectMapper.readValue(json, resultClass);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
//            }
        }
        return (T) transformer.convert(o);
    }

    public String toJson(Object a) throws JsonProcessingException {
        return objectMapper.writeValueAsString(a);
    }

}
