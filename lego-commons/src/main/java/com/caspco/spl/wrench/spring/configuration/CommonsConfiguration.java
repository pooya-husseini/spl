package com.caspco.spl.wrench.spring.configuration;

import com.caspco.spl.wrench.beans.proxy.EntityProxy;
import com.caspco.spl.wrench.beans.transformer.SmartTransformer;
import com.caspco.spl.wrench.locale.LocaleService;
import com.caspco.spl.wrench.utils.report.ReportUtils;
import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

//import org.springframework.boot.orm.jpa.EntityScan;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 11/16/15
 *         Time: 6:46 PM
 */

@Configuration
@ComponentScan(basePackages = {"com.caspco.spl.wrench","${spl.common.scan-package}"})
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)
public class CommonsConfiguration {
    private static final Logger LOGGER = Logger.getLogger(CommonsConfiguration.class);

    @Bean
    public ReportUtils reportUtils() {
        return new ReportUtils();
    }

    @Bean
    public EntityProxy entityProxy() {
        return new EntityProxy();
    }

//    @Bean
//    public TypeUtils typeUtils() {
//        return new TypeUtils();
//    }

    @Bean
    public LocaleService localeService() {
        return new LocaleService();
    }

    @Bean
    public SmartTransformer basicTransformer() {
        return new SmartTransformer();
    }
}