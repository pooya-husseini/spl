package com.caspco.spl.wrench.beans.scanner;

import com.caspco.spl.wrench.spring.configuration.SplCommonProperties;
import com.google.common.base.Predicate;
import org.reflections.Reflections;
import org.reflections.scanners.*;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/27/17
 *         Time: 1:26 PM
 */

@Component
public class TypeScanner {
    //    private static final Logger LOGGER = Logger.getLogger(TypeScanner.class);
    private static final String DEFAULT_PACKAGE = "com.caspco.spl";
    private final Set<URL> urls;
    private final Predicate<String> inputs;
    private final String scanPackage;
    private Reflections reflections;
    private Reflections translationPropertiesReflections;
    private Reflections resourceReflections;
    private final Map<Class<? extends Annotation>, Set<Class<?>>> ANNOTATION_CACHE = new ConcurrentHashMap<>();
    private final Map<Class<?>, Object> SUB_TYPE_CACHE = new ConcurrentHashMap<>();
    private final Map<Class<? extends Annotation>, Set<Method>> METHOD_CACHE = new ConcurrentHashMap<>();

    @Autowired
    private SplCommonProperties splProperties;


    public TypeScanner() {
        this.urls = null;
        this.inputs = null;
        this.scanPackage = null;

    }

    public TypeScanner(Set<URL> urls, Predicate<String> inputs) {
        this.urls = urls;
        this.inputs = inputs;
        this.scanPackage = null;
    }

    public TypeScanner(String scanPackage, Predicate<String> inputs) {
        this.urls = null;
        this.inputs = inputs;
        this.scanPackage = scanPackage;
    }

    public TypeScanner(String scanPackage) {
        this.urls = null;
        this.inputs = null;
        this.scanPackage = scanPackage;
    }

    @PostConstruct
    public void init() {
        Set<URL> urls = new HashSet<>();
        Predicate<String> inputs;
        if (this.urls == null) {
//            urls.addAll(ClasspathHelper.forClassLoader());
            if (extractScanPackage() != null) {
                urls.addAll(ClasspathHelper.forPackage(extractScanPackage()));
                urls.addAll(ClasspathHelper.forPackage(DEFAULT_PACKAGE));
            }
        } else {
            urls = this.urls;
        }
        if (this.inputs == null) {
            inputs = new FilterBuilder().include(FilterBuilder.prefix(extractScanPackage()))
                    .include(FilterBuilder.prefix(DEFAULT_PACKAGE));
        } else {
            inputs = this.inputs;
        }
        try {
            reflections = new Reflections(new ConfigurationBuilder()
                    .addClassLoader(ClasspathHelper.contextClassLoader())
                    .setUrls(urls)
                    .filterInputsBy(inputs)
                    .setScanners(
                            new TypeElementsScanner(),
                            new SubTypesScanner(),
                            new MethodAnnotationsScanner(),
                            new TypeAnnotationsScanner()
                    ));
        } catch (Exception x) {
            System.out.println("Don't worry, go on");
        }
        try {
            translationPropertiesReflections = new Reflections(new ConfigurationBuilder()
                    .addClassLoader(ClasspathHelper.contextClassLoader())
//                .addClassLoader(Application.class.getClassLoader())
                    .setUrls(ClasspathHelper.forClassLoader())
                    .filterInputsBy(new FilterBuilder().include(".*-\\w+_\\w+\\.properties"))
                    .setScanners(new ResourcesScanner()));
        } catch (Exception x) {
            System.out.println("Don't worry, go on");
        }
        try {
            resourceReflections = new Reflections(new ConfigurationBuilder()
                    .addClassLoader(ClasspathHelper.contextClassLoader())
                    .setUrls(ClasspathHelper.forClassLoader())
                    .setScanners(new ResourcesScanner()));
        } catch (Exception x) {
            System.out.println("Don't worry, go on");
        }
    }

    public Set<Class<?>> getTypesAnnotatedWith(final Class<? extends Annotation> annotation) {
        return ANNOTATION_CACHE.computeIfAbsent(annotation, i ->
                this.reflections.getTypesAnnotatedWith(annotation)
        );
    }

    public <T> Set<Class<? extends T>> getSubTypesOf(final Class<T> type) {
        if (SUB_TYPE_CACHE.containsKey(type)) {
            return (Set<Class<? extends T>>) SUB_TYPE_CACHE.get(type);
        } else {

            Set<Class<? extends T>> subTypesOf = this.reflections.getSubTypesOf(type);

            SUB_TYPE_CACHE.put(type, subTypesOf);

            return (Set<Class<? extends T>>) SUB_TYPE_CACHE.get(type);
        }
    }

    public Set<String> getTranslationsResources(Pattern pattern) {
        return this.translationPropertiesReflections.getResources(pattern);
    }

    public Set<String> getResources(Pattern pattern) {
        return this.resourceReflections.getResources(pattern);
    }


    public Set<Method> getMethodsAnnotatedWith(Class<? extends Annotation> annotation) {
        return METHOD_CACHE.computeIfAbsent(annotation, i -> this.reflections.getMethodsAnnotatedWith(annotation));
    }

    public String extractScanPackage() {
        if (this.scanPackage == null) {
            return this.splProperties.getScanPackage();
        } else {
            return this.scanPackage;
        }
    }
}