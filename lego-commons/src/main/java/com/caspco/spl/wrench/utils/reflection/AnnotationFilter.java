package com.caspco.spl.wrench.utils.reflection;

import java.lang.annotation.Annotation;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 1/30/17
 *         Time: 12:30 PM
 */

@FunctionalInterface
public interface AnnotationFilter {
    boolean doFilter(Annotation a);
}
