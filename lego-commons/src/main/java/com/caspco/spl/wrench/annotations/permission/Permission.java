package com.caspco.spl.wrench.annotations.permission;

import com.caspco.spl.model.annotations.cg.Translation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by saeedko on 5/16/17.
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Permission {
    String name();
    boolean autoResolve() default true;
    Translation[] translations();
}
