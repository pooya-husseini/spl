package com.caspco.spl.wrench.beans.transformer;

/**
 * Created by saeedko on 2/25/17.
 */

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTypeResolverBuilder;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTyping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;

@Component
public class CustomTypeResolverBuilder extends DefaultTypeResolverBuilder {

    @Autowired(required = false)
    private SplTypeResolver splTypeResolver;

    public CustomTypeResolverBuilder() {
        super(DefaultTyping.NON_FINAL);
    }


    @Override
    public boolean useForType(JavaType t) {
        if (!t.getRawClass().isAnnotationPresent(Entity.class)) {
            return false;
        }
        return splTypeResolver != null && splTypeResolver.getConversionMap().containsKey(t.getRawClass());
    }
}