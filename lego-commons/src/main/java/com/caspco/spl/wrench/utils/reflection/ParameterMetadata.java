
package com.caspco.spl.wrench.utils.reflection;

import java.lang.annotation.Annotation;

public class ParameterMetadata {

    private Annotation[] annotations;
    private String name;
    private Class<?> type;

    public void setAnnotations(Annotation[] annotations) {
        this.annotations = annotations;
    }

    public Annotation[] getAnnotations() {
        return annotations;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setType(Class<?> type) {
        this.type = type;
    }

    public Class<?> getType() {
        return type;
    }
}