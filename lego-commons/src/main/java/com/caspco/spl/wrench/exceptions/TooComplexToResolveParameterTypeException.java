package com.caspco.spl.wrench.exceptions;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 9/4/17
 * Time: 9:19 AM
 */
public class TooComplexToResolveParameterTypeException extends RuntimeException {
    public TooComplexToResolveParameterTypeException() {
        super();
    }

    public TooComplexToResolveParameterTypeException(String message) {
        super(message);
    }

    public TooComplexToResolveParameterTypeException(String message, Throwable cause) {
        super(message, cause);
    }

    public TooComplexToResolveParameterTypeException(Throwable cause) {
        super(cause);
    }

    protected TooComplexToResolveParameterTypeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
