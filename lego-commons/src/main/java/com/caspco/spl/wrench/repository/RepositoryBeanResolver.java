//package com.caspco.spl.wrench.repository;
//
///**
// * @author : Pooya. h
// *         Email : husseini@caspco.ir
// *         Date: 6/10/17
// *         Time: 5:25 PM
// */
//public interface RepositoryBeanResolver {
//
//    Object resolve(RepositoryInformation repositoryInformation);
//}
