
package com.caspco.spl.wrench.utils.reflection;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.util.Assert;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MethodMetadata {
    private String name;
    private Class<?> type;
    private List<Annotation> annotations;
    private Method method;
    private List<ParameterMetadata> parameters;

    /**
     * Getter for property 'name'.
     *
     * @return Value for property 'name'.
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for property 'name'.
     *
     * @param name Value to set for property 'name'.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for property 'type'.
     *
     * @return Value for property 'type'.
     */
    public Class<?> getType() {
        return type;
    }

    /**
     * Setter for property 'type'.
     *
     * @param type Value to set for property 'type'.
     */
    public void setType(Class<?> type) {
        this.type = type;
    }

    /**
     * Getter for property 'annotations'.
     *
     * @return Value for property 'annotations'.
     */
    public List<Annotation> getAnnotations() {
        return annotations;
    }

    /**
     * Setter for property 'annotations'.
     *
     * @param annotations Value to set for property 'annotations'.
     */
    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = annotations;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    /**
     * Is property annotated with any boolean.
     *
     * @param annotations the annotations
     * @return the boolean
     */
    @SafeVarargs
    @Cacheable
    public final boolean isAnnotatedWithAny(Class<? extends Annotation>... annotations) {
        Assert.notNull(annotations);
        Assert.isTrue(annotations.length > 0);
        List<Class<? extends Annotation>> collect = this.annotations.stream().map(Annotation::annotationType).collect(Collectors.toList());
        collect.retainAll(Arrays.asList(annotations));
        return collect.size() > 0;
    }

    @Cacheable
    public boolean isAnnotatedWith(Class<? extends Annotation> annotation) {
        return isAnnotatedWithAny(annotation);
    }

    /**
     * Is property annotated with all boolean.
     *
     * @param annotations the annotations
     * @return the boolean
     */
    @Cacheable
    public boolean isAnnotatedWithAll(Class<? extends Annotation>... annotations) {
        Assert.isTrue(annotations.length > 0);
        List<Class<? extends Annotation>> collect = this.annotations.stream()
                .map(Annotation::annotationType)
                .collect(Collectors.toList());
        return !Arrays.asList(annotations).retainAll(collect);
    }

    public <T extends Annotation> Optional<T> getAnnotation(Class<T> annotationClass) {
        return (Optional<T>) this.annotations.stream().filter(i -> i.annotationType() == annotationClass).findFirst();
    }

    public List<Annotation> filterAnnotations(AnnotationFilter filter) {
        return annotations.stream()
                .filter(filter::doFilter)
                .collect(Collectors.toList());
    }

    public Object invoke(Object obj, Object... args) throws InvocationTargetException, IllegalAccessException {
        return this.method.invoke(obj, args);
    }

    public Stream<ParameterMetadata> filterParameters(MethodParameterFilter filter) {
        return this.parameters.stream().filter(filter::doFilter);
    }

    public boolean existsParameters(MethodParameterFilter filter) {
        return this.parameters.stream().anyMatch(filter::doFilter);
    }

    public List<ParameterMetadata> getParameters() {
        return parameters;
    }

    public void setParameters(List<ParameterMetadata> parameters) {
        this.parameters = parameters;
    }
}