package com.caspco.spl.wrench.utils.reflection;

@FunctionalInterface
public interface MethodDescriptorFilter {
    boolean doFilter(MethodMetadata descriptor);
}
