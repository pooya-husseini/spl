package com.caspco.spl.wrench.utils.reflection;


import com.caspco.spl.model.wrench.exception.SplBaseException;
import com.caspco.spl.wrench.exceptions.TooComplexToResolveParameterTypeException;
import org.springframework.core.annotation.AnnotationUtils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The type Class utils.
 */
public class ClassUtils {
    private static final Map<String, ClassUtils> INSTANCE_CACHE = new ConcurrentHashMap<>();
    private static final Map<String, List<PropertyMetadata>> METADATA_LIST_CACHE = new ConcurrentHashMap<>();
    private static final Map<String, Map<String, PropertyMetadata>> METADATA_MAP_CACHE = new ConcurrentHashMap<>();
    private static final Map<String, PropertyMetadata> METADATA_CACHE = new ConcurrentHashMap<>();
    private final Class clazz;
    private final Map<String, Class<?>> genericMap;


    private ClassUtils(Class clazz) {
//        Class clazz1 = null;
//        try {
//            clazz.getDeclaredField("CGLIB$BOUND");
//            if (clazz.getSuperclass() != Object.class) {
//                clazz1 = clazz.getSuperclass();
//            } else {
//                for (Class<?> iFace : clazz.getInterfaces()) {
//                    if (iFace != Factory.class) {
//                        clazz1 = iFace;
//                        break;
//                    }
//                }
//                if (clazz1 == null) {
//                    throw new CannotResolveTypeException();
//                }
//            }
//
//        } catch (NoSuchFieldException ignored) {
//            clazz1 = clazz;
//        }

//        this.clazz = clazz1;
        this.clazz = clazz;
        this.genericMap = new HashMap<>();
        Type type =  this.clazz.getGenericSuperclass();
        if(type  instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type[] classes = parameterizedType.getActualTypeArguments();
            TypeVariable[] typeVariables = clazz.getSuperclass().getTypeParameters();
            for (int i = 0; i < classes.length; i++) {
                Type aClass = classes[i];
                this.genericMap.put(typeVariables[i].getName(), (Class<?>) aClass);
            }
        }
    }

    public static ClassUtils create(Class clazz) {
        return INSTANCE_CACHE.computeIfAbsent(clazz.getName(), i -> new ClassUtils(clazz));
    }

    public PropertyMetadata getPropertyMetadata(String propertyName) {
        return METADATA_CACHE.computeIfAbsent(clazz.getName() + propertyName, i -> getPropertiesMetadata()
                .stream()
                .filter(item -> item.getName().equals(propertyName))
                .findFirst()
                .get()
        );
    }

    public List<PropertyMetadata> getPropertiesMetadata() {
        return METADATA_LIST_CACHE.computeIfAbsent(clazz.getName(), i -> {
            try {
                BeanInfo info = Introspector.getBeanInfo(clazz);
                return
                        Arrays.stream(info.getPropertyDescriptors())
                                .filter(item -> !"class".equals(item.getName()))
                                .map(descriptor -> {
                                    PropertyMetadata metadata = new PropertyMetadata();
                                    metadata.setName(descriptor.getName());
                                    metadata.setType(descriptor.getPropertyType());
                                    metadata.setReadMethod(descriptor.getReadMethod());
                                    metadata.setWriteMethod(descriptor.getWriteMethod());
                                    List<Annotation> annotations = new ArrayList<>();
//                                    Collections.addAll(annotations, findField(descriptor.getName()).getAnnotations());
                                    Collections.addAll(annotations, descriptor.getReadMethod().getAnnotations());
                                    Collections.addAll(annotations, descriptor.getWriteMethod().getAnnotations());
                                    metadata.setAnnotations(annotations);
                                    return metadata;
                                })
                                .collect(Collectors.toList());
            } catch (IntrospectionException e) {
                throw new SplBaseException(e);
            }
        });
    }


    public List<MethodMetadata> getMethodMetadata() {
        Method[] methods = clazz.getMethods();

        return Arrays.stream(methods).map(method -> {
            MethodMetadata metadata = new MethodMetadata();
            metadata.setName(method.getName());
            metadata.setType(method.getReturnType());
            metadata.setMethod(method);
            Type[] genericParameterTypes = method.getGenericParameterTypes();
            ArrayList<ParameterMetadata> parameters = new ArrayList<>();
            Parameter[] methodParameters = method.getParameters();

            if (genericParameterTypes != null) {
                if (methodParameters.length != genericParameterTypes.length) {
                    throw new TooComplexToResolveParameterTypeException();
                }
                for (int i = 0; i < methodParameters.length; i++) {
                    Type genericType = genericParameterTypes[i];
                    Parameter parameter = methodParameters[i];

                    if (this.genericMap.containsKey(genericType.getTypeName())) {
                        Class<?> parameterActualType = this.genericMap.get(genericType.getTypeName());
                        ParameterMetadata e = new ParameterMetadata();
                        e.setType(parameterActualType);
                        e.setName(parameter.getName());
                        e.setAnnotations(parameter.getAnnotations());
                        parameters.add(e);
                    } /*else {
                        throw new CannotResolveGenericException();
                    }*/
                }
            } else {
                for (Parameter parameter : methodParameters) {
                    ParameterMetadata e = new ParameterMetadata();
                    e.setType(parameter.getType());
                    e.setName(parameter.getName());
                    e.setAnnotations(parameter.getAnnotations());
                    parameters.add(e);
                }
            }
            metadata.setParameters(parameters);
//            metadata.setParameters(method.getParameters());
            List<Annotation> annotations = new ArrayList<>();
            Collections.addAll(annotations, AnnotationUtils.getAnnotations(method));
            metadata.setAnnotations(annotations);
            return metadata;
        }).collect(Collectors.toList());
    }

    public Map<String, PropertyMetadata> getPropertiesMetadataAsMap() {
        return METADATA_MAP_CACHE.computeIfAbsent(clazz.getName(), i -> getPropertiesMetadata()
                .stream()
                .collect(Collectors.toMap(PropertyMetadata::getName, v -> v))
        );
    }

    public List<PropertyMetadata> filterProperties(PropertyDescriptorFilter filter) {
        Stream<PropertyMetadata> descriptors = getPropertiesMetadata().stream();
        return descriptors
                .filter(filter::doFilter)
                .collect(Collectors.toList());
    }

    public List<MethodMetadata> filterMethods(MethodDescriptorFilter filter) {
        return filterMethodsStream(filter)
                .collect(Collectors.toList());
    }

    public Stream<MethodMetadata> filterMethodsStream(MethodDescriptorFilter filter) {
        Stream<MethodMetadata> descriptors = getMethodMetadata().stream();
        return descriptors.filter(filter::doFilter);
    }

    public boolean existsProperties(PropertyDescriptorFilter filter) {
        Stream<PropertyMetadata> descriptors = getPropertiesMetadata().stream();
        return descriptors.anyMatch(filter::doFilter);

    }

    public PropertyMetadata findPropertyMetadata(String name) {
        return getPropertiesMetadataAsMap().get(name);
    }

    public Class getClazz() {
        return clazz;
    }
}