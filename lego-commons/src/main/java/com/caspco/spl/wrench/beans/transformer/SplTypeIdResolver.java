package com.caspco.spl.wrench.beans.transformer;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DatabindContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;
import com.fasterxml.jackson.databind.type.SimpleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.Entity;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by saeedko on 2/22/17.
 */

@Component
public class SplTypeIdResolver implements TypeIdResolver {

    @Autowired(required = false)
    private SplTypeResolver splTypeResolver;

    private static Map<String, Class<?>> classMap = new HashMap<>();

    private static Map<String, Class<?>> unknownTypeResolver = new HashMap<>();

    @PostConstruct
    public void init() {
        if (splTypeResolver != null) {
            for (Map.Entry<Class<?>, Class<?>> entry : splTypeResolver.getConversionMap().entrySet()) {
                if (!entry.getKey().isAnnotationPresent(Entity.class)) {
                    unknownTypeResolver.put(entry.getKey().getSimpleName(), entry.getKey());
                }
                classMap.put(entry.getKey().getSimpleName(), entry.getValue());
            }
        }
    }

    @Override
    public void init(JavaType javaType) {

    }

    @Override
    public String idFromValue(Object o) {
        return classMap.get(o.getClass().getSimpleName()).getSimpleName();
    }

    @Override
    public String idFromValueAndType(Object o, Class<?> aClass) {
        return null;
    }

    @Override
    public String idFromBaseType() {
        return null;
    }

//    @Override
    public JavaType typeFromId(String id) {
        return SimpleType.construct(classMap.get(id));
    }

    @Override
    public JavaType typeFromId(DatabindContext databindContext, String s) {
        return databindContext.constructType(classMap.get(s));
    }

    @Override
    public String getDescForKnownTypeIds() {
        return null;
    }

    @Override
    public JsonTypeInfo.Id getMechanism() {
        return null;
    }
}