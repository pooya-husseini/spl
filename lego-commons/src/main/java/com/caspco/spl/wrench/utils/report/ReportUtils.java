package com.caspco.spl.wrench.utils.report;

import com.caspco.spl.model.annotations.cg.Translations;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.log4j.Logger;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.util.Assert;

import java.io.IOException;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 3/2/16
 *         Time: 9:16 PM
 */
public class ReportUtils {
    private static final Logger LOGGER = Logger.getLogger(ReportUtils.class);

    @Cacheable("ReportCompiling")
    public JasperReport compiledReport(String path) throws JRException, IOException {
        LOGGER.info("Going to compile report in path " + path);
        JasperReport jasperReport = JasperCompileManager.compileReport(path);
        LOGGER.info("Report already compiled");
        return jasperReport;
    }

    @Cacheable("Translations")
    public Translations fillEnumTranslationsToCache(Class<?> enumClass, String itemName) throws NoSuchFieldException {
        Assert.notNull(itemName);
        for (Object o : enumClass.getEnumConstants()) {
            String name = ((Enum) o).name();
            if (itemName.equals(name)) {
                return enumClass.getField(name).getAnnotation(Translations.class);
            }
//                cache.put(enumClass.getName() + "." + name, translations);
        }
//            cache.put(enumClass.getName(), true);
        return null;
    }
}