package com.caspco.spl.wrench.utils.reflection;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 8/30/17
 * Time: 2:20 PM
 */
public class CannotResolveTypeException extends RuntimeException {
}
