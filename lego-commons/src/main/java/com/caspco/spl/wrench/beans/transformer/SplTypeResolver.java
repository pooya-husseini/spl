package com.caspco.spl.wrench.beans.transformer;

import java.util.Map;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 2/21/17
 *         Time: 11:26 AM
 */
public interface SplTypeResolver {
    Map<Class<?>,Class<?>> getConversionMap();
}
