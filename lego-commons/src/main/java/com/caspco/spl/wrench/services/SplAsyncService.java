package com.caspco.spl.wrench.services;

import org.springframework.scheduling.annotation.Async;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 5/24/17
 * Time: 11:27 AM
 */

public interface SplAsyncService<E, K extends Serializable> extends SplService<E, K> {

    @Async
    default CompletableFuture<Boolean> isEmptyAsync() {
        return CompletableFuture.completedFuture(isEmpty());
    }

    @Async
    default CompletableFuture<E> saveAsync(E obj) {
        return CompletableFuture.completedFuture(save(obj));
    }

    @Async
    default CompletableFuture<E> findByIdAsync(K id) {
        return CompletableFuture.completedFuture(findById(id));
    }

    @Async
    default CompletableFuture<List<E>> findBySampleAsync(E e) {
        return CompletableFuture.completedFuture(findBySample(e));
    }

    @Async
    default CompletableFuture<List<E>> findAllAsync() {
        return CompletableFuture.completedFuture(findAll());
    }

    @Async
    default CompletableFuture<Boolean> existsAsync(K id) {
        return CompletableFuture.completedFuture(exists(id));
    }

    @Async
    default CompletableFuture<Boolean> existsAsync(E e) {
        return CompletableFuture.completedFuture(exists(e));
    }

    @Async
    default CompletableFuture<Long> countAsync(E e) {
        return CompletableFuture.completedFuture(count(e));
    }

    @Async
    default CompletableFuture<Long> countAllAsync() {
        return CompletableFuture.completedFuture(countAll());
    }

    @Async
    default void removeByIdAsync(K id) {
        removeById(id);
    }

    @Async
    default void removeAsync(E e) {
        remove(e);
    }

    @Async
    default void removeAllAsync() {
        removeAll();
    }

    @Async
    default CompletableFuture<List<String>> searchColumnAsync(String column, String value) {
        return CompletableFuture.completedFuture(searchColumn(column, value));
    }

    @Async
    default CompletableFuture<E> changeActiveStatusAsync(E entity) {
        return CompletableFuture.completedFuture(changeActiveStatus(entity));
    }

    @Async
    default CompletableFuture<Boolean> isActivableAsync() {
        return CompletableFuture.completedFuture(isActivable());
    }

    @Async
    default CompletableFuture<Boolean> isActiveAsync(E e) {
        return CompletableFuture.completedFuture(isActive(e));
    }

    @Async
    default CompletableFuture<List<E>> findActivesBySampleAsync(E e) {
        return CompletableFuture.completedFuture(findActivesBySample(e));
    }
}