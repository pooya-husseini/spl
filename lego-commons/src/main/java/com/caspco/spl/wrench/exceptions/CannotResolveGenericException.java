package com.caspco.spl.wrench.exceptions;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 9/3/17
 * Time: 8:52 PM
 */
public class CannotResolveGenericException extends RuntimeException {
    public CannotResolveGenericException() {
        super();
    }

    public CannotResolveGenericException(String message) {
        super(message);
    }

    public CannotResolveGenericException(String message, Throwable cause) {
        super(message, cause);
    }

    public CannotResolveGenericException(Throwable cause) {
        super(cause);
    }

    protected CannotResolveGenericException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
