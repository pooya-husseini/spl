package com.caspco.spl.wrench.exceptionhandler;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/24/17
 *         Time: 11:14 AM
 */
public interface ExceptionPersister {
    void persist(Exception x);
}
