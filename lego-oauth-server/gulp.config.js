var client = './src/main/resources/static/';
var appPath = client + "app/";
var scriptPath = client + '/scripts';
var port = 8080;
var config = {
    buildPath: '',
    styles: [],
    indexPage: client + "index.html",

    distPath: [
        appPath + 'dist/*.*'
    ],
    appPath: appPath,

    browserSync: {
        proxy: 'localhost:' + port,
        port: 3000,
        files: ['**/*.*'],
        ghostMode: { // these are the defaults t,f,t,t
            clicks: true,
            location: false,
            forms: true,
            scroll: true
        },
        logLevel: 'debug',
        logPrefix: 'gulp-patterns',
        notify: true,
        reloadDelay: 1000
    },
    bower: {
        json: require('./bower.json'),
        directory: client + 'vendors',
        ignorePath: './../../'
    },
    cssSources: {
        sources: [
            appPath + 'css/**/*.css',
            "!" + appPath + 'dist/**/*.css'
        ],
        deps: [
            "!" + client + '**/persian-datepicker/dist/**/persian-datepicker-*.min.css',
            "!" + client + '**/persian-datepicker/dist/**/theme/*.css'
        ]
    },
    htmlSources: {
        projectSources: [
            "!" + appPath + 'gen-src/**/*.html',
            appPath + '**/*.html',
            "!" + appPath + 'dist/**/*.*'
        ]
    },
    jsSources: {
        client: appPath + "/js/**/*.js",
        exclude: ["!/**/app.js", "!/**/all.js"],
        sundry: ['./gulpfile.js', './gulpfile.config.js'],

        finalFiles: [
            appPath + 'dist/lego.js',
            appPath + 'dist/project.js'
        ],
        finalTestFiles: [
            'test/js/mainFile.js',
            'test/js/project.js'
        ],
        allSources: [
            appPath + 'gen-src/**/*.js',
            appPath + '**/*.js',
            appPath + 'app.js',
            appPath + '**/templates.js',
            "!" + appPath + "dist/**/*.*"
        ],
        projectSources: [
            "!" + appPath + 'gen-src/**/*.js',
            appPath + '**/*.js',
            appPath + 'app.js',
            "!" + appPath + '**/templates.js',
            "!" + appPath + 'dist/**/*.*'
        ],
        injectable: [
            scriptPath + '/quick-sidebar.js',
            scriptPath + '/demo.js',
            scriptPath + '/layout.js',
            scriptPath + '/metronic.js',
            client + 'app/**/*.module.js',
            client + 'app/**/*.js',
            '!' + appPath + '**/all.js',
            '!' + appPath + '**/app.js',
            '!' + appPath + '**/utils.js',
            '!' + appPath + '**/route.js',
            '!' + appPath + '**/config.js'
        ],
        deps: [
            client + '**/persian-date/dist/**/persian-date-*.js',
            "!" + client + '**/persian-date/dist/**/persian-date-*.min.js',
            "!" + client + '**/persian-datepicker/dist/**/persian-datepicker-*.min.js'
        ]
    }


};


module.exports.config = config;
module.exports.getWireDepOptions = function () {
    return {
        bowerJson: config.bower.json,
        directory: config.bower.directory,
        ignorePath: config.bower.ignorePath
    };
};