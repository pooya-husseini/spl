<html>
<head>
    <link rel="stylesheet" href="../static/css/wro.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css"/>
</head>
<body>
<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">Please Confirm</h3>
    </div>
    <div class="panel-body">
        <form d="confirmationForm" name="confirmationForm"
              action="../oauth/authorize" method="post">
        <p>
            Do you authorize "${authorizationRequest.clientId}" at "${authorizationRequest.redirectUri}" to access your
            protected resources
            with below scopes?
        </p>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">Scopes</h3>
            </div>

            <div class="panel-body">
                <div class="list-group">
                <#list authorizationRequest.scope as scope>
                    <div class="row">
                        <div class="col-xs-3 col-xs-offset-5">
                    <label>
                    ${scope}
                        <input selected="enable()" type="checkbox" name="scopes[${scope}]">
                    </label>
                    </div>
                    </div>
                </#list>

                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-2 col-xs-offset-5" style="display: inline-flex">


                    <input name="user_oauth_approval" value="true" type="hidden"/>
                    <#--<input type="hidden" id="csrf_token" name="${_csrf.parameterName}" value="${_csrf.token}"/>-->
                    <button class="btn btn-primary"  type="submit">Approve</button>

                <#--<form id="denyForm" name="confirmationForm"-->
                      <#--action="../oauth/authorize" method="post">-->
                    <#--<input name="user_oauth_approval" value="false" type="hidden"/>-->
                    <#--&lt;#&ndash;<input type="hidden" id="csrf_token" name="${_csrf.parameterName}" value="${_csrf.token}"/>&ndash;&gt;-->
                    <#--<button class="btn btn-danger" type="submit">Deny</button>-->
                <#--</form>-->
            </div>
        </div>
            <script>
                function enable() {
                    document.getElementById("Approve").disabled = false;
                }
            </script>
        </form>
    </div>
</div>

<script src="../js/wro.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"
        type="text/javascript"></script>
</body>
</html>