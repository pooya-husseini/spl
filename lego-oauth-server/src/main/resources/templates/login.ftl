<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Login page</title>
    <link rel="stylesheet" href="css/wro.css"/>
    <link rel="stylesheet" href="css/app.css"/>
</head>


<body onload="document.getElementById('username').focus();" class="login-body">

<div class="module form-module">

    <div class="form">
        <h2>جهت ورود به سیستم، آیتم های زیر را وارد نمایید</h2>
        <form role="form" action="login" method="post">
                <input type="text" id="username" name="username" placeholder="نام کاربری"/>
                <input type="password" id="password" name="password" placeholder="رمز عبور"/>

            <input type="hidden" id="csrf_token" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button>ورود</button>
        </form>

    </div>

    <!--<div class="cta"><a href="http://andytran.me">Forgot your password?</a></div>-->
</div>

</body>

</html>

