/*
* THIS IS AN AUTO GENERATED CODE.
* @author : Lego Code Generator
* Email : husseini@caspco.ir
* Date: Jul 9, 2017 1:39:54 PM
* @version 2.7.1
*/


interface IModel extends lego.IBasicModel {

}

@lego.Controller({
    type: lego.ControllerType.OPERATION_CONTROLLER,
    requestMapping:{
    Scope:"/scopes"
    }
})
export class ScopeOperationsController implements lego.ILegoController<lego.IBasicModel,lego.ISimpleActions> {

    public item: any;
    public restService;
    instance: ng.IDeferred<any>;

    @lego.OperationValue()
    private data;

    model: any;
    actions: lego.ISimpleActions;
    private Utils:lego.IUtils;

    getConfig(): lego.IAbstractControllerConfig<lego.IBasicModel, lego.ISimpleActions> {
        let gridConfig={
        data:this.model,
        columnDefs: [
                    {
            field: 'id' 
            ,displayName: 'Scope.id'
            ,headerCellFilter: 'translate'
                        ,visible:false
                                                            },
                        {
            field: 'name' 
            ,displayName: 'Scope.name'
            ,headerCellFilter: 'translate'
                                                            },
                                    ]
            }

        return {gridConf:gridConfig};
    }

    public constructor() {
        // todo do not write anything here
        this.actions = new ControllerActions(this);
    }

    preInit(){
        // todo add whatever you want to set before controller init
        if (this.data) {
            this.model = this.data;
        }
    }

    $onInit() {
            // todo add whatever you want to run on controller initialized

    }
}

class ControllerActions extends lego.LegoBasicModalActions<lego.IBasicModel> {

    constructor(public self:ScopeOperationsController) {
        super()
    }

    }