/*
 * THIS IS AN AUTO GENERATED CODE.
 * @author : Lego Code Generator
 * Email : husseini@caspco.ir
 * Date: Jul 13, 2017 2:57:17 PM
 * @version 2.7.1
 */


interface IModel extends lego.IBasicModel {

}

@lego.Controller({
    type: lego.ControllerType.OPERATION_CONTROLLER,
    requestMapping: {
        Application: "/applications"
        , GeneralParameter: "/generalparameters"
    }
})
export class ApplicationOperationsController implements lego.ILegoController<lego.IBasicModel,lego.ISimpleActions> {

    public item: any;
    private GeneralParameter;
    public restService;
    instance: ng.IDeferred<any>;

    @lego.OperationValue()
    private data;

    model: any;
    actions: lego.ISimpleActions;
    private Utils: lego.IUtils;

    getConfig(): lego.IAbstractControllerConfig<lego.IBasicModel, lego.ISimpleActions> {
        let gridConfig = {
            data: this.model,
            columnDefs: [
                {
                    field: 'id'
                    , displayName: 'Application.id'
                    , headerCellFilter: 'translate'
                    , visible: false
                },
                {
                    field: 'description'
                    , displayName: 'Application.description'
                    , headerCellFilter: 'translate'
                },
                {
                    field: 'callbackUrl'
                    , displayName: 'Application.callbackUrl'
                    , headerCellFilter: 'translate'
                },
                {
                    field: 'name'
                    , displayName: 'Application.name'
                    , headerCellFilter: 'translate'
                },


            ]
        }

        return {gridConf: gridConfig};
    }

    public constructor() {
        // todo do not write anything here
        this.actions = new ControllerActions(this);
    }

    beforeInit() {
        // todo add whatever you want to set before controller init
        if (this.data) {
            this.model = this.data;
            this.model.scopes = this.data.scopes;
        }
    }

    $onInit() {
        // todo add whatever you want to run on controller initialized


    }
}

class ControllerActions extends lego.LegoBasicModalActions<lego.IBasicModel> {

    constructor(public self: ApplicationOperationsController) {
        super()
    }

}