/*
 * THIS IS AN AUTO GENERATED CODE.
 * @author : Lego Code Generator
 * Email : husseini@caspco.ir
 * Date: Jul 13, 2017 2:57:17 PM
 * @version 2.7.1
 */

import {ApplicationOperationsService} from "../services/operations/ApplicationOperationsService";
import {log} from "typings/dist/support/cli";

interface IApplicationModel extends lego.IBasicModel {
    scopes: Array;
}

@lego.Controller({
    type: lego.ControllerType.CRUD_CONTROLLER,
    requestMapping: {
        Application: "/applications"
        , GeneralParameter: "/generalparameters"
        , Applications: "/applications"
    }
})
export class ApplicationController implements lego.ILegoController<IApplicationModel,lego.IGeneralActions>,lego.IGeneralActions {


    @lego.Inject()
    public applicationService: ApplicationOperationsService;
    public model: IApplicationModel;
    public actions: lego.IGeneralActions;
    public mainGrid: lego.ILegoGridOptions;
    public Utils: lego.IUtils;
    public NotificationUtils: lego.INotificationUtils;
    public restService;
    public GeneralParameter;
    public showModel;
    public showScopes = [];

    public constructor() {
        // todo do not write anything here
        this.actions = new ControllerActions(this);
    };

    getConfig(): lego.IAbstractControllerConfig<IApplicationModel, lego.IGeneralActions> {

        let gridConf: lego.ILegoGridOptions = {

            columnDefs: [
                {
                    field: 'id'
                    , displayName: 'Application.id'
                    , headerCellFilter: 'translate'
                    , visible: false
                },
                {
                    field: 'description'
                    , displayName: 'Application.description'
                    , headerCellFilter: 'translate'
                },
                {
                    field: 'callbackUrl'
                    , displayName: 'Application.callbackUrl'
                    , headerCellFilter: 'translate'
                },
                {
                    field: 'name'
                    , displayName: 'Application.name'
                    , headerCellFilter: 'translate'
                },
                {
                    field: 'secret'
                    , displayName: 'Application.secret'
                    , headerCellFilter: 'translate'
                },
                {
                    field: 'clientId'
                    , displayName: 'Application.clientId'
                    , headerCellFilter: 'translate'
                },

            ]
        }
        return {
            crudService: this.applicationService,
            restService: this.restService.Application,
            gridConf: gridConf
        };
    }

    $onInit() {
        this.restService.Applications.getScopes().$promise.then(
            (success) => {
                this.GeneralParameter = success;
            });

    }
}

class ControllerActions extends lego.LegoBasicActions<lego.IBasicModel> {

    constructor(public ctrl: ApplicationController) {
        super()
    }
    create(){
        super.create((result)=>{
            this.ctrl.applicationService.showSecret(result);
        });
        // this.ctrl.restService.Application.customAdd(this.ctrl.model).$promise.then((result)=>{
        //     this.ctrl.model = result;
        //
        //     this.reset();
        // })
    }
    reset() {
        super.reset();
    }
}
