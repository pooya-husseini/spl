/*
* THIS IS AN AUTO GENERATED CODE.
* @author : Lego Code Generator
* Email : husseini@caspco.ir
* Date: Jul 9, 2017 1:39:54 PM
* @version 2.7.1
*/

import {ScopeOperationsService} from "../services/operations/ScopeOperationsService";

interface IScopeModel extends lego.IBasicModel{
}

@lego.Controller({
    type: lego.ControllerType.CRUD_CONTROLLER,
    requestMapping:{
    Scope:"/scopes"
    }
})
export class ScopeController implements lego.ILegoController<IScopeModel,lego.IGeneralActions>,lego.IGeneralActions{



    @lego.Inject()
    public scopeService:ScopeOperationsService;
    public model: IScopeModel;
    public actions: lego.IGeneralActions;
    public mainGrid: lego.ILegoGridOptions;
    public Utils: lego.IUtils;
    public NotificationUtils:lego.INotificationUtils;
    public restService;
    public constructor() {
        // todo do not write anything here
        this.actions = new ControllerActions(this);
    };

    getConfig(): lego.IAbstractControllerConfig<IScopeModel, lego.IGeneralActions> {

        let gridConf: lego.ILegoGridOptions ={

            columnDefs: [
                {
                    field: 'id' 
                    ,displayName: 'Scope.id'
                    ,headerCellFilter: 'translate'
                    ,visible:false
                },
                {
                    field: 'name' 
                    ,displayName: 'Scope.name'
                    ,headerCellFilter: 'translate'
                },
            ]
        }
        return  {
            crudService: this.scopeService,
            restService: this.restService.Scope,
            gridConf: gridConf
        };
    }

    $onInit() {
    }
}

class ControllerActions extends lego.LegoBasicActions<lego.IBasicModel>{
    constructor(public ctrl:ScopeController){
        super()
    }

        }