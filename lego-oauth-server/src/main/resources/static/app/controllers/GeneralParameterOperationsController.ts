/*
* THIS IS AN AUTO GENERATED CODE.
* @author : Lego Code Generator
* Email : husseini@caspco.ir
* Date: Jul 13, 2017 2:57:17 PM
* @version 2.7.1
*/


interface IModel extends lego.IBasicModel {

}

@lego.Controller({
    type: lego.ControllerType.OPERATION_CONTROLLER,
    requestMapping:{
    GeneralParameter:"/generalparameters"
    }
})
export class GeneralParameterOperationsController implements lego.ILegoController<lego.IBasicModel,lego.ISimpleActions> {


        private GeneralParameterType;
    public item: any;
    public restService;
    instance: ng.IDeferred<any>;

    @lego.OperationValue()
    private data;

    model: any;
    actions: lego.ISimpleActions;
    private Utils:lego.IUtils;

    getConfig(): lego.IAbstractControllerConfig<lego.IBasicModel, lego.ISimpleActions> {
        let gridConfig={
        data:this.model,
        columnDefs: [
                    {
            field: 'id' 
            ,displayName: 'GeneralParameter.id'
            ,headerCellFilter: 'translate'
                        ,visible:false
                                                            },
                        {
            field: 'mainType' 
            ,displayName: 'GeneralParameter.mainType'
            ,headerCellFilter: 'translate'
                                                            ,cellFilter: 'translate:"GeneralParameterType"'
                        },
                        {
            field: 'code' 
            ,displayName: 'GeneralParameter.code'
            ,headerCellFilter: 'translate'
                                                            },
                        {
            field: 'description' 
            ,displayName: 'GeneralParameter.description'
            ,headerCellFilter: 'translate'
                                                            },
                        {
            field: 'priority' 
            ,displayName: 'GeneralParameter.priority'
            ,headerCellFilter: 'translate'
                                                            },
                                    ]
            }

        return {gridConf:gridConfig};
    }

    public constructor() {
        // todo do not write anything here
        this.actions = new ControllerActions(this);
    }

    preInit(){
        // todo add whatever you want to set before controller init
        if (this.data) {
            this.model = this.data;
        }
    }

    $onInit() {
            // todo add whatever you want to run on controller initialized
        this.restService.GeneralParameter.getGeneralParameterTypes().$promise.then(
            (success)=>{

                this.GeneralParameterType = this.Utils.convertEnum(success,"GeneralParameterType");
        });

    }
}

class ControllerActions extends lego.LegoBasicModalActions<lego.IBasicModel> {

    constructor(public self:GeneralParameterOperationsController) {
        super()
    }

    }