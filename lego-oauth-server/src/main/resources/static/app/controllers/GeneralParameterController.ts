/*
* THIS IS AN AUTO GENERATED CODE.
* @author : Lego Code Generator
* Email : husseini@caspco.ir
* Date: Jul 13, 2017 2:57:17 PM
* @version 2.7.1
*/

import {GeneralParameterOperationsService} from "../services/operations/GeneralParameterOperationsService";

interface IGeneralParameterModel extends lego.IBasicModel{
}

@lego.Controller({
    type: lego.ControllerType.CRUD_CONTROLLER,
    requestMapping:{
    GeneralParameter:"/generalparameters"
    }
})
export class GeneralParameterController implements lego.ILegoController<IGeneralParameterModel,lego.IGeneralActions>,lego.IGeneralActions{



    @lego.Inject()
    public generalParameterService:GeneralParameterOperationsService;
    public model: IGeneralParameterModel;
    public actions: lego.IGeneralActions;
    public mainGrid: lego.ILegoGridOptions;
    public Utils: lego.IUtils;
    public NotificationUtils:lego.INotificationUtils;
    public restService;
    public GeneralParameterType;
    public constructor() {
        // todo do not write anything here
        this.actions = new ControllerActions(this);
    };

    getConfig(): lego.IAbstractControllerConfig<IGeneralParameterModel, lego.IGeneralActions> {

        let gridConf: lego.ILegoGridOptions ={

            columnDefs: [
                {
                    field: 'id' 
                    ,displayName: 'GeneralParameter.id'
                    ,headerCellFilter: 'translate'
                    ,visible:false
                },
                {
                    field: 'mainType' 
                    ,displayName: 'GeneralParameter.mainType'
                    ,headerCellFilter: 'translate'
                    ,cellFilter: 'translate:"GeneralParameterType"'
                },
                {
                    field: 'code' 
                    ,displayName: 'GeneralParameter.code'
                    ,headerCellFilter: 'translate'
                },
                {
                    field: 'description' 
                    ,displayName: 'GeneralParameter.description'
                    ,headerCellFilter: 'translate'
                },
                {
                    field: 'priority' 
                    ,displayName: 'GeneralParameter.priority'
                    ,headerCellFilter: 'translate'
                },
            ]
        }
        return  {
            crudService: this.generalParameterService,
            restService: this.restService.GeneralParameter,
            gridConf: gridConf
        };
    }

    $onInit() {
        this.restService.GeneralParameter.getGeneralParameterTypes().$promise.then(
            (success)=>{

                this.GeneralParameterType = this.Utils.convertEnum(success,"GeneralParameterType");
            }
        );
    }
}

class ControllerActions extends lego.LegoBasicActions<lego.IBasicModel>{
    constructor(public ctrl:GeneralParameterController){
        super()
    }

        }