System.config({
    transpiler: 'typescript',
    packages: {
        app: {
            defaultExtension: 'ts'
        }
    },
    typescriptOptions: {
        module: "system",
        target: "es5",
        sourceMap: true,
        experimentalDecorators: true,
        emitDecoratorMetadata: true,
        noImplicitAny: true,
        typeCheck: true
    }
});