package com.caspco.spl.oauth.server;

import com.caspco.spl.security.configuration.handler.SuccessUrlDeterminer;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 11/12/17
 * Time: 6:05 PM
 */

@Component
public class OauthSuccessUrlDeterminer implements SuccessUrlDeterminer {

    @Override
    public String determine(HttpServletRequest request) {
        return "/oauth/confirm_access";
    }

    @Override
    public Boolean canChangeUrl(HttpServletRequest request) {
//        HttpSession session = request.getSession(false);
//        return session.getAttribute("OAUTH")!=null;
        return true;
    }
}
