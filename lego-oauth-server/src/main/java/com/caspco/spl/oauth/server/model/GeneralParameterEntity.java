package com.caspco.spl.oauth.server.model;

import com.caspco.spl.model.annotations.cg.*;
import com.caspco.spl.model.data.ActiveableEntity;
import com.caspco.spl.model.wrench.Locale;
import com.caspco.spl.oauth.server.model.enums.GeneralParameterType;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Translations({ @Translation(label = "\u0645\u0642\u0627\u062f\u06cc\u0631 \u067e\u0627\u06cc\u0647", locale = Locale.Persian_Iran), @Translation(label = "General Parameters", locale = Locale.English_United_States) })
@Table(name = "GeneralParameter", uniqueConstraints = { @UniqueConstraint(columnNames = { "mainType", "code" }) })
@Audited
@ClientSideCacheable(retainClean = true)
@ServerSideCacheable(retainClean = true)
@Entity(name = "GeneralParameterEntity")
@Generated(value = "XView code generator", date = "", comments = "")
public class GeneralParameterEntity extends ActiveableEntity
{
    private Long id;
    private String mainType;
    private String code;
    private String description;
    private Integer priority;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    @Column
    @Translations({ @Translation(label = "\u06a9\u062f", locale = Locale.Persian_Iran), @Translation(label = "Code", locale = Locale.English_United_States) })
    @NotNull
    @CacheKey(priority = 0)
    @ExactOnSearch
    public String getCode() {
        return this.code;
    }
    
    public void setCode(final String code) {
        this.code = code;
    }
    
    @Column
    @Translations({ @Translation(label = "\u062a\u0648\u0636\u06cc\u062d\u0627\u062a", locale = Locale.Persian_Iran), @Translation(label = "Description", locale = Locale.English_United_States) })
    @NotNull
    @CacheValue(priority = 0)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(final String description) {
        this.description = description;
    }
    
    @Column
    @Translations({ @Translation(label = "\u0646\u0648\u0639", locale = Locale.Persian_Iran), @Translation(label = "Main type", locale = Locale.English_United_States) })
    @NotNull
    @CacheKey(priority = 1)
    @ExactOnSearch
    @EnumeratedWith(GeneralParameterType.class)
    public String getMainType() {
        return this.mainType;
    }
    
    public void setMainType(final String mainType) {
        this.mainType = mainType;
    }
    
    @Translations({ @Translation(label = "\u0627\u0648\u0644\u0648\u06cc\u062a", locale = Locale.Persian_Iran), @Translation(label = "Priority", locale = Locale.English_United_States) })
    public Integer getPriority() {
        return this.priority;
    }
    
    public void setPriority(final Integer priority) {
        this.priority = priority;
    }
}
