package com.caspco.spl.oauth.server.model.services;

/*
 * THIS IS AN AUTO GENERATED CODE.
 * @author : Lego Code Generator
 * Email : husseini@caspco.ir
 * Date: Jul 13, 2017 2:57:17 PM
 * @version 2.7.1
 */

import com.caspco.spl.data.service.AbstractSplDataService;
import com.caspco.spl.model.annotations.cg.Generated;
import com.caspco.spl.oauth.server.model.ApplicationEntity;
import com.caspco.spl.oauth.server.model.QApplicationEntity;
import com.caspco.spl.oauth.server.model.dtos.ApplicationDto;
import com.caspco.spl.oauth.server.model.dtos.ServerDetailsDto;
import com.caspco.spl.oauth.server.model.repositories.ApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.SecureRandom;

@Service
@Generated(comments = "Generated by Spl Code Generator")
public class ApplicationService
        extends
        AbstractSplDataService<ApplicationEntity, java.lang.Long, ApplicationRepository> {
    @Autowired
    private Environment environment;
    public static final QApplicationEntity q = QApplicationEntity.applicationEntity;

    public ApplicationDto generatedSecret(ApplicationDto dto){
        dto.setSecret(getSecretCode());
        dto.setClientId(getSecretCode());
        return dto;
    }

    @Override
    public ApplicationEntity save(ApplicationEntity obj) {
        if (obj.getId() == null) {
            obj.setSecret(getSecretCode());
            obj.setClientId(getSecretCode());
        }
        return super.save(obj);
    }

    public ApplicationEntity findClientName(String name){
       return repository.findByClientId(name);
    }

    private String getSecretCode(){
        SecureRandom random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }

    public ServerDetailsDto getServerDetailsAddsress() throws UnknownHostException {
        String port = environment.getProperty("local.server.port");
        String ip = InetAddress.getLocalHost().getHostAddress();
         String baseUrl ="http://"+ip.concat(":").concat(port);
         ServerDetailsDto serverDetailsDto = new ServerDetailsDto();
         serverDetailsDto.setAuthURL(baseUrl.concat("/oauth/authorize"));
         serverDetailsDto.setAccessTokenURL(baseUrl.concat("/oauth/token"));
         return serverDetailsDto;
    }
}