//package com.caspco.spl.oauth.server;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.annotation.Order;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.userdetails.UserDetailsService;
//
///**
// * @author : Pooya. h
// *         Email : husseini@caspco.ir
// *         Date: 5/23/17
// *         Time: 5:06 PM
// */
//@Configuration
//@Order(-19)
//public class LoginConfig extends WebSecurityConfigurerAdapter {
//
//    @Autowired
//    private AuthenticationManager authenticationManager;
//    @Autowired
//    private UserDetailsService userDetail;
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests().antMatchers("/app/**","/#").permitAll().and().formLogin().loginPage("/login").permitAll()
//                .and()
//                .requestMatchers().antMatchers("/login", "/oauth/authorize", "/oauth/confirm_access", "static/css/**")
//                .and()
//                .authorizeRequests().anyRequest().authenticated()
//                .and().csrf().disable();
//    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userDetail);
//        auth.parentAuthenticationManager(authenticationManager);
//    }
//
//}