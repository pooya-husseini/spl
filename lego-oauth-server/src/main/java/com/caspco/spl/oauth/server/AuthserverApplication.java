package com.caspco.spl.oauth.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.RedirectView;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.TreeMap;
//@EnableSingleLogin(mode = LoginMode.USER_NAME)
@SpringBootApplication
@Controller
@SessionAttributes("authorizationRequest")
@EnableResourceServer
public class AuthserverApplication extends WebMvcConfigurerAdapter {

    @Autowired
    private ClientDetailsServiceBean clientDetailsServiceBean;

    @RequestMapping("/profile")
    @ResponseBody
    public Authentication user(Authentication user) throws NamingException, IOException {
        return user;
    }
    @RequestMapping("/oauth/authorize/approve")
    public View getAccessConfirmation(@ModelAttribute AuthorizationRequest clientAuth , HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession();
        ClientDetails client = null;
        AuthorizationRequest authorizationRequest= (AuthorizationRequest) session.getAttribute("authorizationRequest");
        if(authorizationRequest != null){
            client= clientDetailsServiceBean.loadClientByClientId(authorizationRequest.getClientId());
        }
        TreeMap<String, Object> model = new TreeMap<String, Object>();
        return new RedirectView("authorize");
    }
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/oauth/confirm_access").setViewName("authorize");
    }
    public static void main(String[] args) {
        SpringApplication.run(AuthserverApplication.class, args);
    }
}
