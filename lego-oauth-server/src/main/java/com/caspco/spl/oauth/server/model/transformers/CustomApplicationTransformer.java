package com.caspco.spl.oauth.server.model.transformers;

import com.caspco.spl.oauth.server.model.dtos.ApplicationDto;
import com.caspco.spl.oauth.server.model.dtos.CustomApplicationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by saeedko on 8/13/17.
 */
@Component
public class CustomApplicationTransformer {
    @Autowired
    private Environment environment;
    public CustomApplicationDto customApplicationDto(ApplicationDto dto) {
        CustomApplicationDto customApplicationDto = new CustomApplicationDto();
        customApplicationDto.setId(dto.getId());
        customApplicationDto.setClientId(dto.getClientId());
        customApplicationDto.setSecret(dto.getSecret());
        customApplicationDto.setDescription(dto.getDescription());
        customApplicationDto.setName(dto.getName());
        customApplicationDto.setCallbackUrl(dto.getCallbackUrl());
        customApplicationDto.setScopes(dto.getScopes());
        try {
            customApplicationDto.setAuthURL(getaddress()+"/oauth/authorize");
            customApplicationDto.setAccessTokenURL(getaddress()+"/oauth/token");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return customApplicationDto;
    }
    private String getaddress() throws UnknownHostException {
        String port = environment.getProperty("local.server.port");
        String ip = InetAddress.getLocalHost().getHostAddress();
        return "http://"+ip.concat(":").concat(port);
    }
}
