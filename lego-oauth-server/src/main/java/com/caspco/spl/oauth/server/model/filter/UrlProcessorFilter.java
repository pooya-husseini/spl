package com.caspco.spl.oauth.server.model.filter;

import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.DelegatingFilterProxy;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by saeedko on 8/8/17.
 */
@Component
public class UrlProcessorFilter extends DelegatingFilterProxy {
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpSession session = request.getSession(false);
        if(session != null) {
            DefaultSavedRequest savedRequest = (DefaultSavedRequest) session.getAttribute("SPRING_SECURITY_SAVED_REQUEST");
            if (savedRequest != null) {
                savedRequest.getParameterMap().entrySet().forEach(p -> {
                    if (p.getKey() != null && p.getKey().equals("client_id")) {
                        session.setAttribute("OAUTH", true);
                    }
                });
            }
                AuthorizationRequest authorizationRequest = (AuthorizationRequest) session.getAttribute("authorizationRequest");
                if (authorizationRequest != null) {
                    Set<String> selectedScops = new HashSet<>();
                    Enumeration<String> parameterNames = request.getParameterNames();
                    String name = "";
                    while (parameterNames.hasMoreElements()) {
                        name = parameterNames.nextElement();
                        if (name.startsWith("scopes[")) {
                            name = name.replace("scopes[", "").replace("]", "");
                            if(authorizationRequest.getScope().contains(name))
                                 selectedScops.add(name);
                        }
                    }
                    if (selectedScops.size() > 0) {
                        authorizationRequest.setScope(selectedScops);
                    }
                }
            }
        filterChain.doFilter(request,response);
    }
}
