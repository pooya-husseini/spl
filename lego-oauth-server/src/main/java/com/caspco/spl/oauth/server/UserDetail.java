//package com.caspco.spl.oauth.server;
//
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Component;
//
//import java.util.Arrays;
//
///**
// * @author : Pooya. h
// *         Email : husseini@caspco.ir
// *         Date: 5/9/17
// *         Time: 7:54 PM
// */
//
//@Component
//public class UserDetail implements UserDetailsService {
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        GrantedAuthority a = new SimpleGrantedAuthority("authorization_code");
//        GrantedAuthority b = new SimpleGrantedAuthority("refresh_token");
//        GrantedAuthority c = new SimpleGrantedAuthority("password");
//        GrantedAuthority d = new SimpleGrantedAuthority("client_credentials");
//
//        return new User(username, "123", Arrays.asList(a, b, c, d));
////         todo override spl security user detail service
//    }
//}