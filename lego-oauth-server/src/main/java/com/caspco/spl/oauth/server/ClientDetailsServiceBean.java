package com.caspco.spl.oauth.server;

import com.caspco.spl.oauth.server.model.ApplicationEntity;
import com.caspco.spl.oauth.server.model.GeneralParameterEntity;
import com.caspco.spl.oauth.server.model.services.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by saeedko on 7/17/17.
 */
@Component
public class ClientDetailsServiceBean implements ClientDetailsService {
    @Autowired
    private ApplicationService applicationService;
    @Override
    public ClientDetails loadClientByClientId(String param) throws ClientRegistrationException {
        BaseClientDetails details = new BaseClientDetails();
        ApplicationEntity entity =applicationService.findClientName(param);
        if(entity == null){
            throw  new ClientRegistrationException("client is null");
        }
        //details.setRegisteredRedirectUri(Collections.singleton(entity.getCallbackUrl()));
        details.setClientId(entity.getClientId());
        details.setScope(entity.getScopes().stream().map(GeneralParameterEntity::getDescription).collect(Collectors.toList()));
        details.setClientSecret(entity.getSecret());
        details.setAuthorizedGrantTypes(Arrays.asList("authorization_code","refresh_token","password"));
        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_CLIENT"));
        details.setAuthorities(authorities);
        //details.setAutoApproveScopes(details.getScope());
        return details;
    }
}
