package com.caspco.spl.oauth.server.model;

import com.caspco.spl.model.annotations.cg.Translation;
import com.caspco.spl.model.annotations.cg.Translations;
import com.caspco.spl.model.wrench.Locale;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.List;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 7/9/17
 *         Time: 12:20 PM
 */

@Entity
@Table(name ="Application" )
@Audited
@Translations({ @Translation(label = "برنامه ها", locale = Locale.Persian_Iran), @Translation(label = "General Parameters", locale = Locale.English_United_States) })

public class ApplicationEntity {
    private Long id;
    private List<GeneralParameterEntity> scopes;
    private String description;
    private String callbackUrl;
    private String name;
    private String secret;
    private String clientId;

    @Id
    @GeneratedValue
    @Column(name = "appId")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @ManyToMany(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    @JoinTable(name = "app_scopes", joinColumns =
            { @JoinColumn(name = "appId") }, inverseJoinColumns =
            { @JoinColumn(name = "id") })
    @Translations({ @Translation(label = "دسترسی ها", locale = Locale.Persian_Iran), @Translation(label = "Scopes", locale = Locale.English_United_States) })
    public List<GeneralParameterEntity> getScopes() {
        return scopes;
    }

    public void setScopes(List<GeneralParameterEntity> scopes) {
        this.scopes = scopes;
    }

    @Translations({ @Translation(label = "شرح برنامه", locale = Locale.Persian_Iran), @Translation(label = "Description", locale = Locale.English_United_States) })
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Translations({ @Translation(label = "آدرس سرویس گیرنده", locale = Locale.Persian_Iran), @Translation(label = "CallbackUrl", locale = Locale.English_United_States) })
    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    @Translations({ @Translation(label = "نام", locale = Locale.Persian_Iran), @Translation(label = "Name", locale = Locale.English_United_States) })

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Translations({ @Translation(label = "رمز سرویس گیرنده", locale = Locale.Persian_Iran), @Translation(label = "Secret", locale = Locale.English_United_States) })
    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Translations({ @Translation(label = "مشخصه سرویس گیرنده", locale = Locale.Persian_Iran), @Translation(label = "Secret", locale = Locale.English_United_States) })
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
}
