var gulp = require('gulp');
var gulpConfig = require('./gulp.config');
var concat = require('gulp-concat');
var angularFilesort = require('gulp-angular-filesort'),
    inject = require('gulp-inject');
var mainBowerFiles = require('main-bower-files');
var cleanCSS = require('gulp-clean-css');
// var copy = require('gulp-contrib-copy');
// var through = require('through2');
var rename = require("gulp-rename");
var _ = require("lodash");
var clean = require('gulp-clean');

gulp.task('finalConcat', ["projectMinify"], function () {
    return gulp.src(gulpConfig.config.jsSources.finalFiles)
        .pipe(concat('index.js'))
        .pipe(gulp.dest('./src/main/resources/static/app/dist'));
});

gulp.task('projectMinify', ["depsMinify"], function () {
    return gulp.src(gulpConfig.config.jsSources.allSources)
        .pipe(angularFilesort()).pipe(concat('project.js'))
        // .pipe(uglify())
        .pipe(gulp.dest('./src/main/resources/static/app/dist'));
});

gulp.task('injectCss',["injectDependencies"], function () {
    var cssFiles = mainBowerFiles("**/*.css");
    var lessFiles = mainBowerFiles("**/*.less");

    var allDeps = cssFiles.concat(gulpConfig.config.cssSources.deps)
        .concat(gulpConfig.config.cssSources.sources).concat(lessFiles);
    console.log(allDeps);
    return gulp.src(gulpConfig.config.indexPage)
        .pipe(inject(
            gulp.src(allDeps)
            , {ignorePath: 'src/main/resources/static/', addRootSlash: false,name:"cssDeps"}))
        .pipe(gulp.dest('./src/main/resources/static/'));
});


gulp.task('injectJs', ["injectDependencies", "renderTranslations", "renderRests"], function () {
    return gulp.src(gulpConfig.config.indexPage)
        .pipe(inject(
            gulp.src(gulpConfig.config.jsSources.allSources)
                .pipe(angularFilesort())
            , {ignorePath: 'src/main/resources/static/', addRootSlash: false}))
        .pipe(gulp.dest('./src/main/resources/static/'));
});

gulp.task('injectDependencies'/*, ["renderTranslations", "renderRests"]*/, function () {
    var bowerFiles = mainBowerFiles("**/*.js");
    var allDeps = bowerFiles.concat(gulpConfig.config.jsSources.deps);
    return gulp.src(gulpConfig.config.indexPage)
        .pipe(inject(
            gulp.src(allDeps)
            , {ignorePath: 'src/main/resources/static/', addRootSlash: false, name: "deps"}))
        .pipe(gulp.dest('./src/main/resources/static/'));

});


gulp.task('minifyCss', function () {
    var cssFiles = mainBowerFiles("**/*.css");

    return gulp.src(cssFiles)
        .pipe(concat("index.css"))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./src/main/resources/static/app/dist'));

});

gulp.task('copyResources', function () {
    var bowerFiles = mainBowerFiles("**/*.woff").concat(mainBowerFiles("**/*.ttf"));
    return gulp.src(bowerFiles)
        .pipe(copy())
        .pipe(gulp.dest('./src/main/resources/static/app/dist'));
});

gulp.task('depsMinify', function () {
    var bowerFiles = mainBowerFiles("**/*.js");
    return gulp.src(bowerFiles)
        .pipe(concat('lego.js'))
        // .pipe(uglify())
        .pipe(gulp.dest('./src/main/resources/static/app/dist'));

});

gulp.task('release', ["depsMinify",
    "projectMinify",
    "minifyCss",
    "copyResources",
    "renderTranslations",
    "renderRests",
    "finalConcat"
    ], function () {
    return gulp.src(gulpConfig.config.jsSources.finalFiles)
        .pipe(clean());
});

gulp.task('Test', ["release"], function (done) {
    new Server({
        configFile: __dirname + '/karma.conf.js'
    }, done).start();
});

gulp.task('development', [
        "injectCss",
        "injectDependencies"
        ],
    function () {

    }
);

gulp.task('cdnizer', function () {
    return gulp.src(gulpConfig.config.indexPage)
        .pipe(googlecdn(require('./bower.json'), {
            bowerComponents: './src/main/resources/static/vendors/',
            allowRev: true,
            allowMin: true
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('scripts', function () {
    var tsProject = ts.createProject('tsconfig.json');
    return tsProject.src("./src/main/resources/static/app/**/*.ts")
        .pipe(sourcemaps.init())
        .pipe(ts(tsProject))
        .on('error', function () {
            process.exit(1);
        }).js
        .pipe(sourcemaps.write("./src/main/resources/static/app/"))
        .pipe(gulp.dest("./src/main/resources/static/app/"))
});







gulp.task('beautify', function () {
    // return gulp.src('');
    //     .pipe(beautify({indentSize: 2}))
    //     .pipe(gulp.dest(gulpConfig.config.appPath))
    return gulp.src('./src/main/resources/static/app/**/*.ts')
        .pipe(tsfmt())
        .pipe(gulp.dest(gulpConfig.config.appPath));
});

gulp.task('prettify', function () {
    return gulp.src(gulpConfig.config.htmlSources.projectSources)
        .pipe(prettify({indent_size: 4}))
        .pipe(gulp.dest(gulpConfig.config.appPath))
});

gulp.task('reformatCode', ["prettify", "beautify"], function () {

});