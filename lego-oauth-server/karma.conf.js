// Karma configuration
// Generated on Mon May 18 2015 13:35:51 GMT+0200 (CEST)

module.exports = function (config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],


        // list of files / patterns to load in the browser
        files: [
            'src/main/resources/static/app/dist/index.js',
            'src/test/js/restMocks/*.js',
            'src/test/js/items/*.js',
            'src/test/js/tests/*.js'
        ],

        // list of files to exclude
        exclude: [],
        reporters: ['progress', 'coverage','html'],
        preprocessors: {
            'src/**/*.js': ['coverage']
        },
        coverageReporter: {
            // specify a common output directory
            dir: './src/test/coverageReport',
            reporters: [
                // reporters not supporting the `file` property
                { type: 'html', subdir: 'report-html' }
            ]},
        htmlReporter: {
            outputFile: 'src/test/result/units.html',

            // Optional
            pageTitle: 'Unit Tests',
            subPageTitle: 'A sample project description',
            groupSuites: true,
            useCompactStyle: true,
            useLegacyStyle: true
        },
            // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        //
      browsers: ['PhantomJS'],
         // browsers: ['Chrome'],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: true
    });
};
