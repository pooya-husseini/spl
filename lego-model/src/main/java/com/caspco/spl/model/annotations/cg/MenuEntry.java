package com.caspco.spl.model.annotations.cg;


public @interface MenuEntry {
    String name();
    Translation[] translations();
}