package com.caspco.spl.model.web;

import java.util.List;
import java.util.Map;

/**
 * Created by pooya on 9/25/16.
 */
public class RestMethod {
    private String methodName;
    private List<String> restMethods;
    private Map<String, String> restParameters;
    private String url;
    private Boolean array;

    public RestMethod(String methodName, List<String> restMethods, Map<String, String> restParameters, String url, Boolean array) {
        this.methodName = methodName;
        this.restMethods = restMethods;
        this.restParameters = restParameters;
        this.url = url;
        this.array = array;
    }

    /**
     * Getter for property 'methodName'.
     *
     * @return Value for property 'methodName'.
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     * Setter for property 'methodName'.
     *
     * @param methodName Value to set for property 'methodName'.
     */
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    /**
     * Getter for property 'url'.
     *
     * @return Value for property 'url'.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Setter for property 'url'.
     *
     * @param url Value to set for property 'url'.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Getter for property 'restMethods'.
     *
     * @return Value for property 'restMethods'.
     */
    public List<String> getRestMethods() {
        return restMethods;
    }

    /**
     * Setter for property 'restMethods'.
     *
     * @param restMethods Value to set for property 'restMethods'.
     */
    public void setRestMethods(List<String> restMethods) {
        this.restMethods = restMethods;
    }

    /**
     * Getter for property 'array'.
     *
     * @return Value for property 'array'.
     */
    public Boolean getArray() {
        return array;
    }

    /**
     * Setter for property 'array'.
     *
     * @param array Value to set for property 'array'.
     */
    public void setArray(Boolean array) {
        this.array = array;
    }

    /**
     * Getter for property 'restParameters'.
     *
     * @return Value for property 'restParameters'.
     */
    public Map<String, String> getRestParameters() {
        return restParameters;
    }

    /**
     * Setter for property 'restParameters'.
     *
     * @param restParameters Value to set for property 'restParameters'.
     */
    public void setRestParameters(Map<String, String> restParameters) {
        this.restParameters = restParameters;
    }
}