package com.caspco.spl.model.validation;


import com.caspco.spl.model.annotations.validation.StringEquals;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 17/12/14
 *         Time: 16:32
 */

public class StringEqualityValidator implements ConstraintValidator<StringEquals, String> {
    private boolean ignoreCase;
    private String value;

    @Override
    public void initialize(StringEquals annotation) {
        ignoreCase = annotation.ignoreCase();
        value = annotation.value();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (ignoreCase) {
            return this.value.equalsIgnoreCase(value);
        } else {
            return this.value.equals(value);
        }
    }
}