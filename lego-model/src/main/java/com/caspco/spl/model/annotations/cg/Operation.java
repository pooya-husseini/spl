package com.caspco.spl.model.annotations.cg;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 1/9/17
 *         Time: 5:13 PM
 */
public @interface Operation {
    String name();
    Translation[] translations();
    boolean generateServerSide() default true;
    boolean generateClientSide() default true;
    Class<?> model() default Void.class;
}