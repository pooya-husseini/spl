package com.caspco.spl.model.annotations.cg;


import com.caspco.spl.model.wrench.Locale;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.FIELD})
public @interface Translation {
    Locale locale();
    String label();
}
