package com.caspco.spl.model.data;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 11/9/15
 *         Time: 10:21 PM
 */

public class HistoricalModel {
    private Object id;
    private ModificationType modificationType;

    public HistoricalModel() {
    }


    public HistoricalModel(Object id, ModificationType modificationType) {
        this.id = id;
        this.modificationType = modificationType;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public ModificationType getModificationType() {
        return modificationType;
    }

    public void setModificationType(ModificationType modificationType) {
        this.modificationType = modificationType;
    }

    public enum ModificationType {
        DELETED, MODIFIED, INSERTED
    }
}


