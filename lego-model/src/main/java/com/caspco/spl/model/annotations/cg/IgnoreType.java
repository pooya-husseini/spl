package com.caspco.spl.model.annotations.cg;

public enum IgnoreType {
    ALL,
    UI,
    CLIENT_SIDE_CONTROLLERS,
    CLIENT_SIDE_REST,
    CLIENT_SIDE_SERVICES,
    SERVER_SIDE_CONTROLLER,
    DTO,
    TRANSFORMER,
    PRINCIPAL,
    DO_NOT_GENERATE,
    SERVER_SIDE_SERVICES
}
