package com.caspco.spl.model.annotations.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 30/06/14
 *         Time: 13:18
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.ANNOTATION_TYPE})
public @interface SendMail {
    String[]  to() ;
    String[] bcc() default {};
    String[] cc() default {};

    String from() default "";

    String subject() default "";
    String message() default "";
}