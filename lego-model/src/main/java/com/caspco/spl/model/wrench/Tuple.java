package com.caspco.spl.model.wrench;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 2/21/17
 *         Time: 12:19 PM
 */
public class Tuple<A,B> {
    private final A left;
    private final B right;

    public Tuple(A left, B right) {
        this.left = left;
        this.right = right;
    }


    public static <A,B>Tuple of(A a,B b) {
        return new Tuple<>(a,b);
    }

    /**
     * Getter for property 'left'.
     *
     * @return Value for property 'left'.
     */
    public A getLeft() {
        return left;
    }

    /**
     * Getter for property 'right'.
     *
     * @return Value for property 'right'.
     */
    public B getRight() {
        return right;
    }
}
