package com.caspco.spl.model.annotations.proxy;

import org.hibernate.annotations.common.annotationfactory.AnnotationDescriptor;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 12/28/15
 *         Time: 3:35 PM
 */
public class AnnotationProxy implements Annotation, InvocationHandler {

    private final Class<? extends Annotation> annotationType;
    private final Map<Method, Object> values;

    public AnnotationProxy(AnnotationDescriptor descriptor) {
        this.annotationType = descriptor.type();
        values = getAnnotationValues(descriptor);
    }

    private Map<Method, Object> getAnnotationValues(AnnotationDescriptor descriptor) {
        Map<Method, Object> result = new HashMap<>();
        int processedValuesFromDescriptor = 0;
        for (Method m : annotationType.getDeclaredMethods()) {
            if (descriptor.containsElement(m.getName())) {
                result.put(m, descriptor.valueOf(m.getName()));
                processedValuesFromDescriptor++;
            } else if (m.getDefaultValue() != null) {
                result.put(m, m.getDefaultValue());
            } else {
                throw new IllegalArgumentException("No value provided for " + m.getName());
            }
        }
        if (processedValuesFromDescriptor != descriptor.numberOfElements()) {
            throw new RuntimeException("Trying to instanciate " + annotationType + " with unknown elements");
        }
        return result;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (values.containsKey(method)) {
            return values.get(method);
        }
        return method.invoke(this, args);
    }

    public Class<? extends Annotation> annotationType() {
        return annotationType;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append('@').append(annotationType().getName());
        result.append('(');
        boolean containsElements = false;
        for (Method m : getRegisteredMethodsInAlphabeticalOrder()) {
            Object obj = values.get(m);

            if (obj == null || obj.equals(m.getDefaultValue())) {
                continue;
            }

            containsElements = true;
            String repr = obj.toString();
            if (obj.getClass().isArray()) {
                repr = Arrays.deepToString((Object[]) obj).replace("[", "{").replace("]", "}");
            } else if (obj instanceof Class) {
                repr = ((Class) obj).getName() + ".class";
            } else if (obj instanceof String) {
                repr = "\"" + repr + "\"";
            }
            result.append(m.getName()).append('=').append(repr).append(", ");
        }
        // remove last separator:
        if (containsElements) {
            result.delete(result.length() - 2, result.length());
            result.append(")");
        } else {
            result.delete(result.length() - 1, result.length());
        }

        return result.toString();
    }

    private SortedSet<Method> getRegisteredMethodsInAlphabeticalOrder() {
        SortedSet<Method> result = new TreeSet<>(
                (Comparator<Method>) (o1, o2) -> o1.getName().compareTo(o2.getName())
        );
        result.addAll(values.keySet());
        return result;
    }
}

