package com.caspco.spl.model.annotations.web;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 9/2/17
 * Time: 2:50 PM
 */
public enum RequestMethod {
    GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
}