package com.caspco.spl.model.wrench.exception;

/**
 * Created by amini on 10/23/2016.
 */
public class TranslateModel {
    private String label;
    private String locale;

    public TranslateModel(String label, String locale) {
        this.label = label;
        this.locale = locale;
    }

    public String getLabel() {
        return label;
    }

    public String getLocale() {
        return locale;
    }

}
