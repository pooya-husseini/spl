package com.caspco.spl.model.annotations.proxy;

import com.caspco.spl.model.annotations.cg.*;
import com.caspco.spl.model.wrench.Locale;
import org.hibernate.annotations.common.annotationfactory.AnnotationDescriptor;

import javax.persistence.*;
import java.lang.annotation.Annotation;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 12/28/15
 *         Time: 2:41 PM
 */
public class CgAnnotationMaker {


    public static Annotation createGeneralParameter(final String gpType, Annotation[] translations) {
        AnnotationDescriptor gpDescriptor = new AnnotationDescriptor(GeneralParameter.class);
        gpDescriptor.setValue("type", gpType);
        gpDescriptor.setValue("translations", translations);
        return new AnnotationProxy(gpDescriptor);
    }

    public static Annotation createExactOnSearch() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(ExactOnSearch.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createGenerated() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(GeneratedValue.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createColumn(String name) {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(Column.class);
        descriptor.setValue("name", name);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createTranslation(Locale locale, String label) {
        AnnotationDescriptor translationDescriptor = new AnnotationDescriptor(Translation.class);
        translationDescriptor.setValue("locale", locale);
        translationDescriptor.setValue("label", label);
        return new AnnotationProxy(translationDescriptor);
    }

    public static Annotation createTranslations(Annotation[] translations) {
        AnnotationDescriptor translationDescriptor = new AnnotationDescriptor(Translations.class);
        translationDescriptor.setValue("value", translations);
        return new AnnotationProxy(translationDescriptor);
    }

    public static Annotation createId() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(Id.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createFileId() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(FileId.class);
        return new AnnotationProxy(descriptor);
    }


    public static Annotation createManyToMany() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(ManyToMany.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createManyToOne() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(ManyToOne.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createOneToMany() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(OneToMany.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createOneToOne() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(OneToOne.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createSearchable() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(Searchable.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createOnSelect() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(CreateOnSelect.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createCrudWidget() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(CrudWidget.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createDateRange() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(DateRange.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createDisablility(String dependsOn) {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(Disability.class);
        descriptor.setValue("dependsOn", dependsOn);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createEnumEntity() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(EnumEntity.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createEnumeratedWith(Class<?> clazz) {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(EnumeratedWith.class);
        descriptor.setValue("value", clazz);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createEnumeratedWith(String clazz) {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(EnumeratedWith.class);
        descriptor.setValue("value", clazz);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createIgnore() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(Ignore.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createLookup() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(Lookup.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createNotUi() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(NotUi.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createTable(String name) {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(Table.class);
        descriptor.setValue("name", name);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createPriority() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(Priority.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createSearchPage() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(SearchPage.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createTextArea() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(TextArea.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createTitle() {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(Title.class);
        return new AnnotationProxy(descriptor);
    }

    public static Annotation createVisibility(String dependsOn) {
        AnnotationDescriptor descriptor = new AnnotationDescriptor(Visibility.class);
        descriptor.setValue("dependsOn", dependsOn);
        return new AnnotationProxy(descriptor);
    }
}