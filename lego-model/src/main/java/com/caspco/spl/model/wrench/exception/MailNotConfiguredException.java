package com.caspco.spl.model.wrench.exception;


import com.caspco.spl.model.annotations.cg.Translation;
import com.caspco.spl.model.annotations.rcg.SplException;
import com.caspco.spl.model.wrench.Locale;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 19/01/15
 *         Time: 11:08
 */

@SplException(translations = {
        @Translation(locale = Locale.Persian_Iran,label = "سرور ایمیل تنظیم نشده است")
})
public class MailNotConfiguredException extends SplBaseException {
    public MailNotConfiguredException() {
    }

    public MailNotConfiguredException(String message) {
        super(message);
    }

    public MailNotConfiguredException(String message, Throwable cause) {
        super(message, cause);
    }

    public MailNotConfiguredException(Throwable cause) {
        super(cause);
    }

    public MailNotConfiguredException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
