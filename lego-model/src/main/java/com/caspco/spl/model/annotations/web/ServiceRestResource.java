package com.caspco.spl.model.annotations.web;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 8/30/17
 * Time: 8:39 AM
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ServiceRestResource {
}
