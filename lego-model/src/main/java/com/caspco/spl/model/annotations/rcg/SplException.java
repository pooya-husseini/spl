package com.caspco.spl.model.annotations.rcg;

import com.caspco.spl.model.annotations.cg.Translation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by pooya on 10/19/16.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface SplException {
    Translation[] translations();

    Class<? extends Exception> resolveFor() default Exception.class;
}