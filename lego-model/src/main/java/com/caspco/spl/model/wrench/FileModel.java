package com.caspco.spl.model.wrench;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 5/24/17
 *         Time: 12:25 PM
 */
public class FileModel {
    private byte[] content;
    private String name;
    private String contentType;

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
