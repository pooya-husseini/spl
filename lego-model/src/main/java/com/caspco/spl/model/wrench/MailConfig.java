package com.caspco.spl.model.wrench;

import javax.activation.FileTypeMap;
import java.util.Properties;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 19/01/15
 *         Time: 13:09
 */
public class MailConfig {

    public static final int DEFAULT_PORT = -1;

    private Properties javaMailProperties = new Properties();

    private String protocol;

    private String host;

    private int port = DEFAULT_PORT;

    private String username;

    private String password;

    private String defaultEncoding;

    private FileTypeMap defaultFileTypeMap;

    public Properties getJavaMailProperties() {
        return javaMailProperties;
    }

    public void setJavaMailProperties(Properties javaMailProperties) {
        this.javaMailProperties = javaMailProperties;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDefaultEncoding() {
        return defaultEncoding;
    }

    public void setDefaultEncoding(String defaultEncoding) {
        this.defaultEncoding = defaultEncoding;
    }

    public FileTypeMap getDefaultFileTypeMap() {
        return defaultFileTypeMap;
    }

    public void setDefaultFileTypeMap(FileTypeMap defaultFileTypeMap) {
        this.defaultFileTypeMap = defaultFileTypeMap;
    }
}
