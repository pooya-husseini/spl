package com.caspco.spl.model.annotations.json;

import com.fasterxml.jackson.annotation.JacksonAnnotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: ${Date}
 *         Time: ${time}
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@JacksonAnnotation
public @interface SecuredValue {

}
