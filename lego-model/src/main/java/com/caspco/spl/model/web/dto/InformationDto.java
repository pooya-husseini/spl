package com.caspco.spl.model.web.dto;

/**
 * Created by mokaram on 11/23/2016.
 */
public class InformationDto {
    private String version;

    public InformationDto(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
