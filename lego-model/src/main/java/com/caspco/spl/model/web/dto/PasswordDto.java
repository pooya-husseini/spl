package com.caspco.spl.model.web.dto;

import com.caspco.spl.model.wrench.Locale;
import com.caspco.spl.model.annotations.cg.NotUi;
import com.caspco.spl.model.annotations.cg.Translation;
import com.caspco.spl.model.annotations.cg.Translations;
import com.caspco.spl.model.annotations.cg.security.Password;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotNull;


/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 12/8/15
 *         Time: 5:13 PM
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Translations({
        @Translation(locale = Locale.Persian_Iran,label = "تغییر رمز عبور")
})
public class PasswordDto {
    private String oldPassword;
    private String password;
    private String username;

    @Translations({
            @Translation(locale = Locale.Persian_Iran,label = "رمز عبور قدیمی")
    })
    @Password(makeReEnter = false)
    @NotNull
    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    @Translations({
            @Translation(locale = Locale.Persian_Iran,label = "رمز عبور جدید")
    })
    @Password
    @NotNull
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @NotUi
    @NotNull
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}