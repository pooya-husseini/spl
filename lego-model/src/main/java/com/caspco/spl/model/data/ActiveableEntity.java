package com.caspco.spl.model.data;

import com.caspco.spl.model.annotations.cg.EnableFilter;
import com.caspco.spl.model.annotations.cg.Ignore;
import com.caspco.spl.model.annotations.cg.IgnoreType;
import com.caspco.spl.model.annotations.cg.NotUi;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 2/5/17
 *         Time: 2:19 PM
 */

@MappedSuperclass
@Ignore(mods = IgnoreType.ALL)
public abstract class ActiveableEntity {
    private Boolean active;

    /**
     * Getter for property 'active'.
     *
     * @return Value for property 'active'.
     */
    @EnableFilter
    @NotUi
    public Boolean getActive() {
        return active;
    }

    /**
     * Setter for property 'active'.
     *
     * @param active Value to set for property 'active'.
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    @PrePersist
    public void setDefaults(){
        if (this.active == null) {
            this.active = true;
        }
    }
}
