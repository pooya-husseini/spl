package com.caspco.spl.model.annotations.cg.security;

import java.lang.annotation.*;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 06/01/15
 *         Time: 10:48
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Inherited
public @interface UserPrincipal {}
