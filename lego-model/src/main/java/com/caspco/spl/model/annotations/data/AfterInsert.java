package com.caspco.spl.model.annotations.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 30/06/14
 *         Time: 13:18
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface AfterInsert {
    ActionType actionType();

//    Class<? extends BeanConverter> converter() default BeanConverter.class;

    SendMail[] sendMails() default {};
}