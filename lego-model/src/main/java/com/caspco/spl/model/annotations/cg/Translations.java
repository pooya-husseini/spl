package com.caspco.spl.model.annotations.cg;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 30/06/14
 *         Time: 13:18
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD, FIELD, TYPE, PACKAGE})
public @interface Translations {
    Translation[] value();
}