package com.caspco.spl.model.web.exception;


import com.caspco.spl.model.annotations.cg.Translation;
import com.caspco.spl.model.annotations.rcg.SplException;
import com.caspco.spl.model.wrench.Locale;
import com.caspco.spl.model.wrench.exception.SplBaseException;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 19/01/15
 *         Time: 14:08
 */

@SplException(translations = {
        @Translation(locale = Locale.Persian_Iran,label = "رمز عبور وارد شده نادرست می باشد")
})
public class OldPasswordIsNotCorrect extends SplBaseException {
    public OldPasswordIsNotCorrect() {
    }

    public OldPasswordIsNotCorrect(String message) {
        super(message);
    }

    public OldPasswordIsNotCorrect(String message, Throwable cause) {
        super(message, cause);
    }

    public OldPasswordIsNotCorrect(Throwable cause) {
        super(cause);
    }

    public OldPasswordIsNotCorrect(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }


}
