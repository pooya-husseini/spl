package com.caspco.spl.model.wrench.exception;


import com.caspco.spl.model.annotations.cg.Translation;
import com.caspco.spl.model.annotations.rcg.SplException;
import com.caspco.spl.model.wrench.Locale;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 20/12/14
 *         Time: 11:26
 */

@SplException(translations = {
        @Translation(locale = Locale.Persian_Iran,label = "عملیات مورد نظر قابل انجام نمی باشد")
})
public class NotImplementedException extends SplBaseException {
    public NotImplementedException() {
        super();
    }

    public NotImplementedException(String message) {
        super(message);
    }

    public NotImplementedException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotImplementedException(Throwable cause) {
        super(cause);
    }

    protected NotImplementedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
