package com.caspco.spl.model.wrench.exception;


import com.caspco.spl.model. wrench.Locale;
import com.caspco.spl.model.annotations.cg.Translation;
import com.caspco.spl.model.annotations.rcg.SplException;

/**
 * Created by pooya on 10/26/16.
 */

@SplException(translations = {
        @Translation(locale= Locale.English_United_States,label="Permission is not selected"),
        @Translation(locale= Locale.Persian_Iran,label="لطفا دسترسی های مورد نیاز را انتخاب نمایید")
})
public class PermissionIsNullException extends SplBaseException {

}