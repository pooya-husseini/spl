package com.caspco.spl.model.wrench.exception;

import com.caspco.spl.model.annotations.cg.Translation;
import com.caspco.spl.model.annotations.rcg.SplException;
import com.caspco.spl.model.wrench.Locale;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 31/12/14
 *         Time: 14:23
 */

@SplException(translations = {
        @Translation(locale = Locale.Persian_Iran,label = "متاسفانه عملیات با خطا مواجه شده است")
})
public class SplBaseException extends RuntimeException {

    private String exceptionType;


    public SplBaseException() {
    }

    public SplBaseException(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    public SplBaseException(String message, String exceptionType) {
        super(message);
        this.exceptionType = exceptionType;
    }

    public SplBaseException(String message, Throwable cause, String exceptionType) {
        super(message, cause);
        this.exceptionType = exceptionType;
    }

    public SplBaseException(Throwable cause, String exceptionType) {
        super(cause);
        this.exceptionType = exceptionType;
    }

    public SplBaseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String exceptionType) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.exceptionType = exceptionType;
    }

    public SplBaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public SplBaseException(Throwable cause) {
        super(cause);
    }

    public SplBaseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }


    public String getExceptionType() {
        if (exceptionType == null) {
//            return this.getCause().getClass().getName();
            return this.getClass().getName();
        }
        return exceptionType;
    }

    public  <T extends SplBaseException> T resolve(Exception x) throws NoSuchFieldException, IllegalAccessException {
        this.initCause(x);
        return (T) this;
    }
}
