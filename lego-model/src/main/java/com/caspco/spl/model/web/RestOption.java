package com.caspco.spl.model.web;

import java.util.List;
import java.util.Map;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 2/5/17
 *         Time: 3:58 PM
 */

public class RestOption {
    private List<RestMethod> restMethods;
    private Map<String, String> options;


    public RestOption() {
    }

    public RestOption(List<RestMethod> restMethods, Map<String, String> options) {
        this.restMethods = restMethods;
        this.options = options;
    }

    /**
     * Getter for property 'restMethods'.
     *
     * @return Value for property 'restMethods'.
     */
    public List<RestMethod> getRestMethods() {
        return restMethods;
    }

    /**
     * Setter for property 'restMethods'.
     *
     * @param restMethods Value to set for property 'restMethods'.
     */
    public void setRestMethods(List<RestMethod> restMethods) {
        this.restMethods = restMethods;
    }

    /**
     * Getter for property 'options'.
     *
     * @return Value for property 'options'.
     */
    public Map<String, String> getOptions() {
        return options;
    }

    /**
     * Setter for property 'options'.
     *
     * @param options Value to set for property 'options'.
     */
    public void setOptions(Map<String, String> options) {
        this.options = options;
    }
}