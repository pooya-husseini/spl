package com.caspco.spl.model.annotations.web;

import java.lang.annotation.*;

/**
 * @author : Pooya. h
 * Email : husseini@caspco.ir
 * Date: 8/30/17
 * Time: 8:39 AM
 */

@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface RestParameter {
    String name();
}