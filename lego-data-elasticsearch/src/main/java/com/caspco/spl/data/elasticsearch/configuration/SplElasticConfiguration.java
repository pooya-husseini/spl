package com.caspco.spl.data.elasticsearch.configuration;

import com.caspco.spl.data.elasticsearch.repository.SplElasticFactoryBean;
import com.caspco.spl.data.elasticsearch.repository.SplElasticRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/12/17
 *         Time: 11:55 AM
 */

@Configuration
@EnableElasticsearchRepositories(
        basePackages = "${spl.common.scan-package}",
        repositoryBaseClass = SplElasticRepository.class,
        repositoryFactoryBeanClass = SplElasticFactoryBean.class
)
public class SplElasticConfiguration {

}
