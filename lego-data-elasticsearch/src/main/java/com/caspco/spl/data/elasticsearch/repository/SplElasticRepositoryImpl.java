package com.caspco.spl.data.elasticsearch.repository;

import com.caspco.spl.model.wrench.exception.NotImplementedException;
import com.google.common.collect.Lists;
import com.querydsl.core.types.OrderSpecifier;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.index.query.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.*;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.*;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.elasticsearch.repository.support.ElasticsearchEntityInformation;
import org.springframework.data.elasticsearch.repository.support.SimpleElasticsearchRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/12/17
 *         Time: 12:01 PM
 */

@NoRepositoryBean
public class SplElasticRepositoryImpl<T, ID extends Serializable> implements ElasticsearchRepository<T, ID> , SplElasticRepository<T, ID> {

    static final Logger LOGGER = LoggerFactory.getLogger(SplElasticRepositoryImpl.class);
    protected ElasticsearchOperations elasticsearchOperations;
    protected Class<T> entityClass;
    protected ElasticsearchEntityInformation<T, ID> entityInformation;

//    public SplElasticRepositoryImpl() {
//    }

    public SplElasticRepositoryImpl(ElasticsearchOperations elasticsearchOperations) {

        Assert.notNull(elasticsearchOperations, "ElasticsearchOperations must not be null!");

        this.setElasticsearchOperations(elasticsearchOperations);
    }

    public SplElasticRepositoryImpl(ElasticsearchEntityInformation<T, ID> metadata,
                                    ElasticsearchOperations elasticsearchOperations) {
        this(elasticsearchOperations);

        Assert.notNull(metadata, "ElasticsearchEntityInformation must not be null!");

        this.entityInformation = metadata;
        setEntityClass(this.entityInformation.getJavaType());
        try {
            if (createIndexAndMapping()) {
                createIndex();
                putMapping();
            }
        } catch (ElasticsearchException exception) {
            LOGGER.error("failed to load elasticsearch nodes : " + exception.getDetailedMessage());
        }
    }

    private void createIndex() {
        elasticsearchOperations.createIndex(getEntityClass());
    }

    private void putMapping() {
        elasticsearchOperations.putMapping(getEntityClass());
    }

    private boolean createIndexAndMapping() {
        return elasticsearchOperations.getPersistentEntityFor(getEntityClass()).isCreateIndexAndMapping();
    }

    @Override
    public T findOne(ID id) {
        GetQuery query = new GetQuery();
        query.setId(stringIdRepresentation(id));
        return elasticsearchOperations.queryForObject(query, getEntityClass());
    }

    @Override
    public List<T> find(T e) {
        throw new NotImplementedException();
    }

    @Override
    public List<T> findAll() {
        return Lists.newArrayList(this.findAllIterable());
    }

    private Iterable<T> findAllIterable() {
        int itemCount = (int) this.count();
        if (itemCount == 0) {
            return new PageImpl<T>(Collections.<T>emptyList());
        }
        return this.findAll(new PageRequest(0, Math.max(1, itemCount)));
    }

    @Override
    public Page<T> findAll(Pageable pageable) {
        SearchQuery query = new NativeSearchQueryBuilder().withQuery(matchAllQuery()).withPageable(pageable).build();
        return elasticsearchOperations.queryForPage(query, getEntityClass());
    }

    @Override
    public Iterable<T> findAll(Sort sort) {
        int itemCount = (int) this.count();
        if (itemCount == 0) {
            return new PageImpl<T>(Collections.<T>emptyList());
        }
        SearchQuery query = new NativeSearchQueryBuilder().withQuery(matchAllQuery())
                .withPageable(new PageRequest(0, itemCount, sort)).build();
        return elasticsearchOperations.queryForPage(query, getEntityClass());
    }

    @Override
    public Iterable<T> findAll(Iterable<ID> ids) {
        Assert.notNull(ids, "ids can't be null.");
        SearchQuery query = new NativeSearchQueryBuilder()
                .withIds(stringIdsRepresentation(ids))
                .build();
        return elasticsearchOperations.multiGet(query, getEntityClass());
    }

    @Override
    public long count() {
        SearchQuery query = new NativeSearchQueryBuilder().withQuery(matchAllQuery()).build();
        return elasticsearchOperations.count(query, getEntityClass());
    }

    @Override
    public <S extends T> S save(S entity) {
        Assert.notNull(entity, "Cannot save 'null' entity.");
        elasticsearchOperations.index(createIndexQuery(entity));
        elasticsearchOperations.refresh(entityInformation.getIndexName());
        return entity;
    }

    public <S extends T> List<S> save(List<S> entities) {
        Assert.notNull(entities, "Cannot insert 'null' as a List.");
        Assert.notEmpty(entities, "Cannot insert empty List.");
        List<IndexQuery> queries = new ArrayList<IndexQuery>();
        for (S s : entities) {
            queries.add(createIndexQuery(s));
        }
        elasticsearchOperations.bulkIndex(queries);
        elasticsearchOperations.refresh(entityInformation.getIndexName());
        return entities;
    }

    @Override
    public <S extends T> S index(S entity) {
        return save(entity);
    }

    @Override
    public <S extends T> List<S> save(Iterable<S> entities) {
        Assert.notNull(entities, "Cannot insert 'null' as a List.");
        List<IndexQuery> queries = new ArrayList<IndexQuery>();
        for (S s : entities) {
            queries.add(createIndexQuery(s));
        }
        elasticsearchOperations.bulkIndex(queries);
        elasticsearchOperations.refresh(entityInformation.getIndexName());
        return Lists.newArrayList(entities);
    }

    @Override
    public boolean exists(ID id) {
        return findOne(id) != null;
    }

    @Override
    public Iterable<T> search(QueryBuilder query) {
        SearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(query).build();
        int count = (int) elasticsearchOperations.count(searchQuery, getEntityClass());
        if (count == 0) {
            return new PageImpl<>(Collections.emptyList());
        }
        searchQuery.setPageable(new PageRequest(0, count));
        return elasticsearchOperations.queryForPage(searchQuery, getEntityClass());
    }

    @Override
    public Page<T> search(QueryBuilder query, Pageable pageable) {
        SearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(query).withPageable(pageable).build();
        return elasticsearchOperations.queryForPage(searchQuery, getEntityClass());
    }

    @Override
    public Page<T> search(SearchQuery query) {
        return elasticsearchOperations.queryForPage(query, getEntityClass());
    }

    @Override
    public Page<T> searchSimilar(T entity, String[] fields, Pageable pageable) {
        Assert.notNull(entity, "Cannot search similar records for 'null'.");
        Assert.notNull(pageable, "'pageable' cannot be 'null'");
        MoreLikeThisQuery query = new MoreLikeThisQuery();
        query.setId(stringIdRepresentation(extractIdFromBean(entity)));
        query.setPageable(pageable);
        if (fields != null) {
            query.addFields(fields);
        }
        return elasticsearchOperations.moreLikeThis(query, getEntityClass());
    }

    @Override
    public void delete(ID id) {
        Assert.notNull(id, "Cannot delete entity with id 'null'.");
        elasticsearchOperations.delete(entityInformation.getIndexName(), entityInformation.getType(),
                stringIdRepresentation(id));
        elasticsearchOperations.refresh(entityInformation.getIndexName());
    }

    @Override
    public void delete(T entity) {
        Assert.notNull(entity, "Cannot delete 'null' entity.");
        delete(extractIdFromBean(entity));
        elasticsearchOperations.refresh(entityInformation.getIndexName());
    }

    @Override
    public void delete(Iterable<? extends T> entities) {
        Assert.notNull(entities, "Cannot delete 'null' list.");
        for (T entity : entities) {
            delete(entity);
        }
    }

    @Override
    public void deleteAll() {
        DeleteQuery deleteQuery = new DeleteQuery();
        deleteQuery.setQuery(matchAllQuery());
        elasticsearchOperations.delete(deleteQuery, getEntityClass());
        elasticsearchOperations.refresh(entityInformation.getIndexName());
    }

    @Override
    public void refresh() {
        elasticsearchOperations.refresh(getEntityClass());
    }

    private IndexQuery createIndexQuery(T entity) {
        IndexQuery query = new IndexQuery();
        query.setObject(entity);
        query.setId(stringIdRepresentation(extractIdFromBean(entity)));
        query.setVersion(extractVersionFromBean(entity));
        query.setParentId(extractParentIdFromBean(entity));
        return query;
    }

    @SuppressWarnings("unchecked")
    private Class<T> resolveReturnedClassFromGenericType() {
        ParameterizedType parameterizedType = resolveReturnedClassFromGenericType(getClass());
        return (Class<T>) parameterizedType.getActualTypeArguments()[0];
    }

    private ParameterizedType resolveReturnedClassFromGenericType(Class<?> clazz) {
        Object genericSuperclass = clazz.getGenericSuperclass();
        if (genericSuperclass instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) genericSuperclass;
            Type rawtype = parameterizedType.getRawType();
            if (SimpleElasticsearchRepository.class.equals(rawtype)) {
                return parameterizedType;
            }
        }
        return resolveReturnedClassFromGenericType(clazz.getSuperclass());
    }

    @Override
    public Class<T> getEntityClass() {
        if (!isEntityClassSet()) {
            try {
                this.entityClass = resolveReturnedClassFromGenericType();
            } catch (Exception e) {
                throw new InvalidDataAccessApiUsageException("Unable to resolve EntityClass. Please use according setter!", e);
            }
        }
        return entityClass;
    }

    private boolean isEntityClassSet() {
        return entityClass != null;
    }

    public final void setEntityClass(Class<T> entityClass) {
        Assert.notNull(entityClass, "EntityClass must not be null.");
        this.entityClass = entityClass;
    }

    public final void setElasticsearchOperations(ElasticsearchOperations elasticsearchOperations) {
        Assert.notNull(elasticsearchOperations, "ElasticsearchOperations must not be null.");
        this.elasticsearchOperations = elasticsearchOperations;
    }

    protected ID extractIdFromBean(T entity) {
        if (entityInformation != null) {
            return entityInformation.getId(entity);
        }
        return null;
    }

    private List<String> stringIdsRepresentation(Iterable<ID> ids) {
        Assert.notNull(ids, "ids can't be null.");
        List<String> stringIds = new ArrayList<String>();
        for (ID id : ids) {
            stringIds.add(stringIdRepresentation(id));
        }
        return stringIds;
    }

    public String stringIdRepresentation(ID id) {
        return String.valueOf(id);
    }

    private Long extractVersionFromBean(T entity) {
        if (entityInformation != null) {
            return entityInformation.getVersion(entity);
        }
        return null;
    }

    private String extractParentIdFromBean(T entity) {
        if (entityInformation != null) {
            return entityInformation.getParentId(entity);
        }
        return null;
    }


    @Override
    public Class getTypeArgument() {
        return entityClass;
    }

    @Override
    public long count(T t) {
        throw new NotImplementedException();
    }

    @Override
    public boolean exists(T e) {
        throw new NotImplementedException();
    }

    @Override
    public void deleteAllInBatch() {
        throw new NotImplementedException();
    }

    @Override
    public List<String> searchColumn(String column, String value) {
        throw new NotImplementedException();
    }



    @Override
    public List<T> findActives(T e) {
        throw new NotImplementedException();
    }

    @Override
    public List<T> findActives(T e, OrderSpecifier<?>[] orders) {
        throw new NotImplementedException();
    }

    @Override
    public List<T> find(T e, OrderSpecifier<?>[] orders) {
        throw new NotImplementedException();
    }
}
