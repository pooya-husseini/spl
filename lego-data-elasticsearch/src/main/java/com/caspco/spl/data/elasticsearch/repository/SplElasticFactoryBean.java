package com.caspco.spl.data.elasticsearch.repository;

import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.repository.support.ElasticsearchRepositoryFactory;
import org.springframework.data.elasticsearch.repository.support.ElasticsearchRepositoryFactoryBean;
import org.springframework.data.repository.core.RepositoryInformation;
import org.springframework.data.repository.core.support.RepositoryFactoryBeanSupport;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;
import org.springframework.util.Assert;

import java.io.Serializable;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/12/17
 *         Time: 11:56 AM
 */
public class SplElasticFactoryBean<T extends SplElasticRepository<S, ID>, S, ID extends Serializable> extends RepositoryFactoryBeanSupport<T,S,ID> {
    private ElasticsearchOperations operations;


    /**
     * Creates a new {@link ElasticsearchRepositoryFactoryBean} for the given repository interface.
     *
     * @param repositoryInterface must not be {@literal null}.
     */
    public SplElasticFactoryBean(Class<? extends T> repositoryInterface) {
        super(repositoryInterface);
    }

    public void setElasticsearchOperations(ElasticsearchOperations operations) {

        Assert.notNull(operations, "ElasticsearchOperations must not be null!");

        setMappingContext(operations.getElasticsearchConverter().getMappingContext());
        this.operations = operations;
    }

    @Override
    protected RepositoryFactorySupport createRepositoryFactory() {
        return new SplElasticFactory(operations);
    }


    public class SplElasticFactory extends ElasticsearchRepositoryFactory{

        private final ElasticsearchOperations operations;

        public SplElasticFactory(ElasticsearchOperations elasticsearchOperations) {
            super(elasticsearchOperations);
            this.operations = elasticsearchOperations;
        }

        @Override
        protected Object getTargetRepository(RepositoryInformation metadata) {
            return new SplElasticRepositoryImpl(this.getEntityInformation(metadata.getDomainType()),this.operations);
        }
    }
}