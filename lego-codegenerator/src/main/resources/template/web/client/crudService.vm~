#parse("/template/util/java-license.vm")
#parse("/template/util/model-utils.vm")
#parse("/template/util/string-utils.vm")
#set($modelName=${dm.getAttribute('name')})
#set($restUrl="/$dm.getAttribute('domain').replace('.','/')/#pluralize(${modelName.toLowerCase()})")
#set($searchUrl="#uncapitalize(${dm.getAttribute('name')})")
#set($keyName=$dm.getIdentifier().getAttribute("name"))
#set($ctrl="${modelName}CrudCtrl")
#set($prefixPath="/app/view/${dm.getAttribute('domain').replace('.','/')}/")

(function (module) {
#[[module.service(]]# "${modelName}CrudService", #[[ ['operations', function (operations)]]# {
    return operations.makeBaseCrudService(
                "${prefixPath}Crud${modelName}.html",
                "${ctrl}",
#if($dm.containsAttribute("principal"))
#[[{
        changePassword: function (model) {
            return $modal.open({
                templateUrl:]]# "${prefixPath}ChangePassword.html",
                size: 'sm',
                controller: '${ctrl}',
                resolve: {
                    data: function () {
                        return model;
                    },
                    crudType:function(){
                        return undefined;
                    }
                }

            }).result;
        },
});
#else
{});
#end

#if($dm.fileIdElementsAsJava().size()>0)
#set($extraParams="FileTransferService")
#else
#set($extraParams="")
#end
}]).controller('${ctrl}', [
'$scope',
'${modelName}RestService',
'$modalInstance',
'data',
'utils'
#foreach($model in $dm.uniqueRelatedDataModelsAsJava())#set($typeName=${model.getAttribute("name")}),'${typeName}CrudService'#end,
'$timeout'
#if($dm.fileIdElementsAsJava().size()>0)
,'FileTransferService'
#end
#if($dm.autoInjectedElementsAsJava().size()>0)
,'projectCache'
#end,'operations','crudType',
function ($scope,
                        ${modelName}RestService,
                        $modalInstance,
                        data,
                        utils
                        #foreach($model in $dm.uniqueRelatedDataModelsAsJava())#set($typeName=${model.getAttribute("name")}),${typeName}CrudService#end,
                        $timeout
                        #if($dm.fileIdElementsAsJava().size()>0)
                            ,FileTransferService
                        #end
                        #if($dm.autoInjectedElementsAsJava().size()>0)
                        ,projectCache
                        #end,operations,crudType
)

{
    $scope.Model = {};
    $scope.Actions = {
#foreach($relation in $dm.simpleRelationsAsJava())
    #set($typeName=${relation.getAttribute("plaintypename")})
    #set($relatedType=${dm.getRelatedDataModel($typeName).get()})
    #if(${relation.containsAttribute("createOnSelect")})
        #set($operation="create")
    #else
        #set($operation="select")
    #end
    #set($relationKeyName= ${relatedType.getIdentifier().getAttribute("name")})
    #set($fieldName=${relation.getAttribute("name")})
    add#capitalize($fieldName):function(){
        ${typeName}CrudService.${operation}().then(function (result) {
            $scope.Model.$fieldName = result;
        });
    },

    show#capitalize($fieldName) : function () {
        ${typeName}CrudService.show($scope.Model.$fieldName);
    },

    remove#capitalize($fieldName) : function () {
        $scope.Model.$fieldName = {};
    },

#foreach($element in $dm.fileIdElementsAsJava())
    #set($elementName=${element.getAttribute("name")})
    upload#capitalize($elementName) : function (item) {
        FileTransferService.upload(item).success(function (data) {
            $scope.Model.${elementName}=data;
            notify($scope.translate('app.upload.success'), "success");
        }).error(function (data, status) {
            notify($scope.translate('app.upload.failed'), "danger");
        });
    },

    download#capitalize($elementName) : function () {
        FileTransferService.download($scope.Model.$elementName);
    }
    #end
#end
    };

    $scope.selected = {};

    if (data != undefined) {
        $scope.Model = data;
    }

    $scope.CrudType=crudType;

#foreach($model in $dm.genericRelationsAsJava())
    #set($typeName=${model.getAttribute("plaintypename")})
    #set($name="#capitalize(${model.getAttribute('name')})")
    $scope.${name}Grid = {
        columnDefs:get${typeName}GridHeaders(),
        data: 'Model.${model.getAttribute("name")}',
        multiSelect: false,
        isSelected: true,
        enableHighlighting: true,
        enableColumnReordering:true,
        enableColumnResize:true,
        enableRowSelection: true,
        enableRowHeaderSelection: false
    };

    $scope.Model.${model.getAttribute("name")}=[];

    $scope.$watch('Model.${model.getAttribute("name")}', function () {
        $scope.${name}Grid.isSelected = false;
    });

    $scope.${name}Grid.onRegisterApi = function (gridApi) {
        $scope.${name}Grid.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            $scope.${name}Grid.isSelected = row.isSelected;
        });
    };
#end

    $scope.${modelName}Grid = {
        columnDefs:[
        #foreach($elem in $dm.nonRelatedElementsAsJava())
            #set($elemName=${elem.getAttribute("name")})
        {
            field: '$elemName',
        #[[
            displayName: $scope.translate(]]#'view.${dm.getUniqueName()}.${elemName}.label')
            #if($elem.isGeneratedValue()|| $elem.containsAttribute("password") || $elem.containsAttribute("fileId")),visible:false#end
            #if($elem.getElemType()=="dateTime"),cellFilter : 'persianDate'#end
            #if($elem.containsAttribute("generalParameter")) ,cellFilter: 'GeneralParameterFilter:"$elem.getAttribute("generalParameter")"' #end
        },
        #end
        #foreach($elem in $model.relationsAsJava())

        #set($elemName=${elem.getAttribute("name")})
        #set($elemType=${elem.getAttribute("plaintypename")})
        #set($fullInnerElemType=${elem.getAttribute("typename")})


#foreach($innerElem in $model.getRelatedDataModel($elemType).get().titleElementsAsJava())
#set($innerElemName=${innerElem.getAttribute("name")})
        {
            field: '$elemName.$innerElemName', #[[
            displayName: $scope.translate(]]#'view.${fullInnerElemType}.${innerElemName}.label')
            #if($innerElem.containsAttribute("generalParameter")) ,cellFilter: 'GeneralParameterFilter:"$innerElem.getAttribute("generalParameter")"' #end
        },
#end
#end
    ],
        data: '${modelName}Grid.content',
    };

    utils.initGrid($scope, $scope.${modelName}Grid);

    $scope.$watch('${modelName}Grid.content', function () {
        $scope.${modelName}Grid.isSelected = false;
    });

##    #foreach($model in $dm.uniqueRelatedDataModelsAsJava())
##        #set($name=${model.getAttribute("name")})
##        #set($modelFullName=${model.getUniqueName()})
##        function get${name}GridHeaders(){
##            return  ;
##    }
##    #end



    $scope.Actions=operations.createModalActions({
        restService: ${modelName}RestService,
        grid: #[[$scope]]#.${modelName}Grid,
        model: $scope.Model
    },{
#foreach($relation in $dm.genericRelationsAsJava())
    #set($typeName=${relation.getAttribute("plaintypename")})
    #set($relatedType=${dm.getRelatedDataModel($typeName).get()})

    #set($relationKeyName= ${relatedType.getIdentifier().getAttribute("name")})

    #if(${relation.containsAttribute("createOnSelect")})
        #set($operation="create")
    #else
        #set($operation="select")
    #end

    #set($fieldName=${relation.getAttribute("name")})
    add#capitalize($fieldName):function(){
        if($scope.Model.$fieldName==undefined){
            $scope.Model.$fieldName=[];
        }
        ${typeName}CrudService.${operation}().then(function (result) {
            $scope.Model.${fieldName}.push(result);
        });
    },

#if(${relation.containsAttribute("crudwidget")})
    edit#capitalize($fieldName) : function () {
        var selected = $scope.#capitalize($fieldName)Grid.gridApi.selection.getSelectedRows()[0];
        ${typeName}CrudService.edit(selected).then(function (result) {
            utils.changeGridItem($scope.Model.$fieldName, "id", result);
        });
    },
#end
    remove#capitalize($fieldName) : function () {
        var selected = $scope.#capitalize($fieldName)Grid.gridApi.selection.getSelectedRows()[0];
        question().then(function (success) {
            utils.notifySuccess();
            $scope.#capitalize($fieldName)Grid.isSelected = false;
            $timeout(function() {
                $scope.$apply(function () {
                    utils.removeGridItem($scope.Model.$fieldName, selected);
                });
            })
        })
    },

    show#capitalize($fieldName) : function () {
        var selected = $scope.#capitalize($fieldName)Grid.gridApi.selection.getSelectedRows()[0]
        ${typeName}CrudService.show(selected);
    }

#end
    });

    window.setTimeout(function(){
        $(window).resize();
        $(window).resize();
    }, 1000);

        #foreach($enum in $dm.enumeratedElementsAsJava())
            #set($fullName=$enum.getAttribute("enumType"))
            #set($className="#getClassName($fullName)")
+function() {
            ${modelName}RestService.get#pluralize($className)().#[[$promise.then(
            function(success){
            ]]#
            $scope.${className} = utils.convertEnum(success,'${fullName}');
        });
}();

        #end
}]);
})(angular.module("application"));
