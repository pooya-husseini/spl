package com.caspco.spl.cg.bootstrap;

public enum LegoPluginJobType {
    GENERATE, CLEAN, PREVIEW, COLLECT_RESOURCES
}