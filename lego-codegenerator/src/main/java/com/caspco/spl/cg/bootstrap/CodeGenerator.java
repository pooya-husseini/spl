package com.caspco.spl.cg.bootstrap;

import com.caspco.spl.cg.actor.Bootstrap;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 16.03.14
 *         Time: 13:00
 *
 */
public final class CodeGenerator {

//    public static void xmlCodeGenerator(CodeGenerationConfig config) {
//        final Bootstrap bootstrap = new Bootstrap(config);
//        bootstrap.runXmlCodeGenerator();
//    }
    public static void entityCodeGenerator(CodeGenerationConfiguration config) {
        final Bootstrap bootstrap = new Bootstrap(config);
        bootstrap.runEntityCodeGenerator();
    }

    public static void entityCodePreviewer(CodeGenerationConfiguration config) {
        final Bootstrap bootstrap = new Bootstrap(config);
        bootstrap.runEntityCodePreviewer();
    }

    public static void entityCleaner(CodeGenerationConfiguration config) {
        final Bootstrap bootstrap = new Bootstrap(config);
        bootstrap.runEntityCleaner();
    }
}