package com.caspco.spl.cg.generator.constants

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 01/07/14
  *         Time: 16:11
  */
object TemplatePaths {

  lazy val TemplateFileExtension = ".vm"
  lazy val JAVAClassPath = ".vm"
  lazy val WebTemplatePath = "/template/web/client/"
  lazy val DtoTemplatePath = "/template/java/dto.vm"
  lazy val PasswordDtoTemplatePath = "/template/java/passwordDto.vm"
  lazy val DoTemplatePath = "/template/java/domainobject.vm"
  lazy val PrincipalToDtoTemplatePath = "/template/java/spring/principalToDtoTransformer.vm"
  lazy val PrincipalToEntityTemplatePath = "/template/java/spring/principalToEntityTransformer.vm"
  lazy val LoginControllerTemplatePath = "/template/java/loginController.vm"
  lazy val SecurityProviderTemplatePath = "/template/java/securityProvider.vm"
  lazy val EntityToPrincipalTemplatePath = "/template/java/spring/entityToPrincipalTransformer.vm"
  lazy val BusinessServiceTemplatePath = "/template/java/repository.vm"
  lazy val DomainServiceTemplatePath = "/template/java/service.vm"
  lazy val RoleResolverTemplatePath = "/template/java/roleResolver.vm"
  lazy val RestServiceTemplatePath = "/template/java/restService.vm"
  lazy val EntityTransformerTemplatePath = "/template/java/entitytransformer.vm"
  //  lazy val CacheFillerTemplatePath = "/template/java/cacheFiller.vm"
  lazy val CacheFillerTemplatePath = "/template/java/gpResolver.vm"
  lazy val DtoTransformerTemplatePath = "/template/java/dtotransformer.vm"
  lazy val TransformationMapTemplatePath = "/template/java/transformationMap.vm"
  lazy val TransformerTemplatePath = "/template/java/transformer.vm"
  lazy val JasperReportTemplatePath = "/template/report/main-page-jasper.vm" //    Locale.fa_IR.name() -> "/template/report/main-page-jasper.fa_IR.vm"

  lazy val GeneralParameterTemplatePath = "/template/java/generalParameter.vm"
  lazy val GeneralParameterTypeEnumTemplatePath = "/template/java/enums.vm"

  lazy val CreateTablePath = "/template/sql/mysql/createTable.vm"
  lazy val CreateCollectionPath = "/template/sql/mysql/createCollection.vm"
  lazy val SqlPathPrefix = s"sql"


  lazy val PageTemplate = WebTemplatePath + "html/pages/page.vm"
  //  lazy val WIDGET_TEMPLATE = "/template/web/client/html/pages/widget.ssp"
  lazy val DataModelTemplate = WebTemplatePath + "ts/controller.vm"
  lazy val ServiceTemplate = WebTemplatePath + "ts/service.vm"
  lazy val TranslationTemplate = WebTemplatePath + "json/translations.vm"
  lazy val ClientRestServiceTemplate = WebTemplatePath + "ts/clientRestService.vm"
  lazy val FilterTemplate = WebTemplatePath + "ts/filter.vm"
  lazy val AutoInjectorControllerTemplate = WebTemplatePath + "ts/autoinjectorController.vm"
  lazy val ClientHttpServiceTemplate = WebTemplatePath + "ts/httpservice.vm"
  lazy val ClientCrudServiceTemplate = WebTemplatePath + "ts/crudService.vm"
  lazy val ClientCrudControllersTemplate = WebTemplatePath + "ts/crudController.vm"
  lazy val FileUploaderServiceTemplate = WebTemplatePath + "ts/fileUploadService.vm"

  lazy val FileTransferEntityTemplate = "/template/java/fileTransferEntity.vm"
  lazy val FileTransferRepositoryTemplate = "/template/java/fileTransferRepository.vm"
  lazy val FileTransferRestTemplate = "/template/java/fileTransferRest.vm"

  lazy val MenuTemplate = WebTemplatePath + "json/menu.vm"
  lazy val MainPage = WebTemplatePath + "html/pages/mainPage.vm"
  lazy val MainSearchPage = WebTemplatePath + "html/pages/mainSearchPage.vm"
  lazy val EditPage = WebTemplatePath + "html/pages/editPage.vm"
  lazy val CreatePage = WebTemplatePath + "html/pages/createPage.vm"
  lazy val SearchPage = WebTemplatePath + "html/pages/searchPage.vm"
  lazy val ShowPage = WebTemplatePath + "html/pages/showPage.vm"
  lazy val CrudPage = WebTemplatePath + "html/pages/crudPage.vm"
  lazy val ChangePasswordPage = WebTemplatePath + "html/pages/changePasswordPage.vm"

  lazy val SlickModelPath = "/template/scala/slickTable.vm"
}