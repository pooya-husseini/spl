package com.caspco.spl.cg.generator.javacg.conversion

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 29/04/14
  *         Time: 10:52
  */

//object JavaTypeConversion {
//
//  implicit class DataModelConversion(dm: DataModel) {
//
//    lazy val convert: JavaClass = {
//      val jc = new JavaClass(dm)
//      jc += dm.getAttributes
//      jc
//    }
//
//    lazy val getEnums: Set[String] = convert.fields.filter(p =>p.isEnumerated).map(p => p.getAttribute("enumType")).toSet
//
//    lazy val enumsAsJava: util.Set[String] = getEnums.asJava
//  }
//
//
//}

//case class JavaType(typeName: String) {
//  def getTypeName = typeName
//
//  //  def isEnum=attributes.contains("enumerated")
//  //  def getAttribute(key:String):Option[String]=attributes.get(key)
//}

//class JavaClass(dm: DataModel) extends BaseModel {
//
//  lazy val fields: List[JavaField] = {
//    dm.getElements.map(elem => convertElement(elem))
//  }
//
//  lazy val notGeneratedValueFields: List[JavaField] = {
//    fields.filterNot(_.containsAttribute("generatedValue"))
//  }
//
//  lazy val nonSuperFields: List[JavaField] = {
//    fields.filter(p=>p.getAttribute("inSuperClass").eq("false"))
//  }
//
//  lazy val nonSuperFieldsAsJava=nonSuperFields.asJava
//
//  lazy val notGeneratedValueFieldsAsJava = notGeneratedValueFields.asJava
//  lazy val relatedDataModels: List[JavaClass] = dm.getRelatedDataModels.map(p => new JavaClass(p))
//  lazy val relatedDataModelsAsJava = relatedDataModels.asJava
//  lazy val identifier: JavaField = convertElement(dm.getIdentifiers.head)
//  lazy val fieldsAsJava = fields.asJava
//  lazy val fieldsWithRelation: List[JavaField] = fields.filter(p => p.containsAttribute("relation"))
//  lazy val fieldsWithoutRelation: List[JavaField] = fields.filterNot(p => p.containsAttribute("relation"))
//  lazy val relatedTypes: Set[String] = simpleRelatedTypes ++ genericRelatedTypes
//  lazy val relatedTypesAsJava = relatedTypes.asJava
//  lazy val fieldsWithRelationAsJava = fieldsWithRelation.asJava
//  lazy val fieldsWithoutRelationAsJava = fieldsWithoutRelation.asJava
//  lazy val simpleFieldsWithRelation: List[JavaField] = fieldsWithRelation.filter(p => p.genericType.isEmpty)
//  lazy val simpleRelatedTypes: Set[String] = simpleFieldsWithRelation.map(_.getFieldType).toSet
//  lazy val simpleRelatedTypesAsJava = simpleRelatedTypes.asJava
//  lazy val simpleFieldsWithRelationAsJava = simpleFieldsWithRelation.asJava
//  lazy val simpleUniqueRelationTypes = simpleFieldsWithRelation.map(_.fieldType).toSet
//  lazy val simpleUniqueRelationTypesAsJava = simpleUniqueRelationTypes.asJava
//
//  lazy val genericFieldsWithRelation: List[JavaField] = fieldsWithRelation.filter(p => p.genericType.isDefined)
//  lazy val genericRelatedTypes: Set[String] = genericFieldsWithRelation.map(_.genericType.get).toSet
//  lazy val genericRelatedTypesAsJava = genericRelatedTypes.asJava
//  lazy val genericFieldsWithRelationAsJava = genericFieldsWithRelation.asJava
//
//  lazy val searchableWithRelationFields: List[JavaField] = {
//    val elements: List[AbstractElement] = dm.filterElements(p => p.containsAttribute("relation") && p.containsAttribute("searchable"))
//    elements.map(convertElement)
//  }
//
//  lazy val searchableWithRelationFieldsAsJava = searchableWithRelationFields.asJava
//
//  def getName: String = dm.getAttribute("name")
//
//  def getPackageName: String = dm.getAttribute("domain")
//
//  def convertElement(e: AbstractElement): JavaField = {
//    val attributes = e.getAttributes()
//
//    val javaField = e match {
//      case BooleanType(_) => JavaField("java.lang.Boolean")
//      case CompositionType(_) =>
//        e.getAttributeOption("typename") match {
//          case Some(x) =>
//            val classType = x
//            JavaField(classType)
//          case None =>
//            JavaField("java.lang.Class[_]")
//        }
//
//      case CurrencyType(_) => JavaField("java.util.Currency")
//      case DateTimeType(_) => JavaField("java.util.Date")
//      case DateType(_) => JavaField("java.sql.Date")
//      case DecimalType(_) => JavaField("java.math.BigDecimal")
//      case DoubleType(_) => JavaField("java.lang.Double")
//      case FileType(_) => JavaField("java.io.File")
//      case IntegerType(_) => JavaField("java.lang.Integer")
//      case LongType(_) => JavaField("java.lang.Long")
//      case FloatType(_) => JavaField("java.lang.Float")
//      case ListType(_) => e.getAttributeOption("typename") match {
//        case None =>
//          JavaField("java.util.List")
//        case Some(genericType) =>
//          JavaField("java.util.List", Some(genericType))
//      }
//      case MapType(_) => JavaField("java.util.Map[java.lang.Object,java.lang.Object]")
//      case StringType(_) => JavaField("java.lang.String")
//    }
//    javaField += attributes
//    javaField
//  }
//}