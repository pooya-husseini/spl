package com.caspco.spl.cg.model.view

import java.util

import com.caspco.spl.cg.model.BaseModel
import com.caspco.spl.cg.model.action.ActionType
import com.caspco.spl.cg.model.layout.Layout

import scala.collection.JavaConverters._

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 2/1/14
 *         Time: 4:27 PM
 */

//@deprecated("Use AbstractWidget subtypes")
//class Widget(var widgetType: String = "",
//             @BeanProperty
//             var attributes: Map[String, String] = Map.empty[String, String],
//             var layout: Option[Layout] = None,
//             var innerWidgets: List[Widget] = List.empty[Widget])
//  extends BaseModel {
//
//  def getBoundAttributes: Map[String, String] = {
//    attributes.filter(a => a._1.startsWith("@"))
//  }
//
//  override def containsElement(elemName: String): Boolean =
//    innerWidgets.exists(p => p.attributes.getOrElse("name", "") == elemName)
//
//  override def containsAttribute(attName: String): Boolean = attributes.contains(attName)
//
//  override def +=(attribute: (String, String)): BaseModel = ???
//}


abstract class AbstractWidget(var widgetType: String = "",
                              private var priority: Int = Int.MaxValue - 1) extends BaseModel {

  private var layout: Option[Layout] = None
  private var innerWidgets: List[AbstractWidget] = List.empty[AbstractWidget]
  private var action: Option[ActionType] = None

  def +=(w: AbstractWidget): Unit = {
    innerWidgets :+= w
  }

  def +=(l: Layout): Unit = {
    layout = Some(l)
  }

  def getLayout: Option[Layout] = layout

  def getInnerWidgetsAsJava: util.List[AbstractWidget] = getInnerWidgets.asJava

  def getInnerWidgets: List[AbstractWidget] = innerWidgets

  def getBoundAttributes: Map[String, String] = getAttributes.filter(a => a._1.startsWith("@"))

  def getAction: Option[ActionType] = action

  override def containsElement(elemName: String): Boolean =
    innerWidgets.exists(p => p.getAttributes.getOrElse("name", "") == elemName)

  def +=(a: ActionType): Unit = {
    action = Some(a)
  }

  def getWidgetType: String = widgetType

  def getPriority: Int = {
    getAttributeOption("priority") match{
      case Some(x)=>
        priority=x.toInt
      case None=>
    }
    priority
  }

  def setPriority(n: Int): Unit = priority = n
}

object AbstractWidget {
  def apply(widgetType: String): AbstractWidget = widgetType match {
    case "textBox" => new TextBox
    case "button" => new Button
    case "spinner" => new Spinner
    case "datePicker" => new DatePicker
    case "comboBox" => new ComboBox
    case "checkBox" => new CheckBox
    case "radioButton" => new RadioButton
    case "image" => new Image
    case "label" => new Label
    case "listBox" => new ListBox
    case "grid" => new Grid
    case "container" => new Container
    case "buttonGroup" => new ButtonGroup
    case "searchableTextBox" => new SearchableTextBox
  }
}

class SimpleWidget(initAttributes: Map[String, String],widgetType:String) extends AbstractWidget(widgetType){
  this += initAttributes
}

case class TextBox(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"textBox")
case class TextAreaBox(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"textAreaBox")
case class FileUploader(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"fileUploader")
case class FileDownloader(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"fileDownloader")
case class AmountBox(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"amountBox")
case class Button(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"button")
case class Spinner(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"spinner")
case class DatePicker(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"datePicker")
case class ComboBox(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"comboBox")
case class SearchableTextBox(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"searchableTextBox")
case class CheckBox(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"checkBox")
case class RadioButton(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"radioButton")
case class Image(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"image")
case class Label(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"label")
case class ListBox(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"listBox")
case class Grid(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"1")
case class CrudGrid(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"1")

case class Lookup(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts, "codeResolver")
case class ModelPresenter(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"3")
case class CrudModelPresenter(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"3")
case class CodeResolver(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"codeResolver")
case class NullWidget(atts: Map[String, String] = Map.empty) extends AbstractWidget("0",Int.MaxValue)
case class Container(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"container")
case class ButtonGroup(atts: Map[String, String] = Map.empty) extends SimpleWidget(atts,"buttonGroup")