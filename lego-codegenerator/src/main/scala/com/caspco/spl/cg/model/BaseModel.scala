package com.caspco.spl.cg.model

import java.util
import java.util.regex.Matcher

import com.caspco.spl.cg.util.constant.Constants
import com.caspco.spl.model.annotations.cg.IgnoreType
import com.caspco.spl.model.wrench.Locale

import scala.collection.JavaConverters._
import scala.collection.immutable.HashSet

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 2/1/14
  *         Time: 9:24 PM
  */
trait BaseModel extends Serializable {

  lazy val ignores: Set[IgnoreType] = getAttributeOption("ignore") match {
    case None => Set.empty[IgnoreType]
    case Some(x) => x.split(",").map(IgnoreType.valueOf).toSet
  }

  private var attributes: Map[String, String] = Map.empty[String, String]

  def getAttributeOption(attName: String): Option[String] = attributes.get(attName)

  def getUniqueName: String = getAttributes().get("domain") match {
    case Some(x) => x + "." + getAttributes().getOrElse("name", "")
    case _ => getAttributes().getOrElse("name", "")
  }

  def +(attribute: (String, String)): BaseModel = {

    this += attribute
    this
  }

  def -=(attName: String): BaseModel = {
    getAttributes(attName).foreach(attributes -= _._1)
    this
  }

  def getAttributesAsJava(pattern: String, removePattern: Boolean = false): util.Map[String, String] = {
    getAttributes(pattern, removePattern).asJava
  }

  def getAttributes(pattern: String, removePattern: Boolean = false): Map[String, String] = {
    val result = attributes.map(x => {
      val matcher: Matcher = pattern.r.pattern.matcher(x._1)
      (matcher.matches(), matcher, x)
    }).filter(_._1).map(x => (x._2, x._3))

    if (removePattern) {

      result.map(x => {
        ((1 to x._1.groupCount()).map(x._1.group).mkString, x._2._2)
      }).toMap

    } else {
      result.map(x => (x._2._1, x._2._2)).toMap
    }
  }

  def +=(atts: Map[String, String]): Unit = {
    for (attribute <- atts) {
      this += attribute
    }
  }

  def +=(attribute: (String, String)): Unit = {
    attributes += attribute
  }

  def ?(elemName: String): Boolean = containsElement(elemName)

  def containsElement(elemName: String): Boolean = ???

  def ??(attName: String): Boolean = containsAttribute(attName)

  def containsAttribute(attName: String): Boolean = {
    attributes.contains(attName)
    //attributes.exists(x => attName.r.pattern.matcher(x._1).matches())
  }

  def notContainsAttribute(attName: String): Boolean = !containsAttribute(attName)


  def existsAttribute(f: String => Boolean): Boolean = attributes.exists(p => f(p._1))

  def getPropertyAttributes: Map[String, String] = {

    BaseModel.DisplayableAttributes.map(p => (p, this.getAttributes().getOrElse(p, p))).toMap
    //this.getAttributes -- (this.getAttributes.keySet -- BaseModel.DisplayableAttributes)
  }

  def getAttributes(): Map[String, String] = attributes

  def apply(str: String): String = getAttributeOrElse(str)

  def isIgnoredWith(x: IgnoreType): Boolean = {
    ignores(x)
  }

  def getTranslation(locale: String): String = {
    getTranslation(Locale.valueOf(locale))
  }

  def getTranslation(locale: Locale): String = {
    getAttributeOrElse("translation_" + locale.getLocale, getAttribute("name"))
  }

  //  override def clone(): AnyRef = super.clone()

  def getAttributeOrElse(attName: String, default: String = ""): String = {
    if (attName.startsWith("^") && attName.endsWith("$")) {
      attributes.find(x => attName.r.pattern.matcher(x._1).matches()).getOrElse(default, default)._1
    } else {
      attributes.getOrElse(attName, default)
    }
  }

  def getAttribute(attName: String): String = {
    if (attName.startsWith("^") && attName.endsWith("$")) {
      attributes.find(x => attName.r.pattern.matcher(x._1).matches()).get._1
    } else {
      attributes(attName)
    }
  }

  def getSeparatedAttributesAsJava(attName: String, separator: String): util.List[String] = {
    getSeparatedAttributes(attName, separator).asJava
  }

  def getSeparatedAttributes(attName: String, separator: String): List[String] = {
    attributes.get(attName) match {
      case None =>
        Nil
      case Some(x) =>
        x.split(separator).toList
    }
  }

  // the below code is the shame of programming
  lazy val serverSideOperations: List[String] = {
    getSeparatedAttributes("operations", ",").filter(i => this.getAttribute(s"operation_server_$i").eq("true"))
  }

  lazy val serverSideOperationsAsJava: util.List[String] = serverSideOperations.asJava

  lazy val clientSideOperations: List[String] = {
    getSeparatedAttributes("operations", ",").filter(i => this.getAttribute(s"operation_client_$i").eq("true"))
  }

  lazy val clientSideOperationsAsJava: util.List[String] = clientSideOperations.asJava
  lazy val operationModelMap:Map[String,String]= {
    getAttributes(Constants.OperationModel,removePattern = true)
  }
  lazy val operationModelMapAsJava: util.Map[String, String] =operationModelMap.asJava
}

object BaseModel {
  lazy val DisplayableAttributes = HashSet(/*"title",*/ "label")
}