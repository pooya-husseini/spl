package com.caspco.spl.cg.analyze.actor

import akka.actor.Actor
import com.caspco.spl.cg.analyze.exception.{AnalyzerException, AttributeNotFoundException}
import com.caspco.spl.cg.bootstrap.CodeGenerationConfiguration
import com.caspco.spl.cg.model.BaseModel
import com.caspco.spl.cg.model.datamodel.DataModel
import com.caspco.spl.cg.model.layout.Layout
import com.caspco.spl.cg.model.project.Project
import com.caspco.spl.cg.util.logging.StopWatch._
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 2/3/14
 *         Time: 10:49 AM
 */


object AnalyzerActor {

  case class Analyze(conf: CodeGenerationConfiguration, project: Project, jobId: Int)

  case class AnalyzeSuccess(conf: CodeGenerationConfiguration, project: Project, jobId: Int)

  case class AnalyzeFailure(t: Throwable, jobId: Int)
}

class AnalyzerActor extends Actor {
  val logger = LoggerFactory.getLogger(this.getClass)
  import com.caspco.spl.cg.analyze.MandatoryTypeAttributes._
  import com.caspco.spl.cg.analyze.actor.AnalyzerActor._

  def receive: Actor.Receive = {

    case Analyze(conf, p, jid) =>
      logger.info("Start analyzing!")
      stopWatch {
        analyzeProject(p) onComplete {

          case Success(s) =>
            context.parent ! AnalyzeSuccess(conf, p, jid)
            logger.info("Analyzing successfully finished!")

          case Failure(f) =>
            logger.warn("Analyzing failure during analyzing!", f)
            context.parent ! AnalyzeFailure(f, jid)
        }
      } match {
        case x => logger.info(s"Analyzing proceed in $x milliseconds!")
      }
  }

  def transformWidgetToLayout(project: Project) = {
    //    for (p <- project.getPageContainer.allPages;
    //         layoutName = p.attributes.get("layout");
    //         layout = project.getLayoutContainer.get(layoutName)) {
    //
    //    }
    //todo complete it
  }

  def analyzeProject(project: Project): Future[Unit] = {
    Future {
      checkPageLayoutAndDataModelAvailability(project)
      checkPageAttributes(project)
      checkLayoutAttributes(project.getLayoutContainer.values.toList)
      checkDataModelAttributes(project.getDataModelContainer.values.toList)
      transformWidgetToLayout(project)
    }
  }

  def checkPageLayoutAndDataModelAvailability(project: Project) {
    project.getPageContainer.allPages.foreach {
      case (page, _) =>
        page.getAttributes().get("dataModel") match {
          case Some(dm) =>
            if (!project.getDataModelContainer.contains(dm, recursive = true))
              throw new AnalyzerException(s"Data Model $dm in page ${page.getAttributes("name")} not found!")
          case None =>
        }
        page.getAttributes().get("layout") match {
          case Some(layout) =>
            if (!project.getLayoutContainer.contains(layout, recursive = true))
              throw new AnalyzerException(s"External layout $layout in page ${page.getAttributes("name")} not found!")
          case None =>
        }
    }
  }

  private def checkPageAttributes(project: Project) = {
    project.getPageContainer.allPages.foreach { case (page, _) =>
      PageMandatoryAttributes.diff(page.getAttributes().keys.toList)
      .foreach(m => throw new AttributeNotFoundException(m))

      //      checkElementAttributes(page.widgets)
      //      page.widgets.foreach(widget => {
      //        widget.layout match {
      //          case Some(layout) => checkLayoutAttributes(List(layout))
      //          case None =>
      //        }
      //      })
    }
  }

  private def checkDataModelAttributes(models: List[DataModel]): Unit = {
    models foreach (model => {
      DataModelMandatoryAttributes.diff(model.getAttributes().keys.toList)
      .foreach(m => throw new AttributeNotFoundException(m))
      checkElementAttributes(model.getElements)
      checkDataModelAttributes(model.getInnerDataModels)
    })
  }

  private def checkLayoutAttributes(ls: List[Layout]): Unit = {
    ls.foreach(l => {
      LayoutMandatoryAttributes.diff(l.getAttributes().keys.toList)
      .foreach(m => throw new AttributeNotFoundException(s"In layout ${l.getAttributes()} and attribute $m"))
      l.getGridType match {
        case Some(x) =>
          x.getRows.foreach(r => {
            r.getCells.foreach(c => {
              c.getLayout match {
                case Some(layout) => checkLayoutAttributes(List(layout))
                case None =>
              }
              checkElementAttributes(c.getWidgets)
              c.getWidgets.foreach(widget => {
                widget.getLayout match {
                  case Some(layout) => checkLayoutAttributes(List(layout))
                  case None =>
                }
              })
            })
          })
        case None =>
      }
    })
  }

  private def checkElementAttributes(p: List[BaseModel]) = {
    p foreach (elem => {
      ElementMandatoryAttributes.diff(elem.getAttributes().keys.toList)
      .foreach(m => throw new AttributeNotFoundException(m))
    })
  }
}