package com.caspco.spl.cg.generator.javacg.conversion

import java.lang.annotation.Annotation
import java.util

import com.caspco.spl.cg.model.datamodel._
import com.caspco.spl.model.annotations.proxy.CgAnnotationMaker
import com.caspco.spl.model.wrench.Locale

import scala.collection.JavaConverters._

/**
  * @author : Pooya. h
  *         Email : husseini@caspco.ir
  *         Date: 2/20/17
  *         Time: 3:15 PM
  */

class JavaClassDataModel(collection: Boolean) extends DataModelOperation(collection) {
//  implicit def elementToJField(a: AbstractElement):JavaField= convertElement(a)
//  lazy val fields: List[JavaField] = {
//    getElements.map(elem => convertElement(elem))
//  }
  lazy val notGeneratedValueFields: List[AbstractElement] = {
    getElements.filterNot(_.containsAttribute("generatedValue"))
  }

  lazy val nonSuperFields: List[AbstractElement] = {
    getElements.filter(p => p.getAttribute("inSuperClass").eq("false"))
  }

  lazy val nonSuperFieldsAsJava: util.List[AbstractElement] = nonSuperFields.asJava

  lazy val notGeneratedValueFieldsAsJava: util.List[AbstractElement] = notGeneratedValueFields.asJava
  //  lazy val relatedDataModels: List[DataModel] =
  //  lazy val relatedDataModelsAsJava: util.List[DataModel] = relatedDataModels.asJava
  lazy val identifier = getIdentifiers.head
//  lazy val fieldsWithRelation: List[JavaField] = getelements.filter(p => p.containsAttribute("relation"))
//  lazy val fieldsWithoutRelation: List[JavaField] = fields.filterNot(p => p.containsAttribute("relation"))
//  lazy val relatedTypes: Set[String] = simpleRelatedTypes ++ genericRelatedTypes
//  lazy val relatedTypesAsJava: util.Set[String] = relatedTypes.asJava
//  lazy val fieldsWithRelationAsJava: util.List[JavaField] = fieldsWithRelation.asJava
//  lazy val fieldsWithoutRelationAsJava: util.List[JavaField] = fieldsWithoutRelation.asJava
//  lazy val simpleFieldsWithRelation: List[JavaField] = fieldsWithRelation.filter(p => p.getGenericType.isEmpty)
//  lazy val simpleRelatedTypes: Set[String] = simpleFieldsWithRelation.map(_.getFieldType).toSet
//  lazy val simpleRelatedTypesAsJava: util.Set[String] = simpleRelatedTypes.asJava
//  lazy val simpleFieldsWithRelationAsJava: util.List[JavaField] = simpleFieldsWithRelation.asJava
//  lazy val simpleUniqueRelationTypes: Set[String] = simpleFieldsWithRelation.map(_.getFieldType).toSet
//  lazy val simpleUniqueRelationTypesAsJava: util.Set[String] = simpleUniqueRelationTypes.asJava

//  lazy val genericFieldsWithRelation: List[JavaField] = fieldsWithRelation.filter(p => p.getGenericType.isDefined)
//  lazy val genericRelatedTypes: Set[String] = genericFieldsWithRelation.map(_.getGenericType.get).toSet
//  lazy val genericRelatedTypesAsJava: util.Set[String] = genericRelatedTypes.asJava
//  lazy val genericFieldsWithRelationAsJava: util.List[JavaField] = genericFieldsWithRelation.asJava
  lazy val getEnums: Set[String] = getElements.map(a=>a.asInstanceOf[JavaField]).filter(p => p.isEnumerated).map(p => p.getAttribute("enumType")).toSet
  lazy val enumsAsJava: util.Set[String] = getEnums.asJava

  lazy val searchableWithRelationFields: List[JavaField] = {
    val elements: List[AbstractElement] = filterElements(p => p.containsAttribute("relation") && p.containsAttribute("searchable"))
    elements.map(JavaField.convertElement)
  }

  lazy val searchableWithRelationFieldsAsJava: util.List[JavaField] = searchableWithRelationFields.asJava

  def getName: String = getAttribute("name")

  def getPackageName: String = getAttribute("domain")

  override def getRelatedDataModels: List[DataModel] = super.getRelatedDataModels.map(p => JavaClassDataModel(p))

}

object JavaField {
  def apply(fieldType: String, genericType: Option[String] = None,
            annotations: List[String] = List.empty[String]): JavaField = {
    new JavaField(fieldType, genericType, annotations)
  }

  def convertElement(e: AbstractElement): JavaField = {
    val attributes = e.getAttributes()

    val javaField = e match {
      case BooleanType(_) => JavaField("java.lang.Boolean")
      case CompositionType(_) =>
        e.getAttributeOption("typename") match {
          case Some(x) =>
            val classType = x
            JavaField(classType)
          case None =>
            JavaField("java.lang.Class[_]")
        }

      case CurrencyType(_) => JavaField("java.util.Currency")
      case DateTimeType(_) => JavaField("java.util.Date")
      case DateType(_) => JavaField("java.sql.Date")
      case DecimalType(_) => JavaField("java.math.BigDecimal")
      case DoubleType(_) => JavaField("java.lang.Double")
      case FileType(_) => JavaField("java.io.File")
      case IntegerType(_) => JavaField("java.lang.Integer")
      case LongType(_) => JavaField("java.lang.Long")
      case FloatType(_) => JavaField("java.lang.Float")
      case ListType(_) => e.getAttributeOption("typename") match {
        case None =>
          JavaField("java.util.List")
        case Some(genericType) =>
          JavaField("java.util.List", Some(genericType))
      }
      case MapType(_) => JavaField("java.util.Map[java.lang.Object,java.lang.Object]")
      case StringType(_) => JavaField("java.lang.String")
    }
    javaField += attributes
    javaField
  }

}

class JavaField(fieldType: String, genericType: Option[String] = None,
                annotations: List[String] = List.empty[String]) extends AbstractElement("JavaField") {

  def mkString: String = {
    toString
  }

  override def toString: String = {
    s"Field name : $getFieldName Type : $fieldType, Generic Type : $genericType, Annotations : $annotations"
  }

  def getFieldName: String = getAttributes().getOrElse("name", "")

  def getFieldType: String = fieldType

  def getGenericType: Option[String] = genericType

  def getAnnotationAsJava: util.Collection[String] = annotations.asJavaCollection

  def isEnumerated: Boolean = getAttributes().contains("enumerated")

  def getAnnotationsAsJava: util.List[Object] = getAnnotations.asJava

  def getAnnotations[A <: Annotation]: List[Object] = getAttributes().toList.map({
    case ("generalParameter", x) =>
      val translation: Annotation = CgAnnotationMaker.createTranslation(Locale.English_United_States, "label")
      CgAnnotationMaker.createGeneralParameter(x, Array(translation))
    case ("identifier", _) => CgAnnotationMaker.createId()
    case _ => ""
  })
}

object JavaClassDataModel {
  def apply(dataModel: DataModel): JavaClassDataModel = {
    val result = new JavaClassDataModel(dataModel.isCollection)
    result +=  dataModel.getElements.map(JavaField.convertElement)
    dataModel.getInnerDataModels.foreach(result += _)
    dataModel.getRelatedDataModels.foreach(result addRelatedDataModel)
    result += dataModel.getAttributes()
    result
  }
}
