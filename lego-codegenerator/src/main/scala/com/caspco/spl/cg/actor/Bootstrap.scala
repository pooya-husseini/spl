package com.caspco.spl.cg.actor

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.util.Timeout
import com.caspco.spl.cg.actor.ProjectActor._
import com.caspco.spl.cg.bootstrap.{CodeGenerationConfiguration, LegoPluginJobType}
import com.caspco.spl.cg.util.logging.StopWatch._
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{Await, Promise}
import scala.language.postfixOps

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 16.03.14
 *         Time: 13:42
 */

class Bootstrap(config: CodeGenerationConfiguration) {

  val logger: Logger = LoggerFactory.getLogger(this.getClass)
  implicit val system = ActorSystem("CaspSpecActor")

  private var jobId = 1

  def runEntityCleaner(): Unit = {
    initProjectActor(StartClean(config, getJobId))
  }

  def initProjectActor(message:InitMessage)={
    import scala.concurrent.duration._
    implicit val timeout =config.getTask match{
      case LegoPluginJobType.PREVIEW =>
        Timeout(1 hour)
      case _=> Timeout(1 minute)
    }

    stopWatch {
      val p = Promise[Int]()
      system.actorOf(Props(new Actor {
        val project: ActorRef = context.actorOf(Props[ProjectActor], "ProjectActor")
        project ! message//

        override def receive: Receive = {
          case TotalOperationSucceed(j) =>
            p.success(1)
          case TotalOperationFailed(t, j) =>
            logger.error("Operation failed with error!", t)
            p.failure(t)
        }
      }))

      Await.result(p.future, timeout.duration)
    } match {
      case x=>logger.info(s"Operation succeed in $x milliseconds")
    }
    system.shutdown()
  }

  /**
   * Returns an incremental number
   * @return The incremental number
   */
  private def getJobId: Int = this.synchronized {
    val temp = jobId
    jobId += 1
    temp
  }

  def runEntityCodeGenerator(): Unit = {
      initProjectActor(ProcessClasses(config, getJobId))
  }

  def runEntityCodePreviewer(): Unit = {
    initProjectActor(ProcessClasses(config, getJobId))
  }
}