package com.caspco.spl.cg.model.datamodel

import java.util

import com.caspco.spl.cg.model.exception._
import com.caspco.spl.cg.model.view.ModelDescription
import com.caspco.spl.cg.model.{BaseModel, ModelContainer}
import com.caspco.spl.cg.util.constant.Constants

import scala.collection.JavaConverters._

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 1/29/14
  *         Time: 12:25 PM
  */

class DataModel(collection: Boolean) extends BaseModel {
  //  lazy val allSimpleModels = (simpleRelations.map(p => (p.getAttribute("name"), p)).toMap ++
  //    Map(getAttribute("name") -> this)).map(_._2).toList
  //  lazy val allSimpleModelsAsJava = allSimpleModels.asJava
  private var elements: List[AbstractElement] = List.empty[AbstractElement]
  private var innerDataModels: List[DataModel] = List.empty[DataModel]
  private var relatedDataModels: List[DataModel] = List.empty[DataModel]

  def containsInnerClass: Boolean = innerDataModels.nonEmpty

  // todo solve page widget model bound name that is not in data model
  // todo solve html files reference to js directory problem
  override def containsElement(elemName: String): Boolean =
  elements.exists(p => p.getAttributes().getOrElse("name", "") == elemName)

  def +=(element: AbstractElement): Unit = {
    elements :+= element
  }

  def +=(elems: List[AbstractElement]): Unit = {
    elements ++= elems
  }

  def clearElements(): Unit = {
    elements = List.empty[AbstractElement]
  }

  def getInnerDataModels: List[DataModel] = innerDataModels

  def getRelatedDataModels: List[DataModel] = relatedDataModels

  def +=(dm: DataModel): Unit = {
    innerDataModels :+= dm
  }

  def addRelatedDataModel(dm: DataModel): Unit = {
    relatedDataModels +:= dm
  }

  def getElements: List[AbstractElement] = elements

  def getElementsAsJava: util.List[AbstractElement] = elements.asJava

  def isCollection: Boolean = collection

  def filterElements(p: (AbstractElement) => Boolean): List[AbstractElement] = elements.filter(p)

  override def toString: String = getUniqueName

  def makeElementsString(separator: String, attName: String, prefix: String, postFix: String): String = {
    elements.filter(p => p ?? attName).map(p => p.getAttribute(attName)).map(p => prefix + p + postFix).mkString(separator)
  }

}

class EnumModel extends DataModel(true)

class EntityModel extends DataModel(false)

class DataModelContainer extends ModelContainer[String, DataModel] {

  def convertModelMap[A <: DataModel](f: DataModel => A): DataModelContainer = {
    val map = modelMap.map { case (k, v) => (k, f(v)) }
    val result = new DataModelContainer
    result += map
    result += nameSet
    result
  }

  lazy val allModels: List[DataModel] = {
    modelMap.values.toList
  }

  lazy val entityModels: List[DataModel] = {
    modelMap.values.filter(p => !p.isCollection).toList
  }

  lazy val enumModels: List[DataModel] = {
    modelMap.values.filter(p => p.isCollection).toList
  }
  lazy val categorizeByPrefix: Map[String, List[ModelDescription]] = {
    import com.caspco.spl.cg.util.string.StringGrouping._

    val domains = modelMap.keys.toList
    val group = domains.groupByWithPrefix()
    group.map {
      case (k, v) => k -> v.map(domain => {
        val model = modelMap(domain)
        ModelDescription(
          domain.substring(0, domain.lastIndexOf(".")),
          model.getAttributeOrElse("name"),
          model.getAttributes(Constants.TranslationPattern, removePattern = true).toList,
          model.getAttributes(Constants.UniqueIdPattern, removePattern = true).toList match {
            case Nil => None
            case (x, _) :: _ => Some(x)
          }
        )
      })
    }
  }

  lazy val precedencePackageName: String = {
    val domains = modelMap.keys.toList
    getPrecedencePackageName(domains)
  }

  def getPrecedencePackageName(packages: List[String]): String = {
    val split: Array[String] = packages.head.split("\\.")
    split.reduce(
      (x, y) => {
        if (packages.forall(_.startsWith(s"$x.$y"))) {
          s"$x.$y"
        } else if (packages.forall(_.startsWith(x))) {
          s"$x"
        } else {
          ""
        }
      }
    )
  }

  def getDataModel(name: String): Option[DataModel] = {
    modelMap.get(name)
  }

  def +=(m: DataModel): Unit = {
    if (modelMap.contains(m.getUniqueName))
      throw new DataModelAlreadyDefinedException(s"Data model ${m.getUniqueName}")
    nameSet += m.getUniqueName

    def recursivelyAddInnerModels(model: DataModel): Unit = {
      if (model.containsInnerClass) {
        model.getInnerDataModels foreach {
          inner =>
            val name = model.getUniqueName + "." + inner.getUniqueName
            if (nameSet.contains(name)) {
              throw new DataModelAlreadyDefinedException(name)
            }
            nameSet += name
            recursivelyAddInnerModels(inner)
        }
      }
    }

    recursivelyAddInnerModels(m)
    modelMap ++= Map(m.getUniqueName -> m)
  }
}