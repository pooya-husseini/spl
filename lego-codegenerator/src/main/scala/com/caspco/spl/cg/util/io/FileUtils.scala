package com.caspco.spl.cg.util.io

import java.io.{File, FileWriter, IOException, PrintWriter}

import com.caspco.spl.cg.util.formatter.{HtmlBeautifier, JSCompressor, JavaCodeBeautifier, XmlBeautifier}
import com.caspco.spl.wrench.utils.io.ZipDirectory
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.concurrent.TrieMap
import scala.language.reflectiveCalls


/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 2/12/14
  *         Time: 7:34 PM
  */
object StreamUtils {

  lazy val / : String = java.io.File.separator

  lazy val tempPath: String = {
    val temp = java.io.File.createTempFile("temp-file-name", ".tmp")
    val absolutePath = temp.getAbsolutePath
    absolutePath.
      substring(0, absolutePath.lastIndexOf(java.io.File.separator))
  }

  def recursiveListFiles(f: java.io.File): Array[java.io.File] = {
    val these = f.listFiles
    these ++ these.filter(_.isDirectory).flatMap(recursiveListFiles)
  }

  def recursiveListFileNames(f: String): Array[String] = {
    recursiveListFiles(new File(f)).map(_.getPath)
  }

  def zip(directory: String): Unit = {
    ZipDirectory.zip(directory)
  }

  def zip(out: String, files: Iterable[String]): Unit = {
    import java.io.{BufferedInputStream, FileInputStream}
    import java.util.zip.{ZipEntry, ZipOutputStream}

    val zip = new ZipOutputStream(new java.io.FileOutputStream(out))

    files.foreach { name =>
      zip.putNextEntry(new ZipEntry(name))
      val in = new BufferedInputStream(new FileInputStream(name))
      var b = in.read()
      while (b > -1) {
        zip.write(b)
        b = in.read()
      }
      in.close()
      zip.closeEntry()
    }
    zip.close()
  }

  /**
    * Using is a currying function that close a closable value after the operation
    * For example :
    * val fout=FileOutputStream(....)
    * using(fout){
    * ......
    * }
    * will run the content of using and at end close the fout
    *
    * @param closable
    * @param body
    * @tparam A
    * @return
    */

  def using[A <: {def close()}, B](closable: A)(body: A => B): B = {
    val result = body(closable)
    closable.close()
    result
  }

  /**
    * Creates folders using the given prefixPath and converts domain with dots to a os dependant path
    *
    * @param prefixPath Prefix path
    * @param domainName The type's domain name that contains dots
    * @return Returns the path that ends with java.io.File.separator
    */
  def makeValidPath(prefixPath: String, domainName: Option[String],
                    endWithSeparator: Boolean = true): String = {
    var result: String = domainName match {
      case Some(domain) =>
        val dir = s"${makeValidPath(prefixPath)}${domain.replace(".", /)}${/}"
        new java.io.File(dir).mkdirs()
        dir
      case None =>
        if (prefixPath.endsWith(/) || !endWithSeparator)
          prefixPath
        else
          prefixPath + /
    }

    if (endWithSeparator) {
      result += /
    } else if (result.endsWith(/)) {
      result = result.substring(0, result.length - 1)
    }
    result.replace(/ + /, /)
  }

  /**
    * If the given path ends with os dependent delimiter (/ or \) return the path, otherwise appends the delimiter to
    * the path
    *
    * @param path The given path
    * @return
    */
  def makeValidPath(path: String): String = {
    if (path.endsWith(/))
      path
    else path + /
  }

  //  trait FileManipulation {
  //    def <#(str: String): Unit
  //
  //    def <<#(str: String): Unit
  //
  //    def cp(destPath: String): Unit
  //
  //    def cp(ofile: java.io.File): Unit
  //
  //    def <#!(str: String): Unit
  //  }

  //  implicit class FileListManipulation(f: List[java.io.File]) extends FileManipulation {
  //
  //    override def <#(str: String): Unit = f.foreach {
  //      case x => x <# str
  //    }
  //
  //    override def cp(destPath: String): Unit = f.foreach {
  //      case x => x cp destPath
  //    }
  //
  //    override def cp(ofile: java.io.File): Unit = f.foreach {
  //      case x => x cp ofile
  //    }
  //
  //    override def <#!(str: String): Unit = f.foreach {
  //      case x => x <#! str
  //    }
  //
  //    override def <<#(str: String): Unit = f.foreach {
  //      case x => x <<# str
  //    }
  //  }

  //  implicit class SingleFileManipulation(f: java.io.File) extends FileManipulation {
  //
  //  }

  //  implicit class StringToFile(path: String) {
  //    def toFile: Try[java.io.File] = {
  //      Try(new java.io.File(path))
  //    }
  //  }


}

class MemoryOutputStream(path: String) extends OutputStream(path) {
  //    lazy val cache=new TrieMap[String,String]()
  InMemoryOutputStream.put(path, new StringBuilder())
  lazy val content: StringBuilder = InMemoryOutputStream.get(path)

  val logger: Logger = LoggerFactory.getLogger(this.getClass)


  def <<#(str: String): Unit = {
    logger.debug(s"Started to append on " + path)
    content ++= str
  }

  def cp(destPath: String)(implicit params:StreamParameters): Unit = {
    InMemoryOutputStream.put(destPath, content)
  }

  def <#!(str: String)(implicit params:StreamParameters): Unit = {
    this <# str
  }

  override def <#(str: String)(implicit params:StreamParameters): Unit = {
    logger.debug(s"Started to write on " + path)
    content.clear()
    content ++= str
  }

  override def cp(ostream: OutputStream)(implicit params:StreamParameters): Unit = {
    ostream <# content.result()
  }

  override def exits(): Boolean = InMemoryOutputStream.contains(path)

  override def delete(): Boolean = InMemoryOutputStream.delete(path)
}

class FileOutputStream(path: String) extends OutputStream(path) {
  lazy val file = new java.io.File(path)
  //  require(file.canWrite)
  lazy val extension: String = file.getName.substring(file.getName.lastIndexOf(".") + 1).toLowerCase
  val logger: Logger = LoggerFactory.getLogger(this.getClass)

  def <<#(str: String): Unit = {
    logger.debug(s"Started to append on " + file.getPath)
    file.getParentFile.mkdirs()
    if (!file.exists()) {
      file.createNewFile()
    }
    val out = new FileWriter(file, true)
    out.write(str + "\n")
    out.close()
  }

  def cp(destPath: String)(implicit params:StreamParameters): Unit = {
    val dest = new java.io.File(destPath)
    //      val src = new java.io.File(
    //        "/home/pooya/projects/exchange/caspian-exchange/xview-gen/src/main/resource/input/simple/resource")

    org.apache.commons.io.FileUtils.copyDirectory(file, dest)
  }


  def <#!(str: String)(implicit params:StreamParameters): Unit = {

    if (!file.exists()) {
      file.getParentFile.mkdirs()
      this <# str
    }
  }

  def <#(str: String)(implicit  params:StreamParameters): Unit = {
    logger.debug(s"Started to write on " + file.getPath)
    val output = if (params.formatOutput) {
      extension match {
        case "js" => JSCompressor(str)
        case "java" => JavaCodeBeautifier(str)
        case "xml" | "jrxml" => XmlBeautifier(str)
        case "html" => HtmlBeautifier(str)
        case _ => str
      }
    } else {
      str
    }

    file.getParentFile.mkdirs()
    if (!file.exists() || params.overwriteExisting) {
      file.createNewFile()
      val out = new PrintWriter(file)

      out.write(output)
      out.close()
    }

  }

  def makeDirs(): Unit = {
    if (!file.exists() && !file.mkdirs()) {
      throw new IOException("Can not create file " + file.getPath)
    }
  }

  override def cp(ostream: OutputStream)(implicit params:StreamParameters): Unit = {
    ostream.<#(scala.io.Source.fromFile(file).mkString)
  }

  override def exits(): Boolean = {
    file.exists()
  }

  override def delete(): Boolean = {
    file.delete()
  }
}

object OutputStreamFactory {
  def apply(path: String)(implicit streamType: StreamType): OutputStream = {
    streamType match {
      case FileStreamType => new FileOutputStream(path)
      case InMemoryStreamType => new MemoryOutputStream(path)
    }
  }
}

trait StreamType {}

case class StreamParameters(formatOutput: Boolean,overwriteExisting: Boolean)

case object FileStreamType extends StreamType

case object InMemoryStreamType extends StreamType

abstract class OutputStream(path: String) {

  /**
    * Writes the content to the given file
    *
    * @param str the content that should be write to the file
    */
  def <#(str: String)(implicit params:StreamParameters): Unit

  /**
    * Appends the content to the given file
    *
    * @param str the content that should be append to the file
    */
  def <<#(str: String): Unit

  /**
    * Copies the file to the path
    *
    * @param destPath the path that file should be copied
    */
  def cp(destPath: String)(implicit params:StreamParameters): Unit


  def cp(ostream: OutputStream)(implicit params:StreamParameters): Unit

  /**
    * Writes the content in file if the file does not exist
    *
    * @param str the content should be written in file
    */
  def <#!(str: String)(implicit params:StreamParameters): Unit

  def exits(): Boolean

  def delete(): Boolean
}

object InMemoryOutputStream {
  lazy val cache: TrieMap[String, StringBuilder] = TrieMap[String, StringBuilder]()

  def put(key: String, value: StringBuilder) = {
    cache += ((key, value))
  }

  def get(key: String): StringBuilder = cache(key)

  def contains(key: String) = cache.contains(key)

  def delete(key: String): Boolean = cache.remove(key) match {
    case Some(x) => true
    case _ => false
  }
}