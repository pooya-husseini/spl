package com.caspco.spl.cg.util.constant

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 31/12/14
 *         Time: 19:34
 */
object Constants {
  lazy val TranslationPattern="^translation_(.+)$"
  lazy val UniqueIdPattern="^unique_id_(.+)$"
  lazy val ServerSideOperation="^operation_server_(.+)$"
  lazy val ClientSideOperation="^operation_client_(.+)$"
  lazy val OperationModel="^operation_model_(.+)$"
}
