package com.caspco.spl.cg.util.string

import org.atteo.evo.inflector.English

/**
  * @author : Pooya. h
  *         Email : husseini@caspco.ir
  *         Date: 3/28/17
  *         Time: 4:50 PM
  */
object Inflector {
  def pluralize(str: String): String = English.plural(str)
}
