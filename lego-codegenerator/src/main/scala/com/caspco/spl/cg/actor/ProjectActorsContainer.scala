package com.caspco.spl.cg.actor

import akka.actor.ActorRef
import com.caspco.spl.cg.bootstrap.CodeGenerationConfiguration

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 30/11/14
  *         Time: 16:47
  */
case class ProjectActorsContainer(conf: CodeGenerationConfiguration, objectifier: Option[ActorRef], analyzer: Option[ActorRef],
                                  cg: Option[ActorRef], initiatorActor: ActorRef)
