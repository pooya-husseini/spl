package com.caspco.spl.cg.model.view

import java.util

import com.caspco.spl.cg.model.datamodel.DataModel
import com.caspco.spl.cg.model.exception.{ApplicationAlreadyDefinedException, PageAlreadyDefinedException}
import com.caspco.spl.cg.model.layout.Layout
import com.caspco.spl.cg.model.{BaseModel, ModelContainer}
import com.caspco.spl.cg.util.constant.Constants
import com.caspco.spl.model.annotations.cg.IgnoreType

import scala.collection.JavaConverters._

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 2/1/14
  *         Time: 4:23 PM
  */

class MainPage(dataModel: DataModel,createPage: AbstractPage) extends AbstractPage {
  def getCreatePage: AbstractPage = createPage
  def getDataModel: DataModel =dataModel
}

class AbstractPage extends BaseModel {

  private var layout: Option[Layout] = None
  private var widgets: List[AbstractWidget] = List.empty[AbstractWidget]

  def +=(w: AbstractWidget): Unit = {
    widgets :+= w
  }

  def +=(ws: List[AbstractWidget]): Unit = {
    widgets ++= ws
  }

  def +=(l: Layout): Unit = {
    layout = Some(l)
  }

  def getLayout: Option[Layout] = layout

  override def containsElement(elemName: String): Boolean =
    widgets.exists(p => p.getAttributes().getOrElse("name", "") == elemName)

  def getWidgetsWithAction: List[AbstractWidget] = {
    getLayoutWidgets.filter(p => p.getAction.isDefined)
  }

  def getLayoutWidgets: List[AbstractWidget] = layout match {
    case None => Nil
    case Some(l) => l.getGridType match {
      case None => Nil
      case Some(g) => g.getRows.flatMap(r => r.getCells.map(c => c.getWidgets)).flatten
    }
  }
}

class PopupPage extends AbstractPage

class CreatePopupPage extends PopupPage

class CrudPopupPage(dataModel: DataModel, editPage: PopupPage, showPage: PopupPage, createListPage: PopupPage, innerModels: Map[String, PopupPage]) extends PopupPage {
  //  def getCreatePage = createPage
  def getEditPage: PopupPage = editPage

  def getShowPage: PopupPage = showPage

  def getCreateListPage: PopupPage = createListPage

  def getInnerModelsAsJava: util.Map[String, PopupPage] = innerModels.asJava

  def getInnerModel(name:String): PopupPage = innerModels(name)

  def getDataModel: DataModel =dataModel

}

class PageContainer extends ModelContainer[String, (AbstractPage, Option[List[String]])] {
  lazy val allPages: List[(AbstractPage, Option[List[String]])] = (if (isApplicationDefined) {
    Map(application.get._1.getUniqueName -> application.get) ++ modelMap
  } else {
    modelMap
  }).values.toList
  //  lazy val allPages: List[(AbstractPage, Option[List[String]])] = modelMap.values.toList
  lazy val popupPages: List[(AbstractPage, Option[List[String]])] = allPages.filter {
    case (_: PopupPage, _) => true
    case _ => false
  }

  lazy val popupPagesForCreation: List[(AbstractPage, Option[List[String]])] = allPages.filter {
    case (_: CreatePopupPage, _) => true
    case _ => false
  }

  lazy val mainPages: List[(AbstractPage, Option[List[String]])] = allPages.filter {
    case (_: PopupPage, _) => false
    case _ => true
  }

  lazy val categorizeByPrefix: Map[String, List[ModelDescription]] = {
    import com.caspco.spl.cg.util.string.StringGrouping._
    val mPages = modelMap.filter {
      case (_, (_: PopupPage, _)) => false
      case (_, (p, _)) if p.getAttribute("abstract").equals("true")=>false
      case (_, (p, _)) if p.isIgnoredWith(IgnoreType.UI)=>false
      case _ => true
    }
    val domains = mPages.keys.toList
    val group = domains.groupByWithPrefix()
    group.map {
      case (k, v) => k -> v.map(domain => {
        val page: AbstractPage = modelMap(domain)._1
        ModelDescription(
          domain.substring(0, domain.lastIndexOf(".")),
          page.getAttributeOrElse("name"),
          page.getAttributes(Constants.TranslationPattern).toList,
          page.getAttributes(Constants.UniqueIdPattern, removePattern = true).toList match {
            case Nil => None
            case (x, _) :: _ => Some(x)
          }
        )
      })
    }
  }

  private var application: Option[(AbstractPage, Option[List[String]])] = None

  def +=(m: AbstractPage): Unit = {
    this += (m, None)
  }

  def +=(m: (AbstractPage, Option[List[String]])): Unit = {
    // it does not check inner classes
    val pageName = m._1.getUniqueName
    if (modelMap.exists(p => p._1 == pageName) ||
      (application.isDefined && application.get._1.getUniqueName == pageName))
      throw new PageAlreadyDefinedException(s"Page $pageName")

    nameSet += pageName
    modelMap ++= Map(pageName -> m)
  }

  def isApplicationDefined: Boolean = application.isDefined

  def getApplication: Option[(AbstractPage, Option[List[String]])] = application

  def setApplication(m: AbstractPage): Unit = {
    setApplication((m, None))
  }

  def setApplication(m: (AbstractPage, Option[List[String]])): Unit = {
    val pageName = m._1.getUniqueName
    application match {
      case None =>

        if (modelMap.exists(p => p._1 == pageName))
          throw new PageAlreadyDefinedException(s"Application $pageName")
        application = Some(m)
      case Some(_) => throw new ApplicationAlreadyDefinedException(s"Application $pageName")
    }
  }
}

case class ModelDescription(domain: String, name: String, translations: List[(String, String)], uniqueId: Option[String]) {
  def getDomain: String = domain

  def getName: String = name

  def getUniqueId: Option[String] = uniqueId

  def getTranslations: List[(String, String)] = translations

  def getTranslation(key: String): Option[String] = {
    translations.toMap.get(key)
  }
}