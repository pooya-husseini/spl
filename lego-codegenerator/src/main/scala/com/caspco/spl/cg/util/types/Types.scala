package com.caspco.spl.cg.util.types

import java.lang.{Boolean, Double, Float, Long}
import java.math.BigDecimal
import java.util
import java.util.Currency
import javax.persistence._
import javax.validation.constraints._

import com.caspco.spl.model.annotations.cg._
import com.caspco.spl.model.annotations.cg.security.{Password, Security, UserName, UserPrincipal}
import com.caspco.spl.model.annotations.validation
import com.caspco.spl.model.annotations.validation.{Email, StringEquals}

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 11/05/14
 *         Time: 14:18
 */
object Types {

  lazy val idType: Class[Id] = classOf[Id]
  lazy val notUiType: Class[NotUi] =classOf[NotUi]
  lazy val searchPageType: Class[SearchPage] =classOf[SearchPage]

  lazy val versionType: Class[Version] =classOf[Version]
  lazy val ignoreType: Class[Ignore] =classOf[Ignore]
  lazy val entityType: Class[Entity] = classOf[javax.persistence.Entity]
  lazy val generatedType: Class[Generated] = classOf[Generated]
  lazy val tableType: Class[Table] = classOf[javax.persistence.Table]
  lazy val generatedValueType: Class[GeneratedValue] = classOf[GeneratedValue]
  lazy val searchableType: Class[Searchable] = classOf[Searchable]
  lazy val notNullType: Class[NotNull] = classOf[javax.validation.constraints.NotNull]
  lazy val maxType: Class[Max] = classOf[javax.validation.constraints.Max]
  lazy val minType: Class[Min] = classOf[javax.validation.constraints.Min]
  lazy val emailType: Class[Email] = classOf[Email]
  lazy val numericType: Class[validation.Numeric] = classOf[validation.Numeric]
  lazy val stringEqualType: Class[StringEquals] = classOf[StringEquals]
  lazy val patternType: Class[Pattern] = classOf[javax.validation.constraints.Pattern]
  lazy val sizeType: Class[Size] = classOf[javax.validation.constraints.Size]
  lazy val createOnSelectType: Class[CreateOnSelect] = classOf[CreateOnSelect]
  lazy val passwordType: Class[Password] = classOf[Password]
  lazy val userPrincipalType: Class[UserPrincipal] = classOf[UserPrincipal]
  lazy val usernameType: Class[UserName] = classOf[UserName]
  lazy val generalParameterType: Class[GeneralParameter] = classOf[GeneralParameter]
  lazy val lookupType: Class[Lookup] = classOf[Lookup]
  lazy val textAreaType: Class[TextArea] = classOf[TextArea]
  lazy val priorityType: Class[Priority] = classOf[Priority]
  lazy val operationForAppendType: Class[OperationForAppend] = classOf[OperationForAppend]
  lazy val translationsType: Class[Translations] = classOf[Translations]
  lazy val operations: Class[Operations] = classOf[Operations]
  lazy val visibilityType: Class[Visibility] = classOf[Visibility]
  lazy val disabilityType: Class[Disability] = classOf[Disability]
  lazy val autoInjectType: Class[AutoInject] = classOf[AutoInject]
  lazy val injectableType: Class[Injectable] = classOf[Injectable]
  lazy val securityType: Class[Security] = classOf[Security]
  lazy val enumeratedWithType: Class[EnumeratedWith] = classOf[EnumeratedWith]
  lazy val oneToManyType: Class[OneToMany] = classOf[OneToMany]
  lazy val manyToOneType: Class[ManyToOne] = classOf[ManyToOne]
  lazy val relationDetailsType: Class[RelationDetail] = classOf[RelationDetail]
  lazy val manyToManyType: Class[ManyToMany] = classOf[ManyToMany]
  lazy val oneToOneType: Class[OneToOne] = classOf[OneToOne]
  lazy val titleType: Class[Title] = classOf[Title]
  lazy val intType: Class[Integer] = classOf[java.lang.Integer]
  lazy val stringType: Class[String] = classOf[java.lang.String]
  lazy val booleanType: Class[Boolean] = classOf[java.lang.Boolean]
  lazy val dateType: Class[java.util.Date] = classOf[java.util.Date]
  lazy val sqlDateType: Class[java.sql.Date] = classOf[java.sql.Date]
  lazy val bigDecimalType: Class[BigDecimal] = classOf[java.math.BigDecimal]
  lazy val currencyType: Class[Currency] = classOf[java.util.Currency]
  lazy val doubleType: Class[Double] = classOf[java.lang.Double]
  lazy val listType: Class[util.List[_]] = classOf[java.util.List[_]]
  lazy val longType: Class[Long] =classOf[java.lang.Long]
  lazy val floatType: Class[Float] =classOf[java.lang.Float]
  lazy val fatEntityType: Class[FatEntity] =classOf[FatEntity]
  lazy val crudWidgetType: Class[CrudWidget] =classOf[CrudWidget]
  lazy val fileIdType: Class[FileId] =classOf[FileId]
  lazy val numberType: Class[Number] =classOf[java.lang.Number]
//  lazy val pushToClientCacheType=classOf[PushToClientCache]

  lazy val clientSideCacheableType: Class[ClientSideCacheable] =classOf[ClientSideCacheable]
  lazy val serverSideCacheableType: Class[ServerSideCacheable] =classOf[ServerSideCacheable]
  lazy val cacheKeyType: Class[CacheKey] =classOf[CacheKey]
  lazy val cacheValueType: Class[CacheValue] =classOf[CacheValue]

}