package com.caspco.spl.cg.actor

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import com.caspco.spl.cg.actor.ProjectActor._
import com.caspco.spl.cg.analyze.actor.AnalyzerActor
import com.caspco.spl.cg.analyze.actor.AnalyzerActor.{Analyze, AnalyzeFailure, AnalyzeSuccess}
import com.caspco.spl.cg.bootstrap.{CodeGenerationConfiguration, LegoPluginJobType}
import com.caspco.spl.cg.generator.CodeGeneratorActor._
import com.caspco.spl.cg.generator.MainCodeGenerationActor
import com.caspco.spl.cg.objectifier.actor.ObjectifierActor
import com.caspco.spl.cg.objectifier.actor.ObjectifierActor._
import com.caspco.spl.cg.preview.actor.ProjectPreviewer.{Preview, PreviewStopped}
import org.slf4j.LoggerFactory


/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 23.02.14
 *         Time: 18:55
 */

object ProjectActor {

  trait InitMessage

  abstract class OperationReplay

  case class TotalOperationFailed(t: Throwable, jobId: Int) extends OperationReplay

  case class TotalOperationSucceed(jobId: Int) extends OperationReplay

  case class ProcessPath(inputPath: String, jobId: Int) extends InitMessage

  case class ProcessClasses(conf: CodeGenerationConfiguration, jobId: Int) extends InitMessage

  case class PreviewClasses(conf: CodeGenerationConfiguration, jobId: Int) extends InitMessage

  case class StartClean(conf: CodeGenerationConfiguration, jobId: Int) extends InitMessage

}

class ProjectActor extends Actor {
  val analyzer: ActorRef = context.actorOf(Props[AnalyzerActor], "AnalyzerActor")
  val objectifier: ActorRef = context.actorOf(Props[ObjectifierActor], "EntityObjectifierActor")
  val generator: ActorRef = context.actorOf(Props[MainCodeGenerationActor], "CodeGenActor")
//  val previewer = context.actorOf(Props[ProjectPreviewer], "PreviewerActor")
val logger = LoggerFactory.getLogger(this.getClass)
  def receive: Actor.Receive = {

    case PoisonPill =>
      logger.error("POISON PILL")

    case ProcessClasses(container, jid) =>
      objectifier ! ObjectifyClasses(container, jid)

    case StartClean(conf, jid) =>
      objectifier ! ObjectifyClassesForClean(conf, jid)

    case ObjectifySuccess(conf, p, jId) =>
      analyzer ! Analyze(conf, p, jId)

    case ObjectifySuccessForClean(conf, p, jid) =>
      generator ! CleanCode(conf, p, jid)

    case ObjectifyFailure(t, jId) =>
      context.parent ! TotalOperationFailed(t, jId)

    case CodeGenSuccess(jid, conf, p) =>
      conf.getTask match {
        case LegoPluginJobType.PREVIEW =>
          generator ! Preview(p, conf.getCgConfig,jid)
        case _ =>
          context.parent ! TotalOperationSucceed(jid)
      }

    case PreviewStopped(jid)=>
      context.parent ! TotalOperationSucceed(jid)

    case CodeCleanSuccess(jid) =>
      context.parent ! TotalOperationSucceed(jid)

    case CodeGenFailure(conf, t, jid) =>
      logger.error("Critical error occurred!", t)
      logger.error("Rolling back all operations")
//      objectifier ! ObjectifyClassesForClean(conf, jid)
          context.parent ! TotalOperationFailed(t,jid)

    case AnalyzeSuccess(conf, p, jId) =>
      conf.getTask match {
        case LegoPluginJobType.PREVIEW =>
          generator ! GenerateCode(conf, p, jId, writeInFile = false)
        case _ =>
          generator ! GenerateCode(conf, p, jId)
      }


    case AnalyzeFailure(t, jId) =>
      context.parent ! TotalOperationFailed(t, jId)

    case unknown =>
      throw new IllegalArgumentException(s"Unknown message type $unknown!")
  }
}