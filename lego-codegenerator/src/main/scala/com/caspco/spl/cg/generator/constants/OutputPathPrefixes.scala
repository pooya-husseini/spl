package com.caspco.spl.cg.generator.constants

import com.caspco.spl.cg.util.io.StreamUtils._

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 01/07/14
  *         Time: 16:11
  */
object OutputPathPrefixes {
  lazy val ClientSideExtension = "ts"
  lazy val SqlPathPrefix = s"sql"
  lazy val ControllerPathPrefix = s"app${/}controllers${/}"
  lazy val ServicePathPrefix = s"app${/}services${/}"
  lazy val RestServicePathPrefix = s"app${/}services${/}rest${/}"
  //  lazy val RestServicePathPrefix = s"app${/}conf${/}services${/}rest${/}"
  //  lazy val CrudServicePathPrefix = s"app${/}conf${/}services${/}crud${/}"
  lazy val CrudServicePathPrefix = s"app${/}services${/}operations${/}"
  lazy val PopupControllersPathPrefix = s"app${/}controllers${/}"
  lazy val IoServicePathPrefix = s"app${/}io${/}crud${/}"
  lazy val WebPagesPathPrefix = s"app${/}view${/}"
  lazy val FilterPathPrefix = s"app${/}filter${/}"
  lazy val ResourcePath = s"app${/}resources${/}"
  lazy val I18NResourcePath = s"${ResourcePath}i18n${/}"
  lazy val TranslationsPath = s"app${/}conf${/}translations${/}"
  lazy val SlickPathPrefix = s"db${/}collections"

}