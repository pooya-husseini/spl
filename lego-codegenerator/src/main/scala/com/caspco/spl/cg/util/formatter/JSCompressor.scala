package com.caspco.spl.cg.util.formatter

//import java.io._
//
//import com.yahoo.platform.yui.compressor.JavaScriptCompressor
//import org.mozilla.javascript.{ErrorReporter, EvaluatorException}
import ro.isdc.wro.extensions.processor.support.uglify.UglifyJs

//import com.yahoo.platform.yui.compressor.JavaScriptCompressor

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 13/01/15
 *         Time: 14:59
 */
object JSCompressor extends CodeBeautifier {
  private lazy val uglifyJs = UglifyJs.uglifyJs
  def apply(str: String): String = {
    //    val in = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8))
    //    Try {
    //      apply(in)
    //    } match {
    //      case Success(x) => x
    //      case Failure(f) => str
    //    }
    uglifyJs.process("", str)
  }

//  def apply(inputStream: InputStream): String = {
//
//
////    System.out.println(process)
//  }

//  def applyOld(inputStream: InputStream): String = {
//
//    val in = new InputStreamReader(inputStream, "utf-8")
//    val compressor = new JavaScriptCompressor(in, new ErrorReporter {
//      override def warning(s: String, s1: String, i: Int, s2: String, i1: Int): Unit = {
//
//      }
//
//      override def error(s: String, s1: String, i: Int, s2: String, i1: Int): Unit = {
//
//      }
//
//      def runtimeError(message: String, sourceName: String, line: Int, lineSource: String, lineOffset: Int): EvaluatorException = {
//        new EvaluatorException(message, sourceName, line, lineSource, lineOffset)
//      }
//    })
//    val out = new StringWriter
//    compressor.compress(out, -1, false, true, true, false)
//    out.flush()
//    out.close()
//    out.toString
//  }
}