package com.caspco.spl.cg.generator.javacg.actor

import com.caspco.spl.cg.bootstrap.CGConfig
import com.caspco.spl.cg.bootstrap.CGConfig.BootstrapAttribute
import com.caspco.spl.cg.generator.constants.TemplatePaths._
import com.caspco.spl.cg.generator.javacg.conversion.JavaClassDataModel
import com.caspco.spl.cg.generator.{CodeGeneration, CodeGenerationCompanion, CodeGenerationUnit}
import com.caspco.spl.cg.model.datamodel._
import com.caspco.spl.cg.model.project.Project
import com.caspco.spl.cg.util.io.OutputStreamFactory
import com.caspco.spl.cg.util.io.StreamUtils._
import com.caspco.spl.cg.util.template.{PojoClassToFile, TemplateEngine}
import com.caspco.spl.model.annotations.cg.IgnoreType
import com.caspco.spl.model.wrench.Tuple
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.JavaConverters._


/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 29/04/14
  *         Time: 14:36
  */

class JavaCodeGenerator(p: Project, pathConfig: CGConfig, writeInFile: Boolean) extends CodeGeneration(pathConfig, writeInFile) {

  override lazy val path: String = pathConfig.getConfiguration(BootstrapAttribute.SERVER_OUTPUT_PATH).asInstanceOf[String]
  lazy val operationalDataModel: DataModelContainer = p.getDataModelContainer.convertModelMap(JavaClassDataModel.apply)
  override lazy val generationUnits: List[CodeGenerationUnit] = {

    Option(path) match {
      case Some(_) =>
        List(
          GenerateDto,
          GenerateRepositories,
          GenerateServices,
          GenerateRestServices,
          //          GenerateDtoTransformer,
          //          GenerateEntityTransformer,
          GenerateTransformationMap,
          FakeGenerateDynamicClasses,
          GenerateExtraEntities,
          GenerateExtraEnums,
          GenerateSecurityExtras,
          GeneratePrincipals
        )
      case None => Nil
    }

  }
  val logger: Logger = LoggerFactory.getLogger(this.getClass)

  object GenerateSecurityExtras extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      if (pathConfig.getConfiguration(BootstrapAttribute.GENERATE_SECURITY_EXTRAS, classOf[Boolean])) {
        val domain = p.getDataModelContainer.entityModels.map(_.getAttribute("domain")).distinct.sortWith(_ < _).head
        val outPath = makeValidPath(path, Some(domain))
        val s = s"${outPath}resolvers${/}RoleResolverService.java"
        List((domain, s))
      } else {
        Nil
      }
    }

    override def generate(): Unit = {
      modelAndPath.collect {
        case (domain, outPath) =>
          val result = TemplateEngine.velocity(RoleResolverTemplatePath, Map("packageName" -> domain))
          OutputStreamFactory(outPath) <# result
      }
    }
  }

  object GenerateExtraEntities extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      p.getPlainJavaObjects.filter(!_.isEnum).map(x => {
        val packageName = if (x.getPackage == null) x.getName.substring(0, x.getName.lastIndexOf("."))
        else x.getPackage.getName
        val outPath = makeValidPath(path, Some(packageName))
        val p = s"$outPath${x.getSimpleName}".replace(".", "/").concat(".java")
        (new DataModel(false), p)
      })
    }

    def generate(): Unit = {

      p.getPlainJavaObjects.filter(!_.isEnum).foreach(x => {
        makeJavaFileFromObject(x, path)
      })
    }

    private def makeJavaFileFromObject(x: Class[_], path: String): Unit = {
      val packageName = if (x.getPackage == null) x.getName.substring(0, x.getName.lastIndexOf("."))
      else x.getPackage.getName
      val outPath = makeValidPath(path, Some(packageName))
      val classPath = packageName.replace(".", /) + / + x.getSimpleName + ".class"
      PojoClassToFile.generateFromClassFile(classPath, s"$outPath${x.getSimpleName}.java")
    }
  }

  object GenerateRepositories extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      operationalDataModel.entityModels
        .filterNot(p => p.isIgnoredWith(IgnoreType.SERVER_SIDE_SERVICES))
        .map {
          dm =>
            val outPath = makeValidPath(path, dm.getAttributeOption("domain"))
            val s = s"${outPath}repositories${/}${dm.getAttribute("name")}Repository.java"
            (dm, s)
        }
    }

    def generate(): Unit = {
      modelAndPath foreach {
        case (dm: DataModel, dmPath) =>
          val result = TemplateEngine.velocity(BusinessServiceTemplatePath, Map("dm" -> dm))
          OutputStreamFactory(dmPath) <# result
      }
    }
  }

  object FakeGenerateDynamicClasses extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      operationalDataModel.entityModels.filter(_ ?? "generated").map(entity => {
        val validPath = makeValidPath(path, entity.getAttributeOption("domain"))
        val outPath =s"""$validPath${entity.getAttribute("name")}Entity.java"""
        (Nil, outPath)
      }
      )
    }

    def generate(): Unit = {

    }
  }

  object GenerateServices extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      operationalDataModel.entityModels.filterNot(p => p.isIgnoredWith(IgnoreType.SERVER_SIDE_SERVICES) || (p ?? "baseModel" && !(p ?? "principal"))).map {
        dm =>
          val outPath = makeValidPath(path, dm.getAttributeOption("domain"))
          val s = s"${outPath}services${/}${dm.getAttribute("name")}Service.java"
          (dm, s)
      }
    }

    def generate(): Unit = {
      modelAndPath foreach {
        case (dm: DataModel, dmPath) =>
          val result = TemplateEngine.velocity(DomainServiceTemplatePath, Map("dm" -> dm))
          OutputStreamFactory(dmPath) <# result
      }

    }
  }

  object GenerateRestServices extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      operationalDataModel.entityModels.filterNot(p => p.isIgnoredWith(IgnoreType.SERVER_SIDE_CONTROLLER) || (p ?? "baseModel" && !(p ?? "principal"))).map {
        dm =>
          val outPath = makeValidPath(path, dm.getAttributeOption("domain"))
          val str = s"${outPath}rests${/}${dm.getAttribute("name")}RestService.java"
          (dm, str)
      }
    }

    def generate(): Unit = {
      modelAndPath foreach {
        case (dm: JavaClassDataModel, dmPath) =>
          val result = TemplateEngine.velocity(RestServiceTemplatePath, Map("dm" -> dm,
            "enumTypes" -> dm.enumsAsJava))
          OutputStreamFactory(dmPath) <# result
      }
    }
  }

  object GenerateEntityTransformer extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      operationalDataModel.entityModels.filterNot(p => p.isIgnoredWith(IgnoreType.TRANSFORMER) || p.getAttribute("abstract").equals("true")).map {
        dm =>
          val outPath = makeValidPath(path, dm.getAttributeOption("domain"))
          val p = s"${outPath}transformers${/}${dm.getAttribute("name")}EntityTransformer.java"
          (dm, p)
      }
    }

    def generate(): Unit = {
      modelAndPath foreach {
        case (dm: DataModel, dmPath) =>
          val result = TemplateEngine.velocity(EntityTransformerTemplatePath, Map("dm" -> dm))
          OutputStreamFactory(dmPath) <# result
      }
    }
  }

  object GenerateDtoTransformer extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      operationalDataModel.entityModels.filterNot(p => p.isIgnoredWith(IgnoreType.TRANSFORMER) || p.getAttribute("abstract").equals("true")).map {
        dm =>
          val outPath = makeValidPath(path, dm.getAttributeOption("domain"))
          val p = s"${outPath}transformers${/}${dm.getAttribute("name")}DtoTransformer.java"
          (dm, p)
      }
    }

    def generate(): Unit = {
      modelAndPath foreach {
        case (dm: DataModel, dmPath) =>
          val result = TemplateEngine.velocity(DtoTransformerTemplatePath, Map("dm" -> dm))
          OutputStreamFactory(dmPath) <# result
      }
    }
  }

  object GenerateTransformationMap extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      val r = operationalDataModel.entityModels.filter(p => p ?? "children").flatMap {
        dm =>
          val dmDomain = dm.getAttribute("domain")
          val dmName = dm.getAttribute("name")
          val dtoName = dmDomain + ".dtos." + dmName + "Dto"
          val dmConversion = (dm.getUniqueName + "Entity", dtoName)

          (dm.getSeparatedAttributes("children", ",")
            .map(i => {
              operationalDataModel.getDataModel(i) match {
                case None =>
                  throw new IllegalStateException("Child not found: " + i)
                case Some(x) =>

                  val domain = x.getAttribute("domain")
                  val dtoName = domain + ".dtos." + x.getAttribute("name") + "Dto"
                  val conversion = (i + "Entity", dtoName)
                  (domain, List(conversion, conversion.swap))
              }
            }) ++ List((dmDomain, List(dmConversion, dmConversion.swap))))
            .groupBy(i => i._1)
            .toList
            .map(i => {
              val outPath = makeValidPath(path, Some(i._1))
              val p = s"${outPath}transformers${/}SplTypeResolverImpl.java"
              ((i._1, i._2.flatMap(_._2)), p)
            })
      }
      r

    }

    def generate(): Unit = {
      modelAndPath foreach {
        case (model: (String, List[(String, String)]), outPath) =>
          model match {
            case (domain, tuples) =>
              val items = tuples.map(i => Tuple.of(i._1, i._2)).asJava
              val result = TemplateEngine.velocity(TransformationMapTemplatePath, Map("package" -> domain, "items" -> items))
              OutputStreamFactory(outPath) <# result
          }

      }
    }
  }

  object GenerateExtraEnums extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      operationalDataModel.enumModels.find(p => p.getAttribute("name") == "GeneralParameterType") match {
        case None => Nil
        case Some(x) =>
          val outPath = makeValidPath(path, x.getAttributeOption("domain"))
          val p = s"$outPath${x.getAttribute("name")}.java"
          List((x, p))
      }
    }

    def generate(): Unit = {
      modelAndPath foreach {
        case (x: DataModel, xPath) =>
          val result = TemplateEngine.velocity(GeneralParameterTypeEnumTemplatePath, Map("dm" -> x))
          OutputStreamFactory(xPath) <# result
        case _ => throw new IllegalArgumentException
      }
    }
  }

  object GenerateDto extends CodeGenerationUnit {

    override lazy val modelAndPath: List[(Any, String)] = {
      operationalDataModel.entityModels.filterNot(p => p.isIgnoredWith(IgnoreType.DTO)).map {
        dm =>
          val outPath = makeValidPath(path, dm.getAttributeOption("domain"))
          (dm, s"${outPath}dtos${/}${dm.getAttribute("name")}Dto.java")
      }
    }

    def generate(): Unit = {
      modelAndPath foreach {
        case (dm: DataModel, dmPath) =>
          val result = TemplateEngine.velocity(DtoTemplatePath, Map("dm" -> dm))
          OutputStreamFactory(dmPath) <# result
      }
    }
  }

  object GeneratePrincipals extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      operationalDataModel.entityModels.filter(p => p.containsAttribute("principal") && !p.isIgnoredWith(IgnoreType.PRINCIPAL)).flatMap {
        dm =>
          val outPath = makeValidPath(path, dm.getAttributeOption("domain"))
          val name = dm.getAttribute("name")
          val p = s"${outPath}transformers${/}PrincipalTo${name}EntityTransformer.java"
          val p2 = s"${outPath}transformers${/}PrincipalTo${name}DtoTransformer.java"
          val p3 = s"""${outPath}rests${/}${name}LoginController.java"""
          val p4 = s"""${outPath}services${/}${name}SecurityProvider.java"""
          val p5 = s"""${outPath}transformers${/}${name}EntityToPrincipalTransformer.java"""
          List(
            ((dm, PrincipalToEntityTemplatePath), p),
            ((dm, PrincipalToDtoTemplatePath), p2),
            ((dm, LoginControllerTemplatePath), p3),
            ((dm, SecurityProviderTemplatePath), p4),
            ((dm, EntityToPrincipalTemplatePath), p5)
          )
      }
    }

    def generate(): Unit = {
      modelAndPath foreach {
        case ((dm: DataModel, template: String), dmPath) =>
          val result = TemplateEngine.velocity(template, Map("dm" -> dm))
          OutputStreamFactory(dmPath) <# result
      }
    }
  }

}

object JavaCodeGenerator extends CodeGenerationCompanion {
  def apply(p: Project, pathConfig: CGConfig, writeInFile: Boolean): JavaCodeGenerator = {
    new JavaCodeGenerator(p, pathConfig, writeInFile)
  }
}

//object JavaCodeGenerator {
//  def props(p: Project, path: String): Props = Props(new JavaCodeGenerator(p, path))
//}