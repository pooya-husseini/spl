package com.caspco.spl.cg.util.formatter

import java.util.Properties

import org.eclipse.jdt.core.JavaCore
import org.eclipse.jdt.core.formatter.CodeFormatter
import org.eclipse.jdt.internal.formatter.{DefaultCodeFormatter, DefaultCodeFormatterOptions}
import org.eclipse.jface.text.Document

import scala.collection.JavaConverters._

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: ${Date}
 *         Time: ${time}
 */
object JavaCodeBeautifier extends CodeBeautifier {

  lazy val version = {
    //    val version = System.getProperty("java.version")
    //    val pos = version.indexOf('.', version.indexOf('.') + 1)
    //    version.substring(0, pos)
    "1.5"
  }

  lazy val loadProperties: Map[String, String] = {
    val resource = this.getClass.getClassLoader.getResourceAsStream("db.properties")
    Option(resource) match {
      case None =>
        Map.empty[String, String]
      case Some(x) =>
        val properties = new Properties
        properties.load(resource)
        properties.asScala.toMap
    }
  }

  def apply(code: String): String = {
    //    return code
    val options = Map(
      JavaCore.COMPILER_SOURCE -> version,
      JavaCore.COMPILER_COMPLIANCE -> version,
      JavaCore.COMPILER_CODEGEN_TARGET_PLATFORM -> version
    )

    val cfOptions = DefaultCodeFormatterOptions.getDefaultSettings
    cfOptions.setEclipseDefaultSettings()
    //    modifyCFOptions(cfOptions)
    //    cfOptions.insert_new_line_after_annotation = false
    cfOptions.blank_lines_before_method = 1
    cfOptions.number_of_empty_lines_to_preserve = 1
    cfOptions.insert_new_line_after_annotation_on_type = true
    cfOptions.insert_new_line_after_annotation_on_method = true
    cfOptions.insert_new_line_after_annotation_on_field = true
    cfOptions.insert_new_line_after_annotation_on_local_variable = true
    cfOptions.insert_new_line_after_annotation_on_package = true
    cfOptions.alignment_for_parameters_in_method_declaration = 2
    cfOptions.comment_insert_new_line_for_parameter = true

    cfOptions.tab_char = DefaultCodeFormatterOptions.SPACE

    cfOptions.set(loadProperties.map(x => ("org.eclipse.jdt.core.formatter." + x._1, x._2)).asJava)

    val cf = new DefaultCodeFormatter(cfOptions, options.asJava)
    val te = cf.format(CodeFormatter.K_UNKNOWN, code, 0, code.length, 0, null)
    val dc = new Document(code)
    te.apply(dc)
    dc.get

    //    def modifyCFOptions(cfOptions: DefaultCodeFormatterOptions) {
    //
    //    for (e <- codeFormatterOptionsMap.entrySet) {
    //      val fieldName: String = e.getKey
    //      try {
    //        val f: Field = classOf[DefaultCodeFormatterOptions].getField(fieldName)
    //        f.set(cfOptions, e.getValue)
    //      }
    //      catch {
    //        case exc: NoSuchFieldException => {
    //          throw new IllegalArgumentException("Invalid option " + fieldName)
    //        }
    //        case exc: IllegalAccessException => {
    //          throw new IllegalArgumentException("Option inaccessible: " + fieldName)
    //        }
    //      }
    //    }
    //  }
  }

}
