package com.caspco.spl.cg.objectifier.crud

import java.lang.annotation.Annotation
import java.lang.reflect.Modifier

import com.caspco.spl.cg.bootstrap.CGConfig.BootstrapAttribute
import com.caspco.spl.cg.bootstrap.CodeGenerationConfiguration
import com.caspco.spl.cg.generator.constants.TemplatePaths
import com.caspco.spl.cg.model.datamodel._
import com.caspco.spl.cg.model.layout.{FourColumnLayout, OneColumnLayout, ThreeColumnLayout, TwoColumnLayout}
import com.caspco.spl.cg.model.project.Project
import com.caspco.spl.cg.model.view._
import com.caspco.spl.cg.objectifier.actor.ObjectifierProcess
import com.caspco.spl.cg.util.enums.DynamicClassManipulation._
import com.caspco.spl.cg.util.logging.StopWatch._
import com.caspco.spl.cg.util.reflection.ClassMetaDataExtractor._
import com.caspco.spl.cg.util.reflection.Field
import com.caspco.spl.cg.util.string.ConstantStringMaker
import com.caspco.spl.cg.util.types.Types._
import com.caspco.spl.model.annotations.cg.{IgnoreType, Operation, Translation}
import com.caspco.spl.model.wrench.Locale
import com.caspco.spl.wrench.utils.reflection.TypeUtils
import org.reflections.Reflections
import org.reflections.scanners.SubTypesScanner
import org.reflections.util.{ClasspathHelper, ConfigurationBuilder, FilterBuilder}

import scala.collection.JavaConverters._
import scala.language.postfixOps


/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 15/04/14
  *         Time: 17:04
  */

class EntityObjectifier(conf: CodeGenerationConfiguration, prepareForClean: Boolean) extends ObjectifierProcess(conf) {

  import com.caspco.spl.cg.objectifier.crud.EntityObjectifierActor._

  lazy val avoidableAnnotations = List(generatedValueType, autoInjectType)
  lazy val editPageAvoidableAnnotations = avoidableAnnotations ++ List(idType, passwordType)

  def enhanceEntities(iterable: List[Class[_]]): List[Class[_]] = {

    iterable.map(p => {

      if (p.isAnnotationPresent(fatEntityType)) {
        p.addAnnotationToFields("com.caspco.annotations.cg.CrudWidget", field => {
          val getMethod = p.getMethods.find(m => m.getName == "get" + field.getName.capitalize)
          val annotations = field.getDeclaredAnnotations.toList ++ (getMethod match {
            case None => Nil
            case Some(m) => m.getDeclaredAnnotations
          })

          annotations.exists(p => p.annotationType() == oneToManyType) &&
            annotations.exists(p => p.annotationType() == createOnSelectType)
        })
      } else {
        p
      }
    })
  }

  /**
    *
    * @param entities
    * @return first is enums and the last is classes
    */
  def makeDynamicClasses(entities: List[Class[_]]): (List[Class[_]], List[Class[_]]) = {
    val typeUtil = TypeUtils.INSTANCE
    val generalParameters = entities.flatMap(p => p.convertFields.filter(x => x.isAnnotatedWith(generalParameterType)))
    val a = generalParameters.flatMap(p => p.annotations.filter(a => a.annotationType() == generalParameterType))
    val gpTypes = a.map(x => (typeUtil.getAnnotationValue(x, "type").asInstanceOf[String],Array.empty[Translation])).toMap.toList
//    typeUtil.getAnnotationValue(x, "translations").asInstanceOf[Array[Translation]]
    val firstPackage = entities.map(_.safePackageName).distinct.sortWith(_ < _).head

    if (gpTypes.nonEmpty) {
      (firstPackage + ".enums.GeneralParameterType").makeEnum(gpTypes) match {
        case Some(x) =>
          classOf[GeneralParameterEntity].addEnumeratedToMethod(firstPackage + "" +
            ".GeneralParameterEntity",
            x.getName, "getMainType") match {
            case Some(gpEntity) =>
              (List(x), List(gpEntity))
            case None =>
              (Nil, Nil)
          }
        case None =>
          (Nil, Nil)
      }
    } else {
      (Nil, Nil)
    }
  }

  def makeProject(): Project = {

    val (uiEntities, ignoredEntities) = conf.getClasses.asScala.toList.partition(p => {
      !(p.isAnnotationPresent(ignoreType) && p.getAnnotation(ignoreType).mods().contains(IgnoreType.ALL)) &&
        !Modifier.isAbstract(p.getModifiers)
    })

    val abstractEntities = ignoredEntities.filter(p => Modifier.isAbstract(p.getModifiers))

    val processedEntities = if (!prepareForClean) {
      enhanceEntities(uiEntities)
    } else {
      uiEntities
    }
    //    var enums = List.empty[Class[_]]
    val (generatedEnums, generatedSimpleClassess) = if (!prepareForClean) makeDynamicClasses(processedEntities ++ ignoredEntities) else (Nil, Nil)
    val enums = conf.getEnums.asScala ++ generatedEnums
    val entities = processedEntities ++ generatedSimpleClassess ++ abstractEntities
    //      var entities = List.empty[Class[_]]
    //    entities ++=enhancedEntities
    val p = new Project

    if (!prepareForClean) {
      (generatedSimpleClassess ++ generatedEnums).foreach(p +=)
    } else {
      val generatedEntities = entities.filter(x => x.isAnnotationPresent(generatedType) && x.isAnnotationPresent(entityType))
      val generatedEnums = enums.filter(x => x.isAnnotationPresent(generatedType))
      (generatedEnums ++ generatedEntities).foreach(p +=)
    }

    entities.foreach {
      model =>
        val entityModel = createEntityModel(model)
        p += entityModel
        val innerModels: Map[String, PopupPage] = if (model.isAnnotationPresent(operations)) {
          model.getAnnotation(operations).value()
            .filter(i => i.model() != classOf[Void])
            .map(item => (item.name(), insertionPage(item.model())))
            .toMap
        } else {
          Map.empty
        }
        val crudPage = new CrudPopupPage(entityModel, editingPage(model), showingPage(model), insertionPage(model, "item"), innerModels)
        val attributes = Map(
          "name" -> s"""${model.getSimplePlainName}Operations""",
          "domain" -> model.getPackage.getName,
          "dataModel" -> model.getPlainName,
          "abstract" -> Modifier.isAbstract(model.getModifiers).toString
        ) ++ convertClassAnnotationsToMap(model.getAnnotations) // for operations in html page

        crudPage += attributes
        //        p +=(, Some(List(TemplatePaths.EditPage)))
        //        p +=(, Some(List(TemplatePaths.CreatePage)))
        //        p +=(, Some(List(TemplatePaths.SearchPage)))
        //        p +=(, Some(List(TemplatePaths.ShowPage)))
        p += (crudPage, Some(List(TemplatePaths.CrudPage)))

        if (!model.isAnnotationPresent(notUiType)) {
          if (model.isAnnotationPresent(searchPageType)) {
            p += (mainPage(entityModel, model), Some(List(TemplatePaths.MainSearchPage)))
          } else {
            p += (mainPage(entityModel, model), Some(List(TemplatePaths.MainPage)))
          }
        }
    }

    enums.foreach(model => p += createEnumModel(model))

    val relatedModels = p.getDataModelContainer.allModels.filter(p => p.getElements.exists {
      x => x.containsAttribute("relation")
    })

    relatedModels.foreach {
      x =>
        x.getElements.filter(p => p.containsAttribute("relation")).foreach {
          case element: CompositionType =>
            val s: String = element.getAttribute("typename")
            p.getDataModelContainer.getDataModel(s) match {
              case Some(model) =>
                x.addRelatedDataModel(model)
              case None =>
                logger.info(s"$s not found!")
            }
          case element: ListType =>
            val s: String = element.getAttribute("typename")
            p.getDataModelContainer.getDataModel(s) match {
              case Some(model) =>
                x.addRelatedDataModel(model)
              case None =>
                logger.info(s"$s not found!")
            }
          case _ =>
          // todo add others
        }
    }
    p
  }

  def editingPage(model: Class[_]): PopupPage = {
    val modelName = model.getSimplePlainName
    val attributes = Map(
      "name" -> s"Edit$modelName",
      "domain" -> model.getPackage.getName,
      "dataModel" -> model.getPlainName
    )

    val widgets = model.extractUiFields.filter(p =>
      !p.isAnnotatedWithAny(editPageAvoidableAnnotations: _*)).map {
      x =>
        val widget = widgetMapper(x, model)
        widget += ("data-bind", "model." + x.name)
        widget += ("id", widget("id") + "edit")
        widget
    }

    //    var w = widgets.filterNot(p => p.containsAttribute("password"))
    //    val passwords = widgets.filter(p => p.containsAttribute("password"))

    //    passwords.foreach(p => {
    //      val p2 = new TextBox(p.getAttributes)
    //      p2 +=("name", p2("name") + "Again")
    //      p2 +=("id", p2("name"))
    //      p2 +=("validation_equals", p("data-bind"))
    //      p2 +=("data-bind", "passwordAgain")
    //      w ++= List(p, p2)
    //    })

    createPopupPage(attributes, widgets)
  }

  def insertionPage(model: Class[_], modelPrefix: String = "model"): CreatePopupPage = {
    import com.caspco.spl.cg.util.reflection.ClassMetaDataExtractor._

    val modelName = model.getSimplePlainName

    val attributes = Map(
      "name" -> s"Create$modelName",
      "domain" -> model.getPackage.getName,
      "dataModel" -> model.getPlainName,
      "simpleModel" -> modelName
    )
    //    boolean, list,map, composition, dateTime, currency, decimal,file,double,duration,integer, string
    val widgets: List[AbstractWidget] = model.extractUiFields.filter(p =>
      !p.isAnnotatedWithAny(avoidableAnnotations: _*)).map {
      x =>
        val widget = widgetMapper(x, model)
        widget += ("data-bind", modelPrefix + "." + x.name)
        widget += ("id", widget("id") + "insert")
        widget
    }

    var w = widgets.filterNot(p => p ?? "password" && (p getAttribute "password" eq "re-enter"))

    val passwords = widgets.filter(p => p ?? "password").filter(p => p.getAttribute("password").eq("re-enter"))

    passwords.foreach(p => {
      val p2 = TextBox(p.getAttributes())
      p2 += ("name", p2("name") + "Again")
      p2 += ("id", p2("name"))
      p2 += ("validation_equals", "$ctrl." + p("data-bind"))
      p2 += ("data-bind", "model.passwordAgain")
      p2 += ("passwordAgain", "")
      w ++= List(p, p2)
    })

    val page = new CreatePopupPage()
    page += attributes
    page += makeLayout(w, FourColumns)
    page
  }

  def createMainPage(dataModel: DataModel, createPopupPage: CreatePopupPage, attributes: Map[String, String], widgets: List[AbstractWidget]): MainPage = {
    val page = new MainPage(dataModel, createPopupPage)
    page += attributes

    page += makeLayout(widgets, FourColumns)
    page
  }

  def mainPage(dataModel: DataModel, model: Class[_]): AbstractPage = {
    import com.caspco.spl.cg.util.reflection.ClassMetaDataExtractor._

    val modelName = model.getSimplePlainName

    val attributes = Map(
      "name" -> modelName,
      "domain" -> model.getPackage.getName,
      "dataModel" -> model.getPlainName,
      "abstract" -> Modifier.isAbstract(model.getModifiers).toString
    ) ++ convertClassAnnotationsToMap(model.getAnnotations)

    val allWidgets = model.extractUiFields.filter(p => !p.isAnnotatedWithAny(avoidableAnnotations: _*))
    val searchableWidgets = model.extractUiFields.filter(p => !p.isAnnotatedWithAny(avoidableAnnotations: _*) &&
      p.isAnnotatedWithAny(searchableType))
    val autoInjectWidgets = model.extractUiFields.filter(p => !p.isAnnotatedWithAny(avoidableAnnotations: _*) &&
      p.isAnnotatedWithAny(autoInjectType))

    val unionWidgets = if (searchableWidgets.nonEmpty) searchableWidgets ++ autoInjectWidgets else allWidgets

    val widgets = unionWidgets.map {
      x =>
        val widget = widgetMapper(x, model)
        widget += ("data-bind", s"model." + x.name)
        widget -= "validation_notnull"
        if (conf.getCgConfig.containsConfiguration(BootstrapAttribute.VALIDATE)) {
          widget match {
            case TextBox(_) => widget += ("type-ahead", x.name)
            case _ =>
          }
        }
        //        widget +=("validation", widget("validation_notnull"))
        widget
    }
    widgets.filter(p => p.containsAttribute("autoinject")).foreach(p => p.widgetType = "0")
    createMainPage(dataModel, insertionPage(model), attributes, widgets)
  }

  def relationSearchPages(model: Class[_]): List[AbstractPage] = {
    model.extractUiFields.filter(p =>
      p.isAnnotatedWithAny(oneToOneType, manyToManyType, oneToManyType, manyToOneType)).map { x =>
      simpleSearchingPage(x.fieldType)
    }
  }

  def simpleSearchingPage(model: Class[_]): PopupPage = {
    import com.caspco.spl.cg.util.reflection.ClassMetaDataExtractor._

    val modelName = model.getSimplePlainName

    val attributes = Map(
      "name" -> s"Search$modelName",
      "domain" -> model.getPackage.getName,
      "dataModel" -> model.getPlainName
    )

    val allWidgets = model.extractUiFields.filter(p => !p.isAnnotatedWithAny(avoidableAnnotations: _*))
    val searchableWidgets = model.extractUiFields.filter(p => !p.isAnnotatedWithAny(avoidableAnnotations: _*) &&
      p.isAnnotatedWithAny(searchableType))
    val autoInjectWidgets = model.extractUiFields.filter(p => !p.isAnnotatedWithAny(avoidableAnnotations: _*) &&
      p.isAnnotatedWithAny(autoInjectType))

    val unionWidgets = if (searchableWidgets.nonEmpty) searchableWidgets ++ autoInjectWidgets else allWidgets

    val widgets = unionWidgets.map {
      x =>
        val widget: AbstractWidget = widgetMapper(x, model)
        widget += ("data-bind", s"model." + x.name)
        widget -= "validation_notnull"
        widget
    }

    createPopupPage(attributes, widgets)
  }

  def createPopupPage(attributes: Map[String, String], w: List[AbstractWidget]): PopupPage = {
    val page = new PopupPage()
    page += attributes
    page += makeLayout(w, FourColumns)
    page
  }

  private def makeLayout(widgets: List[AbstractWidget], layoutType: LayoutType = TwoColumns) = {

    val layout = layoutType match {
      case OneColumn => new OneColumnLayout
      case TwoColumns => new TwoColumnLayout
      case ThreeColumns => new ThreeColumnLayout
      case FourColumns => new FourColumnLayout
    }
    var nonRelatedWidgets: List[AbstractWidget] = widgets.filter(p => !p.containsAttribute("relation"))

    if (nonRelatedWidgets.size > layoutType.getCount) {
      val remains = layoutType.getCount * ((nonRelatedWidgets.size / layoutType.getCount) + 1) - nonRelatedWidgets.size
      (0 until remains).foreach(_ => nonRelatedWidgets +:= NullWidget())
    }
    layout += nonRelatedWidgets
    widgets.filter(p => p.containsAttribute("relation")).foreach(x => layout += x)
    layout.getGridType.get.getRows.head.getCells.head.getWidgets.foreach(_ += ("focused", "true"))
    layout
  }

  def widgetMapper(field: Field, modelClass: Class[_],
                   additionalAttributes: Map[String, String] = Map.empty[String, String]): AbstractWidget = field match {
    case Field(n, t, a, g, _, _) =>

      val attributes = Map("id" -> n, "name" -> n, "label" -> getPropertyFileKey(modelClass, n, "label"),
        "data-bind" -> ("model." + n)) ++ convertMethodAnnotationsToMap(a) ++ additionalAttributes

      t match {
        case `booleanType` => CheckBox(attributes)
        case `dateType` | `sqlDateType` => DatePicker(attributes)
        case _ if field.isAnnotatedWith(enumeratedWithType) =>
          val enumType: String = attributes("enumType")
          val simpleEnumType = enumType.substring(enumType.lastIndexOf(".") + 1)
          ComboBox(attributes ++ Map("filler" -> simpleEnumType))
        case _ if field.isAnnotatedWithAll(crudWidgetType) && field.isAnnotatedWithAny(oneToManyType, manyToManyType) =>
          CrudGrid(attributes ++ Map("fieldName" -> g.get.getSimplePlainName))
        case _ if field.isAnnotatedWithAny(oneToManyType, manyToManyType) =>
          if(field.isAnnotatedWith(operationForAppendType) ){
            NullWidget()
          }else {
            Grid(attributes ++ Map("fieldName" -> g.get.getSimplePlainName))
          }
        case _ if field.isAnnotatedWithAny(oneToOneType, manyToOneType) =>
          ModelPresenter(attributes ++ Map("fieldName" -> t.getSimplePlainName, "fullName" -> t.getPlainName))
        case _ if field.isAnnotatedWithAny(oneToOneType, manyToOneType) && field.isAnnotatedWithAll(crudWidgetType) =>
          CrudModelPresenter(attributes ++ Map("fieldName" -> t.getSimplePlainName, "fullName" -> t.getPlainName))
        case _ if field.isAnnotatedWithAny(generalParameterType) =>
          CodeResolver(attributes)
        case _ if field.isAnnotatedWithAny(oneToOneType, manyToOneType) && field.isAnnotatedWith(lookupType) =>
          Lookup(attributes)
        case _ if field.isAnnotatedWith(passwordType) => TextBox(attributes ++ Map("type" -> "password"))
        case _ if field.isAnnotatedWith(textAreaType) => TextAreaBox(attributes)
        case _ if field.isAnnotatedWith(fileIdType) =>
          FileUploader(attributes)
        case `bigDecimalType` => AmountBox(attributes)
        case x if classOf[java.lang.Number].isAssignableFrom(x) =>
          if (x.isAnnotationPresent(numericType)) {
            TextBox(attributes)
          } else {
            TextBox(attributes ++ Map("validation_numeric" -> ""))

          }
        case _ => TextBox(attributes)
      }
  }

  def getPropertyFileKey(model: Class[_], name: String, attributeName: String): String = {
    s"${model.getPlainName}.$name.$attributeName"
  }

  def showingPage(model: Class[_]): PopupPage = {
    val modelName = model.getSimplePlainName
    val attributes = Map(
      "name" -> s"Show$modelName",
      "domain" -> model.getPackage.getName,
      "dataModel" -> model.getPlainName
    )

    val widgets = model.extractUiFields.filter(p =>
      !p.isAnnotatedWithAny(editPageAvoidableAnnotations: _*)).map { x =>
      val widget = widgetMapper(x, model, Map("disabled" -> "true", "data-bind" -> ("model." + x.name)))
      widget -= "validation_notnull"
      widget
    }

    createPopupPage(attributes, widgets)
  }


  def createEntityModel(model: Class[_]): EntityModel = {
    //    boolean, list,map, composition, dateTime, currency, decimal,file,double,duration,integer, string,long
    import com.caspco.spl.cg.util.reflection.ClassMetaDataExtractor._
    val fields = model.extractModelFields.map {
      dataElementMapper
    }

    val dm = new EntityModel
    if (model.getPackage == null) {
      val name: String = model.getName
      dm += ("domain", name.substring(0, name.lastIndexOf(".")))
    } else {
      dm += ("domain", model.getPackage.getName)
    }
    dm += ("name", model.getSimplePlainName)

    if (model.getSuperclass != null && model.getSuperclass != classOf[Object]) {
      if (!(model.getSuperclass.isAnnotationPresent(ignoreType) &&
        model.getSuperclass.getAnnotation(ignoreType).mods().contains(IgnoreType.ALL))) {
        dm += ("baseModel", model.getSuperclass.getSimplePlainName)
        dm += ("baseModelDomain", model.getSuperclass.getPackage.getName)
      }
    }
    if(Modifier.isAbstract(model.getModifiers)){


      val reflections = new Reflections(new ConfigurationBuilder()
        .addClassLoader(ClasspathHelper.contextClassLoader)
        .setUrls(ClasspathHelper.forClassLoader)
        .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("com.caspco")))
        .setScanners(new SubTypesScanner))

      dm += ("children", reflections.getSubTypesOf(model).asScala.map(_.getPlainName).mkString(","))
      dm.getSeparatedAttributesAsJava("children",",").size()
    }
    dm += ("abstract", Modifier.isAbstract(model.getModifiers).toString)

    dm += convertClassAnnotationsToMap(model.getAnnotations)
    dm += fields
    dm
  }

  def dataElementMapper(f: Field): AbstractElement = f match {
    case Field(n, t, a, g, v, inSuperClass) =>

      val attributes = Map("name" -> n, "inSuperClass" -> inSuperClass.toString) ++ convertMethodAnnotationsToMap(a)
      t match {
        case `intType` => IntegerType(attributes).setValue(v)
        case `stringType` => StringType(attributes).setValue(v)
        case `floatType` => FloatType(attributes).setValue(v)
        case `booleanType` => BooleanType(attributes).setValue(v)
        case `doubleType` => DoubleType(attributes).setValue(v)
        case `longType` => LongType(attributes).setValue(v)
        case `listType` => g match {
          case None =>
            ListType(attributes).setValue(v)
          case Some(x) =>
            ListType(Map("typename" -> x.getPlainName, "plaintypename" -> x.getSimplePlainName) ++ attributes)
              .setValue(v)
        }
        case `sqlDateType` => DateType(attributes).setValue(v)
        case `dateType` => DateTimeType(attributes).setValue(v)
        case `currencyType` => CurrencyType(attributes).setValue(v)
        //        case x if x.isAnnotationPresent(entityType) =>
        //          DataModelType(Some(createEntityModel(t)),Map("typename" -> t.getName,
        //            "plaintypename" -> t.getSimplePlainName) ++ attributes)
        case _ =>
          CompositionType(Map("typename" -> t.getPlainName, "plaintypename" -> t.getSimplePlainName) ++ attributes)
            .setValue(v)
      }
  }

  def createEnumModel(model: Class[_]): EnumModel = {
    //    boolean, list,map, composition, dateTime, currency, decimal,file,double,duration,integer, string,long
    val fields: List[AbstractElement] = model.extractEnums.map(field => dataElementMapper(field))

    val dm = new EnumModel

    if (model.getPackage == null) {
      val name: String = model.getName
      dm += ("domain", name.substring(0, name.lastIndexOf(".")))
    } else {
      dm += ("domain", model.getPackage.getName)
    }
    dm += ("name", model.getSimplePlainName)

    dm += fields
    dm
  }
}

object EntityObjectifierActor {
  private lazy val typeUtil = TypeUtils.INSTANCE

  def convertMethodAnnotationsToMap(annotations: Seq[Annotation]): Map[String, String] = {
    annotations.map {
      x =>
        x.annotationType() match {
          case `idType` => List(("identifier", "true"))
          case `generatedValueType` => List(("generatedValue", "true"))
          case `enumeratedWithType` =>
            val clazz = typeUtil.getAnnotationValue(x, "value").asInstanceOf[Class[_]]
            List(("enumerated", "true"), ("enumType", clazz.getName))
          case `oneToManyType` => List(("relation", "OneToMany"))
          case `manyToManyType` => List(("relation", "ManyToMany"))
          case `manyToOneType` => List(("relation", "ManyToOne"))
          case `oneToOneType` => List(("relation", "OneToOne"))
          case `notUiType` => List(("notui", "true"))
          case `relationDetailsType` =>
            val bindableField = typeUtil.getAnnotationValue(x, "bindableField").asInstanceOf[String]
            val headerField = typeUtil.getAnnotationValue(x, "headerField").asInstanceOf[String]
            val detailFields = typeUtil.getAnnotationValue(x, "detailFields").asInstanceOf[Array[String]]
            val fieldsToSelect = typeUtil.getAnnotationValue(x, "fieldsToSelect").asInstanceOf[Array[String]]
            List(
              ("detailFields", detailFields.mkString(",")),
              ("fieldsToSelect", fieldsToSelect.mkString(","))
            ) ++ (if (bindableField.isEmpty) Nil else List(("bindableField", bindableField))) ++
              (if (headerField.isEmpty) Nil else List(("headerField", headerField)))
          case `titleType` => List(("title", ""))
          case `searchableType` => List(("searchable", "true"))
          case `notNullType` => List(("validation_notnull", ""))
          case `maxType` =>
            val value = typeUtil.getAnnotationValue(x, "value").asInstanceOf[Long]
            List(("validation_max", value.toString))
          case `sizeType` =>
            val min = typeUtil.getAnnotationValue(x, "min").asInstanceOf[Int].toString
            val max = typeUtil.getAnnotationValue(x, "max").asInstanceOf[Int].toString
            List(("validation_size_min", min), ("validation_size_max", max))
          case `patternType` =>
            val value = typeUtil.getAnnotationValue(x, "regexp").asInstanceOf[String]
            List(("validation_pattern", value))
          case `numericType` =>
            List(("validation_numeric", ""))
          case `emailType` =>
            List(("validation_email", ""))
          case `stringEqualType` =>
            val value = typeUtil.getAnnotationValue(x, "value").asInstanceOf[String]
            List(("validation_equals", value))
          case `minType` =>
            val value = typeUtil.getAnnotationValue(x, "value").asInstanceOf[Long]
            List(("validation_min", value.toString))
          case `createOnSelectType` =>
            List(("createOnSelect", ""))
          case `operationForAppendType` =>
            List(("operationForAppend", typeUtil.getAnnotationValue(x, "operationName").asInstanceOf[String]))
          case `priorityType` =>
            val order = typeUtil.getAnnotationValue(x, "value").asInstanceOf[Int]
            List(("priority", String.valueOf(order)))
          case `passwordType` =>
            val value = typeUtil.getAnnotationValue(x, "makeReEnter").asInstanceOf[Boolean]
            List(
              ("password", if (value) "re-enter" else "")
            )
          case `usernameType` => List(("username", ""))
          case `generalParameterType` => List(("generalParameter", ConstantStringMaker(typeUtil.getAnnotationValue(x, "type").toString)))
          case `fileIdType` => List(("fileId", ""))
          case `translationsType` =>
            val translations = typeUtil.getAnnotationValue(x, "value").asInstanceOf[Array[Translation]]
            translations.map(p =>
              ("translation_" +
                typeUtil.getAnnotationValue(p, "locale").asInstanceOf[Locale].getLocale,
                typeUtil.getAnnotationValue(p, "label").toString)
            ).toList
          case `visibilityType` =>
            val s: String = typeUtil.getAnnotationValue(x, "dependsOn").toString.replace("'", "\"")
            List(("visibilityOn", s))
          case `cacheKeyType` =>
            val s: String = typeUtil.getAnnotationValue(x, "priority").toString
            List(("cacheKey", s))
          case `cacheValueType` =>
            val s: String = typeUtil.getAnnotationValue(x, "priority").toString
            List(("cacheValue", s))
          case `disabilityType` =>
            val s: String = typeUtil.getAnnotationValue(x, "dependsOn").toString
            List(("disabilityOn", s))
          case `autoInjectType` =>
            List(("autoinject", ""))
          case `crudWidgetType` =>
            List(("crudwidget", ""))
          case _ => Nil
        }
    }.toList.flatten.toMap
  }

  def convertClassAnnotationsToMap(annotations: Seq[Annotation]): Map[String, String] = {
    annotations.map {
      x =>
        x.annotationType() match {
          case `generatedType` => List(("generated", "true"))
          case `tableType` =>
            val tableName = typeUtil.getAnnotationValue(x, "name").asInstanceOf[String]
            List(("tableName", tableName))
          case `ignoreType` =>
            val mods = typeUtil.getAnnotationValue(x, "mods").asInstanceOf[Array[IgnoreType]]
            List(("ignore", mods.map(p => p.name()).mkString(",")))
          case `notUiType` => List(("notui", "true"))
          case `operations` =>
            val ops = typeUtil.getAnnotationValue(x, "value").asInstanceOf[Array[Operation]]
            List(("operations", ops.map(p => p.name()).mkString(","))) ++
              ops.map(p => ("operation_server_" + p.name(), p.generateServerSide().toString)) ++
              ops.map(p => ("operation_client_" + p.name(), p.generateClientSide().toString)) ++
              ops.filter(_.model() != classOf[Void]).map(p => ("operation_model_" + p.name(), p.model().getName))
          case `translationsType` =>
            val translations = typeUtil.getAnnotationValue(x, "value").asInstanceOf[Array[Translation]]
            translations.map(p =>
              ("translation_" +
                typeUtil.getAnnotationValue(p, "locale").asInstanceOf[Locale].getLocale,
                typeUtil.getAnnotationValue(p, "label").toString)
            ).toList
          case `serverSideCacheableType` =>
            val retainClean = typeUtil.getAnnotationValue(x, "retainClean").toString
            val keysDelimiter = typeUtil.getAnnotationValue(x, "keysDelimiter").toString
            val valuesDelimiter = typeUtil.getAnnotationValue(x, "valuesDelimiter").toString
            val prefix = "serverSideCacheable"
            List((prefix, ""),
              (s"${prefix}_retainClean", retainClean.toString),
              (s"${prefix}_keysDelimiter", keysDelimiter),
              (s"${prefix}_valuesDelimiter", valuesDelimiter))
          case `clientSideCacheableType` =>
            val retainClean = typeUtil.getAnnotationValue(x, "retainClean").toString
            val keysDelimiter = typeUtil.getAnnotationValue(x, "keysDelimiter").toString
            val valuesDelimiter = typeUtil.getAnnotationValue(x, "valuesDelimiter").toString
            val prefix = "clientSideCacheable"
            List((prefix, ""),
              (s"${prefix}_retainClean", retainClean.toString),
              (s"${prefix}_keysDelimiter", keysDelimiter),
              (s"${prefix}_valuesDelimiter", valuesDelimiter))
          //todo add retain clean and delimiter to the output code
          case `securityType` =>
            val roles = typeUtil.getAnnotationValue(x, "roles").asInstanceOf[Array[String]]
            roles.map(p => (s"security_role_$p", "")).toList
          case `injectableType` =>
            List(("injectable", ""))
          case `searchPageType` =>
            List(("searchPage", ""))
          case `userPrincipalType` => List(("principal", ""), ("unique_id_principal", ""))
          case _ => Nil
        }
    }.toList.flatten.toMap
  }
}