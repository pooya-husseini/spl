package com.caspco.spl.cg.model.datamodel

import java.util

import com.caspco.spl.cg.model.exception.IdentifierNotDefinedException

import scala.collection.JavaConverters._

/**
  * @author : Pooya. h
  *         Email : husseini@caspco.ir
  *         Date: 2/19/17
  *         Time: 11:25 AM
  */
object DataModelOperation {

  def apply(dataModel: DataModel): DataModelOperation = {
    val result = new DataModelOperation(dataModel.isCollection)
    result += dataModel.getElements
    dataModel.getInnerDataModels.foreach(result += _)
    dataModel.getRelatedDataModels.foreach(result addRelatedDataModel)
    result += dataModel.getAttributes()
    result
  }

}

class DataModelOperation(collection: Boolean) extends DataModel(collection) {

  lazy val getIdentifiers: List[AbstractElement] = {
    val result = getElements.filter(_.isIdentifier)
    if (result.nonEmpty) {
      result
    } else {
      throw new IdentifierNotDefinedException(getUniqueName)
    }
  }

  lazy val relations: List[AbstractElement] = getElements.filter(p => p ?? "relation" && !(p ?? "autoinject"))

  lazy val genericRelations: List[AbstractElement] = {
    relations filter {
      case _: ListType => true
      case _ => false
    }
  }
  // I've assumed that relations are fields with compositionType

  lazy val simpleRelations: List[AbstractElement] = {
    relations filter {
      case x: CompositionType if !(x ?? "autoinject") => true
      case _ => false
    }
  }

  lazy val nonRelatedElements: List[AbstractElement] = getElements.filterNot(p => p ?? "relation" || p ?? "notui")
  lazy val nonRelatedElementsAsJava: util.List[AbstractElement] = nonRelatedElements.asJava
  lazy val cacheKeyElements: List[AbstractElement] = getElements.filter(p => p ?? "cacheKey").sortWith(_ ("cacheKey").toInt < _ ("cacheKey").toInt)
  lazy val cacheKeyElementsAsJava: util.List[AbstractElement] = cacheKeyElements.asJava
  lazy val cacheValueElements: List[AbstractElement] = getElements.filter(p => p ?? "cacheValue").sortWith(_ ("cacheValue").toInt < _ ("cacheValue").toInt)
  lazy val cacheValueElementsAsJava: util.List[AbstractElement] = cacheValueElements.asJava
  lazy val nonRelatedElementsAsJavaReverse: util.List[AbstractElement] = nonRelatedElements.reverse.asJava
  lazy val genericRelationsAsJava: util.List[AbstractElement] = genericRelations.asJava
  lazy val simpleRelationsAsJava: util.List[AbstractElement] = simpleRelations.asJava
  lazy val searchableRelations: List[AbstractElement] = relations.filter(p => p ?? "searchable")
  lazy val relationsAsJava: util.List[AbstractElement] = relations.asJava
  lazy val relationsAsJavaReverse: util.List[AbstractElement] = relations.reverse.asJava
  lazy val relationNames: List[String] = relations.map(p => p.getAttribute("name"))
  lazy val relatedModelNames: List[String] = getRelatedDataModels.map(p => p.getUniqueName)
  lazy val searchableRelationsAsJava: util.List[AbstractElement] = searchableRelations.asJava
  lazy val relatedDataModelsAsJava: util.List[DataModel] = getRelatedDataModels.asJava
  lazy val uniqueRelatedDataModels: List[DataModel] = (getRelatedDataModels.map(p => (p.getAttribute("name"),
    p)).toMap ++ Map(getAttribute("name") -> this)).values.toList

  lazy val uniqueRelatedDataModelsAsJava: util.List[DataModel] = uniqueRelatedDataModels.asJava
  lazy val allUniqueRelatedDataModels: List[DataModel] = getRelatedDataModels.map(p => (p.getAttribute("name"),
    p)).toMap.values.toList
  lazy val allUniqueRelatedDataModelsAsJava: util.List[DataModel] = allUniqueRelatedDataModels.asJava
  lazy val uniqueRelatedDataExceptCurrentModels: List[DataModel] = getRelatedDataModels.map(p => (p.getAttribute("name"), p))
    .toMap.filter(x => x._1 != getAttribute("name")).values.toList
  lazy val enumeratedElements: List[AbstractElement] = {
    getElements.filter(_ ?? "enumerated")
  }

  lazy val uniqueEnumeratedElements: List[AbstractElement] = {
    getElements.filter(_ ?? "enumerated").map(p => (p.getAttribute("enumType"), p)).toMap.values.toList
  }

  lazy val uniqueEnumeratedElementsAsJava: util.List[AbstractElement] = uniqueEnumeratedElements.asJava
  lazy val enumeratedElementsAsJava: util.List[AbstractElement] = enumeratedElements.asJava
  lazy val uniqueRelatedDataExceptCurrentModelsAsJava: util.List[DataModel] = uniqueRelatedDataExceptCurrentModels.asJava
  lazy val autoInjectedElements: List[AbstractElement] = getElements.filter(p => p ?? "autoinject")
  lazy val autoInjectedElementsAsJava: util.List[AbstractElement] = autoInjectedElements.asJava
  lazy val fileIdElements: List[AbstractElement] = getElements.filter(_ ?? "fileId")
  lazy val fileIdElementsAsJava: util.List[AbstractElement] = fileIdElements.asJava
  lazy val pushToClientCacheElements: List[AbstractElement] = getElements.filter(_ ?? "pushToClientCache")
  lazy val pushToClientCacheElementsAsJava: util.List[AbstractElement] = pushToClientCacheElements.asJava
  lazy val appendOperations: List[AbstractElement] = {
    getElements.filter(_ ?? "operationForAppend")
  }

  lazy val appendOperationsAsJava: util.List[AbstractElement] = appendOperations.asJava

  lazy val titleElements: List[AbstractElement] = {
    getElements.filter(p => p ?? "title")
  }

  lazy val titleElementsAsJava: util.List[AbstractElement] = titleElements.asJava
  //  lazy val allSimpleModels = (simpleRelations.map(p => (p.getAttribute("name"), p)).toMap ++
  //    Map(getAttribute("name") -> this)).map(_._2).toList
  //  lazy val allSimpleModelsAsJava = allSimpleModels.asJava


  def getIdentifier: AbstractElement = getIdentifiers.head

  def getSearchableElements: List[AbstractElement] = getElements.filter(_ ?? "searchable")

  def getRelatedDataModel(modelName: String): Option[DataModel] = {
    val index = modelName.lastIndexOf(".") match {
      case x if x < 0 => 0
      case x => x
    }
    val name = modelName.substring(index)
    getRelatedDataModels.filter(_.getAttribute("name") == name) match {
      case Nil => None
      case x :: _ => Some(x)
    }
  }

}