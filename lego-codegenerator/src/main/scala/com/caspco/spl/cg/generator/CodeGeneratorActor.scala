package com.caspco.spl.cg.generator

import _root_.java.io.FileNotFoundException

import akka.actor.Actor
import com.caspco.spl.cg.bootstrap.CGConfig.BootstrapAttribute
import com.caspco.spl.cg.bootstrap.{CGConfig, CodeGenerationConfiguration}
import com.caspco.spl.cg.generator.javacg.actor.JavaCodeGenerator
import com.caspco.spl.cg.generator.report.JasperReportGenerator
import com.caspco.spl.cg.generator.web.actor.ClientSideCodeGenerator
import com.caspco.spl.cg.model.project.Project
import com.caspco.spl.cg.preview.actor.ProjectPreviewer
import com.caspco.spl.cg.util.io._
import org.slf4j.{Logger, LoggerFactory}

import _root_.scala.concurrent.ExecutionContext.Implicits._
import _root_.scala.concurrent.Future
import _root_.scala.util.{Failure, Success, Try}

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 2/12/14
  *         Time: 5:29 PM
  */
object CodeGeneratorActor {

  val logger: Logger = LoggerFactory.getLogger(this.getClass)

  case class GenerateCode(conf: CodeGenerationConfiguration, project: Project, jobId: Int, writeInFile: Boolean = true)

  case class CleanCode(conf: CodeGenerationConfiguration, project: Project, jobId: Int)

  case class CodeGenSuccess(jobId: Int, conf: CodeGenerationConfiguration, project: Project)

  case class CodeCleanSuccess(jobId: Int)

  case class CodeGenFailure(conf: CodeGenerationConfiguration, t: Throwable, jobId: Int)

  case class CodeCleanFailure(t: Throwable, jobId: Int)

  //  def props=Props(new CodeGeneratorActor)

}

trait CodeGenerationCompanion {
  def apply(p: Project, config: CGConfig, writeInFile: Boolean): CodeGeneration
}

abstract class CodeGeneration(cgConfig: CGConfig, writeInFile: Boolean) {

  lazy val path: String = null
  lazy val generationUnits: List[CodeGenerationUnit] = Nil
  implicit val streamType: StreamType = if (writeInFile) FileStreamType else InMemoryStreamType
  implicit val params: StreamParameters = StreamParameters(
    if (cgConfig.containsConfiguration(BootstrapAttribute.FORMAT_OUTPUT)) cgConfig.getConfiguration(BootstrapAttribute.FORMAT_OUTPUT).asInstanceOf[Boolean] else false,
    if (cgConfig.containsConfiguration(BootstrapAttribute.OVERWRITE_EXISTING)) cgConfig.getConfiguration(BootstrapAttribute.OVERWRITE_EXISTING).asInstanceOf[Boolean] else false
  )
  //  val compressJs: Boolean = if (cgConfig.containsConfiguration(BootstrapAttribute.COMPRESS_JS)) cgConfig.getConfiguration(BootstrapAttribute.COMPRESS_JS).asInstanceOf[Boolean] else false

  def generateProject(): Unit = {
    Option(path) match {
      case Some(_) =>
        generationUnits.foreach(_.generate())
      case _ =>
    }
  }

  def cleanProject(): Unit = {
    Option(path) match {
      case Some(_) =>
        generationUnits.foreach(_.removePaths())
      case _ =>
    }
  }

  def validateProject(): Unit = {
    val configuration = cgConfig.getConfiguration(BootstrapAttribute.VALIDATE).asInstanceOf[Boolean]
    if (configuration) {
      Option(path) match {
        case Some(_) =>
          generationUnits.foreach(_.validate())
        case _ =>
      }
    }
  }
}

class MainCodeGenerationActor extends Actor {

  import com.caspco.spl.cg.generator.CodeGeneratorActor._

  lazy val generators: List[CodeGenerationCompanion] = {
    List(
      ClientSideCodeGenerator,
      JavaCodeGenerator,
      JasperReportGenerator,
      ProjectPreviewer

    )
  }

  def receive: Actor.Receive = {
    case CleanCode(conf, p, jid) =>
      Future {
        logger.info("Started cleaning code!")
        generators.foreach(cg => cg(p, conf.getCgConfig, writeInFile = true).cleanProject())
        context.parent ! CodeCleanSuccess(jid)
      }
    case GenerateCode(conf, p, jid, writeInFile) =>
      //      Future {
      logger.info("Started generating code!")
      Try {
        generators.foreach {
          cg =>
            val generator: CodeGeneration = cg(p, conf.getCgConfig, writeInFile)
            generator.generateProject()
            generator.validateProject()
        }
      } match {
        case Success(_) =>
          context.parent ! CodeGenSuccess(jid, conf, p)
        case Failure(f) =>
          context.parent ! CodeGenFailure(conf, f, jid)
        //      }
      }
  }
}

//trait CodeGenerationUnitAspect extends CodeGenerationUnit {
//
//  abstract override def toModelAndPath: List[(Any, String)] = {
//    super.toModelAndPath
//  }
//
//  abstract override def generate(): Unit = {
//
//    super.generate()
//
//  }
//
//  override def removePaths(): Unit = {
//
//    super.removePaths()
//
//  }
//}

abstract class CodeGenerationUnit(implicit val streamType: StreamType) {

  lazy val modelAndPath: List[(Any, String)] = Nil

  val logger: Logger = LoggerFactory.getLogger(this.getClass)

  def generate(): Unit

  def validate(): Unit = {
    modelAndPath.foreach {
      case (_, filePath) =>
        if (!(OutputStreamFactory(filePath) exits())) {

          if (!filePath.contains("%s")) {
            throw new FileNotFoundException(filePath)
          }
        }
      //          case Failure(ex) => throw ex
      //          case Success(x) => logger.info(s"${x.getPath} is already found!")
      //        }
    }
  }

  def removePaths(): Unit = {
    modelAndPath.foreach {
      case (_, filePath) =>
        OutputStreamFactory(filePath) delete()
    }
  }
}