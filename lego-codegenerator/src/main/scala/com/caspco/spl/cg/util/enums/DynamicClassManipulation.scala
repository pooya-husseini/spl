package com.caspco.spl.cg.util.enums

import java.io.File
import java.lang.reflect.Field
import java.net.URL
import javassist._
import javassist.bytecode.annotation._
import javassist.bytecode.{AnnotationsAttribute, Descriptor, FieldInfo}
import javassist.expr.{ExprEditor, MethodCall}
import javax.persistence.{Entity, EnumType, Enumerated, Table}

import com.caspco.spl.cg.dummytype.EmptyEnum
import com.caspco.spl.cg.util.classutils.ClassUtils
import com.caspco.spl.cg.util.io.StreamUtils
import com.caspco.spl.cg.util.reflection.ClassMetaDataExtractor
import com.caspco.spl.cg.util.string.ConstantStringMaker
import com.caspco.spl.cg.util.types.Types
import com.caspco.spl.model.annotations.cg.{EnumeratedWith, Generated, Translation}
import com.caspco.spl.model.wrench.Locale

import scala.collection.JavaConverters._
import scala.util.{Success, Try}

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 04/10/14
  *         Time: 11:41
  */
object DynamicClassManipulation {
  lazy val default: ClassPool = ClassPool.getDefault

  def existsClass(name: String): Option[Class[_]] = {

    Try(default.getClassLoader.loadClass(name)) match {
      case Success(x) =>
        Some(x)
      case _ => None
    }
  }

  implicit class EnumGenerator(name: String) {

    def makeEnum(items: List[(String, Array[Translation])]): Option[Class[_]] = existsClass(name) match {
      case Some(_) =>
        None
      case None =>
        //    def makeEnum(list: List[String]): Class[_] = {
        val path = new ClassClassPath(classOf[EmptyEnum])
        val list = items.map(x => (x._1, x._2)).toMap.toList
        default.insertClassPath(path)
        val dynEnum = default.get(classOf[EmptyEnum].getName)
        dynEnum.setName(name)
        val constPool = dynEnum.getClassFile.getConstPool
        val enumItems = list.zipWithIndex.map {
          case (x, index) =>
            //        val enumField: String = s"""public static final $name ${x._1} = new $name("${x._1}", ${x._2});"""
            val itemName = ConstantStringMaker(x._1)
            val enumField: String = s"""public static final $name $itemName = new $name("$itemName", $index);"""
            val field = CtField.make(enumField, dynEnum)
//            val translations = new Annotation(constPool, default.get(classOf[Translations].getName))
            val trs = x._2.map(item => (item.locale().name(), item.label())).toSet
            val translationValues: Array[MemberValue] = trs.map {
              tr =>
                val translation = new Annotation(constPool, default.get(classOf[Translation].getName))
                val localeEnum = new EnumMemberValue(constPool)
                localeEnum.setType(classOf[Locale].getName)
                localeEnum.setValue(tr._1)
                translation.addMemberValue("locale", localeEnum)
                translation.addMemberValue("label", new StringMemberValue(tr._2, constPool))
                new AnnotationMemberValue(translation, constPool)
            }.toArray
            val memberValue: ArrayMemberValue = new ArrayMemberValue(constPool)
            memberValue.setValue(translationValues)
//            translations.addMemberValue("value", memberValue)
            val actions: ArrayMemberValue = new ArrayMemberValue(constPool)
            actions.setValue(Array.empty)
//            translations.addMemberValue("menuEntries", actions)
            val attribute = new AnnotationsAttribute(constPool, AnnotationsAttribute.visibleTag)
//            if(trs.nonEmpty){
//              attribute.addAnnotation(translations)
//            }

//            field.getFieldInfo.addAttribute(attribute)
            dynEnum.addField(field)
            itemName
        }
        //      dynEnum.makeClassInitializer.insertAfter(s"$$VALUES = new $name[] { " + list.mkString(",") + " };")
        dynEnum.makeClassInitializer.insertAfter(s"$$VALUES = new $name[] { " + enumItems.mkString(",") + " };")
        dynEnum.writeFile(StreamUtils.tempPath + StreamUtils./)
        //      dynEnum.writeFile()
        default.removeClassPath(path)
        Some(dynEnum.toClass)
    }


  }

  implicit class ClassManipulator(c: Class[_]) {


    //    def addAnnotationToFields(): Class[_]={
    //      val path: ClassClassPath = new ClassClassPath(c)
    //      default.insertClassPath(path)
    //
    //    }

    def addAnnotations(): Class[_] = {
      val path: ClassClassPath = new ClassClassPath(c)
      default.insertClassPath(path)
      //      default.insertClassPath(".")
      val clazz: CtClass = default.get(c.getName)

      val constPool = clazz.getClassFile.getConstPool
      val attribute = new AnnotationsAttribute(constPool, AnnotationsAttribute.visibleTag)
      val entityAnnotation = new Annotation(constPool, default.get(classOf[Entity].getName))
      val tableAnnotation = new Annotation(constPool, default.get(classOf[Table].getName))
      entityAnnotation.addMemberValue("name", new StringMemberValue(clazz.getSimpleName, constPool))
      val className = ClassMetaDataExtractor.makeName(classOf[Object], clazz.getSimpleName)
      tableAnnotation.addMemberValue("name", new StringMemberValue(className, constPool))
      attribute.addAnnotation(entityAnnotation)
      attribute.addAnnotation(tableAnnotation)
      clazz.getClassFile.addAttribute(attribute)
      clazz.toClass
    }

    def addFieldWithGetterSetter(fieldType: String, fieldName: String): Class[_] = {
      val path: ClassClassPath = new ClassClassPath(c)
      //      default.insertClassPath(path)
      //      default.insertClassPath(".")
      val clazz: CtClass = default.get(c.getName)
      //      clazz.stopPruning(true)
      val constPool = clazz.getClassFile.getConstPool
      val field: CtField = new CtField(default.get(fieldType), fieldName, clazz)
      clazz.addField(field)
      addGetterSetter(field, fieldName, clazz)

      //      val result=clazz.toClass
      //      clazz.stopPruning(false)
      clazz.toClass
    }

    def changeFieldAddGetterSetter(newClassName: String, newFieldType: String,
                                   fieldName: String): Class[_] = {
      val path: ClassClassPath = new ClassClassPath(c)
      default.insertClassPath(path)
      //      default.insertClassPath(".")

      val clazz: CtClass = default.getAndRename(c.getName, newClassName)
      val constPool = clazz.getClassFile.getConstPool
      val field: CtField = clazz.getField(fieldName)
      field.setType(default.get(newFieldType))
      addGetterSetter(field, fieldName, clazz)
      val attribute = clazz.getClassFile.getAttributes.asScala.find(_.isInstanceOf[AnnotationsAttribute]) match {
        case Some(x) => x.asInstanceOf[AnnotationsAttribute]
        case None => new AnnotationsAttribute(constPool, AnnotationsAttribute.visibleTag)
      }

      val entityAnnotation = new Annotation(constPool, default.get(classOf[Entity].getName))
      entityAnnotation.addMemberValue("name", new StringMemberValue(clazz.getSimpleName, constPool))
      attribute.addAnnotation(entityAnnotation)

      val cgAnnotation = new Annotation(constPool, default.get(classOf[Generated].getName))
      cgAnnotation.addMemberValue("value", new StringMemberValue("XView code generator", constPool))
      attribute.addAnnotation(cgAnnotation)

      clazz.getClassFile.addAttribute(attribute)
      clazz.setModifiers(Modifier.PUBLIC)

      clazz.writeFile(StreamUtils.tempPath + StreamUtils./)
      //      clazz.writeFile()

      clazz.toClass
    }

    private def addGetterSetter(field: CtField, fieldName: String, clazz: CtClass) {
      val constPool = clazz.getClassFile.getConstPool
      //      val getter: CtMethod = CtNewMethod.getter(s"get${fieldName.capitalize}", field)
      val getter: CtMethod = clazz.getDeclaredMethod("getMainType")
      val attribute = getter.getMethodInfo.getAttributes.asScala.find(_.isInstanceOf[AnnotationsAttribute]) match {
        case Some(x) => x.asInstanceOf[AnnotationsAttribute]
        case None => new AnnotationsAttribute(constPool, AnnotationsAttribute.visibleTag)
      }
      //      val attribute = new AnnotationsAttribute(constPool, AnnotationsAttribute.visibleTag)
      val enumerated = new Annotation(constPool, default.get(classOf[Enumerated].getName))
      val enumType = new EnumMemberValue(constPool)
      enumType.setType(EnumType.STRING.getClass.getName)
      enumType.setValue(EnumType.STRING.name())
      enumerated.addMemberValue("value", enumType)
      //      getter.getMethodInfo2.addAttribute(attribute)
      //      clazz.addMethod(getter)
      //      clazz.getClassFile.getMethod(getter.getName).addAttribute(attribute)
      val desc = Descriptor.changeReturnType(field.getType.getName, getter.getMethodInfo.getDescriptor)
      val newMethod = new CtMethod(field.getType, "getMainType", Array.empty[CtClass], clazz)
      newMethod.setBody(s"return $fieldName;")
      //      newMethod.setModifiers(Modifier.PUBLIC & ~Modifier.ABSTRACT)
      //      clazz.setModifiers(Modifier.PUBLIC & ~Modifier.ABSTRACT)
      newMethod.getMethodInfo.addAttribute(attribute)
      clazz.removeMethod(getter)
      clazz.addMethod(newMethod)
      clazz.addMethod(CtNewMethod.setter(s"set${fieldName.capitalize}", field))
    }

    def addEnumeratedToMethod(newClassName: String, enumeratedType: String, methodName: String): Option[Class[_]] = existsClass(newClassName) match {
      case Some(x) =>
        None
      case None =>
        val path: ClassClassPath = new ClassClassPath(c)
        default.insertClassPath(path)
        //      default.insertClassPath(".")
        val clazz: CtClass = default.getAndRename(c.getName, newClassName)
        val constPool = clazz.getClassFile.getConstPool
        val method: CtMethod = clazz.getDeclaredMethod(methodName)

        val methodAttribute = method.getMethodInfo.getAttributes.asScala.find(_.isInstanceOf[AnnotationsAttribute]) match {
          case Some(x) => x.asInstanceOf[AnnotationsAttribute]
          case None => new AnnotationsAttribute(constPool, AnnotationsAttribute.visibleTag)
        }

        val enumeratedAnnotation = new Annotation(constPool, default.get(classOf[EnumeratedWith].getName))
        enumeratedAnnotation.addMemberValue("value", new ClassMemberValue(enumeratedType, constPool))
        methodAttribute.addAnnotation(enumeratedAnnotation)

        method.getMethodInfo.addAttribute(methodAttribute)

        val attribute = clazz.getClassFile.getAttributes.asScala.find(_.isInstanceOf[AnnotationsAttribute]) match {
          case Some(x) => x.asInstanceOf[AnnotationsAttribute]
          case None => new AnnotationsAttribute(constPool, AnnotationsAttribute.visibleTag)
        }

        val entityAnnotation = new Annotation(constPool, default.get(classOf[Entity].getName))
        entityAnnotation.addMemberValue("name", new StringMemberValue(clazz.getSimpleName, constPool))
        attribute.addAnnotation(entityAnnotation)

        val cgAnnotation = new Annotation(constPool, default.get(classOf[Generated].getName))
        cgAnnotation.addMemberValue("value", new StringMemberValue("XView code generator", constPool))
        attribute.addAnnotation(cgAnnotation)

        clazz.getClassFile.addAttribute(attribute)
        clazz.setModifiers(Modifier.PUBLIC)

        clazz.writeFile(StreamUtils.tempPath + StreamUtils./)
        //      clazz.writeFile()

        Some(clazz.toClass)
    }


    def addAnnotationToFields(annotationName: String, fieldFilter: (Field) => Boolean): Class[_] = {

      def fieldTypeClassPaths(clazz: Class[_]): Set[URL] = {
        import com.caspco.spl.cg.util.reflection.ClassMetaDataExtractor._
        def innerFieldIterator(classes: List[Class[_]], typeNames: Set[String]): List[Class[_]] = {
          classes.filter(_.isAnnotationPresent(Types.entityType)).flatMap(p => {
            val declaredFields = p.getDeclaredFields.map(field => field.genericType.getOrElse(field.getType)).filter(f =>
              !typeNames(f.getName))

            declaredFields ++ innerFieldIterator(declaredFields.toList,
              declaredFields.map(_.getName).toSet ++ typeNames)
          })
        }

        val iterator = innerFieldIterator(List(clazz), Set.empty)
        iterator.map(p =>
          ClassUtils.whereFrom(p) match {
            case Some(x) => x
            case _ =>
              new File("").toURI.toURL
          }
        ).toSet

      }

      //      val paths: Set[URL] = fieldTypeClassPaths(c)
      //      val classLoader = new URLClassLoader(paths.toArray,this.getClass.getClassLoader)

      val path: ClassClassPath = new ClassClassPath(c)
      default.insertClassPath(path)
      //      default.insertClassPath(".")
      //      val first: CtClass = default.get(c.getName)
      //      first.detach()
      val clazz: CtClass = default.getAndRename(c.getName, c.getName + "Extended")
      //      clazz.getClassFile.getFields.asScala.foreach(println)
      clazz.defrost()
      val constPool = clazz.getClassFile.getConstPool
      val annotation = new Annotation(constPool, default.get(annotationName))

      val fieldNames = c.getDeclaredFields.filter(fieldFilter).map(_.getName).toSet
      clazz.getClassFile.getFields.asScala.map(_.asInstanceOf[FieldInfo]).filter(p => fieldNames(p.getName)).foreach(p => {
        val attributes = p.getAttributes.asScala
        val attribute = attributes.find(_.isInstanceOf[AnnotationsAttribute]) match {
          case Some(x) => x.asInstanceOf[AnnotationsAttribute]
          case None => new AnnotationsAttribute(constPool, AnnotationsAttribute.visibleTag)
        }
        attribute.addAnnotation(annotation)
        p.addAttribute(attribute)

      })
      val fieldName: String = "$$originalName"
      //      clazz.toClass(classLoader)
      val field: CtField = new CtField(default.get("java.lang.String"), fieldName, clazz)
      field.setModifiers(Modifier.STATIC | Modifier.PUBLIC)
      clazz.addField(field, CtField.Initializer.constant(c.getName))
      //      addGetterSetter(field, fieldName, clazz)
      clazz.toClass
      //       val swapper= new HotSwapper(9090)
      //      swapper.reload(c.getName,clazz.toBytecode)

    }
  }

  class ClassCreator(clazz: CtClass) {

    def addField(fieldType: Class[_], fieldName: String): ClassCreator = {
      val fieldTypeClazz: CtClass = default.get(fieldType.getName)
      val field = new CtField(fieldTypeClazz, fieldName, clazz)
      clazz.addField(field)
      val getter = CtNewMethod.getter("get" + fieldName.capitalize, field)
      val setter = CtNewMethod.setter("set" + fieldName.capitalize, field)
      clazz.addMethod(getter)
      clazz.addMethod(setter)
      this
    }

    def addMethod(methodCode: String): ClassCreator = {
      val method: CtMethod = CtNewMethod.make(methodCode, clazz)
      clazz.addMethod(method)
      this
    }

    def addMethod(name: String, body: String, returnType: Class[_], parameters: Array[Class[_]], exceptions: Array[Class[_]]): ClassCreator = {
      val parametersClazz: Array[CtClass] = parameters.map(p => default.get(p.getName))
      val returnTypeClazz: CtClass = default.get(returnType.getName)
      val exceptionClazz = exceptions.map(p => default.get(p.getName))
      val method: CtMethod = CtNewMethod.make(returnTypeClazz, name, parametersClazz, exceptionClazz, body, clazz)
      clazz.addMethod(method)
      this
    }

    def makeClass(): Class[_] = {
      clazz.toClass
    }
  }

  class MethodReplacer(replacedMethod: CtMethod, replacement: CtMethod) extends ExprEditor {
    override def edit(mcall: MethodCall): Unit = {
      val declaringClass = replacedMethod.getDeclaringClass

      val m = mcall.getMethod
      if (declaringClass.equals(m.getDeclaringClass) && m.equals(replacedMethod)) {
        mcall.replace("$_ = " + replacement.getName + "($$);")
      }
    }
  }

  object MakeClass {
    def apply(name: String): ClassCreator = {
      val clazz: CtClass = default.makeClass(name)
      new ClassCreator(clazz)
    }
  }

}