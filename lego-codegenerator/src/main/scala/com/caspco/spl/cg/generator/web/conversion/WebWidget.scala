package com.caspco.spl.cg.generator.web.conversion

import com.caspco.spl.cg.generator.constants.TemplatePaths._
import com.caspco.spl.cg.model.view._
import com.caspco.spl.cg.util.template.TemplateEngine

import scala.collection.JavaConverters._

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 03/06/14
  *         Time: 19:50
  */


object WebWidget {
  def convert(w: AbstractWidget): String = {
    // it needs three items to pass to the template : tagName, attributes, body
    val template = WidgetAttributeConverter.getWidgetTemplate(w)

    val attributes: Map[String, String] = template.attributes
    val attsList = (attributes.map(a => HtmlAttribute(a._1, a._2)).toList ++ WidgetAttributeConverter
      .convertAttributes(w))
      .filter(p => !p.isEmpty && !template.ignorableAttributes.exists(x => x.r.pattern.matcher(p.getKey).matches()))

    TemplateEngine.velocity(s"${WebTemplatePath}html/widgets/${template.path}.vm", Map("attributes" -> attsList.asJava,
      "widgetType" -> template.labelName, "widget" -> w))
  }
}