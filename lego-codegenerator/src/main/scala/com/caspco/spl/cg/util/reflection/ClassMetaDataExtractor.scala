package com.caspco.spl.cg.util.reflection

import java.lang.annotation.Annotation
import java.lang.reflect.{Modifier, ParameterizedType, Type}

import com.caspco.spl.cg.util.types.Types

import scala.util.{Failure, Success, Try}

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 15/04/14
 *         Time: 17:57
 */

case class Method(name: String, returnType: Class[_], annotations: Array[Annotation])

case class Field(name: String, fieldType: Class[_], annotations: Array[Annotation] = Array.empty[Annotation],
                 genericType: Option[Class[_]] = None, value: Option[String],inSuperClass:Boolean) {
  def isAnnotatedWithAll(c: Class[_ <: Annotation]*): Boolean = {
    c.forall(p=>isAnnotatedWith(p))
  }

  def isAnnotatedWith(c: Class[_ <: Annotation]): Boolean = {
    annotations.exists(p=>p.annotationType()==c)
  }

  def isAnnotatedWithAny(c: Class[_ <: Annotation]*): Boolean = {
    c.exists(p=>isAnnotatedWith(p))
  }

  def getGenericType=genericType

}

object ClassMetaDataExtractor {

  def makeName(c:Class[_],name: String): String = {
    if (name.endsWith("Entity")) {
      name.substring(0, name.length - "Entity".length)
    } else if (c.isEnum) {
      name
    } else {
      name
    }
  }

  def convertJavaPrimitiveType(typeName: String): Option[Class[_]] = typeName match {
    case "int" => Some(classOf[java.lang.Integer])
    case "double" => Some(classOf[java.lang.Double])
    case "boolean" => Some(classOf[java.lang.Boolean])
    case "long" => Some(classOf[java.lang.Long])
    case "byte" => Some(classOf[java.lang.Byte])
    case "short" => Some(classOf[java.lang.Short])
    case "float" => Some(classOf[java.lang.Float])
    case "char" => Some(classOf[java.lang.Character])
    case _ => None
  }

  implicit class FieldImplicitClass(f:java.lang.reflect.Field){
    lazy val genericType = f.getGenericType match {
      case gType: ParameterizedType =>
        Some(gType.getActualTypeArguments()(0).asInstanceOf[Class[_]])
      case _ =>
        None
    }
  }

  implicit class InnerClass(c: Class[_]) {

    lazy val safePackageName:String={
      if(c.getPackage==null){
        c.getName.substring(0,c.getName.lastIndexOf("."))
      }else{
        c.getPackage.getName
      }
    }

    lazy val extractEnums: List[Field] = {
      getValuesOfEnum.map {
        case x =>
          Field(name = x.name(),
          fieldType = x.getDeclaringClass,
          value = Some(x.name()),
          annotations = x.getClass.getField(x.name()).getDeclaredAnnotations
        ,inSuperClass = false)
      }
    }

    lazy val allFields:List[(Boolean,java.lang.reflect.Field)]= {
      if (c.getSuperclass != null) {
        c.getSuperclass.getDeclaredFields.filter(p=> !Modifier.isStatic(p.getModifiers)).toList.map(item=>(true,item))
      } else {
        List.empty
      }
    } ++ c.getDeclaredFields.filter(p=> !Modifier.isStatic(p.getModifiers)).toList.map(item=>(false,item))

    lazy val convertFields: List[Field] = {
      val methods = extractMethods

      allFields.map {
        case (inSuper,field) =>
          val methodNames = List("set" + field.getName.capitalize, "get" + field.getName.capitalize)
          val methodsAnnotation = methods.filter(p =>
            methodNames.contains(p.name)).flatMap { case x => x.annotations }

          convertJavaPrimitiveType(field.getType.getName) match {
            case Some(t) =>
              Field(field.getName, t, field.getDeclaredAnnotations ++ methodsAnnotation, field.genericType,
                value = None,inSuper)
            case None =>
              Field(field.getName, field.getType, field.getDeclaredAnnotations ++ methodsAnnotation, field.genericType,
                value = None,inSuper)
          }
      }
    }

    lazy val extractModelFields: List[Field] = {
      convertFields.filter(p=> !p.isAnnotatedWithAny(Types.versionType,Types.ignoreType))
    }

    lazy val extractUiFields: List[Field] = {
      convertFields.filter(p=> !p.isAnnotatedWithAny(Types.notUiType,Types.versionType,Types.ignoreType))
    }

    lazy val getGenericTypes: Array[Type] = {
      c.getGenericSuperclass.asInstanceOf[ParameterizedType].getActualTypeArguments
    }
    lazy val getOriginalSimpleName:String={
      getOriginalName.substring(getOriginalName.lastIndexOf(".")+1)
    }
    lazy val getOriginalName:String={
      c.getDeclaredFields.find(_.getName=="$$originalName") match{
        case None=>c.getName
        case Some(x)=>x.get(null).asInstanceOf[String]
      }
    }

    def getMethodOption(name: String, parameterTypes: Class[_]*): Option[java.lang.reflect.Method] = {
      Try(c.getDeclaredMethod(name, parameterTypes: _*)) match {
        case Success(x) => Some(x)
        case Failure(f) => None
      }
    }

    def getSimplePlainName: String = {
      makeName(c,getOriginalSimpleName)
    }

    def getPlainName: String = {
      makeName(c,getOriginalName)
    }

    def extractMethods: List[Method] = {
      c.getMethods.filter(_.getDeclaringClass!=classOf[Object]).map(m => Method(m.getName, m.getReturnType,
        m.getDeclaredAnnotations)).toList
    }

    def getValuesOfEnum: List[Enum[_]] = {
      c.getEnumConstants.asInstanceOf[Array[Enum[_]]].toList
    }
  }
}