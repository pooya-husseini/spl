package com.caspco.spl.cg.generator.web.actor

import com.caspco.spl.cg.bootstrap.CGConfig
import com.caspco.spl.cg.bootstrap.CGConfig.BootstrapAttribute
import com.caspco.spl.cg.generator._
import com.caspco.spl.cg.generator.constants.OutputPathPrefixes._
import com.caspco.spl.cg.generator.constants.TemplatePaths._
import com.caspco.spl.cg.model.datamodel._
import com.caspco.spl.cg.model.project.Project
import com.caspco.spl.cg.model.view.{AbstractPage, CrudPopupPage, MainPage, ModelDescription}
import com.caspco.spl.cg.util.io.OutputStreamFactory
import com.caspco.spl.cg.util.template.TemplateEngine
import com.caspco.spl.model.annotations.cg.IgnoreType

import scala.collection.JavaConverters._


/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 2/12/14
  *         Time: 5:28 PM
  */

object ClientSideCodeGenerator extends CodeGenerationCompanion {
  override def apply(p: Project, config: CGConfig, writeInFile: Boolean): CodeGeneration = {
    new ClientSideGenerator(p, config, writeInFile)
  }
}

class ClientSideGenerator(p: Project, config: CGConfig, writeInFile: Boolean) extends CodeGeneration(config, writeInFile) {

  import com.caspco.spl.cg.util.io.StreamUtils._

  override lazy val path: String = config.getConfiguration(BootstrapAttribute.CLIENT_OUTPUT_PATH).asInstanceOf[String]
  val operationalModelContainer: DataModelContainer = p.getDataModelContainer.convertModelMap(DataModelOperation.apply)

  override lazy val generationUnits: List[CodeGenerationUnit] = {
    Option(path) match {
      case Some(_) =>
        List(
          GenerateView,
          GenerateController,
          GenerateMenu,
          GenerateClientCrudService,
          GenerateClientCrudControllers
        )
      case None =>
        Nil
    }
  }

  object GenerateMenu extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      List((Nil, s"${makeValidPath(path)}app${/}conf${/}menu.json"),
        ("menu", s"$path${/}app${/}conf${/}translations${/}menu-%s.json"))
    }

    override def generate(): Unit = {
      val menus = p.getPageContainer.categorizeByPrefix

      val javaMap = new java.util.HashMap[String, java.util.List[ModelDescription]]()
      menus.foreach {
        case (k, v) => javaMap.put(k, v.asJava)
      }
      val result = TemplateEngine.velocity(MenuTemplate, Map("menus" -> javaMap))
      OutputStreamFactory(s"${makeValidPath(path)}app${/}conf${/}menu.json") <# result
    }
  }

  object GenerateClientCrudControllers extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      operationalModelContainer.entityModels.filterNot(p => p.getAttribute("abstract").equals("true") || p.isIgnoredWith(IgnoreType.CLIENT_SIDE_CONTROLLERS)).map {
        dm =>
          val outputDir = makeValidPath(s"$path${/}$PopupControllersPathPrefix", None)
          val str = s"$outputDir${dm.getAttribute("name")}OperationsController." + ClientSideExtension
          (dm, str)
      }
    }

    override def generate(): Unit = {
      modelAndPath.foreach {
        case (dm: DataModel, outPath) =>
          val result = TemplateEngine.velocity(ClientCrudControllersTemplate, Map("dm" -> dm))
          OutputStreamFactory(outPath) <# result
      }
    }
  }

  object GenerateClientCrudService extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      operationalModelContainer.entityModels.filterNot(p => p.getAttribute("abstract").equals("true") || p.isIgnoredWith(IgnoreType.CLIENT_SIDE_SERVICES) ).map {
        dm =>
          //          val outputDir = makeValidPath(s"$path${/}$CrudServicePathPrefix", dm.getAttributeOption("domain"))
          val outputDir = makeValidPath(s"$path${/}$CrudServicePathPrefix", None)
          val str = s"$outputDir${dm.getAttribute("name")}OperationsService." + ClientSideExtension
          (dm, str)
      }
    }

    override def generate(): Unit = {
      modelAndPath.foreach {
        case (dm: DataModel, outPath) =>
          val result = TemplateEngine.velocity(ClientCrudServiceTemplate, Map("dm" -> dm))
          OutputStreamFactory(outPath) <# result
      }
    }
  }

  object GenerateView extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      p.getPageContainer.allPages.filterNot(p => p._1.isIgnoredWith(IgnoreType.UI) || p._1.getAttribute("abstract").equals("true")).map {
        case (page, templates) =>
          //          val outputDir = makeValidPath(s"$path${/}$WebPagesPathPrefix", page.getAttributeOption("domain"))
          val outputDir = makeValidPath(s"$path${/}$WebPagesPathPrefix", None)
          val str = s"$outputDir${page.getAttribute("name")}.html"
          ((page, templates), str)
      }
    }

    override def generate(): Unit = {
      modelAndPath.foreach {

        case (pt: (AbstractPage, Option[List[String]]), xPath) =>
          val (page, templates) = pt
          val fullClassName = page.getAttributeOption("dataModel")

          val dataModel: Option[DataModel] = page match {
            case x: CrudPopupPage => Some(DataModelOperation(x.getDataModel))
            case x:MainPage => Some(DataModelOperation(x.getDataModel))
            case _=>None
          }

          val className = fullClassName match {
            case Some(x) if x.contains(".") => x.substring(x.lastIndexOf(".") + 1)
            case Some(x) => x
            case None => ""
          }

          val results = templates match {
            case Some(x) =>
              x.map {
                template =>
                  TemplateEngine.velocity(s"$template",
                    Map("page" -> page, "dataModel" -> dataModel, "className" -> className, "fullClassName" -> fullClassName.get))
              }
            case None =>
              List(TemplateEngine.velocity(PageTemplate, Map("page" -> page, "className" -> className,
                "fullClassName" -> fullClassName.get)))
          }

          results.foreach(result => OutputStreamFactory(xPath) <# result)
        case _ =>
      }
    }
  }

  object GenerateController extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      operationalModelContainer.entityModels.filterNot(x => x.isIgnoredWith(IgnoreType.CLIENT_SIDE_CONTROLLERS) || x.getAttribute("abstract").equals("true")).map {
        dm =>
          val outputDir = makeValidPath(s"$path${/}$ControllerPathPrefix", None)
          val str = s"$outputDir${dm.getAttribute("name")}Controller." + ClientSideExtension
          (dm, str)
      }
    }

    override def generate(): Unit = {
      modelAndPath.foreach {
        case (dm: DataModel, s) =>
          val result = TemplateEngine.velocity(DataModelTemplate, Map("dm" -> dm))
          OutputStreamFactory(s) <# result
      }
    }
  }
}