package com.caspco.spl.cg.generator.web.conversion

import com.caspco.spl.cg.model.view._
import com.caspco.spl.cg.util.constant.Constants
import com.caspco.spl.cg.util.string.ConstantStringMaker

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 2/12/14
  *         Time: 1:33 PM
  */

case class TemplateWidgetConversion(path: String, var labelName: String = "",
                                    var attributes: Map[String, String] = Map.empty[String, String],
                                    ignorableAttributes: Set[String] = Set.empty[String]) {

}

trait Attribute {

  val tuple: (String, String)

  def toString: String

  def isEmpty: Boolean

  def getKey: String

  def getValue: Option[String]
}


object HtmlAttribute {
  def apply(key: String, value: String): HtmlAttribute = {
    new HtmlAttribute(key, Some(value))
  }

  def apply(key: String): HtmlAttribute = {
    new HtmlAttribute(key, None)
  }

  def apply() = {
    throw new IllegalArgumentException("Key is empty!")
  }

  def unapply(htmlAttr: HtmlAttribute): Option[(String, String)] = {
    Some((htmlAttr.getKey,
      if (htmlAttr.getValue.isDefined) htmlAttr.getValue.get else ""))
  }
}

class HtmlAttribute(key: String, value: Option[String]) extends Attribute {

  require(!key.isEmpty)
  require(!key.equals("null"))
  override lazy val tuple: (String, String) = (key, value) match {
    case (k, None) => (k, "")
    case (k, Some("")) => (k, "")
    case (k, Some(v)) => (k, v)
  }
  //  val (key, value) = (NullWrapper(_x), NullWrapper(_y))

  override def toString: String = tuple match {
    case (u, "") => u
    case (u, v) => s"$u='$v'"
  }

  def isEmpty: Boolean = key.isEmpty && value.isEmpty

  override def getKey: String = key

  override def getValue: Option[String] = value
}

object WidgetAttributeConverter {

  type AttConversion = ((Attribute) => List[Attribute])

  def convertAttributes(widget: AbstractWidget): Iterable[Attribute] = {
    val generals = for (p <- widget.getAttributes(); attribute = HtmlAttribute(p._1, p._2))
      yield convertWidgetAttributes(widget)(attribute) ++ generalAttributes(widget, attribute)

    val group = generals.flatten.groupBy(_.getKey)
    group.map(x => HtmlAttribute(x._1, x._2.map(_.getValue).filter(_.isDefined).map(_.get).reduce(_ + _))) // concats value of the repeated attributes
  }

  private def generalAttributes(w: AbstractWidget, attrib: Attribute): List[Attribute] = {

    var s: String = w.getAttribute("data-bind")

    val indexOf: Int = s.lastIndexOf(".")

    if (indexOf >= 0) {
      s = s.substring(0, indexOf + 1)
    }

    def getAttributes(attr: Attribute): List[Attribute] = attr match {
      case HtmlAttribute("passwordAgain", _) => List(HtmlAttribute("label", "app.passwordAgain"))
      case HtmlAttribute("horizontalAlignment", "left") => List(HtmlAttribute("style", "float:left;"))
      case HtmlAttribute("horizontalAlignment", "right") => List(HtmlAttribute("style", "float:right;"))
      case HtmlAttribute("horizontalAlignment", "center") => List(HtmlAttribute("style", "float:center;"))
      case HtmlAttribute("horizontalAlignment", "none") => List(HtmlAttribute())
      case HtmlAttribute("value", v) if v.startsWith("@") => List(HtmlAttribute("ng-model", "$ctrl.model." + v.substring(1)))
      case HtmlAttribute("enable", "false") => List(HtmlAttribute("disabled", ""))
      case HtmlAttribute("disabled", v) => List(HtmlAttribute("disabled", s"$v"))
      case HtmlAttribute("visible", v) => List(HtmlAttribute("visible", s"{{$v}}"))
      case HtmlAttribute("tabindex", v) => List(HtmlAttribute("tabindex", v))
      case HtmlAttribute("data-bind", v) => List(HtmlAttribute("ng-model", "$ctrl." + v))
      case HtmlAttribute("action", v) => List(HtmlAttribute("action", s"Action.$v()"))
      case HtmlAttribute("autoinject", v) => List(HtmlAttribute("autoinject", v))
      case HtmlAttribute("type-ahead", v) => List(HtmlAttribute("type-ahead", v))
      case HtmlAttribute("focused", v) => List(HtmlAttribute("autofocus", v))
      case HtmlAttribute("name", v) => List(HtmlAttribute("name", v))
      case AttributeTranslationMatcher(_) =>
        List(attr)
      case HtmlAttribute("validation_notnull", v) =>
        List(HtmlAttribute("required-validator", ""), HtmlAttribute("not-null", "true"))
      case HtmlAttribute("validation_equals", v) =>
        List(HtmlAttribute("equals-validator", v))
      case HtmlAttribute("validation_numeric", v) =>
        List(HtmlAttribute("number-validator", ""))
      case HtmlAttribute("validation_email", v) =>
        List(HtmlAttribute("email-validator", ""))
      case HtmlAttribute("validation_min", v) =>
        List(HtmlAttribute("greater-than-validator", v))
      case HtmlAttribute("validation_max", v) =>
        List(HtmlAttribute("less-than-validator", v))
      case HtmlAttribute("validation_size_min", v) =>
        List(HtmlAttribute("min-length-validator", v))
      case HtmlAttribute("validation_size_max", v) =>
        List(HtmlAttribute("max-length-validator", v))
      case HtmlAttribute("validation_pattern", v) =>
        List(HtmlAttribute("pattern-validator", v))
      case HtmlAttribute("bindableField", v) =>List(HtmlAttribute("bindable-field", v))
      case HtmlAttribute("headerField", v) =>List(HtmlAttribute("header-field", v))
      case HtmlAttribute("detailFields", v) =>List(HtmlAttribute("detail-fields", v))
      case HtmlAttribute("fieldsToSelect", v) =>List(HtmlAttribute("fields-to-select", v))
      case HtmlAttribute("visibilityOn", v) =>
        List(HtmlAttribute("ng-show", v))
      case HtmlAttribute("disabilityOn", v) =>
        List(HtmlAttribute("disabled", s + v), HtmlAttribute("ng-disabled", s + v))
      case HtmlAttribute(x, y) => Nil
    }

    attrib match {
      case HtmlAttribute(x, v) if v.startsWith("@") =>
        getAttributes(HtmlAttribute(x, s"{{${v.substring(1)}}}"))
      case x => getAttributes(x)
    }
  }

  private def convertWidgetAttributes(widget: AbstractWidget): AttConversion = widget match {

    case ButtonGroup(_) => x: Attribute => x match {
      case HtmlAttribute("flow", v) => List(HtmlAttribute("orientation", v))
      case _ => Nil
    }

    case Image(_) => x: Attribute => x match {
      case HtmlAttribute("src", v) => List(HtmlAttribute("src", v))
      case HtmlAttribute("alt", v) => List(HtmlAttribute("alt", v))
      case HtmlAttribute("content", v) => List(HtmlAttribute("content", v))
      case _ => Nil
    }

    case CheckBox(_) => x: Attribute => x match {
      case HtmlAttribute("checked", "true") => List(HtmlAttribute("checked"))
      case HtmlAttribute("checked", "false") => Nil
      case _ => Nil
    }

    case AmountBox(_) => x: Attribute => x match {
      case _ => Nil
    }

    case ListBox(_) => x: Attribute => x match {
      case HtmlAttribute("selectionMode", "single") => List(HtmlAttribute("selection", "single"))
      case HtmlAttribute("selectionMode", "multiple") => List(HtmlAttribute("selection", "multiple"))
      case _ => Nil
    }

    case TextBox(_) => x: Attribute => x match {
      case HtmlAttribute("textAlignment", "left") => List(HtmlAttribute("text-alignment", "left"))
      case HtmlAttribute("textAlignment", "right") => List(HtmlAttribute("text-alignment", "right"))
      case HtmlAttribute("textAlignment", "center") => List(HtmlAttribute("text-alignment", "center"))
      case HtmlAttribute("textAlignment", "justified") => List(HtmlAttribute("text-alignment", "justified"))
      case HtmlAttribute("type", v) => List(HtmlAttribute("type", v))
      case _ => Nil
    }
    case ComboBox(_) => x: Attribute => x match {
      case HtmlAttribute("source", v) => List(HtmlAttribute("source", "$ctrl." + v))
      case HtmlAttribute("filler", v) => List(HtmlAttribute("source-items", "$ctrl." + v))
      case _ => Nil
    }
    case Lookup(_) => x: Attribute => x match {
      case HtmlAttribute("fieldName", v) => List(HtmlAttribute("service", v + "Service"))
      case HtmlAttribute("data-bind", v) => List(HtmlAttribute("source-items", v))
      case _ => Nil
    }
    case ModelPresenter(_) | CrudModelPresenter(_) => x: Attribute => x match {
      case HtmlAttribute("fieldName", v) => List(HtmlAttribute("source-items", "$ctrl." + v))
      case _ => Nil
    }

    case FileUploader(_) => x: Attribute => x match {
      case HtmlAttribute("data-bind", v) => Nil
      case att@HtmlAttribute("onchange", _) => List(att)
      case _ => Nil
    }
    case CodeResolver(_) => x: Attribute => x match {
      case HtmlAttribute("generalParameter", v) => List(HtmlAttribute("main-type", v))
      case _ => Nil
    }

    case DatePicker(_) => x: Attribute => x match {
      case _ => Nil
    }
    case TextAreaBox(_) => x: Attribute => x match {
      case _ => Nil
    }
    case _ => x: Attribute => x match {
      case _ => Nil
    }
  }

  def getWidgetTemplate(widget: AbstractWidget): TemplateWidgetConversion = widget match {
    case ComboBox(_) => TemplateWidgetConversion("codeResolver", attributes = Map("selected-name" -> "code"), ignorableAttributes = Set("translation(.*)"))
    case AmountBox(_) => TemplateWidgetConversion("widget", "amount-currency", Map("currency-code" -> ("currency" + widget("name"))))
    case CheckBox(_) => TemplateWidgetConversion("widget", "input-checkbox")
    case RadioButton(_) => TemplateWidgetConversion("widget", "form-input", Map("type" -> "radio"))
    case Label(_) => TemplateWidgetConversion("widget", "form-input", Map("type" -> "static"))
    case ListBox(_) => TemplateWidgetConversion("widget", "form-select", Map("type" -> "list"))
    case TextBox(_) => TemplateWidgetConversion("widget", "input-text", ignorableAttributes = Set("translation(.*)"))
    case Button(_) => TemplateWidgetConversion("button")
    case Container(_) => TemplateWidgetConversion("panel")
    case Image(_) => TemplateWidgetConversion("widget")
    case ButtonGroup(_) => TemplateWidgetConversion("buttonGroup")
    case DatePicker(_) => TemplateWidgetConversion("widget", "date-picker", Map("class" -> "datepicker"))
    case Grid(_) => TemplateWidgetConversion("widget", "input-list-selector", ignorableAttributes = Set("translation(.*)"))
    case CrudGrid(_) => TemplateWidgetConversion("crudgrid")
    case SearchableTextBox(_) => TemplateWidgetConversion("searchableTextBox")
    case Lookup(_) => TemplateWidgetConversion("widget", "exelector", attributes = Map(/*"bindable-field" -> "id"*/), ignorableAttributes = Set("translation(.*)"))
    case ModelPresenter(_) => TemplateWidgetConversion("widget", "exelector", attributes = Map(/*"bindable-field" -> "id"*/), ignorableAttributes = Set("translation(.*)"))
    case CodeResolver(_) => TemplateWidgetConversion("codeResolver", attributes = Map(
    ), ignorableAttributes = Set("translation(.*)"))
    case TextAreaBox(_) => TemplateWidgetConversion("widget", "textarea")
    case FileUploader(_) => TemplateWidgetConversion("widget","file-upload")
    case NullWidget(_) => TemplateWidgetConversion("null")
  }

  object AttributeTranslationMatcher {
    def unapply(x: HtmlAttribute): Option[HtmlAttribute] = {
      if (x.getKey.matches(Constants.TranslationPattern))
        Some(x)
      else
        None

    }
  }
}