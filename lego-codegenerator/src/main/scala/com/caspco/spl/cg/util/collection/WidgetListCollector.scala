package com.caspco.spl.cg.util.collection

import com.caspco.spl.cg.model.view.AbstractWidget

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 09/07/14
  *         Time: 15:32
  */
object WidgetListCollector {

  trait WidgetTuple

  implicit class Collector(list: List[AbstractWidget]) {
    def groupWidgets(length: Int, sort: Boolean): List[WidgetTuple] = {
      require(length <= 12 && length > 0)

      val sortedList = if (sort) {
        list.groupBy(f => f.getPriority).toList.sortWith(_._1 < _._1).flatMap(p =>
          p._2.sortWith(_.getWidgetType > _.getWidgetType))
        //        list.sortWith((a, b) => /*a.getWidgetType > b.getWidgetType || */a.getPriority < b.getPriority)
      } else {
        list
      }

      sortedList.grouped(length).collect {
        case a :: Nil => OneWidget(a)
        case a :: xs => MultipleWidgets(a :: xs)
      }.toList
    }
  }

  case class OneWidget(w: AbstractWidget) extends WidgetTuple

  case class MultipleWidgets(ws: List[AbstractWidget]) extends WidgetTuple

  //  case class TwoWidgets(w1: AbstractWidget, w2: AbstractWidget) extends WidgetTuple
  //
  //  case class ThreeWidgets(w1: AbstractWidget, w2: AbstractWidget, w3: AbstractWidget) extends WidgetTuple
  //
  //  case class FourWidgets(w1: AbstractWidget, w2: AbstractWidget, w3: AbstractWidget, w4: AbstractWidget) extends WidgetTuple

}