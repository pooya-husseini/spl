package com.caspco.spl.cg.model.datamodel

import com.caspco.spl.cg.model.BaseModel

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 2/1/14
 *         Time: 11:18 AM
 */
class Restriction extends BaseModel {

  private var values: Map[String, String] = Map.empty[String, String]

  override def containsElement(elemName: String): Boolean =
    values.contains(elemName)

  def +=(rv: RestrictionValue): Restriction = {
    values ++= Map(rv.key -> rv.value)
    this
  }
}

case class RestrictionValue(key: String, value: String)