package com.caspco.spl.cg.util.string

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
  *         Date: ${Date}
  *         Time: ${time}
  */
object ConstantStringMaker {
  def apply(str: String): String = {
    if (str.filter(_ != '_').forall(_.isUpper)) {
      str
    } else {
      str.zipWithIndex.map(x => if (x._1.isUpper && x._2 != 0) s"_${x._1}" else x._1.toUpper).mkString.replace("__", "_")
    }
  }
}
